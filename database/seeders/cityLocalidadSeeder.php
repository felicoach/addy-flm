<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class cityLocalidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('city_localidad')->insert(['id' => '1389', 'name' => 'Comuna 1' ]);
         DB::table('city_localidad')->insert(['id' => '1390', 'name' => 'Comuna 10' ]);
         DB::table('city_localidad')->insert(['id' => '1391', 'name' => 'Comuna 11' ]);
         DB::table('city_localidad')->insert(['id' => '1392', 'name' => 'Comuna 12' ]);
         DB::table('city_localidad')->insert(['id' => '1393', 'name' => 'Comuna 13' ]);
         DB::table('city_localidad')->insert(['id' => '1394', 'name' => 'Comuna 14' ]);
         DB::table('city_localidad')->insert(['id' => '1395', 'name' => 'Comuna 15' ]);
         DB::table('city_localidad')->insert(['id' => '1396', 'name' => 'Comuna 16' ]);
         DB::table('city_localidad')->insert(['id' => '1397', 'name' => 'Comuna 17' ]);
         DB::table('city_localidad')->insert(['id' => '1398', 'name' => 'Comuna 18' ]);
         DB::table('city_localidad')->insert(['id' => '1399', 'name' => 'Comuna 19' ]);
         DB::table('city_localidad')->insert(['id' => '1400', 'name' => 'Comuna 2' ]);
         DB::table('city_localidad')->insert(['id' => '1401', 'name' => 'Comuna 20' ]);
         DB::table('city_localidad')->insert(['id' => '1402', 'name' => 'Comuna 21' ]);
         DB::table('city_localidad')->insert(['id' => '1403', 'name' => 'Comuna 22' ]);
         DB::table('city_localidad')->insert(['id' => '1404', 'name' => 'Comuna 3' ]);
         DB::table('city_localidad')->insert(['id' => '1405', 'name' => 'Comuna 4' ]);
         DB::table('city_localidad')->insert(['id' => '1406', 'name' => 'Comuna 5' ]);
         DB::table('city_localidad')->insert(['id' => '1407', 'name' => 'Comuna 6' ]);
         DB::table('city_localidad')->insert(['id' => '1408', 'name' => 'Comuna 7' ]);
         DB::table('city_localidad')->insert(['id' => '1409', 'name' => 'Comuna 8' ]);
         DB::table('city_localidad')->insert(['id' => '1410', 'name' => 'Comuna 9' ]);
         DB::table('city_localidad')->insert(['id' => '1411', 'name' => 'Otras' ]);
         DB::table('city_localidad')->insert(['id' => '4839', 'name' => 'Cali Rural' ]);
         DB::table('city_localidad')->insert(['id' => '5941', 'name' => 'Encanto del valle' ]);
    }
}
