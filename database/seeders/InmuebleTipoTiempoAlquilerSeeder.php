<?php

namespace Database\Seeders;

use App\Models\InmuebleTipoTiempoAlquiler;
use Illuminate\Database\Seeder;

class InmuebleTipoTiempoAlquilerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { //"Mensual", "Trimestral", "Semestral", "Anual
        InmuebleTipoTiempoAlquiler::create([ 'id' => 1, 'tipo' => 'mensual', 'descripcion' => 'Mensual']);
        InmuebleTipoTiempoAlquiler::create([ 'id' => 2, 'tipo' => 'trimestral', 'descripcion' => 'Trimestral']);
        InmuebleTipoTiempoAlquiler::create([ 'id' => 3, 'tipo' => 'semestral', 'descripcion' => 'Semestral']);
        InmuebleTipoTiempoAlquiler::create([ 'id' => 4, 'tipo' => 'anual', 'descripcion' => 'Anual']);

    }
}
