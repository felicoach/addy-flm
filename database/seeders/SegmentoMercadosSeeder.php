<?php

namespace Database\Seeders;

use App\Models\SegmentoMercados;
use Illuminate\Database\Seeder;

class SegmentoMercadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    {
        SegmentoMercados::create(['id' => 1, 'name' => 'residencial']);
        SegmentoMercados::create(['id' => 2, 'name' => 'comercial']);
    }
}
