<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EtiquetaSeeder extends Seeder
{

    public function run()
    {  
        DB::table('etiquetas')->insert(
            ['label' => 'Medio', 'value' => 'medio', 'id' => 1],
        );
        DB::table('etiquetas')->insert(
            ['label' => 'Bajo', 'value' => 'bajo', 'id' => 2],
        );
        DB::table('etiquetas')->insert(
            ['label' => 'Actualizado', 'value' => 'actualizado', 'id' => 3],
        );
        DB::table('etiquetas')->insert(
            ['label' => 'Equipo', 'value' => 'equipo', 'id' => 4],
        );
        DB::table('etiquetas')->insert(
            ['label' => 'Alto', 'value' => 'alto', 'id' => 5],
        );
    }
}
