<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username'          => 'Administrador',
            'email'         => 'admin@admin.com',
            'password'      => Hash::make('admin'),
            'email_verified_at' => now(),
            'id_empresa'    => '1',
            'estado_user'   => '1',
            'type_user' => 1
        ]);

        User::create([
            'username'          => 'Cesar',
            'email'         => 'cesargiraldo7@hotmail.com',
            'password'      => Hash::make('cesargiraldo7'),
            'email_verified_at' => now(),
            'id_empresa'    => '1',
            'estado_user'   => '1',
            'type_user' => 1
        ]);

        User::create([
            'username'          => 'Andres',
            'email'         => 'andres@addy.la',
            'password'      => Hash::make('andres'),
            'email_verified_at' => now(),
            'id_empresa'    => '1',
            'estado_user'   => '1',


        ]);

        User::create([
            'username'          => 'Felipe',
            'email'         => 'felipe@addy.la',
            'password'      => Hash::make('felipe'),
            'email_verified_at' => now(),
            'id_empresa'    => '1',
            'estado_user'   => '1',
            'type_user' => 1

        ]);

        User::create([
            'username'          => 'Monica',
            'email'         => 'staff@addy.la',
            'password'      => Hash::make('monica123'),
            'email_verified_at' => now(),
            'id_empresa'    => '1',
            'estado_user'   => '1',

        ]);


        User::create([
            'username'          => 'cliente',
            'email'         => 'cliente@admin.com',
            'password'      => Hash::make('cliente'),
            'email_verified_at' => now(),
            'id_empresa'    => '1',
            'estado_user'   => '1',
            'type_user' => 1
        ]);

        User::create([
            'username'          => 'tkpipe15',
            'email'         => 'tkpipe15@gmail.com',
            'password'      => Hash::make('andres'),
            'email_verified_at' => now(),
            'id_empresa'    => '1',
            'estado_user'   => '1',
            'type_user' => 1
        ]);
    }
}
