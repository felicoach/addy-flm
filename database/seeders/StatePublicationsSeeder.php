<?php

namespace Database\Seeders;

use App\Models\StatePublications;
use Illuminate\Database\Seeder;

class StatePublicationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatePublications::create(['id'=>1, 'name'=>'Activo']);
        StatePublications::create(['id'=>2, 'name'=>'Inactivo']);
        StatePublications::create(['id'=>3, 'name'=>'Borrador']);
    }
}
