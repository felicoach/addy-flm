<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InmueblesAntiguedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inmueble_antiguedad')->insert(
            ['id' => 0, 'name' => 'No Especificado'],
        );
        DB::table('inmueble_antiguedad')->insert(
            ['id' => 1, 'name' => 'Arriendo'],
        );
        DB::table('inmueble_antiguedad')->insert(
            ['id' => 2, 'name' => '1 a 8 años'],
        );
        DB::table('inmueble_antiguedad')->insert(
            ['id' => 3, 'name' => '9 a 15 años'],
        );
        DB::table('inmueble_antiguedad')->insert(
            ['id' => 4, 'name' => '16 a 30 años'],
        );
        DB::table('inmueble_antiguedad')->insert(
            ['id' => 5, 'name' => 'Más de 30 años'],
        );
    }
}
