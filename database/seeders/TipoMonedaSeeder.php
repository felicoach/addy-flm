<?php

namespace Database\Seeders;

use App\Models\TipoMoneda;
use Illuminate\Database\Seeder;

class TipoMonedaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoMoneda::create(['id' => '1', 'tipo' => 'COD', 'descripcion' => 'COD',]);
        TipoMoneda::create(['id' => '2', 'tipo' => 'DOLAR', 'descripcion' => 'DOLAR',]);
    }
}
