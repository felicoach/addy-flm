<?php

namespace Database\Seeders;

use App\Models\TipoDocumento;
use Illuminate\Database\Seeder;

class tipoDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoDocumento::create([
            'nombre' => 'Cedula de ciudadania',
            'descripcion' => 'Cedula colombiana'
        ]);

        TipoDocumento::create([
            'nombre' => 'Cedula de extrangería',
            'descripcion' => 'Cedula de extrangería'
        ]);

        TipoDocumento::create([
            'nombre' => 'Pasaporte',
            'descripcion' => 'Pasaporte colombiano'
        ]);

        TipoDocumento::create([
            'nombre' => 'ID',
            'descripcion' => 'ID'
        ]);
    }
}
