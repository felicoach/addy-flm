<?php

namespace Database\Seeders;

use App\Models\InmueblesCaracteristicasInternas;
use Illuminate\Database\Seeder;

class InmueblesCaracteristicasInternasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InmueblesCaracteristicasInternas::create(['id' => '1', 'texto' => 'Adosado', 'value' => 'Adosado',]);
        InmueblesCaracteristicasInternas::create(['id' => '2', 'texto' => 'Aire acondicionado', 'value' => 'Aire acondicionado',]);
        InmueblesCaracteristicasInternas::create(['id' => '3', 'texto' => 'Alarma', 'value' => 'Alarma',]);
        InmueblesCaracteristicasInternas::create(['id' => '4', 'texto' => 'Amoblado', 'value' => 'Amoblado',]);
        InmueblesCaracteristicasInternas::create(['id' => '5', 'texto' => 'Armarios Empotrados', 'value' => 'Armarios Empotrados',]);
        InmueblesCaracteristicasInternas::create(['id' => '6', 'texto' => 'Balcón', 'value' => 'Balcón',]);
        InmueblesCaracteristicasInternas::create(['id' => '7', 'texto' => 'Baño auxiliar', 'value' => 'Baño auxiliar',]);
        InmueblesCaracteristicasInternas::create(['id' => '8', 'texto' => 'Baño en habitación principal', 'value' => 'Baño en habitación principal',]);
        InmueblesCaracteristicasInternas::create(['id' => '9', 'texto' => 'Barra estilo americano', 'value' => 'Barra estilo americano',]);
        InmueblesCaracteristicasInternas::create(['id' => '10', 'texto' => 'Biblioteca/Estudio', 'value' => 'Biblioteca/Estudio',]);
        InmueblesCaracteristicasInternas::create(['id' => '11', 'texto' => 'Bifamiliar', 'value' => 'Bifamiliar',]);
        InmueblesCaracteristicasInternas::create(['id' => '12', 'texto' => 'Calentador', 'value' => 'Calentador',]);
        InmueblesCaracteristicasInternas::create(['id' => '13', 'texto' => 'Chimenea', 'value' => 'Chimenea',]);
        InmueblesCaracteristicasInternas::create(['id' => '14', 'texto' => 'Citófono / Intercomunicador', 'value' => 'Citófono / Intercomunicador',]);
        InmueblesCaracteristicasInternas::create(['id' => '15', 'texto' => 'Clósets', 'value' => 'Clósets',]);
        InmueblesCaracteristicasInternas::create(['id' => '16', 'texto' => 'Cocina equipada', 'value' => 'Cocina equipada',]);
        InmueblesCaracteristicasInternas::create(['id' => '17', 'texto' => 'Cocina integral', 'value' => 'Cocina integral',]);
        InmueblesCaracteristicasInternas::create(['id' => '18', 'texto' => 'Cocina tipo americano', 'value' => 'Cocina tipo americano',]);
        InmueblesCaracteristicasInternas::create(['id' => '19', 'texto' => 'Comedor auxiliar', 'value' => 'Comedor auxiliar',]);
        InmueblesCaracteristicasInternas::create(['id' => '20', 'texto' => 'Depósito', 'value' => 'Depósito',]);
        InmueblesCaracteristicasInternas::create(['id' => '21', 'texto' => 'Despensa', 'value' => 'Despensa',]);
        InmueblesCaracteristicasInternas::create(['id' => '22', 'texto' => 'Doble Ventana', 'value' => 'Doble Ventana',]);
        InmueblesCaracteristicasInternas::create(['id' => '23', 'texto' => 'Gas domiciliario', 'value' => 'Gas domiciliario',]);
        InmueblesCaracteristicasInternas::create(['id' => '24', 'texto' => 'Habitación conductores', 'value' => 'Habitación conductores',]);
        InmueblesCaracteristicasInternas::create(['id' => '25', 'texto' => 'Habitación servicio', 'value' => 'Habitación servicio',]);
        InmueblesCaracteristicasInternas::create(['id' => '26', 'texto' => 'Hall de alcobas', 'value' => 'Hall de alcobas',]);
        InmueblesCaracteristicasInternas::create(['id' => '27', 'texto' => 'Hospedaje Turismo', 'value' => 'Hospedaje Turismo',]);
        InmueblesCaracteristicasInternas::create(['id' => '28', 'texto' => 'Jacuzzi', 'value' => 'Jacuzzi',]);
        InmueblesCaracteristicasInternas::create(['id' => '29', 'texto' => 'Reformado', 'value' => 'Reformado',]);
        InmueblesCaracteristicasInternas::create(['id' => '30', 'texto' => 'Sauna', 'value' => 'Sauna',]);
        InmueblesCaracteristicasInternas::create(['id' => '31', 'texto' => 'Suelo de cerámica / mármol', 'value' => 'Suelo de cerámica / mármol',]);
        InmueblesCaracteristicasInternas::create(['id' => '32', 'texto' => 'Trastero', 'value' => 'Trastero',]);
        InmueblesCaracteristicasInternas::create(['id' => '33', 'texto' => 'Trifamiliar', 'value' => 'Trifamiliar',]);
        InmueblesCaracteristicasInternas::create(['id' => '34', 'texto' => 'Turco', 'value' => 'Turco',]);
        InmueblesCaracteristicasInternas::create(['id' => '35', 'texto' => 'Unifamiliar', 'value' => 'Unifamiliar',]);
        InmueblesCaracteristicasInternas::create(['id' => '36', 'texto' => 'Vista panorámica', 'value' => 'Vista panorámica',]);
        InmueblesCaracteristicasInternas::create(['id' => '37', 'texto' => 'Zona de lavandería', 'value' => 'Zona de lavandería',]);
    }
}
