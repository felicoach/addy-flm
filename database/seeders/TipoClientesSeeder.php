<?php

namespace Database\Seeders;

use App\Models\TipoCliente;
use Illuminate\Database\Seeder;

class TipoClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoCliente::create([ 'nombre' => 'Comprador', 'descripcion' => 'Comprador']);
        TipoCliente::create([ 'nombre' => 'Arrendatario', 'descripcion' => 'Arrendatario']);
        TipoCliente::create([ 'nombre' => 'Propietario', 'descripcion' => 'Propietario']);
        TipoCliente::create([ 'nombre' => 'Familia', 'descripcion' => 'Familia']);
        TipoCliente::create([ 'nombre' => 'Amigos', 'descripcion' => 'Amigos']);
        TipoCliente::create([ 'nombre' => 'Otros', 'descripcion' => 'Otros']);

    }
}
