<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoCitasAgendas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_citas')->insert([
            "color" => 'success',
            "label" => 'MOSTRAR INMUEBLE'
        ]);

        DB::table('tipo_citas')->insert([
            "color" => 'warning',
            "label" => 'REUNION'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'danger',
            "label" => 'INVENTARIO'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'primary',
            "label" => 'PlANTA'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'secundary',
            "label" => 'BARRIDO ZONA'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'info',
            "label" => 'CAPTACIÓN'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'light',
            "label" => 'GESTIÓN DE LLAMADAS'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'dark',
            "label" => 'CITA PROPIETARIO'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'success',
            "label" => 'ENTREGA INMUEBLE'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'primary',
            "label" => 'INVENTARIO INMUEBLE'
        ]);
        DB::table('tipo_citas')->insert([
            "color" => 'info',
            "label" => 'TOMA DE FOTOS'
        ]);

        DB::table('tipo_citas')->insert([
            "color" => 'light',
            "label" => 'COLOCAR LETREROS'
        ]);
    }
}
