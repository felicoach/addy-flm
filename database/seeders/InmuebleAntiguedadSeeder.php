<?php

namespace Database\Seeders;

use App\Models\InmuebleAntiguedad;
use Illuminate\Database\Seeder;

class InmuebleAntiguedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InmuebleAntiguedad::create([
            'id' => 1,
            'id_fincarraiz' => '0',
            'name' => 'No Especificado',
        ]);

        InmuebleAntiguedad::create([
            'id' => 2,
            'id_fincarraiz' => '1',
            'name' => 'Menos de 1 año',
        ]);

        InmuebleAntiguedad::create([
            'id' => 3,
            'id_fincarraiz' => '2',
            'name' => '1 a 8 años',
        ]);

        InmuebleAntiguedad::create([
            'id' => 4,
            'id_fincarraiz' => '3',
            'name' => '9 a 15 años',
        ]);

        InmuebleAntiguedad::create([
            'id' => 5,
            'id_fincarraiz' => '4',
            'name' => '16 a 30 años',
        ]);

        InmuebleAntiguedad::create([
            'id' => 6,
            'id_fincarraiz' => '5',
            'name' => 'Más de 30 años',
        ]);
    }
}
