<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id'    => '1',
            'nombre' => 'Administrador',
            'slug' => 'administrator',
            'descripcion' => 'Super usuario del sistema Addy',

        ]);

        Role::create([
            'id'    => '2',
            'nombre' => 'Agentes',
            'slug' => 'agentes',
            'descripcion' => 'Agentes, tendrás mucho más acceso en Addy. Se dará permisos para crear, comprar referidos en nuestra plaforma.',

        ]);


        Role::create([
            'id'    => '3',
            'nombre' => 'Socio referidor',
            'slug' => 'socio_referidor',
            'descripcion' => 'Socio referidor, solo tendrá permisos para referir nuevas personas.',

        ]);


        Role::create([
            'id'    => '4',
            'nombre' => 'Visitantes',
            'slug' => 'visitantes',
            'descripcion' => 'Visitante, solo podrás entrar y actualizar el perfil.',

        ]);


        Role::create([
            'id'    => '5',
            'nombre' => 'user_empresa',
            'slug' => 'user_empresa',
            'descripcion' => 'admin usuario del sistema Addy',

        ]);


        Role::create([
            'id'    => '6',
            'nombre' => 'Staff',
            'slug' => 'staff',
            'descripcion' => 'call center.',

        ]);

        Role::create([
            'id'    => '7',
            'nombre' => 'Asesor',
            'slug' => 'asesor',
            'descripcion' => 'call center.',
        ]);

        Role::create([
            'id'    => '8',
            'nombre' => 'Director comercial',
            'slug' => 'director_comercial',
            'descripcion' => 'call center.',
        ]);
    }
}
