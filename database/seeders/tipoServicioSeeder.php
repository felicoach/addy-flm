<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tipoServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portales_type_services')->insert(
            ['name' => 'SOAP', 'description' => 'Soap'],
        );
        DB::table('portales_type_services')->insert(
            ['name' => 'API_REST', 'description' => 'Api Rest' ],
        );
        
    }
}
