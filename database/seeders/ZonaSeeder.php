<?php

namespace Database\Seeders;

use App\Models\Zona;
use Illuminate\Database\Seeder;

class ZonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Zona::create(['id' => '1', 'ciudad_id' => 20502, 'name' => 'Sin zona', 'descripcion' => 'Sin descripcion']);
    }
}
