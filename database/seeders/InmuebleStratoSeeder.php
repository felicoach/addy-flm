<?php

namespace Database\Seeders;

use App\Models\InmuebleStrato;
use Illuminate\Database\Seeder;

class InmuebleStratoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InmuebleStrato::create([ 'id' => 6, 'name' => 'estrato 6']);
        InmuebleStrato::create([ 'id' => 1, 'name' => 'estrato 1']);
        InmuebleStrato::create([ 'id' => 2, 'name' => 'estrato 2']);
        InmuebleStrato::create([ 'id' => 3, 'name' => 'estrato 3']);
        InmuebleStrato::create([ 'id' => 4, 'name' => 'estrato 4']);
        InmuebleStrato::create([ 'id' => 5, 'name' => 'estrato 5']);
        InmuebleStrato::create([ 'id' => 100, 'name' => 'estrato +6']);
      

    }
}
