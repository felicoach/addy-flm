<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateAgendCitaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agendas_estado_citas')->insert([
            "name" => 'Pendiente',
        ]);

        DB::table('agendas_estado_citas')->insert([
            "name" => 'Cancelada',
        ]);

        DB::table('agendas_estado_citas')->insert([
            "name" => 'Cumplida',
        ]);

        DB::table('agendas_estado_citas')->insert([
            "name" => 'incumplida',
        ]);
    }
}
