<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;


class PermissionSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Permission::create([
            'id' => 1,
            "subject" => 'clientes',
            "title" => 'Clientes',
        ]);

        Permission::create([
            'id' => 2,
            "subject" => 'inmuebles',
            "title" => 'Inmuebles',
        ]);

        Permission::create([
            'id' => 3,
            "subject" => 'referidos',
            "title" => 'Referidos',
        ]);

        Permission::create([
            'id' => 4,
            "subject" => 'mercadeo',
            "title" => 'Mercadeos',
        ]);

        Permission::create([
            'id' => 5,
            "subject" => 'agendas',
            "title" => 'Agendas',
        ]);

        Permission::create([
            'id' => 6,
            "subject" => 'tareas',
            "title" => 'Tareas',
        ]);

        Permission::create([
            'id' => 7,
            "subject" => 'permisos',
            "title" => 'Roles & Permisos',
        ]);


        Permission::create([
            'id' => 8,
            "subject" => 'perfil',
            "title" => 'Perfil',
        ]);

        Permission::create([
            'id' => 9,
            "subject" => 'preguntas',
            "title" => 'Preguntas',
        ]);

        Permission::create([
            'id' => 10,
            "subject" => 'dashboard_referido',
            "title" => 'Dashboard referidos',
        ]);

        Permission::create([
            'id' => 11,
            "subject" => 'all',
            "title" => 'todos',
        ]);

        Permission::create([
            'id' => 12,
            "subject" => 'home',
            "title" => 'Inicio',
        ]);

        Permission::create([
            'id' => 13,
            "subject" => 'usuarios',
            "title" => 'Usuarios',
        ]);
        Permission::create([
            'id' => 14,
            "subject" => 'dashboard_agente',
            "title" => 'Dashboard Agente',
        ]);
        Permission::create([
            'id' => 15,
            "subject" => 'perfil_agente',
            "title" => 'Perfil Agente',
        ]);
        Permission::create([
            'id' => 16,
            "subject" => 'configuration',
            "title" => 'Configuracion',
        ]);
        
        Permission::create([
            'id' => 17,
            "subject" => 'perfil_referido',
            "title" => 'Perfil referido',
        ]);
        // dashboard_referido
        // perfil_referido
        // perfil_agente
    }
}
