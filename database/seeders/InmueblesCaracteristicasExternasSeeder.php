<?php

namespace Database\Seeders;

use App\Models\InmueblesCaracteristicasExterna;
use App\Models\InmueblesCaracteristicasInternas;
use Illuminate\Database\Seeder;

class InmueblesCaracteristicasExternasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InmueblesCaracteristicasExterna::create(['id' => '1', 'texto' => ' Acceso pavimentado', 'value' => ' Acceso pavimentado',]);
        InmueblesCaracteristicasExterna::create(['id' => '2', 'texto' => ' Árboles frutales', 'value' => ' Árboles frutales',]);
        InmueblesCaracteristicasExterna::create(['id' => '3', 'texto' => ' Área Social', 'value' => ' Área Social',]);
        InmueblesCaracteristicasExterna::create(['id' => '4', 'texto' => ' Áreas Turísticas', 'value' => ' Áreas Turísticas',]);
        InmueblesCaracteristicasExterna::create(['id' => '5', 'texto' => ' Ascensor', 'value' => ' Ascensor',]);
        InmueblesCaracteristicasExterna::create(['id' => '6', 'texto' => ' Barbacoa / Parrilla / Quincho', 'value' => ' Barbacoa / Parrilla / Quincho',]);
        InmueblesCaracteristicasExterna::create(['id' => '7', 'texto' => ' Bosque nativos', 'value' => ' Bosque nativos',]);
        InmueblesCaracteristicasExterna::create(['id' => '8', 'texto' => ' Bungalow / pareado', 'value' => ' Bungalow / pareado',]);
        InmueblesCaracteristicasExterna::create(['id' => '9', 'texto' => ' Calles de Tosca', 'value' => ' Calles de Tosca',]);
        InmueblesCaracteristicasExterna::create(['id' => '10', 'texto' => ' Cancha de baloncesto', 'value' => ' Cancha de baloncesto',]);
        InmueblesCaracteristicasExterna::create(['id' => '11', 'texto' => ' Cancha de futbol', 'value' => ' Cancha de futbol',]);
        InmueblesCaracteristicasExterna::create(['id' => '12', 'texto' => ' Cancha de squash', 'value' => ' Cancha de squash',]);
        InmueblesCaracteristicasExterna::create(['id' => '13', 'texto' => ' Cancha de tenis', 'value' => ' Cancha de tenis',]);
        InmueblesCaracteristicasExterna::create(['id' => '14', 'texto' => ' Centros comerciales', 'value' => ' Centros comerciales',]);
        InmueblesCaracteristicasExterna::create(['id' => '15', 'texto' => ' Cerca zona urbana', 'value' => ' Cerca zona urbana',]);
        InmueblesCaracteristicasExterna::create(['id' => '16', 'texto' => ' Circuito cerrado de TV', 'value' => ' Circuito cerrado de TV',]);
        InmueblesCaracteristicasExterna::create(['id' => '17', 'texto' => ' Club House', 'value' => ' Club House',]);
        InmueblesCaracteristicasExterna::create(['id' => '18', 'texto' => ' Club Social', 'value' => ' Club Social',]);
        InmueblesCaracteristicasExterna::create(['id' => '19', 'texto' => ' Cochera / Garaje', 'value' => ' Cochera / Garaje',]);
        InmueblesCaracteristicasExterna::create(['id' => '20', 'texto' => ' Colegios / Universidades', 'value' => ' Colegios / Universidades',]);
        InmueblesCaracteristicasExterna::create(['id' => '21', 'texto' => ' Establo', 'value' => ' Establo',]);
        InmueblesCaracteristicasExterna::create(['id' => '22', 'texto' => ' Galpón', 'value' => ' Galpón',]);
        InmueblesCaracteristicasExterna::create(['id' => '23', 'texto' => ' Garaje', 'value' => ' Garaje',]);
        InmueblesCaracteristicasExterna::create(['id' => '24', 'texto' => ' Garita de Entrada', 'value' => ' Garita de Entrada',]);
        InmueblesCaracteristicasExterna::create(['id' => '25', 'texto' => ' Gimnasio', 'value' => ' Gimnasio',]);
        InmueblesCaracteristicasExterna::create(['id' => '26', 'texto' => ' Invernadero', 'value' => ' Invernadero',]);
        InmueblesCaracteristicasExterna::create(['id' => '27', 'texto' => ' Jardín', 'value' => ' Jardín',]);
        InmueblesCaracteristicasExterna::create(['id' => '28', 'texto' => ' Jaula de golf', 'value' => ' Jaula de golf',]);
        InmueblesCaracteristicasExterna::create(['id' => '29', 'texto' => ' Kiosko', 'value' => ' Kiosko',]);
        InmueblesCaracteristicasExterna::create(['id' => '30', 'texto' => ' Lago', 'value' => ' Lago',]);
        InmueblesCaracteristicasExterna::create(['id' => '31', 'texto' => ' Laguna', 'value' => ' Laguna',]);
        InmueblesCaracteristicasExterna::create(['id' => '32', 'texto' => ' Montaña', 'value' => ' Montaña',]);
        InmueblesCaracteristicasExterna::create(['id' => '33', 'texto' => ' Oficina de negocios', 'value' => ' Oficina de negocios',]);
        InmueblesCaracteristicasExterna::create(['id' => '34', 'texto' => ' Parqueadero visitantes', 'value' => ' Parqueadero visitantes',]);
        InmueblesCaracteristicasExterna::create(['id' => '35', 'texto' => ' Parques cercanos', 'value' => ' Parques cercanos',]);
        InmueblesCaracteristicasExterna::create(['id' => '36', 'texto' => ' Patio', 'value' => ' Patio',]);
        InmueblesCaracteristicasExterna::create(['id' => '37', 'texto' => ' Pesebrera', 'value' => ' Pesebrera',]);
        InmueblesCaracteristicasExterna::create(['id' => '38', 'texto' => ' Piscina', 'value' => ' Piscina',]);
        InmueblesCaracteristicasExterna::create(['id' => '39', 'texto' => ' Pista de Padel', 'value' => ' Pista de Padel',]);
        InmueblesCaracteristicasExterna::create(['id' => '40', 'texto' => ' Planta eléctrica', 'value' => ' Planta eléctrica',]);
        InmueblesCaracteristicasExterna::create(['id' => '41', 'texto' => ' Playas', 'value' => ' Playas',]);
        InmueblesCaracteristicasExterna::create(['id' => '42', 'texto' => ' Portería / Recepción', 'value' => ' Portería / Recepción',]);
        InmueblesCaracteristicasExterna::create(['id' => '43', 'texto' => ' Pozo de agua natural', 'value' => ' Pozo de agua natural',]);
        InmueblesCaracteristicasExterna::create(['id' => '44', 'texto' => ' Río/Quebrada cercano', 'value' => ' Río/Quebrada cercano',]);
        InmueblesCaracteristicasExterna::create(['id' => '45', 'texto' => ' Sala de internet', 'value' => ' Sala de internet',]);
        InmueblesCaracteristicasExterna::create(['id' => '46', 'texto' => ' Salón Comunal', 'value' => ' Salón Comunal',]);
        InmueblesCaracteristicasExterna::create(['id' => '47', 'texto' => ' Sistema de riego', 'value' => ' Sistema de riego',]);
        InmueblesCaracteristicasExterna::create(['id' => '48', 'texto' => ' Sobre vía principal', 'value' => ' Sobre vía principal',]);
        InmueblesCaracteristicasExterna::create(['id' => '49', 'texto' => ' Terraza', 'value' => ' Terraza',]);
        InmueblesCaracteristicasExterna::create(['id' => '50', 'texto' => ' Trans. público cercano', 'value' => ' Trans. público cercano',]);
        InmueblesCaracteristicasExterna::create(['id' => '51', 'texto' => ' Trastero', 'value' => ' Trastero',]);
        InmueblesCaracteristicasExterna::create(['id' => '52', 'texto' => ' Urbanización Cerrada', 'value' => ' Urbanización Cerrada',]);
        InmueblesCaracteristicasExterna::create(['id' => '53', 'texto' => ' Vigilancia', 'value' => ' Vigilancia',]);
        InmueblesCaracteristicasExterna::create(['id' => '54', 'texto' => ' Vivienda bifamiliar', 'value' => ' Vivienda bifamiliar',]);
        InmueblesCaracteristicasExterna::create(['id' => '55', 'texto' => ' Vivienda multifamiliar', 'value' => ' Vivienda multifamiliar',]);
        InmueblesCaracteristicasExterna::create(['id' => '56', 'texto' => ' Vivienda unifamiliar', 'value' => ' Vivienda unifamiliar',]);
        InmueblesCaracteristicasExterna::create(['id' => '57', 'texto' => ' Zona campestre', 'value' => ' Zona campestre',]);
        InmueblesCaracteristicasExterna::create(['id' => '58', 'texto' => ' Zona camping', 'value' => ' Zona camping',]);
        InmueblesCaracteristicasExterna::create(['id' => '59', 'texto' => ' Zona comercial', 'value' => ' Zona comercial',]);
        InmueblesCaracteristicasExterna::create(['id' => '60', 'texto' => ' Zona industrial', 'value' => ' Zona industrial',]);
        InmueblesCaracteristicasExterna::create(['id' => '61', 'texto' => ' Zona infantil', 'value' => ' Zona infantil',]);
        InmueblesCaracteristicasExterna::create(['id' => '62', 'texto' => ' Zona residencial', 'value' => ' Zona residencial',]);
        InmueblesCaracteristicasExterna::create(['id' => '63', 'texto' => ' Zonas deportivas', 'value' => ' Zonas deportivas',]);
        InmueblesCaracteristicasExterna::create(['id' => '64', 'texto' => ' Zonas verdes', 'value' => ' Zonas verdes',]);
    }
}
