<?php

namespace Database\Seeders;

use App\Models\TypeRole;
use Illuminate\Database\Seeder;

class TypeRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeRole::create([
            "nombre" => "private"
        ]);

        TypeRole::create([
            "nombre" => "public"
        ]);
    }
}
