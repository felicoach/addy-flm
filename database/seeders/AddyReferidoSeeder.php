<?php

namespace Database\Seeders;

use App\Models\AddyReferido;
use Illuminate\Database\Seeder;

class AddyReferidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AddyReferido::create([
            'cedula_persona' => '12345678',
            'primer_nombre' => 'andres',
            'primer_apellido' => 'torres',
            'celular_movil' => '3142453534',
            'celular_whatsApp' => '2342432434',
            'correo_persona' => 'rr@we.com',
            'codigo_pais' => 'Co',
            'codigo_ciudad' => 12,
            'estado_persona'    => 2131,
        ]);
    
    }
}
