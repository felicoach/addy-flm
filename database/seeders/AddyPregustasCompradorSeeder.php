<?php

namespace Database\Seeders;

use App\Models\AddyPreguntasVendedor;
use Illuminate\Database\Seeder;

class AddyPregustasCompradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AddyPreguntasVendedor::create([
            'id'    =>  '1',
            'descripcion_pregunta'=> "Tipo de inmueble",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '2',
            'descripcion_pregunta'=> "Direccion",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '3',
            'descripcion_pregunta'=> "Condominio",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '4',
            'descripcion_pregunta'=> "Barrio",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '5',
            'descripcion_pregunta'=> "estrato",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '6',
            'descripcion_pregunta'=> "Año de construccion",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '7',
            'descripcion_pregunta'=> "Area lote",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '8',
            'descripcion_pregunta'=> "Area construida",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '9',
            'descripcion_pregunta'=> "Frente",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '10',
            'descripcion_pregunta'=> "Fondo",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '11',
            'descripcion_pregunta'=> "Numero de pisos",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '12',
            'descripcion_pregunta'=> "Matricula inmobiliaria",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '13',
            'descripcion_pregunta'=> "Servicios públicos Disponibles",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '14',
            'descripcion_pregunta'=> "Valor comercial venta",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '15',
            'descripcion_pregunta'=> "Valor avaluo catastral",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '16',
            'descripcion_pregunta'=> "Valor administracion",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '17',
            'descripcion_pregunta'=> "Saldo hipoteca",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPreguntasVendedor::create([
            'id'    =>  '18',
            'descripcion_pregunta'=> "Acepta permuta",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);
    }
}
