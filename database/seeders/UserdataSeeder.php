<?php

namespace Database\Seeders;

use App\Models\Userdata;
use Illuminate\Database\Seeder;

class UserdataSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Userdata::create([

			'cedula_persona' => '12345678',
			'tipo_identificacion' => '1',
			'primer_nombre' => 'Felipe',
			'segundo_nombre' => 'Cliente',
			'primer_apellido' => 'Torres',
			'segundo_apellido' => 'Velez',
			'celular_movil' => '+57 300 123 1212',
			'celular_whatsapp' => '+57 300 123 1212',
			'id_pais' => 48,
			'id_estado' => 2904,
			'id_ciudad' => 20634,
			'id_perfil' => '1',
			'id_usuario' => 7,
			'estado_persona' => '1',
			'tipo_cliente'  => 2

		]);

		Userdata::create([

			'cedula_persona'	=> '123456789',
			'tipo_identificacion' => '1',
			'primer_nombre' => 'Administrador',
			'segundo_nombre' => '',
			'primer_apellido' => 'Administrador',
			'segundo_apellido' => '',
			'celular_movil' => '+57 123 456 7891',
			'celular_whatsapp' => '+57 123 456 7891',
			'id_pais' => 48,
			'id_estado' => 2904,
			'id_ciudad' => 20634,
			'id_perfil' => '1',
			'id_usuario' => 1,
			'estado_persona' => '1',
			'tipo_cliente'  => 2

		]);

		
		Userdata::create([

			'cedula_persona'	=> '12345678910',
			'tipo_identificacion' => '1',
			'primer_nombre' => 'Andres',
			'segundo_nombre' => '',
			'primer_apellido' => 'Administrador',
			'segundo_apellido' => '',
			'celular_movil' => '+57 123 456 7892',
			'celular_whatsapp' => '+57 123 456 7892',
			'id_pais' => 48,
			'id_estado' => 2904,
			'id_ciudad' => 20634,
			'id_perfil' => '1',
			'id_usuario' => 3,
			'estado_persona' => '1',
			'tipo_cliente'  => 2

		]);

		Userdata::create([

			'cedula_persona'	=> '1234567891011',
			'tipo_identificacion' => '1',
			'primer_nombre' => 'felipe',
			'segundo_nombre' => '',
			'primer_apellido' => 'Administrador',
			'segundo_apellido' => '',
			'celular_movil' => '+57 123 456 7893',
			'celular_whatsapp' => '+57 123 456 7893',
			'id_pais' => 48,
			'id_estado' => 2904,
			'id_ciudad' => 20634,
			'id_perfil' => '1',
			'id_usuario' => 4,
			'estado_persona' => '1',
			'tipo_cliente'  => 2

		]);

		Userdata::create([

			'cedula_persona'	=> '123456789133',
			'tipo_identificacion' => '1',
			'primer_nombre' => 'Cesar',
			'segundo_nombre' => '',
			'primer_apellido' => 'Administrador',
			'segundo_apellido' => '',
			'celular_movil' => '+57 123 456 7894',
			'celular_whatsapp' => '+57 123 456 7894',
			'id_pais' => 48,
			'id_estado' => 2904,
			'id_ciudad' => 20634,
			'id_perfil' => '1',
			'id_usuario' => 2,
			'estado_persona' => '1',
			'tipo_cliente'  => 2

		]);

		Userdata::create([

			'cedula_persona'	=> '123456789111',
			'tipo_identificacion' => '1',
			'primer_nombre' => 'Monica',
			'segundo_nombre' => '',
			'primer_apellido' => 'Giraldo',
			'segundo_apellido' => '',
			'celular_movil' => '+57 123 456 7891',
			'celular_whatsapp' => '+57 123 456 7891',
			'id_pais' => 48,
			'id_estado' => 2904,
			'id_ciudad' => 20634,
			'id_perfil' => '1',
			'id_usuario' => 5,
			'estado_persona' => '1',
			'tipo_cliente'  => 2

		]);

	}
}
