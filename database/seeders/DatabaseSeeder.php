<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountrySeeder::class);
        $this->call(StateSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(TypeRoleSeeder::class);
        $this->call(ZonaSeeder::class);
        $this->call(BarrioSeeder::class);
        $this->call(ModuleSeeder::class);
        $this->call(ActionSeeder::class);


        $this->call(PermissionSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(RolePermissionSeeder::class);

        $this->call(PermissionRolesSeeder::class);
        $this->call(TypeUserSeeder::class);
        $this->call(TypeRoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(RolesUserSeeder::class);
        $this->call(TipoClientesInfoSeeder::class);
        $this->call(PreguntasSeeder::class);
        $this->call(UserdataSeeder::class);
        $this->call(AddyPregustasCompradorSeeder::class);
        $this->call(tipoInmuebleSeeder::class);
        $this->call(tipoNegocioSeeder::class);
        $this->call(EstadoPropiedadSeeder::class);
        $this->call(InmuebleTipoContratoSeeder::class);
        $this->call(TipoMonedaSeeder::class);
        $this->call(TipoPeriodoAdmonSeeder::class);
        $this->call(InmueblesStateSeeder::class);
        $this->call(InmuebleAntiguedadSeeder::class);

        $this->call(InmuebleTipoPrecioSeeder::class);
        $this->call(StatePublicationsSeeder::class);
        $this->call(SegmentoMercadosSeeder::class);
        $this->call(InmuebleStadoFisicoSeeder::class);
        $this->call(InmuebleStratoSeeder::class);
        $this->call(InmuebleTipoParqueaderoSeeder::class);
        $this->call(InmuebleTipoTiempoAlquilerSeeder::class);
        $this->call(tipoDocumentoSeeder::class);
        $this->call(TipoClientesSeeder::class);
        $this->call(GeneroSeeder::class);

        $this->call(EtiquetaSeeder::class);
        $this->call(RaguePriceInmuebleSeeder::class);



        $this->call(InmueblesCaracteristicasExternasSeeder::class);
        $this->call(InmueblesCaracteristicasInternasSeeder::class);
    }
}
