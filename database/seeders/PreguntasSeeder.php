<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AddyPregunta;


class PreguntasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        AddyPregunta::create([
            'id'    =>  '1',
            'descripcion_pregunta' => "¿Cual es la modadlidad de compra?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);

        AddyPregunta::create([
            'id' =>  '2',
            'descripcion_pregunta' => "¿Cuál es el rango de precio de la compra?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);


        AddyPregunta::create([
            'id' =>  '3',
            'descripcion_pregunta' => "¿Que tipo de inmueble esta buscando?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '4',
            'descripcion_pregunta' => "¿Tiene credito aprobado?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);

        AddyPregunta::create([
            'id' => '5',
            'descripcion_pregunta' => "¿Cual es el valor de la cuota inicial que tiene disponible?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);


        AddyPregunta::create([
            'id' => '6',
            'descripcion_pregunta' => "¿En que pais?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);
        AddyPregunta::create([
            'id' => '61',
            'descripcion_pregunta' => "¿En que departamento?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);


        AddyPregunta::create([
            'id' => '7',
            'descripcion_pregunta' => "¿En que ciudad?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);

        AddyPregunta::create([
            'id' => '8',
            'descripcion_pregunta' => "¿En qué Zona?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);

        AddyPregunta::create([
            'id' => '9',
            'descripcion_pregunta' => "¿En qué Barrio / Localidad?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);

        AddyPregunta::create([
            'id' => '10',
            'descripcion_pregunta' => "¿Área ideal para la propiedad?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);

        AddyPregunta::create([
            'id' => '11',
            'descripcion_pregunta' => "   ¿Cantidad minima de habitaciones?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]); 

        AddyPregunta::create([
            'id' => '12',
            'descripcion_pregunta' => "¿Alguna caracteristica indispensable?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);

        AddyPregunta::create([
            'id' => '13',
            'descripcion_pregunta' => "¿Hace cuanto timepo esta buscando inmueble?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]); 
        
        AddyPregunta::create([
            'id' => '14',
            'descripcion_pregunta' => "¿En cuanto tiempo necesita comprar propiedad?",
            'valor_pregunta' => '6.25',
            'slug_modulo' => 'preguntas',
            'opcional_pregunta' => 'cliente_comprador',
            'estado_pregunta' => '1',
        ]);
       

    }
}
