<?php

namespace Database\Seeders;

use App\Models\TipoPeriodoAdmon;
use Illuminate\Database\Seeder;

class TipoPeriodoAdmonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoPeriodoAdmon::create(['id' => '1', 'tipo' => 'Mensual', 'descripcion' => 'Mensual',]);
        TipoPeriodoAdmon::create(['id' => '2', 'tipo' => 'bimensual', 'descripcion' => 'bimensual',]);
        TipoPeriodoAdmon::create(['id' => '3', 'tipo' => 'trimestral', 'descripcion' => 'trimestral',]);
        TipoPeriodoAdmon::create(['id' => '4', 'tipo' => 'semestral', 'descripcion' => 'semestral',]);
        TipoPeriodoAdmon::create(['id' => '5', 'tipo' => 'anual', 'descripcion' => 'anual',]);
    }
}
