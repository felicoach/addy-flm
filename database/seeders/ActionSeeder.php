<?php

namespace Database\Seeders;

use App\Models\Action;
use Illuminate\Database\Seeder;

class ActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Action::create([
            "action" => "create",
            "title" => "Crear"
        ]);
        Action::create([
            "action" => "updated",
            "title" => "Actualizar"
        ]);
        Action::create([
            "action" => "read",
            "title" => "Leer"
        ]);
        Action::create([
            "action" => "delete",
            "title" => "Eliminar"
        ]);
        Action::create([
            "action" => "manage",
            "title" => "Todos"
        ]);
    }
}
