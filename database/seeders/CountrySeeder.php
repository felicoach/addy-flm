<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create(['id' => '11', 'name' => 'Argentina', 'iso3' => 'ARG', 'iso2' => 'AR', 'phone_code' => '54', 'capital' => 'Buenos Aires', 'currency' => 'ARS', 'currency_symbol' => '$', 'tld' => '.ar', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '-34', 'longitude' => '-64', 'emojiU' => 'U+1F1E6 U+1F1F7',]);
        Country::create(['id' => '27', 'name' => 'Bolivia', 'iso3' => 'BOL', 'iso2' => 'BO', 'phone_code' => '591', 'capital' => 'Sucre', 'currency' => 'BOB', 'currency_symbol' => 'Bs.', 'tld' => '.bo', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '-17', 'longitude' => '-65', 'emojiU' => 'U+1F1E7 U+1F1F4',]);
        Country::create(['id' => '31', 'name' => 'Brazil', 'iso3' => 'BRA', 'iso2' => 'BR', 'phone_code' => '55', 'capital' => 'Brasilia', 'currency' => 'BRL', 'currency_symbol' => 'R$', 'tld' => '.br', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '-10', 'longitude' => '-55', 'emojiU' => 'U+1F1E7 U+1F1F7',]);
        Country::create(['id' => '39', 'name' => 'Canada', 'iso3' => 'CAN', 'iso2' => 'CA', 'phone_code' => '1', 'capital' => 'Ottawa', 'currency' => 'CAD', 'currency_symbol' => '$', 'tld' => '.ca', 'region' => 'Americas', 'subregion' => 'Northern America', 'latitude' => '60', 'longitude' => '-95', 'emojiU' => 'U+1F1E8 U+1F1E6',]);
        Country::create(['id' => '44', 'name' => 'Chile', 'iso3' => 'CHL', 'iso2' => 'CL', 'phone_code' => '56', 'capital' => 'Santiago', 'currency' => 'CLP', 'currency_symbol' => '$', 'tld' => '.cl', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '-30', 'longitude' => '-71', 'emojiU' => 'U+1F1E8 U+1F1F1',]);
        Country::create(['id' => '48', 'name' => 'Colombia', 'iso3' => 'COL', 'iso2' => 'CO', 'phone_code' => '57', 'capital' => 'Bogota', 'currency' => 'COP', 'currency_symbol' => '$', 'tld' => '.co', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '4', 'longitude' => '-72', 'emojiU' => 'U+1F1E8 U+1F1F4',]);
        Country::create(['id' => '53', 'name' => 'Costa Rica', 'iso3' => 'CRI', 'iso2' => 'CR', 'phone_code' => '506', 'capital' => 'San Jose', 'currency' => 'CRC', 'currency_symbol' => '₡', 'tld' => '.cr', 'region' => 'Americas', 'subregion' => 'Central America', 'latitude' => '10', 'longitude' => '-84', 'emojiU' => 'U+1F1E8 U+1F1F7',]);
        Country::create(['id' => '56', 'name' => 'Cuba', 'iso3' => 'CUB', 'iso2' => 'CU', 'phone_code' => '53', 'capital' => 'Havana', 'currency' => 'CUP', 'currency_symbol' => '$', 'tld' => '.cu', 'region' => 'Americas', 'subregion' => 'Caribbean', 'latitude' => '21.5', 'longitude' => '-80', 'emojiU' => 'U+1F1E8 U+1F1FA',]);
        Country::create(['id' => '62', 'name' => 'Dominican Republic', 'iso3' => 'DOM', 'iso2' => 'DO', 'phone_code' => '1-809 and 1-829', 'capital' => 'Santo Domingo', 'currency' => 'DOP', 'currency_symbol' => '$', 'tld' => '.do', 'region' => 'Americas', 'subregion' => 'Caribbean', 'latitude' => '19', 'longitude' => '-70.66666666', 'emojiU' => 'U+1F1E9 U+1F1F4',]);
        Country::create(['id' => '64', 'name' => 'Ecuador', 'iso3' => 'ECU', 'iso2' => 'EC', 'phone_code' => '593', 'capital' => 'Quito', 'currency' => 'USD', 'currency_symbol' => '$', 'tld' => '.ec', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '-2', 'longitude' => '-77.5', 'emojiU' => 'U+1F1EA U+1F1E8',]);
        Country::create(['id' => '66', 'name' => 'El Salvador', 'iso3' => 'SLV', 'iso2' => 'SV', 'phone_code' => '503', 'capital' => 'San Salvador', 'currency' => 'USD', 'currency_symbol' => '$', 'tld' => '.sv', 'region' => 'Americas', 'subregion' => 'Central America', 'latitude' => '13.83333333', 'longitude' => '-88.91666666', 'emojiU' => 'U+1F1F8 U+1F1FB',]);
        Country::create(['id' => '90', 'name' => 'Guatemala', 'iso3' => 'GTM', 'iso2' => 'GT', 'phone_code' => '502', 'capital' => 'Guatemala City', 'currency' => 'GTQ', 'currency_symbol' => 'Q', 'tld' => '.gt', 'region' => 'Americas', 'subregion' => 'Central America', 'latitude' => '15.5', 'longitude' => '-90.25', 'emojiU' => 'U+1F1EC U+1F1F9',]);
        Country::create(['id' => '142', 'name' => 'Mexico', 'iso3' => 'MEX', 'iso2' => 'MX', 'phone_code' => '52', 'capital' => 'Mexico City', 'currency' => 'MXN', 'currency_symbol' => '$', 'tld' => '.mx', 'region' => 'Americas', 'subregion' => 'Central America', 'latitude' => '23', 'longitude' => '-102', 'emojiU' => 'U+1F1F2 U+1F1FD',]);
        Country::create(['id' => '170', 'name' => 'Panama', 'iso3' => 'PAN', 'iso2' => 'PA', 'phone_code' => '507', 'capital' => 'Panama City', 'currency' => 'PAB', 'currency_symbol' => 'B/.', 'tld' => '.pa', 'region' => 'Americas', 'subregion' => 'Central America', 'latitude' => '9', 'longitude' => '-80', 'emojiU' => 'U+1F1F5 U+1F1E6',]);
        Country::create(['id' => '172', 'name' => 'Paraguay', 'iso3' => 'PRY', 'iso2' => 'PY', 'phone_code' => '595', 'capital' => 'Asuncion', 'currency' => 'PYG', 'currency_symbol' => '₲', 'tld' => '.py', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '-23', 'longitude' => '-58', 'emojiU' => 'U+1F1F5 U+1F1FE',]);
        Country::create(['id' => '173', 'name' => 'Peru', 'iso3' => 'PER', 'iso2' => 'PE', 'phone_code' => '51', 'capital' => 'Lima', 'currency' => 'PEN', 'currency_symbol' => 'S/.', 'tld' => '.pe', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '-10', 'longitude' => '-76', 'emojiU' => 'U+1F1F5 U+1F1EA',]);
        Country::create(['id' => '178', 'name' => 'Puerto Rico', 'iso3' => 'PRI', 'iso2' => 'PR', 'phone_code' => '1-787 and 1-939', 'capital' => 'San Juan', 'currency' => 'USD', 'currency_symbol' => '$', 'tld' => '.pr', 'region' => 'Americas', 'subregion' => 'Caribbean', 'latitude' => '18.25', 'longitude' => '-66.5', 'emojiU' => 'U+1F1F5 U+1F1F7',]);
        Country::create(['id' => '207', 'name' => 'Spain', 'iso3' => 'ESP', 'iso2' => 'ES', 'phone_code' => '34', 'capital' => 'Madrid', 'currency' => 'EUR', 'currency_symbol' => '€', 'tld' => '.es', 'region' => 'Europe', 'subregion' => 'Southern Europe', 'latitude' => '40', 'longitude' => '-4', 'emojiU' => 'U+1F1EA U+1F1F8',]);
        Country::create(['id' => '233', 'name' => 'United States', 'iso3' => 'USA', 'iso2' => 'US', 'phone_code' => '1', 'capital' => 'Washington', 'currency' => 'USD', 'currency_symbol' => '$', 'tld' => '.us', 'region' => 'Americas', 'subregion' => 'Northern America', 'latitude' => '38', 'longitude' => '-97', 'emojiU' => 'U+1F1FA U+1F1F8',]);
        Country::create(['id' => '235', 'name' => 'Uruguay', 'iso3' => 'URY', 'iso2' => 'UY', 'phone_code' => '598', 'capital' => 'Montevideo', 'currency' => 'UYU', 'currency_symbol' => '$', 'tld' => '.uy', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '-33', 'longitude' => '-56', 'emojiU' => 'U+1F1FA U+1F1FE',]);
        Country::create(['id' => '239', 'name' => 'Venezuela', 'iso3' => 'VEN', 'iso2' => 'VE', 'phone_code' => '58', 'capital' => 'Caracas', 'currency' => 'VEF', 'currency_symbol' => 'Bs', 'tld' => '.ve', 'region' => 'Americas', 'subregion' => 'South America', 'latitude' => '8', 'longitude' => '-66', 'emojiU' => 'U+1F1FB U+1F1EA',]);
    }
}
