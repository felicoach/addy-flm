<?php

namespace Database\Seeders;

use App\Models\TipoInmueble;
use Illuminate\Database\Seeder;

class tipoInmuebleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        TipoInmueble::create(['id' => '2', 'tipo' => 'Casa', 'descripcion' => 'casa', 'id_fincarraiz' =>  '3']);
        TipoInmueble::create(['id' => '3', 'tipo' => 'Apartamento', 'descripcion' => 'Apartamento', 'id_fincarraiz' =>  '2']);
        TipoInmueble::create(['id' => '4', 'tipo' => 'Apartaestudios ', 'descripcion' => 'Apartaestudios', 'id_fincarraiz' =>  '56']);
        TipoInmueble::create(['id' => '7', 'tipo' => 'Edificio', 'descripcion' => 'Edificio', 'id_fincarraiz' =>  '57']);
        TipoInmueble::create(['id' => '8', 'tipo' => 'Finca', 'descripcion' => 'Finca', 'id_fincarraiz' =>  '51']);
        TipoInmueble::create(['id' => '9', 'tipo' => 'Hotel', 'descripcion' => 'Finca - Hotel', 'id_fincarraiz' =>  '57']);
        TipoInmueble::create(['id' => '11', 'tipo' => 'Consultorio', 'descripcion' => 'Consultorio', 'id_fincarraiz' =>  '56']);
        TipoInmueble::create(['id' => '12', 'tipo' => 'Hostal', 'descripcion' => 'Hostal', 'id_fincarraiz' =>  '57']);
        TipoInmueble::create(['id' => '13', 'tipo' => 'Hoteles', 'descripcion' => 'Hoteles', 'id_fincarraiz' =>  '57']);
        TipoInmueble::create(['id' => '14', 'tipo' => 'Isla', 'descripcion' => 'Isla', 'id_fincarraiz' =>  '51']);
        TipoInmueble::create(['id' => '15', 'tipo' => 'Local', 'descripcion' => 'Local', 'id_fincarraiz' =>  '41']);
        TipoInmueble::create(['id' => '19', 'tipo' => 'Bodega', 'descripcion' => 'Bodega', 'id_fincarraiz' =>  '54']);
        TipoInmueble::create(['id' => '20', 'tipo' => 'Oficina', 'descripcion' => 'Oficina', 'id_fincarraiz' =>  '33']);
    }
}
