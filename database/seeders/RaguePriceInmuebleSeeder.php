<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RaguePriceInmuebleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *Hasta $50 Millones
     * @return void
     */

    public function run()
    {
        DB::table('inmueble_range_price')->insert([
            "value" => 'De $50 a $75 Millones',
        ]);
        DB::table('inmueble_range_price')->insert([
            "value" => 'De $75 a $100 Millones',
        ]);
        DB::table('inmueble_range_price')->insert([
            "value" => 'De $100 a $150 Millones',
        ]);
        DB::table('inmueble_range_price')->insert([
            "value" => 'De $200 a $250 Millones',
        ]);
        DB::table('inmueble_range_price')->insert([
            "value" => 'De $250 a $300 Millones',
        ]);
        DB::table('inmueble_range_price')->insert([
            "value" => 'De $300 a $350 Millones',
        ]);
        DB::table('inmueble_range_price')->insert([
            "value" => 'De $350 a $400 Millones',
        ]);
        DB::table('inmueble_range_price')->insert([
            "value" => 'Más de $400 Millones',
        ]);
    }
}
