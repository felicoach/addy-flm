<?php

namespace Database\Seeders;

use App\Models\Barrio;
use Illuminate\Database\Seeder;

class BarrioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Barrio::create(['id' => '1', 'zona_id' => 1, 'name' => 'Sin barrio', 'descripcion' => 'Sin descripcion']);
    }
}
