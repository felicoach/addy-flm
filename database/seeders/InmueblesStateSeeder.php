<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InmueblesStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inmuebles_states')->insert(
            ['id' => 2, 'name' => 'Activo'],
        );

        DB::table('inmuebles_states')->insert(
            ['id' => 3, 'name' => 'Eliminado'],
        );

        DB::table('inmuebles_states')->insert(
            ['id' => 4, 'name' => 'Inactivo'],
        );
    }
}
