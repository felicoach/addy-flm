<?php

namespace Database\Seeders;

use App\Models\InmuebleStadoFisico;
use Illuminate\Database\Seeder;

class InmuebleStadoFisicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InmuebleStadoFisico::create(['id'=>1, 'name'=>'Disponible', 'id_fincarraiz' => '3']);
        InmuebleStadoFisico::create(['id'=>2, 'name'=>'Arrendado', 'id_fincarraiz' => '3']);
        InmuebleStadoFisico::create(['id'=>3, 'name'=>'Vendido', 'id_fincarraiz' => '3']);
        InmuebleStadoFisico::create(['id'=>4, 'name'=>'Retirado', 'id_fincarraiz' => '3']);
        InmuebleStadoFisico::create(['id'=>5, 'name'=>'En mantenimiento', 'id_fincarraiz' => '0']);
        InmuebleStadoFisico::create(['id'=>6, 'name'=>'Nuevo', 'id_fincarraiz' => '1']);
        InmuebleStadoFisico::create(['id'=>7, 'name'=>'En preoceso', 'id_fincarraiz' => '0']);
        InmuebleStadoFisico::create(['id'=>8, 'name'=>'Por desocupar', 'id_fincarraiz' => '0']);
        InmuebleStadoFisico::create(['id'=>9, 'name'=>'No compartir', 'id_fincarraiz' => '0']);
        InmuebleStadoFisico::create(['id'=>10, 'name'=>'Arrendado por propiedad', 'id_fincarraiz' => '0']);
        InmuebleStadoFisico::create(['id'=>11, 'name'=>'Vendido por propiedad', 'id_fincarraiz' => '0']);

    }
}
