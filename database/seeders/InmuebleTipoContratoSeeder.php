<?php

namespace Database\Seeders;

use App\Models\InmuebleTipoContrato;
use Illuminate\Database\Seeder;

class InmuebleTipoContratoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InmuebleTipoContrato::create(['id' => '1', 'tipo' => 'Contrato de compraventa', 'descripcion' => 'Contrato de compraventa',]);
        InmuebleTipoContrato::create(['id' => '2', 'tipo' => 'Contrato de arrendamiento', 'descripcion' => 'Contrato de compraventa',]);
        InmuebleTipoContrato::create(['id' => '3', 'tipo' => 'Certificado de tradicion', 'descripcion' => 'Contrato de compraventa',]);
    }
}
