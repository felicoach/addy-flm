<?php

namespace Database\Seeders;

use App\Models\extendedProps;
use Illuminate\Database\Seeder;

class ExtendedPropsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        extendedProps::create(['id' => '1', 'calendar' => 'Personal',]);
        extendedProps::create(['id' => '2', 'calendar' => 'Business',]);
        extendedProps::create(['id' => '3', 'calendar' => 'Family',]);
        extendedProps::create(['id' => '4', 'calendar' => 'Holiday',]);
        extendedProps::create(['id' => '5', 'calendar' => 'ETC',]);
    }
}
