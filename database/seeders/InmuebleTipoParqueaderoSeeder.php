<?php

namespace Database\Seeders;

use App\Models\InmuebleTipoParqueadero;
use Illuminate\Database\Seeder;

class InmuebleTipoParqueaderoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InmuebleTipoParqueadero::create([ 'id' => 1, 'tipo' => 'sencillo']);
        InmuebleTipoParqueadero::create([ 'id' => 2, 'tipo' => 'doble']);
        InmuebleTipoParqueadero::create([ 'id' => 3, 'tipo' => 'triple']);
        InmuebleTipoParqueadero::create([ 'id' => 4, 'tipo' => 'sin parqueadero']);

    }
}
