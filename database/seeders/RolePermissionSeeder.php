<?php

namespace Database\Seeders;

use App\Models\ActivePermissionActions;
use App\Models\RolePermission;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $data = RolePermission::create([
            "permission" => 11,
            "role" => 1
        ]);

        ActivePermissionActions::create([
            "role_permission" => $data->id,
            "action" => 5
        ]);
    }
}
