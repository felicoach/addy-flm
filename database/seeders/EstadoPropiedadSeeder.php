<?php

namespace Database\Seeders;

use App\Models\EstadoPropiedad;
use Illuminate\Database\Seeder;

class EstadoPropiedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        EstadoPropiedad::create(['id' => '1', 'estado' => 'usado', 'descripcion' => 'usado',]);
        EstadoPropiedad::create(['id' => '2', 'estado' => 'nuevo', 'descripcion' => 'nuevo',]);
        EstadoPropiedad::create(['id' => '3', 'estado' => 'proyecto', 'descripcion' => 'proyecto',]);
        EstadoPropiedad::create(['id' => '4', 'estado' => 'en contruccion', 'descripcion' => 'en contruccion',]);
    }
}
