<?php

namespace Database\Seeders;

use App\Models\InmuebleTipoPrecio;
use Illuminate\Database\Seeder;

class InmuebleTipoPrecioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InmuebleTipoPrecio::create(['id' => '1', 'tipo' => 'fijo', 'descripcion' => 'Fijo',]);
        InmuebleTipoPrecio::create(['id' => '2', 'tipo' => 'negociable', 'descripcion' => 'Negociable',]);
        InmuebleTipoPrecio::create(['id' => '3', 'tipo' => 'remate', 'descripcion' => 'Remate',]);
    }
}
