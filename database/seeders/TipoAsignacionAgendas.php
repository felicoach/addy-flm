<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoAsignacionAgendas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_asignacion')->insert([
            "name" => 'ASIGNADO',
            "description" => 'ASIGNADO'
        ]);
        DB::table('tipo_asignacion')->insert([
            "name" => 'CREADO',
            "description" => 'CREADO'
        ]);
       
    }
}
