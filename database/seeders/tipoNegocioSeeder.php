<?php

namespace Database\Seeders;

use App\Models\TipoNegocio;
use Illuminate\Database\Seeder;

class tipoNegocioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoNegocio::create(['id' => '1', 'tipo' => 'Venta', 'descripcion' => 'Venta',]);
        TipoNegocio::create(['id' => '2', 'tipo' => 'Alquiler', 'descripcion' => 'Alquiler',]);
        TipoNegocio::create(['id' => '3', 'tipo' => 'Vender - Alquiler', 'descripcion' => 'Vender - Alquiler',]);
    }
}
