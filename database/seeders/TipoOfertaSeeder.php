<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoOfertaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inmueble_tipo_oferta')->insert(
            ['id' => 1, 'name' => 'Venta'],
        );
        DB::table('inmueble_tipo_oferta')->insert(
            ['id' => 1, 'name' => 'Arriendo'],
        );
        DB::table('inmueble_tipo_oferta')->insert(
            ['id' => 1, 'name' => 'Venta'],
        );
    }
}
