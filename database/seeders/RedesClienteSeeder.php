<?php

namespace Database\Seeders;

use App\Models\InmuebleTipoTiempoAlquiler;
use App\Models\RedesSocialesUser;
use Illuminate\Database\Seeder;

class RedesClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { //"Mensual", "Trimestral", "Semestral", "Anual
      RedesSocialesUser::create([ 'nombre' => 'fecebook']);
    }
}
