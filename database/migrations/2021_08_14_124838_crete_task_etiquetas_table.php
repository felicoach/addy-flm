<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreteTaskEtiquetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('task_etiquetas', function (Blueprint $table) {
            $table->id();
         
            $table->unsignedBigInteger('etiqueta_id')->nullable();
            $table->unsignedBigInteger('task_id')->nullable();

            $table->foreign('etiqueta_id')->references('id')->on('etiquetas');
            $table->foreign('task_id')->references('id')->on('tareas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_etiquetas');
    }
}
