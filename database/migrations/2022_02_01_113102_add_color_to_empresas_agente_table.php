<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorToEmpresasAgenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresa_agente', function (Blueprint $table) {
            $table->string('color_primario', 7)->nullable();
            $table->string('color_secundario', 7)->nullable();
            $table->string('font_color', 7)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresa_agente', function (Blueprint $table) {
            $table->dropColumn('color_primario');
            $table->dropColumn('color_secundario');
            $table->dropColumn('font_color');
        });
    }
}
