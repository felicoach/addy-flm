<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaAgenteTable extends Migration
{

    public function up()
    {
        Schema::create('empresa_agente', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('agente')->nullable();
            $table->unsignedBigInteger('id_pais')->nullable()->unsigned();;
            $table->unsignedBigInteger('id_estado')->nullable()->unsigned();;
            $table->unsignedBigInteger('id_ciudad')->nullable()->unsigned();;

            $table->string("slogan")->nullable();
            $table->string("nombre")->nullable();
            $table->string("nit")->nullable();
            $table->string("logo")->nullable();
            $table->string("razon_social")->nullable();

            $table->string("pagina_web")->nullable();
            $table->longText("descripcion")->nullable();
            $table->string('facebook', 120)->nullable();
            $table->string('instagram', 120)->nullable();
            $table->string('linkedin', 120)->nullable();
            
            $table->string('direccion')->nullable(true);

            $table->string('latitud')->nullable(true);
            $table->string('longitud')->nullable(true);

            $table->foreign('agente')->references('id')->on('users');
            $table->foreign('id_pais')->nullable()->references('id')->on('countries');
            $table->foreign('id_estado')->nullable()->references('id')->on('states');
            $table->foreign('id_ciudad')->nullable()->references('id')->on('cities');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empresa_agente');
    }
}
