<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMercadeoTypeToMercadeosTable extends Migration
{

    public function up()
    {
        Schema::table('mercadeos_admin', function (Blueprint $table) {

            $table->unsignedBigInteger('mercadeo_type')->nullable()->unsigned();
            $table->foreign('mercadeo_type')->nullable()->references('id')->on('mercadeos_type');
        });
    }

    public function down()
    {
        Schema::table('mercadeos_admin', function (Blueprint $table) {
            $table->dropColumn("mercadeo_private");
        });
    }
}
