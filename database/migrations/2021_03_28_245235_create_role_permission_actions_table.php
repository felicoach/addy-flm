<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePermissionActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_permission_actions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('role_permission')->nullable();
            $table->unsignedBigInteger('action')->nullable();

            $table->foreign('action')->references('id')->on('actions');
            $table->foreign('role_permission')->references('id')->on('role_permissions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permission_actions');
    }
}
