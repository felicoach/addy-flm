<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInmueblesTipoInmueblesToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inmuebles_tipo_inmuebles', function (Blueprint $table) {
            $table->text("id_fincarraiz");
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inmuebles_tipo_inmuebles', function (Blueprint $table) {
            $table->dropColumn("id_fincarraiz");
        });
    }
}
