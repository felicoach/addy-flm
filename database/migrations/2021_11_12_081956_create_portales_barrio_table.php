<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesBarrioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_barrio', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('barrio_id')->nullable()->unsigned();
            $table->unsignedBigInteger('portale_id')->nullable()->unsigned();
            $table->foreign('barrio_id')->nullable()->references('id')->on('barrio');
            $table->foreign('portale_id')->nullable()->references('id')->on('portales');
            $table->string("codigo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_barrio');
    }
}
