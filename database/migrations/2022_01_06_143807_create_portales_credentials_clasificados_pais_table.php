<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesCredentialsClasificadosPaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_credentials_clasificados_pais', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->unsignedBigInteger('portale_id')->nullable()->unsigned();
            $table->foreign('user_id')->nullable()->references('id')->on('users');
            $table->foreign('portale_id')->nullable()->references('id')->on('portales');
            $table->string('username');
            $table->string('password');
            $table->string('authorization')->default('Sin token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_credentials_clasificados_pais');
    }
}
