<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesRangePriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_range_price', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('portale_id')->nullable()->unsigned();
            $table->string('codigo')->nullable();

            $table->unsignedBigInteger('rp_id')->nullable()->unsigned();


            $table->foreign('portale_id')->nullable()->references('id')->on('portales');
            $table->foreign('rp_id')->nullable()->references('id')->on('inmueble_range_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_range_price');
    }
}
