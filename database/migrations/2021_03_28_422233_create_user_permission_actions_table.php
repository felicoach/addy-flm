<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPermissionActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_permission_actions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('create_by')->nullable();
            $table->unsignedBigInteger('active_permission_action')->nullable();

            $table->foreign('create_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('active_permission_action')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_permission_actions');
    }
}
