<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_urls', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('portal_id')->nullable()->unsigned();
            $table->foreign('portal_id')->nullable()->references('id')->on('portales');
            $table->unsignedBigInteger('inmueble_id')->nullable()->unsigned();
            $table->foreign('inmueble_id')->nullable()->references('id')->on('inmuebles');
            $table->longText('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_urls');
    }
}
