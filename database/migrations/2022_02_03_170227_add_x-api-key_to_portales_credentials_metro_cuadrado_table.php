<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddXApiKeyToPortalesCredentialsMetroCuadradoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portales_credentials_metro_cuadrado', function (Blueprint $table) {
            $table->string('xapikey');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portales_credentials_metro_cuadrado', function (Blueprint $table) {
            $table->dropColumn('xapikey');
        });
    }
}
