<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyAccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_acciones', function (Blueprint $table) {
            $table->integer('codigo_accion')->primary();
            $table->longText('descripcion_accion');
            $table->char('modulo_accion', 50)->nullable();
            $table->integer('id_estado_accion')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_acciones');
    }
}
