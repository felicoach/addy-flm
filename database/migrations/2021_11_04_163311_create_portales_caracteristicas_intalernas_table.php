<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesCaracteristicasIntalernasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_caracteristicas_intarnas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ctin_id')->nullable()->unsigned();
            $table->unsignedBigInteger('portale_id')->nullable()->unsigned();
            $table->foreign('ctin_id')->nullable()->references('id')->on('inmuebles_caracteristicas_internas');
            $table->foreign('portale_id')->nullable()->references('id')->on('portales');
            $table->string("codigo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_caracteristicas_internas');
    }
}
