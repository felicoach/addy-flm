<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_cities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('city_id')->nullable()->unsigned();
            $table->unsignedBigInteger('portale_id')->nullable()->unsigned();

            $table->foreign('city_id')->nullable()->references('id')->on('cities');
            $table->foreign('portale_id')->nullable()->references('id')->on('portales');
            $table->string("codigo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_cities');
    }
}
