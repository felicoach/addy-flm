<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesEstratosInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_estratos_inmuebles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('portale_id')->nullable()->unsigned();
            $table->unsignedBigInteger('estrato_id')->nullable()->unsigned();


            $table->foreign('portale_id')->nullable()->references('id')->on('portales');
            $table->foreign('estrato_id')->nullable()->references('id')->on('inmueble_estrato');

            $table->string("codigo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_estratos_inmuebles');
    }
}
