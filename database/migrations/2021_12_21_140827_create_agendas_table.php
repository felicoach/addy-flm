<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->longText('nota')->nullable();
            $table->string('direccion')->nullable();

            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->unsignedBigInteger('created_by')->nullable()->unsigned();
            $table->unsignedBigInteger('updated_by')->nullable()->unsigned();
            $table->unsignedBigInteger('inmueble_id')->nullable()->unsigned();


            $table->unsignedBigInteger('t_cita')->nullable()->unsigned();
            $table->unsignedBigInteger('cliente_id')->nullable()->unsigned();
            $table->unsignedBigInteger('t_asignacion')->nullable()->unsigned();


            $table->timestamp('start')->default(Carbon::now());
            $table->timestamp('end')->default(Carbon::now());

            $table->foreign('user_id')->nullable()->references('id')->on('users');
            $table->foreign('created_by')->nullable()->references('id')->on('users');
            $table->foreign('updated_by')->nullable()->references('id')->on('users');

            $table->foreign('t_cita')->nullable()->references('id')->on('tipo_citas');
            $table->foreign('inmueble_id')->nullable()->references('id')->on('inmuebles');
            $table->foreign('cliente_id')->nullable()->references('id')->on('clientes');
            $table->foreign('t_asignacion')->nullable()->references('id')->on('tipo_asignacion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
