<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyReferidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_referidos', function (Blueprint $table) {
            $table->id();

            $table->integer('codigo_ciudad')->nullable()->unsigned();
            $table->integer('codigo_pais')->nullable()->unsigned();
            $table->integer('tipo_identificacion')->length(12)->unsigned()->nullable(false);
            $table->longText('foto_persona')->nullable();
            $table->bigInteger('cedula_persona')->unique();
            $table->char('porcentaje_perfil', 50)->nullable();
            $table->char('primer_nombre', 150);
            $table->char('segundo_nombre', 150)->nullable();
            $table->char('primer_apellido', 150);
            $table->char('segundo_apellido', 150)->nullable();
            $table->char('fecha_nacimiento', 50)->nullable();
            $table->char('correo_persona', 150)->unique();
            $table->char('telefono_fijo', 20)->nullable();
            $table->char('celular_movil', 20)->unique();
            $table->char('celular_whatsapp', 20)->unique();
            $table->longText('direccion_persona')->nullable();


            $table->integer('codigo_postal')->length(12)->unsigned()->nullable();
            $table->integer('estado_persona')->length(12)->unsigned()->nullable(false);
            $table->foreign('codigo_ciudad')->references('id_ciudades')->on('addy_ciudades');
            $table->foreign('codigo_pais')->references('codigo')->on('addy_paises');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_referidos');
    }
}
