<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaAgenteEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_agente_emails', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('agente_empresa')->nullable();
            $table->string('email');
            $table->foreign('agente_empresa')->references('id')->on('empresa_agente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_agente_emails');
    }
}
