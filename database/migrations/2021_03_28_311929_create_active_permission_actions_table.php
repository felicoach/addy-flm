<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivePermissionActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_permission_actions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('role_permission')->nullable();
            $table->unsignedBigInteger('action')->nullable();

            $table->foreign('action')->references('id')->on('actions')->onDelete('cascade');
            $table->foreign('role_permission')->references('id')->on('role_permissions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_permission_actions');
    }
}
