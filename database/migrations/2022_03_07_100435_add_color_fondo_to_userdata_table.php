<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorFondoToUserdataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userdata', function (Blueprint $table) {
            $table->string('color_fondo')->default('#000000');
            $table->string('color_fuente')->default('#ffffff');;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userdata', function (Blueprint $table) {
            $table->dropColumn('color_fondo');
            $table->dropColumn('color_fuente');

        });
    }
}
