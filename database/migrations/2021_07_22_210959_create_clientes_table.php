<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->unsignedBigInteger('tipo_identificacion')->unsigned()->nullable();
            $table->unsignedBigInteger('tipo_cliente')->unsigned()->nullable();
            $table->unsignedBigInteger('genero')->unsigned()->nullable();
            
            $table->string('estado')->default('registrado');
            $table->string('nombre');
            $table->string('apellido')->nullable();
          
            $table->string('telefono')->nullable();
           
            $table->date('fecha_nacimiento')->nullable();
            $table->string('n_identificacion')->nullable();
            $table->text('adicional')->nullable();
            $table->string('pais')->nullable();;
            $table->string('departamento')->nullable();;
            $table->string('ciudad')->nullable();;
            $table->string('direccion')->nullable();;
            $table->string('clasificacion')->nullable(true);
            $table->string('facebook')->nullable(true);
            $table->string('instagram')->nullable(true);
            $table->string('twitter')->nullable(true);
            $table->longText('observaciones')->nullable();

            $table->foreign('user_id')->nullable()->references('id')->on('users');
            $table->foreign('tipo_identificacion')->nullable()->references('id')->on('tipo_documentos');
            $table->foreign('tipo_cliente')->nullable()->references('id')->on('tipo_clientes');
            $table->foreign('genero')->nullable()->references('id')->on('generos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
