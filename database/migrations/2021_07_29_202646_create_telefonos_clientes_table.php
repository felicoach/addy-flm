<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelefonosClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telefonos_clientes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cliente')->unsigned()->nullable();

            $table->string('number');

            $table->foreign('cliente')->nullable()->references('id')->on('clientes');

            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('telefonos_clientes');
    }
}
