<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValorPreguntaCreditoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valor_pregunta_credito', function (Blueprint $table) {
            $table->id();
            $table->string("divisa")->nullable(true);
            $table->integer('value')->length(12)->unsigned()->nullable(true);
            $table->integer('id_respuesta_r')->length(12)->unsigned()->nullable(false);
            $table->integer('id_referido_r')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valor_pregunta_credito');
    }
}
