<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeSeriveToPortalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portales', function (Blueprint $table) {
            $table->unsignedBigInteger('type_service')->nullable()->unsigned();
            $table->foreign('type_service')->nullable()->references('id')->on('portales_type_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portales', function (Blueprint $table) {
            $table->dropColumn("type_service");
        });
    }
}
