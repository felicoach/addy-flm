<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaReporteCitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_reporte_cita', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_agenda')->nullable()->unsigned();
            $table->unsignedBigInteger('state_cita')->nullable()->unsigned();
            $table->foreign('id_agenda')->nullable()->references('id')->on('agendas');
            $table->foreign('state_cita')->nullable()->references('id')->on('agendas_estado_citas');
            $table->longText("comentario_cita")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_reporte_cita');
    }
}
