<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyReferidosPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_referidos_personas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_referido')->length(12)->unsigned()->nullable(false);
            $table->bigInteger('id_cedula')->length(12)->unsigned()->nullable(false);
            $table->bigInteger('id_tipo_clientes')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_referidos_personas');
    }
}
