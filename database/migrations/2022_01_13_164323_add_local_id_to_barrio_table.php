<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLocalIdToBarrioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barrio', function (Blueprint $table) {
            $table->unsignedBigInteger('local_id')->nullable()->unsigned();
            $table->foreign('local_id')->nullable()->references('id')->on('city_localidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barrio', function (Blueprint $table) {
            $table->dropForeign('despatch_discrepancies_local_id_foreign');
            $table->dropColumn('local_id');
        });
    }
}
