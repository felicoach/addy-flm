<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyDetallePreguntaRespuestaReferidoRangoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_detalle_pregunta_respuesta_referido_rango', function (Blueprint $table) {
            $table->id();
            $table->string("divisa")->nullable(true);

            $table->integer('inicial')->length(12)->unsigned()->nullable(true);
            $table->integer('final')->length(12)->unsigned()->nullable(true);
            $table->integer('id_respuesta_r')->length(12)->unsigned()->nullable(false);
            $table->integer('id_referido_r')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_detalle_pregunta_respuesta_referido_rango');
    }
}
