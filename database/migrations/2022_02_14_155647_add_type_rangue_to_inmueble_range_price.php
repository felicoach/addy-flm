<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeRangueToInmuebleRangePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inmueble_range_price', function (Blueprint $table) {
            $table->enum('type', ['Alquiler', 'Venta']);
        });
    }

    public function down()
    {
        Schema::table('inmueble_range_price', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
