<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->char('iso3', 3)->nullable(true);
            $table->char('iso2', 2)->nullable(true);
            $table->string('phone_code', 255)->default('no tiene');
            $table->string('capital', 255)->default('no tiene');
            $table->string('currency', 255)->default('no tiene');
            $table->string('currency_symbol', 255)->default('no tiene');
            $table->string('tld', 255)->default('no tiene');
            $table->string('native', 255)->default('no tiene');
            $table->string('region', 255)->default('no tiene');
            $table->string('subregion', 255)->default('no tiene');
            $table->decimal('latitude')->nullable(true);
            $table->decimal('longitude')->nullable(true);
            $table->string('emoji', 191)->default('no tiene');
            $table->string('emojiU', 191)->default('no tiene');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
