<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_token', function (Blueprint $table) {

            $table->id();
            
            $table->unsignedBigInteger('portal_id')->nullable()->unsigned();
            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->unsignedBigInteger('created_by')->nullable()->unsigned();


            $table->foreign('portal_id')->nullable()->references('id')->on('portales');
            $table->foreign('user_id')->nullable()->references('id')->on('users');
            $table->foreign('created_by')->nullable()->references('id')->on('users');

            $table->longText('token');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_token');
    }
}
