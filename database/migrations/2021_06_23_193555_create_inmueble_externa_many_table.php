<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmuebleExternaManyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmueble_externa_many', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inmueble')->nullable();
            $table->unsignedBigInteger('caracteristicas_externas')->nullable();

            $table->foreign('inmueble')->references('id')->on('inmuebles');
            $table->foreign('caracteristicas_externas')->references('id')->on('inmuebles_caracteristicas_externas');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmueble_externa_many');
    }
}
