<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyPreguntasVendedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_preguntas_vendedors', function (Blueprint $table) {
            $table->id();
            $table->longText('descripcion_pregunta');
            $table->double('valor_pregunta', 8, 2)->unsigned()->nullable(false);
            $table->char('slug_modulo', 150);
            $table->char('opcional_pregunta', 150)->nullable();
            $table->char('tipo_cliente', 100)->nullable();
            $table->integer('estado_pregunta')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_preguntas_vendedors');
    }
}
