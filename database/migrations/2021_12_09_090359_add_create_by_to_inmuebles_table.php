<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreateByToInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inmuebles', function (Blueprint $table) {

            $table->unsignedBigInteger('created_by')->nullable()->unsigned();
            $table->foreign('created_by')->nullable()->references('id')->on('users');
            $table->unsignedBigInteger('updated_by')->nullable()->unsigned();
            $table->foreign('updated_by')->nullable()->references('id')->on('users');
            $table->string("cedula")->nullable();
            $table->string("certificado_tradicion")->nullable();
            $table->string("predial")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inmuebles', function (Blueprint $table) {
            $table->dropForeign("created_by");
            $table->dropForeign("updated_by");
            $table->dropColumn("cedula");
            $table->dropColumn("certificado_tradicion");
            $table->dropColumn("predial");
        });
    }
}
