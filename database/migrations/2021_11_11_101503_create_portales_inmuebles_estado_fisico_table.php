<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesInmueblesEstadoFisicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_inmuebles_estado_fisico', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('state_fisico')->nullable()->unsigned();
            $table->unsignedBigInteger('portale_id')->nullable()->unsigned();
            $table->foreign('state_fisico')->nullable()->references('id')->on('inmuebles_estado_fisico');
            $table->foreign('portale_id')->nullable()->references('id')->on('portales');
            $table->string("codigo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_inmuebles_estado_fisico');
    }
}
