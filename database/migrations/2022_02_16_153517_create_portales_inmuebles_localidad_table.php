<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesInmueblesLocalidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_inmuebles_localidad', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inm_id')->nullable()->unsigned();
            $table->foreign('inm_id')->nullable()->references('id')->on('inmuebles');
            $table->unsignedBigInteger('loc_id')->nullable()->unsigned();
            $table->foreign('loc_id')->nullable()->references('id')->on('ciencuadra_localidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_inmuebles_localidad');
    }
}
