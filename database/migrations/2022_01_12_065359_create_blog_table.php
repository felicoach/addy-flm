<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
			$table->string('titulo');
			$table->string('slug');
			$table->text('descripcion');
			$table->text('contenido');
			$table->string('image')->nullable();
			$table->integer('views')->default(0);
			$table->unsignedInteger('estado_registro');
			$table->unsignedInteger('id_user');
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
