<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesCodigoResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_codigo_response', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_portal')->nullable()->unsigned();
            $table->unsignedBigInteger('id_inmueble')->nullable()->unsigned();

            $table->foreign('id_inmueble')->nullable()->references('id')->on('inmuebles');
            $table->foreign('id_portal')->nullable()->references('id')->on('portales');
            $table->string('codigo')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_codigo_response');
    }
}
