<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyBitacorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_bitacoras', function (Blueprint $table) {
            $table->char('codigo_bitacora', 100)->primary();
            $table->longText('descripcion_bitacora');
            $table->longText('fecha_bitacora');
            $table->integer('id_creador')->length(12)->unsigned()->nullable(false);
            $table->integer('cedula_persona')->length(12)->unsigned()->nullable(false);
            $table->integer('id_accion')->length(12)->unsigned()->nullable(false);
            $table->integer('id_estado')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_bitacoras');
    }
}
