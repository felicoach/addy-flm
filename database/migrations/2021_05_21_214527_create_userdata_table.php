<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserdataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userdata', function (Blueprint $table) {

            $table->id();

            $table->unsignedBigInteger('id_pais')->nullable()->unsigned();;
            $table->unsignedBigInteger('id_estado')->nullable()->unsigned();;
            $table->unsignedBigInteger('id_ciudad')->nullable()->unsigned();;
            $table->unsignedBigInteger('id_usuario')->nullable()->unsigned();;

            $table->integer('tipo_identificacion')->length(12)->unsigned()->nullable();
            $table->longText('foto_persona')->nullable();
            $table->longText('foto_portada_persona')->nullable();
            $table->bigInteger('cedula_persona')->unique()->nullable();;
            $table->char('primer_nombre', 150)->nullable();
            $table->longText('descripcion')->nullable();

            $table->char('segundo_nombre', 150)->nullable();
            $table->char('primer_apellido', 150)->nullable();
            $table->char('segundo_apellido', 150)->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->char('telefono_fijo', 20)->nullable();
            $table->char('genero')->nullable();
            $table->text('celular_movil', 20)->nullable();
            $table->char('celular_whatsapp', 20)->nullable();

            $table->string('facebook', 120)->nullable();
            $table->string('instagram', 120)->nullable();
            $table->string('linkedin', 120)->nullable();

            $table->longText('direccion_persona')->nullable();
            $table->integer('tipo_cliente')->length(12)->unsigned()->nullable();
            $table->integer('id_perfil')->length(12)->unsigned()->nullable();
            $table->integer('porcentaje_perfil')->length(12)->unsigned()->default(0);
            $table->integer('estado_persona')->length(12)->unsigned()->nullable(true);

            $table->foreign('id_pais')->nullable()->references('id')->on('countries');
            $table->foreign('id_estado')->nullable()->references('id')->on('states');
            $table->foreign('id_ciudad')->nullable()->references('id')->on('cities');
            $table->foreign('id_usuario')->nullable()->references('id')->on('users');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userdata');
    }
}
