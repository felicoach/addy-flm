<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesTipoInmueblePaisVentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_tipo_inmueble_pais_venta', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tI_id')->nullable()->unsigned();
            $table->foreign('tI_id')->nullable()->references('id')->on('inmuebles_tipo_inmuebles');
            $table->string("codigo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_tipo_inmueble_pais_venta');
    }
}
