<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalesStateInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portales_state_inmuebles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_portal')->nullable()->unsigned();
            $table->unsignedBigInteger('id_inmueble')->nullable()->unsigned();

            $table->foreign('id_inmueble')->nullable()->references('id')->on('inmuebles');
            $table->foreign('id_portal')->nullable()->references('id')->on('portales');

            $table->enum('state', ['publicado', 'despublicado', 'actualizado', 'pendiente'])->default('publicado');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portales_state_inmuebles');
    }
}
