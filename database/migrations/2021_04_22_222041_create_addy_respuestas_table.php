<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('addy_respuestas', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('id_pregunta')->references('id')->on('addy_preguntas')->onDelete('cascade'); //Foraneas
            $table->longText('descripcion_respuesta')->nullable();
            $table->double('valor_respuesta', 8, 2)->length(12)->unsigned()->nullable(false);
            $table->integer('estado_respuesta')->length(12)->unsigned()->nullable(false);
            $table->foreignId('id_pregunta')->references('id')->on('addy_preguntas')->onDelete('cascade'); //Foraneas
            //$table->integer('id_pregunta')->length(12)->unsigned()->nullable(false);
            $table->char('tipo_formulario')->nullable();
            $table->char('state')->default('inicial')->nullable();
            $table->integer('cedula_referido_detalle')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_respuestas');
    }
}
