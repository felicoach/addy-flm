<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmuebles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->unsignedBigInteger('tipo_inmueble')->nullable()->unsigned();
            $table->unsignedBigInteger('tipo_negocio')->nullable()->unsigned();
            $table->unsignedBigInteger('tipo_contrato')->nullable()->unsigned();
            $table->unsignedBigInteger('tipo_precio')->nullable()->unsigned();
            $table->unsignedBigInteger('periodo_admon')->nullable()->unsigned();
            $table->unsignedBigInteger('tipo_moneda')->nullable()->unsigned();
            $table->unsignedBigInteger('estado_propiedad')->nullable()->unsigned();
            $table->unsignedBigInteger('pais_id')->nullable()->unsigned();
            $table->unsignedBigInteger('estado_id')->nullable()->unsigned();
            $table->unsignedBigInteger('ciudad_id')->nullable()->unsigned();
            $table->unsignedBigInteger('zona_id')->nullable()->unsigned();
            $table->unsignedBigInteger('barrio_id')->nullable()->unsigned();
           
            $table->unsignedBigInteger('state')->nullable()->unsigned();
            $table->unsignedBigInteger('segmento')->nullable()->unsigned();
            $table->unsignedBigInteger('state_fisico')->nullable()->unsigned();
            $table->unsignedBigInteger('estrato')->nullable()->unsigned();
            $table->unsignedBigInteger('parqueadero')->nullable()->unsigned();
            $table->unsignedBigInteger('antiguedad')->nullable()->unsigned();

            $table->unsignedBigInteger('tiempo_alquiler')->nullable()->unsigned();
            $table->unsignedBigInteger('propietario_id')->nullable()->unsigned();


            $table->uuid('code')->nullable(true);
            $table->string('titulo_inmueble', 120);
            $table->string('slug', 120)->nullable(true);
            $table->boolean('hasFreeShipping')->default(false);
            $table->integer('rating')->default(1);
            $table->string('estado_publicacion')->nullable(true);
            $table->string('matricula_inmobiliaria')->nullable(true);
            $table->date('fecha_captacion')->nullable(true);
            $table->date('fecha_expiracion')->nullable(true);
            $table->string('precio_venta')->nullable(true);
            $table->string('precio_alquiler')->nullable(true);
            $table->string('precio_administracion')->nullable(true);
            $table->text('descripcion')->nullable(true);
            $table->string('image_principal')->nullable(true);
            $table->string('url_video')->nullable(true);
            $table->string('latitud')->nullable(true);
            $table->string('longitud')->nullable(true);
            $table->string('alcobas')->nullable(true);
            $table->string('banos')->nullable(true);
            $table->string('garaje')->nullable(true);
            $table->string('pisos')->nullable(true);
            $table->string('area_lote')->nullable(true);
            $table->string('area_contruida')->nullable(true);
            $table->string('frente')->nullable(true);
            $table->string('fondo')->nullable(true);
            $table->string('area_total')->nullable(true);
            $table->string('direccion')->nullable(true);
            $table->string('ano_construcion')->nullable(true);
            $table->string('cantidad_parqueadero')->nullable(true);




            $table->foreign('user_id')->nullable()->references('id')->on('users');
            $table->foreign('tipo_inmueble')->nullable()->references('id')->on('inmuebles_tipo_inmuebles');
            $table->foreign('tipo_negocio')->nullable()->references('id')->on('inmuebles_tipo_negocio');
            $table->foreign('tipo_contrato')->nullable()->references('id')->on('inmuebles_tipo_contrato');
            $table->foreign('tipo_precio')->nullable()->references('id')->on('inmuebles_tipo_precio');
            // $table->foreign('tipo_tiempo_alquiler')->references('id')->on('inmuebles_tipo_tiempo_alquiler');
            $table->foreign('periodo_admon')->nullable()->references('id')->on('inmuebles_tipo_periodo_admon');
            $table->foreign('tipo_moneda')->nullable()->references('id')->on('addy_tipo_moneda');
            $table->foreign('estado_propiedad')->nullable()->references('id')->on('inmuebles_estado_propiedad');
            $table->foreign('pais_id')->nullable()->references('id')->on('countries');
            $table->foreign('estado_id')->nullable()->references('id')->on('states');
            $table->foreign('ciudad_id')->nullable()->references('id')->on('cities');
            $table->foreign('zona_id')->nullable()->references('id')->on('zona');
            $table->foreign('barrio_id')->nullable()->references('id')->on('barrio');
            $table->foreign('state')->nullable()->references('id')->on('state_publication');
            $table->foreign('segmento')->nullable()->references('id')->on('segmento_mercado');
            $table->foreign('state_fisico')->nullable()->references('id')->on('inmuebles_estado_fisico');
            $table->foreign('estrato')->nullable()->references('id')->on('inmueble_estrato');
            $table->foreign('parqueadero')->nullable()->references('id')->on('inmueble_tipo_parqueadero');
            $table->foreign('tiempo_alquiler')->nullable()->references('id')->on('inmuebles_tipo_tiempo_alquiler');
            $table->foreign('propietario_id')->nullable()->references('id')->on('inmueble_propietario');
            $table->foreign('antiguedad')->nullable()->references('id')->on('inmueble_antiguedad');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmuebles');
    }
}
