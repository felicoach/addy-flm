<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmuebleInternaManyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmueble_interna_many', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inmueble')->nullable();
            $table->unsignedBigInteger('caracteristicas_internas')->nullable();

            $table->foreign('inmueble')->references('id')->on('inmuebles');
            $table->foreign('caracteristicas_internas')->references('id')->on('inmuebles_caracteristicas_internas');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmueble_interna_many');
    }
}
