<?php
namespace App\Tasks;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use SendGrid\Mail\From;
use SendGrid\Mail\Mail;
use SendGrid\Mail\To;

class SendDailyMetrics
{
        public function __invoke()
        {
          
            $this->sendEmail();
        }

        private function sendEmail() {
            // only append the '%' sign if an actual percentage was provided.
            return (new MailMessage)
            ->greeting('Hola!')
            ->subject(Lang::get('Confirmacion de correo electronico'))
            ->line(Lang::get('Hola, Pulsa el boton que esta abajo para verficar tu correo electronico'))
            ->action('Verificar email', '')
            ->line('Gracias por usar Addy!');
        }
}
