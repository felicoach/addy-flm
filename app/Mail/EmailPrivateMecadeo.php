<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailPrivateMecadeo extends Mailable
{
    use Queueable, SerializesModels;

    protected $new_arrival;
    protected $user;
    protected $template;
    protected $userdata;
    protected $inmueble;

    public function __construct($user, $new_arrival, $template, $userdata, $inmueble)
    {
        $this->user = $user;
        $this->new_arrival = $new_arrival;
        $this->template = $template;
        $this->userdata = $userdata;
        $this->inmueble = $inmueble;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = $this->new_arrival->title;
        $user = $this->userdata;
        $name =  "$user->primer_nombre $user->segundo_nombre $user->primer_apellido";
        if ($this->template->mercadeo_type->name == "agente") {
            $data = $this->userdata;
        } else if ($this->template->mercadeo_type->name == "inmueble") {
            $data = $this->inmueble;
        }

        return $this->markdown($this->template->template)
            ->from(env('MAIL_USERNAME'), $name)
            ->subject($title)
            ->with([
                'data' => $data,
                "user" => $user,
            ]);;
    }
}
