<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $mensaje;
    public $name;
    public $lastname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $mensaje, $name, $lastname)
    {
        
        $this->name=$name;
        $this->lastname=$lastname;
        $this->user=$user;
        $this->mensaje=$mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome');

        return $this->from('noresponder@crmaddy.online')
        ->subject('Gracias por Contactarnos | Addy')
        ->markdown('emails.contacto');
    }
}
