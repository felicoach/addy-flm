<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $new_arrival;
    protected $url;
    protected $template;

    public function __construct($url, $new_arrival, $template = "")
    {
        $this->url = $url;
        $this->new_arrival = $new_arrival;
        $this->template = $template;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $data = $this->new_arrival;


        return $this->markdown('mails.verified-email', compact('data'))
          

            ->with([
                'url' => $this->url,       
            ]);
    }
}
