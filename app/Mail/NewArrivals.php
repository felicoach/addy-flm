<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewArrivals extends Mailable
{
    use Queueable, SerializesModels;

    protected $new_arrival;
    protected $user;
    protected $template;
    protected $userdata;

    public function __construct($user, $new_arrival, $template = "", $userdata)
    {
        $this->user = $user;
        $this->new_arrival = $new_arrival;
        $this->template = $template;
        $this->userdata = $userdata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $data = $this->new_arrival;

        return $this->markdown('mails.newarrivals', compact('data'))
            ->subject($this->new_arrival->title)
            

            ->with([
                'user' => $this->user,
                'template' => $this->template,
            ]);
    }
}
