<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleInternaMany extends Model
{
    use HasFactory;

    protected $table = 'inmueble_interna_many';

    protected $fillable = [
        'caracteristicas_internas',
        'inmueble'
    ];

    public function caracteristicasInternas()
    {
        return $this->belongsTo(InmueblesCaracteristicasInternas::class, 'caracteristicas_internas');
    }
}
