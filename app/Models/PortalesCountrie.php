<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortalesCountrie extends Model
{
    use HasFactory;

    protected $table = "portales_countrie";

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
