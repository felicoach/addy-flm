<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EtiquetasCrm extends Model
{
    use HasFactory;

    protected $fillable = [
        "id_crm",
        "nombre"
    ];

    public function id_crm()
    {
        return $this->belongsTo(AddyCrm::class, 'addycrm', 'id');
    }
}
