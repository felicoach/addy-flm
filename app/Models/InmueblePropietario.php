<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmueblePropietario extends Model
{
    use HasFactory;

    protected $table = 'inmueble_propietario';

    protected $fillable = [
        'inmueble',
        'primer_nombre',
        'segundo_nombre',
        'primer_apellido',
        'segundo_apellido',
        'email',
        'celular',
        'telefono_fijo',
    ];

    public function inmuebles()
    {
        return $this->hasMany(inmuebles::class, 'propietario_id');
    }
}
