<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'estado_id');
    }


    public function cliente()
    {
        return $this->hasMany(Cliente::class, 'departamento');
    }
    

    public function empresas()
    {
        return $this->hasMany(Empresa::class, 'id_estado', "id");
    }
}
