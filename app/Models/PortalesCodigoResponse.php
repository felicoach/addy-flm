<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortalesCodigoResponse extends Model
{
    use HasFactory;

    protected $table = "portales_codigo_response";

    public function Inmuebles()
    {
        return $this->belongsTo(Inmueble::class, 'id_inmueble', 'inmueble_id');
    }
}
