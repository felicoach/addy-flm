<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmueblesCaracteristicasExterna extends Model
{
    use HasFactory;

    protected $table = 'inmuebles_caracteristicas_externas';


    public function caracthasMany()
    {
        return $this->hasMany(InmuebleInternaMany::class, 'caracteristicas_externas');
    }
}
