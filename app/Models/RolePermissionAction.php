<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePermissionAction extends Model
{
    use HasFactory;

    protected $table = "role_permission_actions";

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function permission() {
        return $this->hasMany(Permission::class, "id");
    }

    public function action() {
        return $this->hasMany(Action::class, "id");
    }

    public function rolePermission()
    {
        return $this->belongsTo(RolePermission::class, "id");
    }
}
