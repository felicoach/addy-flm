<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortalesCities extends Model
{
    use HasFactory;

    protected $table = "portales_cities";
}
