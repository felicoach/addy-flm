<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeRole extends Model
{
    use HasFactory;

    protected $table = "type_role";

    protected $fillable = [
        "nombre",
        "tipe"
    ];
}
