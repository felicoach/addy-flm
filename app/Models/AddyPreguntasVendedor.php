<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyPreguntasVendedor extends Model
{
    use HasFactory;

    protected $fillable = [
        'descripcion_pregunta',
        'valor_pregunta',
        'slug_modulo',
        'opcional_pregunta',
        'estado_pregunta',
    ];
}
