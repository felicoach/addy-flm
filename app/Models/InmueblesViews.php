<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class InmueblesViews extends Model
{
    use HasFactory;

    protected $table = 'inmuebles_views';

    protected $fillable = [
        'id_inmueble', 
        'id_user'
    ];

    
}
