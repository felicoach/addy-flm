<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    use HasFactory;

    protected $table = "permission_role";

    public function role_id()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function permission_id()
    {
        return $this->belongsTo(Permission::class, 'permission_id', 'id');
    }

    public function action()
    {
        return $this->belongsTo(Action::class, "action");
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
