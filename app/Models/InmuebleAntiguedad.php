<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleAntiguedad extends Model
{
    use HasFactory;

    protected $table = "inmueble_antiguedad";

    public function inmuebles()
    {
        return $this->hasMany(inmuebles::class, 'antiguedad');
    }
}
