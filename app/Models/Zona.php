<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{

    use HasFactory;

    protected $table = 'zona';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'zona_id');
    }
}



