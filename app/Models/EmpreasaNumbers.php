<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpreasaNumbers extends Model
{
    use HasFactory;


    protected $table = "empresa_agente_numbers";

    protected $fillable = [
        "agente_empresa", "number"
    ];

    public function empresaAgente()
    {
        return $this->belongsTo(Empresa::class);
    }
}
