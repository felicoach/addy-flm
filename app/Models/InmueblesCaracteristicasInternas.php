<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmueblesCaracteristicasInternas extends Model
{
    use HasFactory;

    protected $table = 'inmuebles_caracteristicas_internas';

    public function caracthasMany()
    {
        return $this->hasMany(InmuebleInternaMany::class, 'caracteristicas_internas');
    }
}
