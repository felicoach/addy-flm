<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portales extends Model
{
    use HasFactory;

    protected $table = "portales";

    protected $fillable = [
        "name",
        "slug",
        "image",
        "description",
        "type_service"
    ];
}
