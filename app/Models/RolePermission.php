<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    use HasFactory;

    protected $table = "role_permissions";

    protected $fillable = [
        "role", "permission", "create_by"
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class, "permission", "id");
    }

    public function rolePermissionAction()
    {
        return $this->hasMany(RolePermissionAction::class, "id");
    }
}
