<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    
    protected $fillable = [
        "mercadeo_id", "title", "body", "delivered", "date_string", "send_date", "user_id"
    ];

    public function mercadeo_id()
	{
		return $this->belongsTo(Mercadeo::class, 'mercadeo_id', 'id');
	}
}
