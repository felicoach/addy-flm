<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpreasaEmails extends Model
{
    use HasFactory;

    protected $table = "empresa_agente_emails";

    protected $fillable = [
        "agente_empresa", "email"
    ];

    public function empresaAgente()
    {
        return $this->belongsTo(Empresa::class);
    }
}
