<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Credenciales extends Model
{
    use HasFactory;

    protected $table = 'credenciales';

    protected $fillable = [
        'id_user', 
        'dominio', 
        'cpanel_user', 
        'cpanel_token', 
    ];

    
}
