<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $fillable = [
        "subject", "title"
    ];



    public function action()
    {
        return $this->hasMany(Action::class, "id");
    }

    public function permissionAction()
    {
        return $this->hasMany(PermissionsAction::class, "id");
    }

    
    public function rolePermissionAction()
    {
        return $this->belongsTo(RolePermissionAction::class, "id");
    }

    public function activePermissionAction()
    {
        return $this->hasMany(ActivePermissionActions::class, "permission");
    }


    public function rolePermissions()
    {
        return $this->hasMany(RolePermission::class,  "permission");
    }
}
