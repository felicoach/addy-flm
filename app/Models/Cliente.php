<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'estado',
        'nombre',
        'apellido',
        'genero',
        'created_by',
        'telefono',
        'fecha_nacimiento',
        'n_identificacion',
        'adicional',
        'pais',
        'departamento',
        'tipo_identificacion',
        'tipo_cliente',
        'ciudad',
        'direccion',
        'observaciones',
    ];

    public function userId()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function pais()
    {
        return $this->belongsTo(Country::class, 'pais', 'id');
    }

    public function ciudad()
    {
        return $this->belongsTo(City::class, 'ciudad', 'id');
    }

    public function departamento()
    {
        return $this->belongsTo(State::class, 'departamento', 'id');
    }

    public function tipo_identificacion()
    {
        return $this->belongsTo(TipoDocumento::class, 'tipo_identificacion', 'id');
    }

    public function tipo_cliente()
    {
        return $this->belongsTo(TipoCliente::class, 'tipo_cliente', 'id');
    }

    public function numeros()
    {
        return $this->hasMany(TelefonosClientes::class, 'cliente', 'id');
    }

    public function correos()
    {
        return $this->hasMany(Correos::class, 'cliente');
    }


    public function genero()
    {
        return $this->belongsTo(Genero::class, 'genero', 'id');
    }

    public function addycrms()
    {
        return $this->hasMany(AddyCrm::class);
    }
}
