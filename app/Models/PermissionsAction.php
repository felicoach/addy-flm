<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionsAction extends Model
{
    use HasFactory;

    protected $table = "permissions_actions";

    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission', 'id');
    }

    public function action()
    {
        return $this->belongsTo(Action::class, 'action', 'id');
    }
}
