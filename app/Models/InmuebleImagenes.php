<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleImagenes extends Model
{
    use HasFactory;

    protected $table = 'inmueble_imagenes';

    protected $fillable = [
        'url', 'inmueble', 'order'
    ];

    public function Inmuebles()
    {
        return $this->belongsTo(Inmueble::class, 'inmueble', 'inmueble_id');
    }
}
