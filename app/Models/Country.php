<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table = 'countries';

    public function city()
    {
        return $this->hasMany(City::class);
    }

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'pais_id');
    }

    public function portalesCountries()
    {
        return $this->hasMany(PortalesCountrie::class, 'countrie_id');
    }

    public function empresas()
    {
        return $this->hasMany(Empresa::class, 'id_pais', "id");
    }

    public function cliente()
    {
        return $this->hasMany(Cliente::class, 'pais');
    }
}
