<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = "roles";

    protected $fillable = [
        "nombre", "slug", "create_by", "type", "descripcion", "full-access", "permissions"
    ];


    public function rolePermissionAction()
    {
        return $this->hasMany(RolePermissionAction::class,  "role");
    }
}
