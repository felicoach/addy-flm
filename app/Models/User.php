<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Permisos\Traits\UserTrait;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Str;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, UserTrait, HasApiTokens;

    protected $fillable = [
        'username',
        'email',
        'password',
        'avatar',
        'id_facebook',
        'id_google',
        'id_empresa',
        'estado_user',
        'provider',
        'estado_user',
        'referred_by',
        'referral_code'
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected  static  function  boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->referral_code = (string) Str::uuid();
        });
    }

    public function referredBy()
    {
        return $this->belongsTo(User::class, 'referred_by');
    }

    public function roles() {
        return $this->belongsTo(Role::class,"roles");
    }

    public function referrals()
    {
        return $this->hasMany(User::class, 'referred_by');
    }

    public function persona()
    {
        return $this->belongsTo(AddyPersona::class, 'id_user', 'id');
    }

    public function empresa()
    {
        return $this->hasOne(Empresa::class, 'agente', 'id');
    }

    public function userdata()
    {
        return $this->hasOne(Userdata::class, 'id_usuario', 'id');
    }

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'user_id');
    }

    public function cliente()
    {
        return $this->hasMany(Cliente::class, 'user_id');
    }

    public function tareas()
    {
        return $this->hasMany(Tareas::class);
    }

    

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyNotification());
    }

    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }
}
