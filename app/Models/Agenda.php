<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'start',
        'end',
        'allDay',
        't_cita',
        'cliente_id',
        'inmueble_id',
        'direccion',
        'user_id',
        'created_by',
        'updated_by',
        'nota'
    ];

    protected $casts = [
        'extendedProps' => 'array',
        'allDay' => 'boolean',
    ];

    // public function setPropertiesAttribute($value)
    // {
    //     $properties = [];

    //     foreach ($value as $array_item) {
    //         if (!is_null($array_item['key'])) {
    //             $properties[] = $array_item;
    //         }
    //     }

    //     $this->attributes['extendedProps'] = json_encode($properties);
    // }


    public function t_cita()
    {
        return $this->belongsTo(TipoCita::class, "t_cita", "id");
    }

    public function cliente_id()
    {
        return $this->belongsTo(Cliente::class, "cliente_id", "id")->with("correos");
    }

    public function inmueble_id()
    {
        return $this->belongsTo(Inmuebles::class, "inmueble_id", "id");
    }

    public function state_cita()
    {
        return $this->belongsTo(AgendaEstadoCita::class, "state_cita ", "id");
    }
}
