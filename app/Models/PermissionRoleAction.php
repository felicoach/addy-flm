<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionRoleAction extends Model
{
    use HasFactory;

    public function permissionRole()
    {
        return $this->belongsTo(permissionRole::class, "permission", "id");
    }
    public function permission()
    {
        return $this->belongsTo(Permission::class, "id_permission_role", "id");
    }

}
