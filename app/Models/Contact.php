<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $table = "contacts_inmueble";

    protected $fillable =  [
        "user_id", "full_name", "email", "celular", "message"
    ];
}
