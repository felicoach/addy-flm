<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barrio extends Model
{
    use HasFactory;

    protected $table = 'barrio';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'barrio_id');
    }
}
