<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SegmentoMercados extends Model
{
    use HasFactory;

    protected $table = 'segmento_mercado';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'segmento');
    }
}
