<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyPaise extends Model {

    use HasFactory;

    protected $fillable = [
        'codigo',
        'pais',
    ];

    public function persona()
    {
        return $this->hasMany(AddyPersona::class);
    }

}
