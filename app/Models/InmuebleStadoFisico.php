<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleStadoFisico extends Model
{
    use HasFactory;

    protected $table = 'inmuebles_estado_fisico';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'state_fisico');
    }
}
