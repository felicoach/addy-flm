<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleStrato extends Model
{
    use HasFactory;

    protected $table = 'inmueble_estrato';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'estrato');
    }
}
