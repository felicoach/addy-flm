<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Correos extends Model
{
    use HasFactory;

    protected $fillable =  [
       'email', 'cliente'
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cliente, id');
    }
}
