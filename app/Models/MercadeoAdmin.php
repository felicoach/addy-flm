<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MercadeoAdmin extends Model
{
    use HasFactory;

    protected $table = "mercadeos_admin";

    protected $fillable = [
        "name", "url", "template", "mercadeo_type"
    ];
}
