<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleExternaMany extends Model
{
    use HasFactory;

    protected $table = 'inmueble_externa_many';

    protected $fillable = [
        'caracteristicas_externas',
        'inmueble'
    ];

    public function caracteristicasExternas()
    {
        return $this->belongsTo(InmueblesCaracteristicasExterna::class, 'caracteristicas_externas');
    }
}
