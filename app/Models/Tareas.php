<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tareas extends Model
{
    protected $table = 'tareas';

    protected $fillable = [
        'title',
        'dueDate',
        'user_id',
        'assignee',
        'updated_by',
        'description',
        'tags',
        'isCompleted',
        'isDeleted',
        'isImportant'
    ];
    protected $casts = [
        'tags' => 'array',
        'isCompleted' => 'boolean',
        'isDeleted' => 'boolean',
        'isImportant' => 'boolean'
    ];

    public function setPropertiesAttribute($value)
    {
        $properties = [];

        foreach ($value as $array_item) {
            if (!is_null($array_item['key'])) {
                $properties[] = $array_item;
            }
        }

        $this->attributes['tags'] = json_encode($properties);
    }

    public function assignee()
    {
        return $this->belongsTo(User::class, 'assignee', 'id')->with("userdata");
    }

    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function inmueble()
    {
        return $this->belongsTo(Inmuebles::class, 'inmueble', 'id');
    }

    public function etiquetasCrm()
    {
        return $this->hasMany(EtiquetasCrm::class);
    }
}
