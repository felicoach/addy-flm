<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleImagenesTemporal extends Model
{
    use HasFactory;

    protected $table = 'inmueble_imagenes_temporal';

    protected $fillable = [
        'url', 
        'inmueble', 
        'orden'
    ];

    
}
