<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatePublications extends Model
{
    use HasFactory;

    protected $table = 'state_publication';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'estado_publicacion');
    }
}
