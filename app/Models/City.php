<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'ciudad_id');
    }

    public function clientes()
    {
        return $this->hasMany(Cliente::class, 'ciudad');
    }

    public function empresas()
    {
        return $this->hasMany(Empresa::class, 'id_ciudad', "id");
    }
}
