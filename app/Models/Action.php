<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    use HasFactory;

    protected $fillable = [
        "action", "title"
    ];


    public function rolePermissionAction()
    {
        return $this->belongsTo(Action::class);
    }

    public function permissionAction()
    {
        return $this->hasMany(PermissionsAction::class, "id");
    }
}
