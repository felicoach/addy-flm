<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mercadeo extends Model
{
    use HasFactory;

    protected $fillable = [
        "html", "html_json", "titulo", "user_id"
    ];

    protected $casts = [
        'html_json' => 'array',
    ];

    public function setPropertiesAttribute($value)
    {
        $properties = [];

        foreach ($value as $array_item) {
            if (!is_null($array_item['key'])) {
                $properties[] = $array_item;
            }
        }

        $this->attributes['html_json'] = json_encode($properties);
    }

    public function message()
    {
        return $this->hasOne(Message::class, 'mercadeo_id');
    }
}
