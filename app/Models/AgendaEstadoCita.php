<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgendaEstadoCita extends Model
{
    use HasFactory;

    protected $table = "agenda_reporte_cita";

    protected $fillable = [
        "id_agenda",
        "state_cita",
        "comentario_cita"
    ];
}
