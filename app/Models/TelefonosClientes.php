<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelefonosClientes extends Model
{
    use HasFactory;

    protected $fillable = [
        'number',
        'cliente'
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cliente, id');
    }
}
