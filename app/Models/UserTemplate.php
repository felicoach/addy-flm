<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserTemplate extends Model
{
    use HasFactory;

    protected $table = 'user_template';

    protected $fillable = [
        'id_user', 
        'id_template', 
    ];

    
}
