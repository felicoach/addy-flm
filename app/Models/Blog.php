<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $table = 'blog';

    protected $fillable = [
        'titulo', 
        'slug', 
        'descripcion',
        'contenido',
        'image',
        'views',
        'estado_registro',
        'id_user',
        'views'
    ];

    
}
