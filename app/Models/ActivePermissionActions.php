<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivePermissionActions extends Model
{
    use HasFactory;

    protected $tatle = "active_permission_actions";

    protected $fillable = [
        "role_permission",
        "action"
    ];

    public function permission() {
        return $this->hasMany(Permission::class, "id");
    }

}
