<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoNegocio extends Model
{
    use HasFactory;

    protected $table = 'inmuebles_tipo_negocio';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'tipo_negocio');
    }
}
