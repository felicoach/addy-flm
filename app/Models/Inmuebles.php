<?php

namespace App\Models;

use App\Models\Country;
use App\Models\InmuebleImagenes;
use App\Models\InmueblePropietario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Inmuebles extends Model
{
    use HasFactory;

    protected $casts = [
        'hasFreeShipping' => 'boolean'
    ];

    protected $fillable = [
        'antiguedad',
        'titulo_inmueble',
        'tipo_inmueble',
        'tipo_negocio',
        'rangue_price',
        'habitaciones',
        'url_video',
        'segmento',
        'estado_publicacion',
        'state_fisico',
        'created_by',
        'updated_by',
        'cedula',
        'certificado_tradicion',
        'predial',
        'matricula_inmobiliaria',
        'estrato',
        'ano_construcion',
        'alcobas',
        'banos', 'garaje', 'pisos', 'cantidad_parqueadero',
        'parqueadero',
        'area_lote',
        'state_inmueble',
        'area_contruida',
        'frente',
        'fondo',
        'area_total', 'descripcion',
        'precio_venta',
        'precio_alquiler',
        'precio_administracion',
        'periodo_admon',
        'tiempo_alquiler',
        'tipo_precio',
        'user_id',
        'slug',
        'propietario_id',
        'pais_id',
        'estado_id',
        'ciudad_id',
        'zona_id',
        'barrio_id',
        'latitud',
        'longitud',
        'direccion',
    ];


    public function InmuebleImagenes()
    {
        return $this->hasMany(InmuebleImagenes::class, 'inmueble')->orderBy('order');
    }

    public function antiguedad()
    {
        return $this->belongsTo(InmuebleAntiguedad::class, 'antiguedad', 'id');
    }

    public function propietario()
    {
        return $this->belongsTo(InmueblePropietario::class, 'propietario_id', 'id');
    }

    public function PortalesCodigoResponse()
    {
        return $this->hasMany(PortalesCodigoResponse::class, 'id_inmueble', 'id');
    }

    public function PortalesUrls()
    {
        return $this->hasMany(PortalesUrl::class, 'inmueble_id', 'id');
    }


    public function PortaleStateInmueble()
    {
        return $this->hasMany(PortaleStatesInmueble::class, 'id_inmueble', 'id');
    }




    public function userId()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->with("userdata");
    }

    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by', 'id')->with("userdata");
    }

    public function updated_by()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id')->with("userdata");
    }



    public function paisId()
    {
        return $this->belongsTo(Country::class, 'pais_id', 'id')->with("portalesCountries");
    }

    public function ciudadId()
    {
        return $this->belongsTo(City::class, 'ciudad_id', 'id');
    }

    public function zonaId()
    {
        return $this->belongsTo(Zona::class, 'zona_id', 'id');
    }

    public function estadoId()
    {
        return $this->belongsTo(State::class, 'estado_id', 'id');
    }

    public function barrioId()
    {
        return $this->belongsTo(Barrio::class, 'barrio_id', 'id');
    }

    public function tipoInmueble()
    {
        return $this->belongsTo(TipoInmueble::class, 'tipo_inmueble', 'id');
    }

    public function tipo_negocio()
    {
        return $this->belongsTo(TipoNegocio::class, 'tipo_negocio', 'id');
    }
    public function estadoPublicacion()
    {
        return $this->belongsTo(StatePublications::class, 'estado_publicacion', 'id');
    }
    public function stateFisico()
    {
        return $this->belongsTo(InmuebleStadoFisico::class, 'state_fisico', 'id');
    }

    public function estrato()
    {
        return $this->belongsTo(InmuebleStrato::class, 'estrato', 'id');
    }

    public function tipoPrecio()
    {
        return $this->belongsTo(InmuebleTipoPrecio::class, 'tipo_precio', 'id');
    }
    public function periodoAdmon()
    {
        return $this->belongsTo(TipoPeriodoAdmon::class, 'periodo_admon', 'id');
    }

    public function tiempoAlquiler()
    {
        return $this->belongsTo(InmuebleTipoTiempoAlquiler::class, 'tiempo_alquiler', 'id');
    }

    public function parqueadero()
    {
        return $this->belongsTo(InmuebleTipoParqueadero::class, 'parqueadero', 'id');
    }

    public function segmento()
    {
        return $this->belongsTo(SegmentoMercados::class, 'segmento', 'id');
    }

    public function addycrms()
    {
        return $this->hasMany(AddyCrm::class);
    }
}
