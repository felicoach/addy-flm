<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleTipoTiempoAlquiler extends Model
{
    use HasFactory;

    protected $table = 'inmuebles_tipo_tiempo_alquiler';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'tiempo_alquiler');
    }
}
