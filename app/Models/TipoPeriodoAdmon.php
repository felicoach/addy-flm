<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoPeriodoAdmon extends Model
{
    use HasFactory;

    protected $table = 'inmuebles_tipo_periodo_admon';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'periodo_admon');
    }
}
