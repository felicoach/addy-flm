<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleTipoParqueadero extends Model
{
    use HasFactory;

    protected $table = 'inmueble_tipo_parqueadero';

    public function inmuebles()
    {
        return $this->hasMany(Inmuebles::class, 'parqueadero');
    }
}
