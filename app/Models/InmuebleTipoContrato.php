<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleTipoContrato extends Model
{
    use HasFactory;

    protected $table = 'inmuebles_tipo_contrato';
}
