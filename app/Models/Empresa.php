<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $table = "empresa_agente";

    protected $fillable = [
        "agente",
        "id_pais",
        "id_estado",
        "id_ciudad",
        "email",
        "celular",
        "logo",
        "nombre",
        'telefono',
        "nit",
        "dv",
        "pagina_web",
        "descripcion",
        "instagram",
        "facebook",
        "linkedin",
        "color_primario",
        "color_secundario",
        "font_color",
        "complete",
        "mision",
        "vision",
        "longitud",
        "latitud",
        "slug",
        "id_domain"
    ];

    public function empresaEmails()
    {
        return $this->hasMany(EmpreasaEmails::class, "agente_empresa", "id");
    }

    public function agente()
    {
        return $this->belongsTo(User::class, 'agente', 'id');
    }

    public function empresaNumbers()
    {
        return $this->hasMany(EmpreasaNumbers::class, "agente_empresa", "id");
    }

    public function empresaRedes()
    {
        return $this->hasMany(EmpreasaRedes::class, "agente_empresa", "id");
    }

    public function id_pais()
    {
        return $this->belongsTo(Country::class, 'id_pais', 'id');
    }

    public function id_estado()
    {
        return $this->belongsTo(State::class, 'id_estado', 'id');
    }

    public function id_ciudad()
    {
        return $this->belongsTo(City::class, 'id_ciudad', 'id');
    }
}
