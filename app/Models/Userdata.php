<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Userdata extends Model
{
	use HasFactory;

	protected $table = "userdata";

	protected $fillable = [
		'cedula_persona',
		'tipo_identificacion',
		'foto_persona',
		'primer_nombre',
		'segundo_nombre',
		'primer_apellido',
		'segundo_apellido',
		'fecha_nacimiento',
		'telefono_fijo',
		'celular_movil',
		'celular_whatsapp',
		'direccion_persona',
		'id_estado',
		 'genero',
		'id_pais',
		'facebook',
		'instagram',
		'linkedin',
		'descripcion',
		'id_ciudad',
		'nit_empresa',
		'id_perfil',
		'id_usuario',
		'estado_persona',
		'color_fondo',
		'color_fuente',
		'tipo_cliente'
	];

	public function id_usuario()
	{
		return $this->belongsTo(User::class, 'id_usuario', 'id');
	}
}
