<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpreasaRedes extends Model
{
    use HasFactory;


    protected $table = "empresa_agente_redes";

    protected $fillable = [
        "agente_empresa", "name", "url"
    ];

    public function empresaAgente()
    {
        return $this->belongsTo(Empresa::class);
    }
}
