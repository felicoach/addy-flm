<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

if (!function_exists('getUsersRoles')) {

    function getUsersRoles($auth)
    {
        $role = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

        if ($role->slug == "inmobiliaria") {
            return $auth->id;
        } else {
            return $auth->create_by;
        }
    }
}
if (!function_exists('eliminar_tildes')) {
    function eliminar_tildes($cadena)
    {

        $no_permitidas = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã‚", "ÃŠ", "ÃŽ", "Ã”", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã„", "Ã‹");
        $permitidas = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E");
        $texto = str_replace($no_permitidas, $permitidas, $cadena);
        return $texto;
    }
}
if (!function_exists('isValidURL')) {

    function isValidURL($url)
    {
        if (@file_get_contents($url,false,NULL,0,1))
        {
            return true;
        }
        return false;
    }

}
