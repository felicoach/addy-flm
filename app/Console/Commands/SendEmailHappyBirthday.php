<?php

namespace App\Console\Commands;

use App\Mail\WelcomeMessage;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;

class SendEmailHappyBirthday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:happymessage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando sirve para programar correos de cumpleaños';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = Auth::user();

        $data = User::with('userdata')->where("referred_by", $users->id)->get();
        
        $dt = Carbon::today()->toDateString();

        foreach ($data as $da) {
            if (isset($da->userdata['fecha_nacimiento']) && $da->userdata['fecha_nacimiento'] == $dt) {
                Mail::to($da->email)->send(new WelcomeMessage());
            }
        }
    }
}
