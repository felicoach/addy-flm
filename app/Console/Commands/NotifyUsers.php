<?php

namespace App\Console\Commands;

use App\Jobs\SendMailJob;
use App\Mail\NewArrivals;
use App\Models\Message;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class NotifyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notificar al usuario';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now =   $now = date("Y-m-d H:i", strtotime(Carbon::now()));

        $auth = Auth::user();
        $messages = Message::with('mercadeo_id')->where('user_id', $auth->id)->get();

        if ($messages !== null) {
            $messages->where('date_string',  $now)->each(function ($message) {

                if ($message->delivered == 'En proceso') {

                    $auth = Auth::user();
                    $users = User::with('userdata')->where("referred_by", $auth->id)->get();

                    foreach ($users as $user) {
                        dispatch(
                            new SendMailJob(
                                $user->email,
                                new NewArrivals($user, $message)
                            )
                        );
                    }
                    $message->delivered = 'Enviado';
                    $message->save();
                }
            });
        }
    }
}
