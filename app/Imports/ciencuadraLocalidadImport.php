<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ciencuadraLocalidadImport implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        $find = DB::table('ciencuadra_localidades')->where('codigo',   $row['id'])->first();
        if (!$find) {
            DB::table('ciencuadra_localidades')->insert(
                ['nombre' => $row['nombre'], 'codigo' => $row['id']],
            );
        } else {
            DB::table('ciencuadra_localidades')->where('codigo', $find->id)->update(
                ['codigo' => $row['id']],
            );
        }
    }
}
