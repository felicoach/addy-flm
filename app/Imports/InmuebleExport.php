<?php

namespace App\Imports;

use App\Models\Inmuebles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;


class InmuebleExport implements FromCollection, WithMapping, WithHeadings
{

    // a place to store the inmueble dependency
    private $inmueble;

    // use constructor to handle dependency injection
    public function __construct($inmueble)
    {
        $this->inmueble = $inmueble;
    }

    // set the collection of inmuebles to export
    public function collection()
    {
        return $this->inmueble;
    }

    // map what a single inmueble row should look like
    // this method will iterate over each collection item
    public function map($inmueble): array
    {
        return [
            $inmueble->titulo_inmueble,
            $inmueble->precio_venta,
            $inmueble->precio_alquiler,
            $inmueble->matricula_inmobiliaria,
            $inmueble->direccion,
            $inmueble->ano_construcion,
            $inmueble->parqueadero,
            $inmueble->habitaciones,
            // $inmueble->alcobas,
            $inmueble->banos,
            //$inmueble->garaje,
            $inmueble->pisos,
            $inmueble->tipo_inmueble,
            $inmueble->tipo_negocio,
            $inmueble->paises,
            $inmueble->departamento,

            $inmueble->ciudades,


            $inmueble->area_contruida,
            $inmueble->area_lote,
            $inmueble->area_total
        ];
    }

    // this is fine
    public function headings(): array
    {
        return [
            'Titulo inmueble',
            'Precio venta',
            'Precio alquiler',
            'Matricula inmobiliaria',
            'Direccion',
            'Año contruccion',
            'Parqueadero',
            'Habitaciones',
            'Baños',
            'Pisos',
            "Tipo inmueble",
            "Tipo negocio",
            "Tipo contrato",
            "pais",
            "departamento",
            "ciudades",
            'Estrato',
            'Area contruida',
            'Area lote',
            'Area total'
        ];
    }
}
