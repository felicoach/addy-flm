<?php

namespace App\Imports;

use App\Models\Cliente;
use App\Models\Correos;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FincarraizBarriosImport implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        ini_set('max_execution_time', 80); // 5 minutes

        $find_state = DB::table('states')->where('country_id', 48)->get();
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%finca%")->first();

        for ($st = 0; $st < count($find_state); $st++) {

            $find_cities = DB::table('cities')->where('state_id', $find_state[$st]->id)->get();

            for ($ct = 0; $ct < count($find_cities); $ct++) {

                $find_zonas = DB::table('zona')->where('ciudad_id', $find_cities[$ct]->id)->get();

                for ($zt = 0; $zt < count($find_zonas); $zt++) {

                    $find_barrio = DB::table('barrio')->where('zona_id', $find_zonas[$zt]->id)->get();

                    for ($bt = 0; $bt < count($find_barrio); $bt++) {
                        if ($find_barrio[$bt]->name ==  $row["nombre"]) {

                            $find = DB::table('portales_barrio')->where('barrio_id',  $find_barrio[$bt]->id)->where('portale_id', $portal->id)->first();

                            if (!$find) {
                                DB::table('portales_barrio')->insert(
                                    ['barrio_id' => $find_barrio[$bt]->id, 'portale_id' => $portal->id, 'codigo' => $row['id']],
                                );
                            } else {
                                DB::table('portales_barrio')->where('id', $find->id)->update(
                                    ['codigo' => $row['id']],
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}
