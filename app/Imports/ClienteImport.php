<?php

namespace App\Imports;

use App\Models\Cliente;
use App\Models\Correos;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ClienteImport implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        $auth = Auth::user();
        
        $data = Cliente::create([
            "user_id" => $auth->id,
            'nombre' => $row["nombre"],
            'apellido' => $row["apellido"],
            'telefono' => $row["telefono"],
            'tipo_cliente' => $row["tipo_cliente"],
        ]);

        if ($data) {
            Correos::create([
                'email' => $row["correo"],
                'cliente' => $data->id
            ]);
        }
    }
}
