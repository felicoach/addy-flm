<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CiencuadraBarriosImport implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%cien%")->first();
        $find_barrio = DB::table('barrio')->where('name', '% ' . $row["nombre"] . '%')->first();

        if ($find_barrio) {
            $find = DB::table('portales_barrio')->where('barrio_id',  $find_barrio->id)->where('portale_id', $portal->id)->first();
            if (!$find) {
                DB::table('portales_barrio')->insert(
                    ['barrio_id' => $find_barrio->id, 'portale_id' => $portal->id, 'codigo' => $row['id']],
                );
            } else {
                DB::table('portales_barrio')->where('id', $find->id)->update(
                    ['codigo' => $row['id']],
                );
            }
        }
    }
}
