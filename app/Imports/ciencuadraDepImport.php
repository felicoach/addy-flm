<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ciencuadraDepImport implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%cien%")->first();

        $find_state = DB::table('states')->where('country_id', 48)->get();

        for ($i=0; $i < count($find_state); $i++) { 
           
            if ( $find_state[$i]->name ==  $row["nombre"]) {
               
                $find = DB::table('portales_states')->where('state_id',  $find_state[$i]->id)->where('portale_id', $portal->id)->first();
                if (!$find) {
                    DB::table('portales_states')->insert(
                        ['state_id' => $find_state[$i]->id, 'portale_id' => $portal->id, 'codigo' => $row['id']],
                    );
                } else {
                    DB::table('portales_states')->where('state_id', $find_state[$i]->id)->where("portale_id", $portal->id)->update(
                        ['codigo' => $row['id']],
                    );
                }
            }
        }      
    }
}
