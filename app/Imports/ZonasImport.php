<?php

namespace App\Imports;

use App\Models\Cliente;
use App\Models\Correos;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ZonasImport implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {

        $find_state = DB::table('states')->where('country_id', 48)->get();
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%finca%")->first();

        for ($st = 0; $st < count($find_state); $st++) {

            $find_cities = DB::table('cities')->where('state_id', $find_state[$st]->id)->get();

            for ($ct = 0; $ct < count($find_cities); $ct++) {

                $find = DB::table('portales_cities')->where('codigo',  $row['city_id'])
                    ->where('portale_id', $portal->id)
                    ->where('city_id', $find_cities[$ct]->id)
                    ->first();

                if ($find) {
                    DB::table('zona')->insert(
                        ['ciudad_id' => $find->city_id, 'name' => $row['nombre']],
                    );
                }
            }
        }
    }
}
