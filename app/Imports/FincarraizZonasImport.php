<?php

namespace App\Imports;

use App\Models\Cliente;
use App\Models\Correos;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FincarraizZonasImport implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {

        $find_state = DB::table('states')->where('country_id', 48)->get();
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%finca%")->first();

        for ($st = 0; $st < count($find_state); $st++) {

            $find_cities = DB::table('cities')->where('state_id', $find_state[$st]->id)->get();

            for ($ct = 0; $ct < count($find_cities); $ct++) {

                $find_zonas = DB::table('zona')->where('ciudad_id', $find_cities[$ct]->id)->get();

                for ($zt = 0; $zt < count($find_zonas); $zt++) {

                    if ($find_zonas[$zt]->name ==  $row["nombre"]) {

                        $find = DB::table('portales_zona')->where('zona_id',  $find_zonas[$zt]->id)->where('portale_id', $portal->id)->first();

                        if (!$find) {
                            DB::table('portales_zona')->insert(
                                ['zona_id' => $find_zonas[$zt]->id, 'portale_id' => $portal->id, 'codigo' => $row['id']],
                            );
                        } else {
                            DB::table('portales_zona')->where('id', $find->id)->update(
                                ['codigo' => $row['id']],
                            );
                        }
                    }
                    
                
                }

            }
        }
    }
}
