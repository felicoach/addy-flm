<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyRespuesta as Respuesta;
use App\Http\Controllers\API\ApiController;
use App\Models\AddyRespuestasVendedor;
use App\Models\Userdata;
use Illuminate\Support\Facades\DB;

class RespuestasController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */

    protected $value_comprador = 6.666666666666667;

    protected $value_vendedor = 10;

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   //$GLOBALS['variable'] = something;

        if ($request->tipo_cliente == 'cliente_comprador') {
            

            if (empty($request->respuesta1)) {
                $respuesta1 = "Sin informacion";
                $valor = 0;
            } else {
                $respuesta1 = implode(",", $request->respuesta1);
                $valor  = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta1, $valor, 1);

            if (empty($request->respuesta2['inicial'])) {
                $inicial = null;
                $valorI = null;
            } else {
                $inicial  = str_replace('.', '', $request->respuesta2['inicial']);
                $valorI = $this->value_comprador / 2;
            }

            if (empty($request->respuesta2['final'])) {
                $final = null;
                $valorF = null;
            } else {
                $final  = str_replace('.', '', $request->respuesta2['final']);
                $valorF = $this->value_comprador / 2;
            }
            if ($inicial == null && $final == null) {
                $respuesta2 = 'Sin informacion';
                $valor = 0;
            } else {
                $respuesta2 =   "Inicial-" . $inicial . " final-" . $final;
                $valor = $valorI + $valorF;
            }

            $this->registerRange($request->respuesta2['divisa'], $inicial, $final, $request, $respuesta2, $valor, 2);


            if (empty($request->respuesta3)) {
                $respuesta3 = "Sin informacion";
                $valor = 0;
            } else {
                $respuesta3 =   $request->respuesta3;
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta3, $valor, 3);


            if (empty($request->respuesta4['valor'])) {

                $descripcion = "Sin informacion";
                $valor =   0;
                $divisa = '';
                $valormony = null;
            } else {

                if ($request->respuesta4['valor'] == 'no') {
                    $descripcion = $request->respuesta4['valor'];
                    $valor =    0;
                } else {
                    if ($request->respuesta4['subrespuesta'] != '' || $request->respuesta4['subrespuesta'] != null) {
                        $valormony = str_replace('.', '', $request->respuesta4['subrespuesta']);
                    } else {
                        $valormony = null;
                    }
                    $descripcion = $request->respuesta4['valor'] . ", " . $request->respuesta4['subrespuesta'];
                    $divisa = $request->respuesta4['divisa'];
                    $valor =    $this->value_comprador;
                }
            }

            $this->registerCredito($descripcion, $request, $request->pregunta4, $valor, $divisa, $valormony);


            if (empty($request->respuesta5['value'])) {
                $respuesta5 = "Sin informacion";
                $valor = 0;
            } else {
                if ($request->respuesta5['value'] != null || $request->respuesta5['value'] != '') {
                    $valormony = str_replace('.', '', $request->respuesta5['value']);
                } else {
                    $valormony = null;
                }
                $respuesta5 = $request->respuesta5['divisa'] . ", " . str_replace('.', '', $request->respuesta5['value']);
                $divisa = $request->respuesta5['divisa'];
                $valor = $this->value_comprador;
            }


            $this->registerCredito($respuesta5, $request, $request->pregunta5, $valor, $divisa, $valormony);

            if ($request->respuesta6['pais'] == null) {
                $respuesta6 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta6 = $request->respuesta6['pais']['id'];
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta6, $valor, 6);

            if( $request->respuesta61['estado'] == null) {
                $respuesta61 = "sin informacion";
                $valor = 0;
            }else{
                $respuesta61 = $request->respuesta61['estado']['id'];
                $valor = $this->value_comprador;
            }
            
            $this->registerResReferido($request, $respuesta61, $valor, 61);
           

            if ($request->respuesta7['ciudad'] == null) {
                $respuesta7 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta7 = $request->respuesta7['ciudad']['id'];
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta7, $valor, 7);

            if ($request->respuesta8['zona'] == null) {
                $respuesta8 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta8 = $request->respuesta8['zona']['id'];
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta8, $valor, 8);

            if ($request->respuesta9['barrio'] == null) {
                $respuesta9 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta9 = $request->respuesta9['barrio']['id'];
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta9, $valor, 9);


            if (empty($request->respuesta5['value'])) {
                $respuesta10 = "Sin informacion";
                $valor = 0;
            } else {
                if (!empty($request->respuesta10['longitud'])) {
                    $respuesta10 = $request->respuesta10['longitud'] . " " . $request->respuesta10['value'];
                    $valor = $this->value_comprador;
                } else {
                    $respuesta10 = "Sin informacion";
                    $valor = 0;
                }
            }

            $this->registerResReferido($request, $respuesta10, $valor, 10);

            if (empty($request->respuesta11['numero_habitaciones'])) {
                $respuesta11 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta11 = $request->respuesta11['numero_habitaciones'];
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta11, $valor, 11);



            if (empty($request->respuesta12['caracteristica'])) {
                $respuesta12 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta12 = $request->respuesta12['caracteristica'];
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta12, $valor, 12);

            
            if (empty($request->respuesta13['tiempo_buscando'])) {
                $respuesta13 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta13 = $request->respuesta13['tiempo_buscando'];
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta13, $valor, 13);


            
            if (empty($request->respuesta14['tiempo_necesitando'])) {
                $respuesta14 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta14 = $request->respuesta14['tiempo_necesitando'];
                $valor = $this->value_comprador;
            }

            $this->registerResReferido($request, $respuesta14, $valor, 14);

            $this->sumRespuestas($request->id_referido, 'c');
        } else if ($request->tipo_cliente == 'cliente_vendedor') {

            /* RESPUESTA 1 */

            if (!isset($request->respuesta1)) {
                $respuesta1 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta1 =   $request->respuesta1;
                $valor = $this->value_vendedor;
            }


            $this->registerResReferidoVendedor($request, $respuesta1, $valor, 1);
            /* ========================== RESPUESTA 2 ========================== */

            if (!isset($request->respuesta2)) {
                $respuesta2 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta2 =   $request->respuesta2;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta2, $valor, 2);

            /* ========================== RESPUESTA 3 ========================== */

            if (!isset($request->respuesta3)) {
                $respuesta3 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta3 =   $request->respuesta3;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta3, $valor, 3);

            /* ========================== RESPUESTA 4 ========================== */

            if (!isset($request->respuesta2)) {
                $respuesta4 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta4 =   $request->respuesta4;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta4, $valor, 4);

            /* ========================== RESPUESTA 5 ========================== */

            if (!isset($request->respuesta5)) {
                $respuesta5 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta5 =   $request->respuesta5;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta5, $valor, 5);

            /* ========================== RESPUESTA 6 ========================== */

            if (!isset($request->respuesta6)) {
                $respuesta6 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta6 =   $request->respuesta6;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta6, $valor, 6);

            /* ========================== RESPUESTA 7 ========================== */

            if (!isset($request->respuesta7)) {
                $respuesta7 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta7 =   $request->respuesta7;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta7, $valor, 7);

            /* ========================== RESPUESTA 8 ========================== */

            if (!isset($request->respuesta8)) {
                $respuesta8 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta8 =   $request->respuesta8;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta8, $valor, 8);

            /* ========================== RESPUESTA 9 ========================== */

            if (!isset($request->respuesta2)) {
                $respuesta9 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta9 =   $request->respuesta9;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta9, $valor, 9);

            /* ========================== RESPUESTA 10 ========================== */

            if (!isset($request->respuesta10)) {
                $respuesta10 =  "Sin informacion";
                $valor = 0;
            } else {
                $respuesta10 =   $request->respuesta10;
                $valor = $this->value_vendedor;
            }

            $this->registerResReferidoVendedor($request, $respuesta10, $valor, 10);

            /* ========================== REGISTRO EN BASE DE DATOS ========================== */

            $this->sumRespuestas($request->id_referido, 'v');
        } else {
        }

        return response()->json([
            "status" => 200,
            "message" => "Registro exitoso"
        ]);
    }


    public function showVendedor($id)
    {
        $preguntas =  DB::table('addy_respuestas_vendedor AS ad')
            ->select('ad.*')
            ->where('ad.cedula_referido_detalle', '=', $id)
            ->get();

        $data['preguntas'] = $preguntas;

        return  $this->sendResponse($data, "Listado de respuestas");
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * 
     */
    public function show($id)
    {
        $preguntas =  DB::table('addy_respuestas AS ad')
            ->select('ad.*')
            ->where('ad.cedula_referido_detalle', '=', $id)
            ->get();

        $range =  DB::table('addy_detalle_pregunta_respuesta_referido_rango AS ad')
            ->select('ad.inicial', 'ad.final')
            ->where('ad.id_referido_R', '=', $id)
            ->get();

        $mony = DB::table('valor_pregunta_credito AS ad')
            ->select('ad.divisa', 'ad.value', 'ad.id_respuesta_r', 'ad.id_referido_r')
            ->where('ad.id_referido_r', '=', $id)
            ->get();

        $data['preguntas'] = $preguntas;
        $data['rangue'] = $range;
        $data['mony'] = $mony;

        return  $this->sendResponse($data, "Listado de respuestas");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, $id)
    {

       // return $request;
        //se valida con el puntage
        if ($request->tipo_cliente == "cliente_comprador") {

            if (empty($request->respuesta1)) {
                $respuesta1 = "Sin informacion";
                $valor = 0;
            } else {
                $respuesta1 = implode(",", $request->respuesta1);
                $valor  = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta1, $respuesta1, $valor, 'v');

            if (empty($request->respuesta2['inicial'])) {
                $inicial = null;
                $valorI = null;
            } else {
                $inicial  = str_replace('.', '', $request->respuesta2['inicial']);
                $valorI = $this->value_comprador / 2;
            }

            if (empty($request->respuesta2['final'])) {
                $final = null;
                $valorF = null;
            } else {
                $final  = str_replace('.', '', $request->respuesta2['final']);
                $valorF = $this->value_comprador / 2;
            }
            if ($inicial == null && $final == null) {
                $respuesta2 = 'Sin informacion';
                $valor = 0;
            } else {
                $respuesta2 =   "Inicial-" . $inicial . " final-" . $final;
                $valor = $valorI + $valorF;
            }

            $this->getPuntageRange($id, $inicial, $final, $request->pregunta2, $valor, $respuesta2);


            if (empty($request->respuesta3)) {
                $respuesta3 = "Sin informacion";
                $valor = 0;
            } else {
                $respuesta3 =   $request->respuesta3;
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta3, $respuesta3, $valor, 'c');


            if (empty($request->respuesta4['valor'])) {

                $descripcion = "Sin informacion";
                $valor =   0;
                $divisa = '';
                $valormony = null;
            } else {

                if ($request->respuesta4['valor'] == 'no') {
                    $descripcion = $request->respuesta4['valor'];
                    $valor =    0;
                } else {
                    if ($request->respuesta4['subrespuesta'] != '' || $request->respuesta4['subrespuesta'] != null) {
                        $valormony = str_replace('.', '', $request->respuesta4['subrespuesta']);
                    } else {
                        $valormony = null;
                    }
                    $descripcion = $request->respuesta4['valor'] . ", " . $request->respuesta4['subrespuesta'];
                    $divisa = $request->respuesta4['divisa'];
                    $valor =    $this->value_comprador;
                }
            }

            DB::table('addy_respuestas AS ad')
                ->where('ad.cedula_referido_detalle', '=', $id)
                ->where('ad.id_pregunta',  '=',  $request->pregunta4)
                ->update([
                    'ad.descripcion_respuesta' => $descripcion,
                    'ad.valor_respuesta' => $valor
                ]);

            DB::table('valor_pregunta_credito AS ad')
                ->where('ad.id_referido_r', '=', $id)
                ->where('ad.id_respuesta_r',  $request->pregunta4)
                ->update([
                    'ad.value' => $valormony,
                    'ad.divisa' => $divisa
                ]);


            if (empty($request->respuesta5['value'])) {
                $respuesta5 = "Sin informacion";
                $valor = 0;
            } else {
                if ($request->respuesta5['value'] != null || $request->respuesta5['value'] != '') {
                    $valormony = str_replace('.', '', $request->respuesta5['value']);
                } else {
                    $valormony = null;
                }
                $respuesta5 = $request->respuesta5['divisa'] . ", " . str_replace('.', '', $request->respuesta5['value']);
                $divisa = $request->respuesta5['divisa'];
                $valor = $this->value_comprador;
            }

            DB::table('addy_respuestas AS ad')
                ->where('ad.cedula_referido_detalle', '=', $id)
                ->where('ad.id_pregunta',  '=',  $request->pregunta5)
                ->update([
                    'ad.descripcion_respuesta' => $respuesta5,
                    'ad.valor_respuesta' => $valor
                ]);

            DB::table('valor_pregunta_credito AS ad')
                ->where('ad.id_referido_r', '=', $id)
                ->where('ad.id_respuesta_r', '=', $request->pregunta5)
                ->update([
                    'ad.value' => $valormony,
                    'ad.divisa' => $divisa
                ]);

          

            if ($request->respuesta6['pais'] == null) {
                $respuesta6 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta6 = $request->respuesta6['pais']['id'];
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta6, $respuesta6, $valor, 'c');

            if( $request->respuesta61['estado'] == null) {
                $respuesta61 = "sin informacion";
                $valor = 0;
            }else{
                $respuesta61 = $request->respuesta61['estado']['id'];
                $valor = $this->value_comprador;
            }
            
          

            $this->getPuntage($id, 61, $respuesta61, $valor, 'c');

            if ($request->respuesta7['ciudad'] == null) {
                $respuesta7 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta7 = $request->respuesta7['ciudad']['id'];
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta7, $respuesta7, $valor, 'c');

            if ($request->respuesta8['zona'] == null) {
                $respuesta8 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta8 = $request->respuesta8['zona']['id'];
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta8, $respuesta8, $valor, 'c');

            if ($request->respuesta9['barrio'] == null) {
                $respuesta9 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta9 = $request->respuesta9['barrio']['id'];
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta9, $respuesta9, $valor, 'c');

            if (empty($request->respuesta10['value'])) {
                $respuesta10 = "Sin informacion";
                $valor = 0;
            } else {
                if (!empty($request->respuesta10['longitud'])) {
                    $respuesta10 = $request->respuesta10['longitud'] . " " . $request->respuesta10['value'];
                    $valor = $this->value_comprador;
                } else {
                    $respuesta10 = "Sin informacion";
                    $valor = 0;
                }
            }
            $this->getPuntage($id, $request->pregunta10, $respuesta10, $valor, 'c');
          

            if (empty($request->respuesta11['numero_habitaciones'])) {
                $respuesta11 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta11 = $request->respuesta11['numero_habitaciones'];
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta11, $respuesta11, $valor, 'c');


            if (empty($request->respuesta12['caracteristica'])) {
                $respuesta12 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta12 = $request->respuesta12['caracteristica'];
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta12, $respuesta12, $valor, 'c');

            if (empty($request->respuesta13['tiempo_buscando'])) {
                $respuesta13 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta13 = $request->respuesta13['tiempo_buscando'];
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta13, $respuesta13, $valor, 'c');

            
            if (empty($request->respuesta14['tiempo_necesitando'])) {
                $respuesta14 = "sin informacion";
                $valor = 0;
            } else {
                $respuesta14 = $request->respuesta14['tiempo_necesitando'];
                $valor = $this->value_comprador;
            }

            $this->getPuntage($id, $request->pregunta14, $respuesta14, $valor, 'c');

            $this->sumRespuestas($id, 'c');

        } elseif ($request->tipo_cliente == "cliente_vendedor") {


            $respuesta1 = '';
            $valor1 = '';

            if (!isset($request->respuesta1)) {
                $respuesta1 =  "Sin informacion";
                $valor1 = 0;
            } else {
                $respuesta1 = $request->respuesta1;
                $valor1 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta1, $respuesta1, $valor1, 'v');

            $respuesta2 = '';
            $valor2 = '';

            if (!isset($request->respuesta2)) {
                $respuesta2 =  "Sin informacion";
                $valor2 = 0;
            } else {
                $respuesta2 = $request->respuesta2;
                $valor2 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta2, $respuesta2, $valor2, 'v');

            $respuesta3 = '';
            $valor3 = '';

            if (!isset($request->respuesta3)) {
                $respuesta3 =  "Sin informacion";
                $valor3 = 0;
            } else {
                $respuesta3 = $request->respuesta3;
                $valor3 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta3, $respuesta3, $valor3, 'v');

            $respuesta4 = '';
            $valor4 = '';

            if (!isset($request->respuesta4)) {
                $respuesta4 =  "Sin informacion";
                $valor4 = 0;
            } else {
                $respuesta4 = $request->respuesta4;
                $valor4 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta4, $respuesta4, $valor4, 'v');

            $respuesta5 = '';
            $valor5 = '';

            if (!isset($request->respuesta5)) {
                $respuesta5 =  "Sin informacion";
                $valor5 = 0;
            } else {
                $respuesta5 = $request->respuesta5;
                $valor5 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta5, $respuesta5, $valor5, 'v');

            $respuesta6 = '';
            $valor6 = '';

            if (!isset($request->respuesta6)) {
                $respuesta6 =  "Sin informacion";
                $valor6 = 0;
            } else {
                $respuesta6 = $request->respuesta6;
                $valor6 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta6, $respuesta6, $valor6, 'v');

            $respuesta7 = '';
            $valor7 = '';

            if (!isset($request->respuesta7)) {
                $respuesta7 =  "Sin informacion";
                $valor7 = 0;
            } else {
                $respuesta7 = $request->respuesta7;
                $valor7 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta7, $respuesta7, $valor7, 'v');

            $respuesta8 = '';
            $valor8 = '';

            if (!isset($request->respuesta8)) {
                $respuesta8 =  "Sin informacion";
                $valor8 = 0;
            } else {
                $respuesta8 = $request->respuesta8;
                $valor8 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta8, $respuesta8, $valor8, 'v');

            $respuesta9 = '';
            $valor9 = '';

            if (!isset($request->respuesta9)) {
                $respuesta9 =  "Sin informacion";
                $valor9 = 0;
            } else {
                $respuesta9 = $request->respuesta9;
                $valor9 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta9, $respuesta9, $valor9, 'v');

            $respuesta10 = '';
            $valor10 = '';

            if (!isset($request->respuesta10)) {
                $respuesta10 =  "Sin informacion";
                $valor10 = 0;
            } else {
                $respuesta10 = $request->respuesta10;
                $valor10 = $this->value_vendedor;
            }

            $this->getPuntage($id, $request->pregunta10, $respuesta10, $valor10, 'v');

            $this->sumRespuestas($request->id_referido, 'v');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function registerCredito($descripcion, $request, $pregunta, $valor, $divisa, $valormony)
    {

        Respuesta::create([
            'descripcion_respuesta' =>  $descripcion,
            'valor_respuesta'       =>  $valor,
            'estado_respuesta'      =>  "1",
            'id_pregunta'   =>   $pregunta,
            'tipo_formulario' => $request->tipo_cliente,
            'cedula_referido_detalle'   =>  $request->id_referido,
        ]);



        DB::table('valor_pregunta_credito')->insert([
            'divisa'   =>  $divisa,
            'value'  =>  $valormony,
            'id_respuesta_r' => $pregunta,
            'id_referido_r' => $request->id_referido
        ]);
    }


    public static function registerRange($divisa, $rangoInicial, $rangofinal, $request, $res, $valor, $id_pregunta)
    {

        Respuesta::create([
            'descripcion_respuesta' =>  $res,
            'valor_respuesta'       =>  $valor,
            'estado_respuesta'      =>  "1",
            'id_pregunta'   =>  $id_pregunta,
            'tipo_formulario' => $request->tipo_cliente,
            'cedula_referido_detalle'   =>  $request->id_referido,
        ]);

        $respuesta = Respuesta::all()->last();

        DB::table('addy_detalle_pregunta_respuesta_referido_rango')->insert([
            'divisa'   =>  $divisa,
            'inicial'  =>  $rangoInicial,
            'final' => $rangofinal,
            'id_respuesta_r' => $respuesta->id,
            'id_referido_r' => $request->id_referido
        ]);
    }
    //Helpers

    public static function registerResReferido($request, $res, $valor, $id_pregunta)
    {
        Respuesta::create([
            'descripcion_respuesta' =>  $res,
            'valor_respuesta'       =>  $valor,
            'estado_respuesta'      =>  "1",
            'id_pregunta'   =>  $id_pregunta,
            'tipo_formulario' => $request->tipo_cliente,
            'cedula_referido_detalle'   =>  $request->id_referido,
        ]);
    }

    public static function registerResReferidoVendedor($request, $res, $valor, $id_pregunta)
    {
        AddyRespuestasVendedor::create([
            'descripcion_respuesta' =>  $res,
            'valor_respuesta'       =>  $valor,
            'estado_respuesta'      =>  "1",
            'id_pregunta'   =>  $id_pregunta,
            'tipo_formulario' => $request->tipo_cliente,
            'cedula_referido_detalle'   =>  $request->id_referido,
        ]);
    }

    public static function getPuntageRange($id, $rangoInicial, $rangofinal, $id_pregunta,  $valor_pregunta, $respuesta)
    {
        DB::table('addy_respuestas AS ad')
            ->where('ad.cedula_referido_detalle', '=', $id)
            ->where('ad.id_pregunta',  '=',  $id_pregunta)
            ->update([
                'ad.descripcion_respuesta' => $respuesta,
                'ad.valor_respuesta' => $valor_pregunta
            ]);

        DB::table('addy_detalle_pregunta_respuesta_referido_rango AS ad')
            ->where('ad.id_referido_r', '=', $id)
            ->update([
                'ad.inicial' => $rangoInicial,
                'ad.final' => $rangofinal
            ]);
    }


    public static function getPuntage($id, $id_pregunta, $respuesta1, $valor, $tipo)
    {
        if ($tipo == 'v') {
            $table = 'addy_respuestas_vendedor';
        } else {
            $table = 'addy_respuestas';
        }

        DB::table($table)
            ->where("$table.cedula_referido_detalle", '=', $id)
            ->where("$table.id_pregunta",  '=',  $id_pregunta)
            ->update([
                "$table.descripcion_respuesta" => $respuesta1,
                "$table.valor_respuesta" => $valor
            ]);
    }

    public static function sumRespuestas($id_referido, $tipo)
    {
        $table = 0;

        if ($tipo == 'c') {
            $table = 'addy_respuestas';
        } else {
            $table = 'addy_respuestas_vendedor';
        }

        $suma = DB::table($table)
            ->join('addy_preguntas', "$table.id_pregunta", '=', 'addy_preguntas.id')
            ->where("$table.cedula_referido_detalle", $id_referido)
            ->sum("$table.valor_respuesta");

        if (count(array($suma)) > 0 && count(array($suma)) < 9) {
            DB::table($table)
                ->where('cedula_referido_detalle', $id_referido)
                ->update(['state' => 'en_proceso']);
        } elseif (count(array($suma)) == 9) {
            DB::table($table)
                ->where('cedula_referido_detalle', $id_referido)
                ->update(['state' => 'completado']);
        }

        Userdata::where('id_usuario', $id_referido)->update([
            'porcentaje_perfil' => intval($suma),
        ]);
    }
}
