<?php

namespace App\Http\Controllers\API;

use App\Models\Empresa;
use App\Models\Banners;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Intervention\Image\Facades\Image as Image;
use Cloudinary;

use AWS;

class ApiBannerController extends ApiController
{
    public function store(Request $request)
    {




        $auth = Auth::user();

       // dd($request->all());

        $path='';

        $file = $request->file('files');

        
        $uploadedFileUrl = Cloudinary::upload($file[0]->getRealPath())->getSecurePath();

       # dd($uploadedFileUrl);

       #   $filename = $file[0]->getClientOriginalName();
      #  $filename = str_replace(' ', '', $filename);

      #  $filename=time().$filename;
       # $path = $file[0]->storeAs('paginaweb/'.$auth->id.'/banners', $filename, 'public');

      #  $imagen = Image::make('storage/'.$path);
       ## $imagen->fit(1200, 500);
       # $imagen->save();



        $data = Banners::create([
            "id_user" => $auth->id,
            "titulo" => $request->titulo,
            "descripcion" => $request->descripcion,
            "imagen" => $uploadedFileUrl,
            "enlace" => $request->enlace,
            "posicion" => $request->posicion
        ]);


        if ($data) {
            return $this->sendResponse($data, "Registro exitoso");
        }
    }


    public function upd(Request $request)
    {

        $auth = Auth::user();

        #dd($request->all());

        $path='';

        $banner=Banners::where('id', $request->id)->first();

        if(!is_null($request->file('files'))){

            $file = $request->file('files');

            $uploadedFileUrl = Cloudinary::upload($file[0]->getRealPath())->getSecurePath();


          #  $filename = $file[0]->getClientOriginalName();
          #  $filename = str_replace(' ', '', $filename);
           # $path = $file[0]->storeAs('paginaweb/'.$auth->id.'/banners', $filename, 'public');

          #  $imagen = Image::make('storage/'.$path);
       # $imagen->fit(1200, 500);
       # $imagen->save();

            $banner->update([
                "imagen" => $uploadedFileUrl,
            ]);

        }

        $banner->update([
            "id_user" => $auth->id,
            "titulo" => $request->titulo,
            "descripcion" => $request->descripcion,
            "enlace" => $request->enlace,
            "posicion" => $request->posicion
        ]);

        if ($banner) {
            return $this->sendResponse($banner, "El campose registro exitosamente");
        }
    }


    public function del(Request $request)
    {

        $auth = Auth::user();

        $banner=Banners::where('id', $request->id)->first();

        if(isset($banner->id)){

            $banner->delete();

        }

        return $this->sendResponse($banner, "Registro exitoso");
    }



    public function list(Request $request)
    {

        $auth = Auth::user();
        
        $role = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

        if ($role->slug == "agentes") {
            $user = $auth->id;
        } else {
            $user = $auth->create_by;
        }

        $data["banners"] = Banners::where("id_user", $auth->id)->get();

        return $this->sendResponse($data, "Listado de Banners");
    }


    public function get(Request $request)
    {
        $auth = Auth::user();

        $banner=Banners::where('id', $request->id)->first();
        
        $data["banner"] = $banner;

        return $this->sendResponse($data, "Listado de Banners");
    }


    public function index(Request $request)
    {
        $empresa=Empresa::where('slug', '=', $request->code)->first();
    

        if(isset($empresa->id)){

        $auth = User::where("id", $empresa->agente)->first();


            $data["banners"] = Banners::where('id_user', $auth->id)->get();
            return $this->sendResponse($data, "Listado de banners");

        }else{

            return $this->sendResponse([], "Listado de banners");

        }
        
    }



}
