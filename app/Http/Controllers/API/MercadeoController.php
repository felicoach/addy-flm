<?php

namespace App\Http\Controllers\API;

use App\Models\Mercadeo;
use App\Models\MercadeoAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use function GuzzleHttp\json_decode;

class MercadeoController extends ApiController
{

    public function index($id_user)
    {

        $auth = Auth::user();

        $template = Mercadeo::where("user_id", $auth->id)->get();

        $data['mercadeo'] = $template;

        return $this->sendResponse($data, "templates");
    }

    public function register(Request $request)
    {
        $template = Mercadeo::create([
            "html_json" => $request->html_json,
            "html" => $request->html,
            "user_id" => $request->user_id,
            "titulo" => $request->titulo,

        ]);

        $data['mercadeo'] = $template;

        return $this->sendResponse($data, "templates");
    }

    public function getMercadeo()
    {

        $template = MercadeoAdmin::all();

        $template_json = json_decode(json_encode($template));

        for ($i = 0; $i < count($template_json); $i++) {
            $mercadeo_type = DB::table("mercadeos_type")->where("id", $template_json[$i]->mercadeo_type)->first();
            $template_json[$i]->mercadeo_type = $mercadeo_type;
        }

        $data['mercadeo_admin'] = $template_json;

        return $this->sendResponse($data, "templates");
    }

    public function addMercadeoAdmin(Request $request)
    {
        $image = "";

        if ($request->hasFile('image')) {

            $avatarPath = $request->file('image');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('image')->storeAs('portal_admin/', $avatarName, 'public');
            $image =  $path;
        }
        $template = MercadeoAdmin::create([
            "name" => $request->name,
            "template" => $request->template,
            "mercadeo_type" => $request->mercadeo_type,
            "url" => $image,
        ]);

        $data['mercadeo_admin'] = $template;

        return $this->sendResponse($data, "templates");
    }

    public function editMercadeoAdmin(Request $request)
    {
       // return $request;
        $imagen = $request->image;
        $mercadeo_type = json_decode($request->mercadeo_type);

        if ($request->file('image')) {
            Storage::delete('portal_admin/' . $request->image);

            $avatarPath = $request->file('image');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('image')->storeAs('portal_admin/', $avatarName, 'public');
            $imagen =  $path;
        }

        $template = MercadeoAdmin::where("id", $request->id)->update([
            "name" => $request->name,
            "template" => $request->template,
            "url" => $imagen,
            "mercadeo_type" => $mercadeo_type->id,
        ]);

        $data['mercadeo_admin'] = $template;

        return $this->sendResponse($data, "templates");
    }

    public function getMercadeoType()
    {

        $data["mercadeo_type"] = DB::table("mercadeos_type")->get();

        return $this->sendResponse($data, "listado de typo de mercadeos");
    }
}
