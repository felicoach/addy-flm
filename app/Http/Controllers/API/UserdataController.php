<?php

namespace App\Http\Controllers\API;

use App\Models\Country;
use App\Models\Empresa;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\Userdata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserdataController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $data["users"] = User::with("userdata")->where("create_by", $user->id)->get();

        return $this->sendResponse($data, "Listado de usuarios");
    }

    public function getReferer($referer_code)
    {

        $referer_by =  User::select('users.*')
            ->where('users.referral_code', $referer_code)
            ->first();

        if ($referer_by != null) {
            $role = $this->getRoles($referer_by->id);

            if ($role->slug == 'administrator' || $role->slug == 'user_empresa' || $role->slug == 'staff') {

                $user = User::select(
                    'users.*',
                    'userdata.tipo_identificacion',
                    'userdata.foto_persona',
                    'userdata.foto_portada_persona',
                    'userdata.cedula_persona',
                    'userdata.primer_nombre',
                    'userdata.id_usuario',
                    'userdata.segundo_nombre',
                    'userdata.primer_apellido',
                    'userdata.segundo_apellido',
                    'userdata.fecha_nacimiento',
                    'userdata.telefono_fijo',
                    'userdata.celular_movil',
                    'userdata.celular_whatsapp',
                    'userdata.direccion_persona',
                    'userdata.id_ciudad',
                    'userdata.id_pais',
                    'userdata.id_estado',
                    'userdata.id_perfil',
                    'userdata.estado_persona',
                    'userdata.descripcion',
                    'userdata.tipo_cliente',
                    'userdata.porcentaje_perfil',
                    'addy_tipo_clientes.slug_tipo',
                    'addy_tipo_clientes.nombre_tipo'
                )

                    ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
                    ->join('addy_tipo_clientes', 'userdata.tipo_cliente', '=', 'addy_tipo_clientes.id')

                    ->get();
            } else {
                $user = User::select(
                    'users.*',
                    'userdata.tipo_identificacion',
                    'userdata.foto_persona',
                    'userdata.foto_portada_persona',
                    'userdata.cedula_persona',
                    'userdata.primer_nombre',
                    'userdata.id_usuario',
                    'userdata.segundo_nombre',
                    'userdata.primer_apellido',
                    'userdata.segundo_apellido',
                    'userdata.fecha_nacimiento',
                    'userdata.telefono_fijo',
                    'userdata.celular_movil',
                    'userdata.celular_whatsapp',
                    'userdata.direccion_persona',
                    'userdata.descripcion',
                    'userdata.id_pais',
                    'userdata.id_estado',
                    'userdata.id_ciudad',
                    'userdata.id_perfil',
                    'userdata.estado_persona',
                    'userdata.tipo_cliente',
                    'userdata.porcentaje_perfil',
                    'addy_tipo_clientes.slug_tipo',
                    'addy_tipo_clientes.nombre_tipo'
                )

                    ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
                    ->join('addy_tipo_clientes', 'userdata.tipo_cliente', '=', 'addy_tipo_clientes.id')
                    ->where('users.referred_by', $referer_by->id)
                    ->get();
            }
            $data['referidos'] = $user;

            return $this->sendResponse($data, "referidos recuperados correctamente");
        }

        return $this->sendError("Error", ["El usuario no existe"], 404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'numero_cedula' => 'required|int|unique:userdata,cedula_persona',
            'primer_nombre' => 'required',
            'celular' => 'unique:userdata,celular_movil',
            'whatsApp' => 'required|unique:userdata,celular_whatsapp',
            'correo' => 'required|email|unique:users,email',
            'codigo_pais' => 'required',
            'codigo_estado' => 'required',
            'codigo_ciudad' => 'required',
            'estado_persona'    => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
        }

        $foto = null;

        $user = User::create([
            'username' => $request->numero_cedula,
            'email' => $request->correo,
            'password' => bcrypt($request->numero_cedula),
            'referred_by' => $request->id_usuario,
        ]);

        User::where('id', $user->id)->update([
            'email_verified_at'   => now()
        ]);

        $role = new RoleUser();

        $role->role_id = 4;
        $role->user_id = $user->id;

        $role->save();


        Userdata::create([
            'tipo_identificacion'   => $request->code_tipo_documento,
            'cedula_persona'        => $request->numero_cedula,
            'foto_persona'          => $foto,
            'primer_nombre'         => $request->primer_nombre,
            'segundo_nombre'        => $request->segundo_nombre,
            'primer_apellido'       => $request->primer_apellido,
            'segundo_apellido'      => $request->segundo_apellido,
            'fecha_nacimiento'      => $request->fecha_nacimiento,
            'id_usuario'            => $user->id,
            'telefono_fijo'         => "0",
            'id_perfil' => $user->id,
            'celular_movil'         => $request->celular,
            'celular_whatsapp'      => $request->whatsApp,
            'direccion_persona'     => "No registra",
            'id_pais'               => $request->codigo_pais,
            'id_estado'             => $request->codigo_estado,
            'id_ciudad'             => $request->codigo_ciudad,
            'estado_persona'        => $request->estado_persona,
            'tipo_cliente'          => intval($request->tipo_cliente)
        ]);

        // DB::table('userdata_incompletos')->insert([
        //     'id_referido'   => $userdata->numero_cedula,
        //     'nit_empresa'   => '900534143',
        // ]);

        return response()->json([
            "status" => 200,
            "message" => "Registro exitoso"
        ]);
    }


    public function showData($referral_code)
    {
        $user =  User::select('users.*')
            ->where('users.referral_code', $referral_code)
            ->first();

        $userdata = Userdata::where('id_usuario', $user->id)->first();

        if ($userdata != null) {
            $data['user'] = User::select(
                'users.*',
                'userdata.tipo_identificacion',
                'userdata.foto_persona',
                'userdata.foto_portada_persona',
                'userdata.cedula_persona',
                'userdata.primer_nombre',
                'userdata.id_usuario',
                'userdata.segundo_nombre',
                'userdata.primer_apellido',
                'userdata.segundo_apellido',
                'userdata.fecha_nacimiento',
                'userdata.telefono_fijo',
                'userdata.celular_movil',
                'userdata.celular_whatsapp',
                'userdata.direccion_persona',
                'userdata.id_pais',
                'userdata.descripcion',
                'userdata.genero',
                'userdata.facebook',
                'userdata.instagram',
                'userdata.linkedin',
                'userdata.id_estado',
                'userdata.id_ciudad',
                'userdata.id_perfil',
                'userdata.estado_persona',
                'userdata.tipo_cliente'
            )
                ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
                ->where('users.referral_code', $referral_code)
                ->first();
        }

        $user_json = json_decode(json_encode($data['user']));

        $user_json->roles = [];

        $empresa = Empresa::with(
            "empresaEmails",
            "empresaNumbers",
            "empresaRedes",
            "id_pais",
            "id_estado",
            "id_ciudad"
        )->where("agente", $user_json->id)->first();


        $pais = Country::where("id", $user_json->id_pais)->first();
        $state = DB::table("states")->where("id", $user_json->id_estado)->first();
        $ciudad = DB::table("cities")->where("id", $user_json->id_ciudad)->first();

        $user_json->id_pais = [];
        $user_json->id_estado = [];
        $user_json->id_ciudad = [];


        $rol = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $user_json->id)
            ->first();

        $user_json->id_pais = $pais;
        $user_json->id_estado = $state;
        $user_json->id_ciudad = $ciudad;
        $user_json->roles = $rol;
        $user_json->empresa = $empresa;


        $users["user"] = $user_json;

        return $this->sendResponse($users, "referido recuperado correctamente");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
            'userdata.segundo_nombre',
            'userdata.primer_apellido',
            'userdata.segundo_apellido',
            'userdata.descripcion',
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.id_estado',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente',
            'userdata.porcentaje_perfil',
            'addy_tipo_clientes.slug_tipo',
            'addy_tipo_clientes.nombre_tipo'
        )

            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->join('addy_tipo_clientes', 'userdata.tipo_cliente', '=', 'addy_tipo_clientes.id')
            ->where('users.id', $id)
            ->first();

        if ($user != null) {
            $data['referido'] = $user;

            return $this->sendResponse($data, "referido recuperado correctamente");
        }

        return $this->sendError("Error", ["El usuario no existe"], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $referido = User::where('id', $id)->first();

        if ($referido != null) {

            Userdata::where('id_usuario', $id)->update([
                'tipo_identificacion'   => $request->code_tipo_documento,
                'primer_nombre' => $request->primer_nombre,
                'segundo_nombre'    => $request->segundo_nombre,
                'primer_apellido'   => $request->primer_apellido,
               
                'celular_movil'     =>  $request->celular,
                'celular_whatsapp'  =>  $request->whatsApp,
                'id_pais'               => $request->codigo_pais,
                'id_estado'             => $request->codigo_estado,
                'id_ciudad'             => $request->codigo_ciudad,
            ]);

            $data['referido'] = $referido;

            return $this->sendResponse($data, "referido recuperado correctamente");
        }
        return $this->sendError("Error", ["El usuario no existe"], 404);
    }

    public static function getRoles($id)
    {

        $informacion = DB::table('role_user')
            ->select('role_user.role_id', 'role_user.user_id', 'users.id', 'users.username', 'roles.id', 'roles.nombre', 'roles.slug')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where('user_id', $id)
            ->first();

        return $informacion;
    }

    public function countReferidos($id)
    {

        $informacion = DB::table('role_user')
            ->select('role_user.role_id', 'role_user.user_id', 'users.id', 'users.username', 'roles.id', 'roles.nombre', 'roles.slug')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where('user_id', $id)
            ->first();



        if ($informacion->slug == 'administrator') {

            $referidos = DB::table('users')
                ->count();

            $vendedor =  DB::table('userdata')
                ->where('tipo_cliente', '=', '1')
                ->count();

            $comprador = DB::table('userdata')
                ->where('tipo_cliente', '=', '2')
                ->count();


            $consul =  DB::table('addy_detalle_pregunta_respuesta_referido_rango AS ad')
                ->sum('ad.final');


            $mony = $consul * 0.005;
        } else {

            $persona = User::where('id', $id)->first();

            $referidos  = DB::table('users')
                ->selectRaw('COUNT(users.id) as cantidad_referidos')
                ->where('users.referred_by', '=', $persona->id)
                ->count();

            $vendedor =  DB::table('users')
                ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
                ->where('users.referred_by', '=', $persona->id)
                ->where('userdata.tipo_cliente', '=', '1')
                ->count();

            $comprador = DB::table('users')
                ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
                ->where('users.referred_by', '=', $persona->id)
                ->where('userdata.tipo_cliente', '=', '2')
                ->count();

            $consul =  DB::table('addy_detalle_pregunta_respuesta_referido_rango AS ad')
                ->join('users AS ap', 'ap.id', '=', 'ad.id_referido_r')
                ->where('ap.referred_by', '=', $persona->id)
                ->sum('ad.final');


            $mony = $consul * 0.005;
        }

        return response()->json([
            "success" => true,
            "message" => "info referidos",
            "data" => [
                'cantidad' => $referidos,
                'mony' => $mony,
                'vendedor' => $vendedor,
                'comprador' => $comprador,
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function getUser($cedula)
    {

        $user = User::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
            
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.id_estado',
            'userdata.descripcion',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.genero',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente',
            'userdata.descripcion'
        )
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->where('userdata.cedula_persona', $cedula)
            ->first();

        return $user;
    }

    public static function getUserPrivate($cedula)
    {

        $user = User::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
           
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.id_estado',
            'userdata.descripcion',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.genero',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente',
            'userdata.descripcion'
        )
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->where('userdata.id_usuario', $cedula)
            ->first();

        return $user;
    }



    public function actualizacion_perfil(Request $request)
    {
        // return $request;

        $validator = Validator::make($request->all(), [
            'numero_cedula'     => 'required|unique:userdata,cedula_persona',
            'primer_nombre'     => 'required',
            'celular'           => 'required|unique:userdata,celular_movil',
            'whatsApp'          => 'required|unique:userdata,celular_whatsapp',

        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors()->toJson(), "error_validacion", 400);
            //  return response()->json(['error_validacion' => $validator->errors()->toJson()], 400);
        }

        $foto_persona = null;
        $foto_portada = null;

        if ($request->hasFile('foto_persona')) {
            $avatarPath = $request->file('foto_persona');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('foto_persona')->storeAs('perfil/' . $request->numero_cedula, $avatarName, 'public');
            $foto_persona =  $path;
        }

        if ($request->hasFile('foto_portada')) {

            $avatarPath = $request->file('foto_portada');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('foto_portada')->storeAs('portada/' . $request->numero_cedula, $avatarName, 'public');
            $foto_portada =  $path;
        }

        $find_userdata = Userdata::where('id_usuario', $request->id)->first();

        if ($find_userdata) {

            $array = [
                'cedula_persona'        => $request->numero_cedula,
                'tipo_identificacion'   => $request->code_tipo_documento,
                'foto_persona'          => $foto_persona,
                'foto_portada_persona'  => $foto_portada,
                'primer_nombre'         => $request->primer_nombre,
               
                'fecha_nacimiento'      => $request->fecha_nacimiento,
                'telefono_fijo'         => $request->telefono_fijo,
                'celular_movil'         => $request->celular,
                'celular_whatsapp'      => $request->whatsApp,
                'direccion_persona'     => "No registra",
                'id_pais'               => $request->codigo_pais,
                'id_estado'             => $request->codigo_estado,
                'id_ciudad'             => $request->codigo_ciudad,
                'id_perfil'             => "2",
                'id_usuario'            => $request->id, //Crypt::decrypt($request->id),
                'estado_persona'        => $request->estado_persona,
                'color_fondo'        => $request->color_fondo,
                'color_fuente'        => $request->color_fuente,
                'tipo_cliente'          => 1
            ];

            Userdata::where('cedula_persona', $request->numero_cedula)->update($array);
        } else {
            Userdata::create([
                'cedula_persona'        => $request->numero_cedula,
                'tipo_identificacion'   => $request->code_tipo_documento,
                'foto_persona'          => $foto_persona,
                'foto_portada_persona'  => $foto_portada,
                'primer_nombre'         => $request->primer_nombre,
               
                'fecha_nacimiento'      => $request->fecha_nacimiento,
                'telefono_fijo'         => $request->telefono_fijo,
                'celular_movil'         => $request->celular,
                'celular_whatsapp'      => $request->whatsApp,
                'direccion_persona'     => "No registra",
                'id_pais'               => $request->codigo_pais,
                'id_estado'             => $request->codigo_estado,
                'id_ciudad'             => $request->codigo_ciudad,
                'id_perfil'             => "2",
                'id_usuario'            => $request->id, //Crypt::decrypt($request->id),
                'estado_persona'        => $request->estado_persona,
                'color_fondo'        => $request->color_fondo,
                'color_fuente'        => $request->color_fuente,
                'tipo_cliente'          => 1
            ]);
        }


        if ($request->tipo_cliente == 'agentes') {
            DB::table('role_user')->where('user_id', '=',  $request->id)->update([
                'role_id'   =>  '2'
            ]);

            //return response()->json(['informacion' => 'Actualización exitosa.', 'code' => 200]);
        } else if ($request->tipo_cliente == 'socio_referidor') {



            DB::table('role_user')->where('user_id', '=',  $request->id)->update([
                'role_id'   =>  '3'
            ]);
        } else if ($request->tipo_cliente == 'visitantes') {


            DB::table('role_user')->where('user_id', '=', $request->id)->update([
                'role_id'   =>  '4'
            ]);
        } else {
            return response()->json(['error_validacion' => "Debe elegir un tipo de cliente", 'code' => 400]);
        }

        $users = $this->getUserPrivate($request->id);

        return $this->sesion($users);
    }


    public function actualizar_informacion_perfil(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'numero_cedula'     => 'required',
            'primer_nombre'     => 'required',
            'celular'           => 'required',
            'whatsApp'          => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->all(), 'code' => 400]);
        }

        $users = $this->getUser($request->numero_cedula);

        $foto_persona = '';
        $foto_portada = '';

        if ($request->hasFile('foto_persona')) {

            Storage::delete('public/' . $users->foto_persona);

            $avatarPath = $request->file('foto_persona');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('foto_persona')->storeAs('perfil/' . $request->numero_cedula, $avatarName, 'public');
            $foto_persona = $path;
        }

        if ($request->hasFile('foto_portada')) {
            Storage::delete('public/' . $users->foto_portada);

            $avatarPath = $request->file('foto_portada');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('foto_portada')->storeAs('portada/' . $request->numero_cedula, $avatarName, 'public');
            $foto_portada =  $path;
        }

        $array = [
            'tipo_identificacion'   => $request->code_tipo_documento,
            'primer_nombre' => $request->primer_nombre,
            'foto_persona' => $foto_persona,
            'foto_portada_persona' => $foto_portada,
           
            'celular_movil'     =>  $request->celular,
            'celular_whatsapp'  =>  $request->whatsApp,
            'id_pais'               => $request->codigo_pais,
            'id_estado'             => $request->codigo_estado,
            'id_ciudad'             => $request->codigo_ciudad,
        ];

        if ($foto_persona == "") {
            unset($array['foto_persona']);
        }
        if ($foto_portada == "") {
            unset($array['foto_portada_persona']);
        }

        Userdata::where('cedula_persona', $request->numero_cedula)->update($array);

        $users = $this->getUser($request->numero_cedula);

        return $this->sesion($users);
    }

    public function actualizar_informacion_perfil_agente(Request $request)
    {
        //return $request;
        $validator = Validator::make($request->all(), [
            'numero_cedula'     => 'required',
            'primer_nombre'     => 'required',
            'celular'           => 'required',
            'whatsapp'          => 'required',
        ]);

        if ($validator->fails()) {
         //   return $this->sendError("", $validator->errors()->all(), 400);
            return response()->json(['message' => $validator->errors()->all()], 400);
        }

        $users = $this->getUserPrivate($request->id);

        $foto_persona = '';
        $foto_portada_persona = '';

        $social = json_decode($request->social);

        if ($request->hasFile('foto_persona')) {
            $word = "https";
            // Userdata::where('cedula_persona', $request->numero_cedula)->update(['foto_persona' => ""]);
            if (!filter_var($users->foto_persona, FILTER_VALIDATE_URL)) {

                Storage::delete('public/' . $users->foto_persona);
            }

            $avatarPath = $request->file('foto_persona');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('foto_persona')->storeAs('perfil/' . $users->id, $avatarName, 'public');
            $foto_persona = $path;
        }

        if ($request->hasFile('foto_portada_persona')) {

            Storage::delete('public/' . $users->foto_portada_persona);


            $avatarPath = $request->file('foto_portada_persona');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('foto_portada_persona')->storeAs('portada/' . $users->id, $avatarName, 'public');
            $foto_portada_persona =  $path;
        }

        $array = [
            'tipo_identificacion'   => $request->code_tipo_documento,
            'primer_nombre' => $request->primer_nombre,
            'foto_persona' => $foto_persona,
            'foto_portada_persona' => $foto_portada_persona,
          
            'celular_movil'     =>  str_replace(" ", "", $request->celular),
            'celular_whatsapp'  =>  str_replace(" ", "", $request->whatsapp),
            'descripcion' => $request->descripcion,
            'direccion_persona' => $request->direccion_persona,
            'genero' => $request->genero,
            'id_pais' => $request->codigo_pais == 'null' ? null : $request->codigo_pais,
            'id_estado' => $request->codigo_estado == 'null' ? null : $request->codigo_estado,
            'facebook' => $social->facebook,
            'instagram' => $social->instagram,
            'linkedin' => $social->linkedin,
            'id_ciudad' => $request->codigo_ciudad == 'null' ? null : $request->codigo_ciudad,
        ];

        if ($foto_persona == "") {
            unset($array['foto_persona']);
        }
        if ($foto_portada_persona == "") {
            unset($array['foto_portada_persona']);
        }

        Userdata::where('id_usuario', $request->id)->update($array);

        $users = $this->getUserPrivate($request->id);

        return $this->sesion($users);
    }

    public function getUserForCodeView($referral_code)
    {
        $user =  User::with('userdata')->where('referral_code', $referral_code)->first();

        $description = Str::limit(strip_tags($user->userdata->descripcion), 200);

        $data['tags'] = [
            'og:app_id' => '4549589748545',
            'og:image' =>  env('APP_URL') . Storage::url($user->userdata['foto_persona']),
            'og:description' => $description,
            'description' => $description,
            'og:title' => $user->userdata->primer_nombre . ' ' . $user->userdata->primer_apellido,
            'og:url' => env('APP_URL') . 'user/' . $user->referral_code,

        ];


        return view('application', compact('data'));
    }



    public static function sesion($users)
    {

        $rol = DB::table('role_user')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $users->id)
            ->first();

        $users['role'] = $rol;

        if ($rol->slug == 'administrator') {


            $permisos = self::permisos($rol);

            $users['ability'] = $permisos;
        } else {

            $permisos = self::permisos($rol);

            $users['ability'] = $permisos;
        }


        return response()->json([
            'userData' => $users,
        ]);
    }

    public static function permisos($rol = null)
    {
        $permisos  = DB::table('roles')
            ->select(
                'permissions.subject',
                'actions.action'
            )
            ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
            ->join('active_permission_actions', 'role_permissions.id', '=', 'active_permission_actions.role_permission')
            ->join('permissions', 'role_permissions.permission', '=', 'permissions.id')
            ->join('actions', 'active_permission_actions.action', '=', 'actions.id')

            ->where('roles.slug', $rol->slug)
            ->get();

        return $permisos;
    }


    public function validacion(Request $request)
    {

        $validator_cedula = Validator::make($request->all(), [
            'numero_cedula' => 'required|unique:userdata,cedula_persona',
        ]);

        if ($validator_cedula->fails()) {
            return response()->json([
                'message' => 'El numero de cedula ya se encuentra registrado', 'status' => 400
            ], 400);
        }


        $validator_celular = Validator::make($request->all(), [
            'celular' => 'required|unique:userdata,celular_movil',
        ]);

        if ($validator_celular->fails()) {
            return response()->json([
                'message' => 'El numero de celular ya se encuentra registrado', 'status' => 400
            ], 400);
        }



        $validator_whatsapp = Validator::make($request->all(), [
            'whatsApp' => 'required|unique:userdata,celular_whatsapp',
        ]);

        if ($validator_whatsapp->fails()) {
            return response()->json([
                'message' => 'El numero de whatsapp ya se encuentra registrado', 'status' => 400
            ], 400);
        }



        $validator_correo = Validator::make($request->all(), [
            'correo' => 'required|email|unique:users,email',
        ]);

        if ($validator_correo->fails()) {
            return response()->json([
                'message' => 'El correo electronico ya se encuentra registrado', 'status' => 400
            ], 400);
        }
    }
}
