<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Action;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermisosController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsers()
    {
        $data['users'] = User::all();

        return $this->sendResponse($data, 'Listado de usuarios');
    }

    public function index()
    {
        $data["permissions"] = DB::table('roles')
            ->select(
                'permissions.subject',
                'actions.action'
            )
            ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
            ->join('role_permission_actions', 'role_permissions.id', '=', 'role_permission_actions.role_permission')
            ->join('permissions', 'role_permissions.permission', '=', 'permissions.id')
            ->join('actions', 'role_permission_actions.action', '=', 'actions.id')
            ->get();


        return $this->sendResponse($data, "Listado de permisos");
    }

  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
