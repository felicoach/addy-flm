<?php

namespace App\Http\Controllers\API;


use App\Models\Empresa;
use App\Models\Credenciales;
use App\Models\UserTemplate;
use App\Models\Template;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use function GuzzleHttp\json_decode;
use Carbon\Carbon;
use Cloudinary;
use AWS;

class EmpresaController extends ApiController
{

    public function getEmpresas()
    {
        $data["empresas"] = Empresa::with(
            "empresaEmails",
            "empresaNumbers",
            "empresaRedes",
            "id_pais",
            "id_estado",
            "id_ciudad",
        )->get();


            

        return $this->sendResponse($data, "Listado de empresas");
    }


    public function getEmpresasForId()
    {
        $auth = Auth::user();

        $c=Credenciales::where('id', '1')->first();

        $user = getUsersRoles($auth);

        $empresa = Empresa::with(
            "empresaEmails",
            "empresaNumbers",
            "empresaRedes",
            "id_pais",
            "id_estado",
            "id_ciudad"
        )->where("agente", $user)->first();


       # $data["empresa"]->dominio=$c->dominio;
        $data["dominio"]=$c->dominio;

        if(isset($empresa->id)){

            $date = Carbon::createFromTimestampUTC($empresa->updated_at); 

            $hoy=$date->format('Y-m-d h:m:s');

            $now = Carbon::now();

            $now2=$now->format('Y-m-d h:m:s');

            $empresa->hoy=$hoy;
            $empresa->now2=$now2;
            $empresa->u=$empresa->updated_at;


            $diff = $now->diffInHours($date); 
            $empresa->diff=$diff;


            if ($diff>30) {

                $empresa->sitioactivo=1;

            }else{

                $empresa->sitioactivo=$diff;

            }

        }

       
        $data['empresa']=$empresa;


        
        

        return $this->sendResponse($data, "Listado de empresa");
    }

    public function addEmpresas(Request $request)
    {
        $auth = Auth::user();

        $pais = json_decode($request->id_pais);
        $state = json_decode($request->id_estado);
        $city = json_decode($request->id_ciudad);

        $user = getUsersRoles($auth);

        if ($request->id != 'null') {

            $logo = $request->logo;
            // return $request;
            if ($request->hasFile('logo')) {

                $uploadedFileUrl = Cloudinary::upload($request->file('logo')->getRealPath())->getSecurePath();

                $logo =  $uploadedFileUrl;

            }

            $e=Empresa::where("id", $request->id)->first();

            if($request->slug==$e->slug){

            }else{

                $emp=Empresa::where("slug", $request->slug)->first();

                if(isset($emp->id)){

                    return $this->sendResponse([], "Ya Existe una empresa con ese subdominio de pagina");

                }else{

                    $res=$this->crearSubdomain($request->slug, $auth);

                    if(isset($res->status)){

                        if($res->status=='1'){

                            $e->update(['id_domain'=>$res->status]);


                        }else{

                            if(isset($res->errors[0])){

                                return $this->sendResponse([], 'La Url de la empresa ya existe');

                            }else{

                                return $this->sendResponse([], "Error al crear el subdominio por favor intente nuevamente");


                            }

                    ## dd();


                            

                        }


                    }else{

                        return $this->sendResponse([], "Error al crear el subdominio por favor intente nuevamente");
                    }


                    

                }
                

            }

            $data = Empresa::where("id", $request->id)->update([
                "agente" => $user,
                'id_pais' => $pais == null ? null :  $pais->id,
                'id_estado' =>  $state == null ? null :  $state->id,
                'id_ciudad' => $city == null ? null : $city->id,
                "nit" => $request->nit,
                "dv" => $request->dv,
                "logo" => $logo,
                "nombre" => $request->nombre,
                "slug" => $request->slug,
                "email" => $request->email,
                "celular" => $request->celular,
                "telefono" => $request->telefono,
                "pagina_web" => $request->pagina_web,
                "descripcion" => $request->descripcion,
                'latitud' => $request->latitud,
                'longitud' => $request->longitud,
                'direccion' => $request->direccion,
                "instagram" => $request->instagram,
                "facebook" => $request->facebook,
                "linkedin" => $request->linkedin,
                "color_primario" => $request->color_primario,
                "color_secundario" => $request->color_secundario,
                "mision" => $request->mision,
                "vision" => $request->vision,
                "font_color" => $request->font_color,
                "complete" => '1',
            ]);

            

            return $this->sendResponse($data, "Se actualizo la empresa");


        } else {
            // return $request;
            $logo = null;

            if ($request->hasFile('logo')) {

                $uploadedFileUrl = Cloudinary::upload($request->file('logo')->getRealPath())->getSecurePath();

                $logo =  $uploadedFileUrl;
            }

            $e=Empresa::where("slug", $request->slug)->first();

            if(isset($e->id)){

                return $this->sendResponse([], "Ya Existe una empresa con ese nombre");

            }else{


                $idhz=$this->crearSubdomain($request->slug, $auth);

                $data = Empresa::create([
                    "agente" => $user,
                    'id_pais' => $pais == null ? null :  $pais->id,
                    'id_estado' =>  $state == null ? null :  $state->id,
                    'id_ciudad' => $city == null ? null : $city->id,
                    "razon_social" => $request->razon_social,
                    "nit" => $request->nit,
                    "dv" => $request->dv,
                    "email" => $request->email,
                    "celular" => $request->celular,
                    "logo" => $logo,
                    "nombre" => $request->nombre,
                    "slug" => $request->slug,
                    "pagina_web" => $request->pagina_web,
                    "descripcion" => $request->descripcion,
                    'latitud' => $request->latitud,
                    'longitud' => $request->longitud,
                    'direccion' => $request->direccion,
                    "instagram" => $request->instagram,
                    "facebook" => $request->facebook,
                    "linkedin" => $request->linkedin,
                    "color_primario" => $request->color_primario,
                    "color_secundario" => $request->color_secundario,
                    "mision" => $request->mision,
                    "vision" => $request->vision,
                    "font_color" => $request->font_color,
                    'id_domain'=>$idhz,
                    "complete"=>1
                ]);
    
                return $this->sendResponse($data, "Registro de empresa");


            }


           
        }
    }

    public function registerNumero($agente, $numbers = [])
    {
        $data["number"] = null;
        if (is_array($numbers)) {
            if (count($numbers) > 0) {
                for ($i = 0; $i < count($numbers); $i++) {
                    if ($numbers[$i]->numero != "") {
                        $exit_number = DB::table("empresa_agente_numbers")->where("number", $numbers[$i]->numero)->first();
                        if (!$exit_number) {
                            $data["number"] = DB::table("empresa_agente_numbers")
                                ->insert([
                                    "agente_empresa" => $agente,
                                    "number" => $numbers[$i]->numero,
                                    "operathor" => "1"
                                ]);
                        }
                    }
                }
            }
        }


        return $this->sendResponse($data, "Listado de numeros");
    }


    public function getNumero($code)
    {
        $data["numbers"] = DB::table("empresa_agente_numbers")
            ->where("empresa_agente", $code)
            ->get();

        return $this->sendResponse($data, "Listado de numeros");
    }


    public function registerEmails($agente, $emails = [])
    {
        $data["email"] = null;
        if (is_array($emails)) {
            if (count($emails) > 0) {
                for ($i = 0; $i < count($emails); $i++) {
                    if ($emails[$i]->correo != "") {

                        $exit_emails = DB::table("empresa_agente_emails")->where("email", $emails[$i]->correo)->first();

                        if (!$exit_emails) {

                            $data["email"] = DB::table("empresa_agente_emails")
                                ->insert([
                                    "agente_empresa" => $agente,
                                    "email" => $emails[$i]->correo,
                                ]);
                        }
                    }
                }
            }
        }

        return $this->sendResponse($data, "Registro de emails");
    }


    public function getEmails()
    {

        $data["emails"] = DB::table("empresa_agente_emails")->get();

        return $this->sendResponse($data, "Listado de emails");
    }


    public function registerSocial(Request $request)
    {
        $data["redes"] = DB::table("empresa_agente_redes")
            ->insert([
                "agente_empresa" => $request->agente_empresa,
                "name" => $request->name,
                "url" => $request->url
            ]);
        return $this->sendResponse($data, "Listado de redes");
    }


    public function getRedes()
    {
        $data["redes"] = DB::table("empresa_agente_redes")->get();

        return $this->sendResponse($data, "Listado de redes");
    }


    public function reloadSubdomain(){

            $empresas=Empresa::get();

            $res='';

            foreach($empresas as $e){

                $emp=Empresa::where('id', $e->id)->first();

                if(!is_null($e->nombre)){


                        $slug = \Str::slug($e->nombre);

                        $idhz=$this->crearSubdomainReload($slug);

                        $res=$res.$slug.' / '.$idhz.' / ';

                        $emp->update([
                            'slug'=>$slug,
                            'id_domain'=>$idhz
                        ]);

                        //dd($emp);
    

                }

            }

            return $res;


    }


    public function crearSubdomainAmazon($slug, $auth){

        $c=Credenciales::where('id', '1')->first();

        $template=UserTemplate::select('templates.url')
        ->join('templates', 'user_template.id_template', '=', 'templates.id')
        ->where('user_template.id_user', $auth->id)->first();

        $t=Template::first();

        if(isset($template->url)){

        }else{

            UserTemplate::create([
                'id_template'=>$t->id,
                'id_user'=>$auth->id,
            ]);

            $template=UserTemplate::select('templates.url')
            ->join('templates', 'user_template.id_template', '=', 'templates.id')
            ->where('user_template.id_user', $auth->id)->first();

         #   return 'No posee template seleccionado';
        }

       # dd($template);

        $route = AWS::createClient('route53', [
          'version'     => 'latest',
          'region'      => $c->aws_region,
          'credentials' => [
              'key'    => $c->aws_access_key,
              'secret' => $c->aws_key_id,
          ],
      ]);
    
        $nhz=$slug.'.'.$c->dominio;
        $nhzp=$slug.'.'.$c->dominio.'.';
        $ohz=$c->dominio;
        $hzp=$c->dominio.'.';

        $hz=$route->listHostedZones([]);
        
        $HostedZones = $hz->get('HostedZones');

       # dd($HostedZones);

        $ban=0;

        foreach($HostedZones as $hz){
            
            if($hz['Name']==$nhz.'.'){
               # dd($hz['Id']);
                $idhz=$hz['Id'];
                $ban=1;
            }
    
            if($hz['Name']==$ohz.'.'){
                $idhzp=$hz['Id'];
            }
    
        }

      #  dd($ban.' / '.$nhz);

        if($ban==0){

                //zona no existe 

                $result = $route->createHostedZone(array(
                    // Name is required
                    'Name' => $nhz,
                    // CallerReference is required
                    'CallerReference' => $nhz,
                    'HostedZoneConfig' => array(
                        'Comment' => 'zona para '.$nhz,
                        'PrivateZone' => false,
                    )
                ));

                $hz=$route->listHostedZones([]);
        
                $HostedZones = $hz->get('HostedZones');
            
                $idhz='0';
                $idhzp='0';

                # dd($HostedZones);
                foreach($HostedZones as $hz){
                    
                    if($hz['Name']==$nhz.'.'){
                    # dd($hz['Id']);
                        $idhz=$hz['Id'];
                    }
            
                    if($hz['Name']==$ohz.'.'){
                        $idhzp=$hz['Id'];
                    }
            
                }
        
        # dd($idhz);
        
                if($idhz=='0'){

                    dd('Nose encontro zona ');
            
                }else{
            
                    $result = $route->changeResourceRecordSets(array(
                        // HostedZoneId is required
                        'HostedZoneId' => $idhz,
                        // ChangeBatch is required
                        'ChangeBatch' => array(
                            'Comment' => 'creacion de registro A para '.$nhz,
                            // Changes is required
                            'Changes' => array(
                                array(
                                    // Action is required
                                    'Action' => 'CREATE',
                                    // ResourceRecordSet is required
                                    'ResourceRecordSet' => array(
                                        // Name is required
                                        'Name' => $nhz,
                                        // Type is required
                                        'Type' => 'A',
                                    
                                        'TTL' => 300,
                                        'ResourceRecords' => array(
                                            array(
                                                // Value is required
                                                'Value' => $template->url,
                                            ),
                                            // ... repeated
                                        )
                                    ),
                                ),
                                // ... repeated
                            ),
                        ),
                    ));
            
                    $result = $route->listResourceRecordSets(array(
                        // HostedZoneId is required
                        'HostedZoneId' => $idhz,
                    ));
            
                    $ResourceRecordSets=$result->get('ResourceRecordSets');
            
                    $rrhz='0';
            
                    foreach($ResourceRecordSets as $rrs){
            
                        if($rrs['Type']=='NS'){
                
                            //dd($rrs['ResourceRecords']);
                
                            $rrhz=$rrs['ResourceRecords'];
                        }
                
                    }
            
                    if($rrhz=='0'){
                        dd('No se encontraron registros de NS');
                    }else{
            
                    # dd($idhzp);
            
                        $result = $route->changeResourceRecordSets(array(
                            // HostedZoneId is required
                            'HostedZoneId' => $idhzp,
                            // ChangeBatch is required
                            'ChangeBatch' => array(
                                'Comment' => 'creacion de registro NS ',
                                // Changes is required
                                'Changes' => array(
                                    array(
                                        // Action is required
                                        'Action' => 'CREATE',
                                        // ResourceRecordSet is required
                                        'ResourceRecordSet' => array(
                                            // Name is required
                                            'Name' => $nhz,
                                            // Type is required
                                            'Type' => 'NS',
                                        
                                            'TTL' => 300,
                                            'ResourceRecords' => $rrhz
                                                // ... repeated
                                        ),
                                    ),
                                    // ... repeated
                                ),
                            ),
                        ));
                    }
                }
            
                $ResourceRecordSets = $result->get('ResourceRecordSets');

                return $idhz;

        }else{

            //ya Existe 

            return $idhz;
        }
    
        
       
        
      
    
    }

    public function crearSubdomainReloadAmazon($slug){

        $c=Credenciales::where('id', '1')->first();

        $empresa=Empresa::where('slug', $slug)->first();

        if(isset($empresa->id)){

        }else{
            return false;
        }

        $template=UserTemplate::select('templates.url')
        ->join('templates', 'user_template.id_template', '=', 'templates.id')
        ->where('user_template.id_user', $empresa->agente)->first();

        $t=Template::first();

        if(isset($template->url)){

        }else{

            UserTemplate::create([
                'id_template'=>$t->id,
                'id_user'=>$empresa->agente,
            ]);

            $template=UserTemplate::select('templates.url')
            ->join('templates', 'user_template.id_template', '=', 'templates.id')
            ->where('user_template.id_user', $empresa->agente)->first();

         #   return 'No posee template seleccionado';
        }



        

        $route = AWS::createClient('route53', [
            'version'     => 'latest',
            'region'      => $c->aws_region,
            'credentials' => [
                'key'    => $c->aws_access_key,
                'secret' => $c->aws_key_id,
            ],
        ]);
    
        $nhz=$slug.'.'.$c->dominio;
        $nhzp=$slug.'.'.$c->dominio.'.';
        $ohz=$c->dominio;
        $hzp=$c->dominio.'.';

        $hz=$route->listHostedZones([]);
        
        $HostedZones = $hz->get('HostedZones');

       # dd($HostedZones);

        $ban=0;

        foreach($HostedZones as $hz){
            
            if($hz['Name']==$nhz.'.'){
               # dd($hz['Id']);
                $idhz=$hz['Id'];
                $ban=1;
            }
    
            if($hz['Name']==$ohz.'.'){
                $idhzp=$hz['Id'];
            }
    
        }

      #  dd($ban.' / '.$nhz);

        if($ban==0){

                //zona no existe 

                $result = $route->createHostedZone(array(
                    // Name is required
                    'Name' => $nhz,
                    // CallerReference is required
                    'CallerReference' => $nhz,
                    'HostedZoneConfig' => array(
                        'Comment' => 'zona para '.$nhz,
                        'PrivateZone' => false,
                    )
                ));

                $hz=$route->listHostedZones([]);
        
                $HostedZones = $hz->get('HostedZones');
            
                $idhz='0';
                $idhzp='0';

                # dd($HostedZones);
                foreach($HostedZones as $hz){
                    
                    if($hz['Name']==$nhz.'.'){
                    # dd($hz['Id']);
                        $idhz=$hz['Id'];
                    }
            
                    if($hz['Name']==$ohz.'.'){
                        $idhzp=$hz['Id'];
                    }
            
                }
        
        # dd($idhz);
        
                if($idhz=='0'){

                    dd('Nose encontro zona ');
            
                }else{
            
                    $result = $route->changeResourceRecordSets(array(
                        // HostedZoneId is required
                        'HostedZoneId' => $idhz,
                        // ChangeBatch is required
                        'ChangeBatch' => array(
                            'Comment' => 'creacion de registro A para '.$nhz,
                            // Changes is required
                            'Changes' => array(
                                array(
                                    // Action is required
                                    'Action' => 'CREATE',
                                    // ResourceRecordSet is required
                                    'ResourceRecordSet' => array(
                                        // Name is required
                                        'Name' => $nhz,
                                        // Type is required
                                        'Type' => 'A',
                                    
                                        'TTL' => 300,
                                        'ResourceRecords' => array(
                                            array(
                                                // Value is required
                                                'Value' => $template->url,
                                            ),
                                            // ... repeated
                                        )
                                    ),
                                ),
                                // ... repeated
                            ),
                        ),
                    ));
            
                    $result = $route->listResourceRecordSets(array(
                        // HostedZoneId is required
                        'HostedZoneId' => $idhz,
                    ));
            
                    $ResourceRecordSets=$result->get('ResourceRecordSets');
            
                    $rrhz='0';
            
                    foreach($ResourceRecordSets as $rrs){
            
                        if($rrs['Type']=='NS'){
                
                            //dd($rrs['ResourceRecords']);
                
                            $rrhz=$rrs['ResourceRecords'];
                        }
                
                    }
            
                    if($rrhz=='0'){
                        dd('No se encontraron registros de NS');
                    }else{
            
                    # dd($idhzp);
            
                        $result = $route->changeResourceRecordSets(array(
                            // HostedZoneId is required
                            'HostedZoneId' => $idhzp,
                            // ChangeBatch is required
                            'ChangeBatch' => array(
                                'Comment' => 'creacion de registro NS ',
                                // Changes is required
                                'Changes' => array(
                                    array(
                                        // Action is required
                                        'Action' => 'CREATE',
                                        // ResourceRecordSet is required
                                        'ResourceRecordSet' => array(
                                            // Name is required
                                            'Name' => $nhz,
                                            // Type is required
                                            'Type' => 'NS',
                                        
                                            'TTL' => 300,
                                            'ResourceRecords' => $rrhz
                                                // ... repeated
                                        ),
                                    ),
                                    // ... repeated
                                ),
                            ),
                        ));
                    }
                }
            
                $ResourceRecordSets = $result->get('ResourceRecordSets');

                return $idhz;

        }else{

            //ya Existe 

            return $idhz;
        }
    
        
       
        
      
    
    }



    public function sendemail(){


     /*   $SesClient = new SesClient([
            'profile' => 'default',
            'version' => '2010-12-01',
            'region'  => 'us-west-2'
        ]);*/

        $c=Credenciales::where('id', '1')->first();
        


        $SesClient = AWS::createClient('ses', [
            'version'     => 'latest',
            'region'      => $c->aws_region,
            'credentials' => [
                'key'    => $c->aws_access_key,
                'secret' => $c->aws_key_id,
            ],
        ]);

        $sender_email = 'noresponder@crmaddy.online';
        
        $recipient_emails = ['miguelmachadoaa@gmail.com'];
        
        $subject = 'Amazon SES test (AWS SDK for PHP)';
        $plaintext_body = 'This email was sent with Amazon SES using the AWS SDK for PHP.' ;
        $html_body =  '<h1>AWS Amazon Simple Email Service Test Email</h1>'.
                      '<p>This email was sent with <a href="https://aws.amazon.com/ses/">'.
                      'Amazon SES</a> using the <a href="https://aws.amazon.com/sdk-for-php/">'.
                      'AWS SDK for PHP</a>.</p>';
        $char_set = 'UTF-8';
        
        try {
            $result = $SesClient->sendEmail([
                'Destination' => [
                    'ToAddresses' => $recipient_emails,
                ],
                'ReplyToAddresses' => [$sender_email],
                'Source' => $sender_email,
                'Message' => [
                  'Body' => [
                      'Html' => [
                          'Charset' => $char_set,
                          'Data' => $html_body,
                      ],
                      'Text' => [
                          'Charset' => $char_set,
                          'Data' => $plaintext_body,
                      ],
                  ],
                  'Subject' => [
                      'Charset' => $char_set,
                      'Data' => $subject,
                  ],
                ],
            ]);
            $messageId = $result['MessageId'];

            return $messageId;

        } catch (AwsException $e) {
            // output error message if fails
           # echo $e->getMessage();
          #  echo("The email was not sent. Error message: ".$e->getAwsErrorMessage()."\n");
           # echo "\n";

            return $e->getMessage();
        }




    }



    public function crearSubdomain($slug, $auth){

        if(isset($auth->id)){


            $c=Credenciales::where('id', '1')->first();

            $template=UserTemplate::select('templates.url')
            ->join('templates', 'user_template.id_template', '=', 'templates.id')
            ->where('user_template.id_user', $auth->id)->first();
    
            $t=Template::first();
    
            if(isset($template->url)){
    
            }else{
    
                UserTemplate::create([
                    'id_template'=>$t->id,
                    'id_user'=>$auth->id,
                ]);
    
                $template=UserTemplate::select('templates.url')
                ->join('templates', 'user_template.id_template', '=', 'templates.id')
                ->where('user_template.id_user', $auth->id)->first();
    
             #   return 'No posee template seleccionado';
            }
    
           # dd($template);
    
            
            // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
            $ch = curl_init();
    
           ## dd('https://'.$c->dominio.':2083/execute/SubDomain/addsubdomain?domain='.$slug.'&rootdomain='.$c->dominio.'&dir='.$template->url);
    
            curl_setopt($ch, CURLOPT_URL, 'https://'.$c->dominio.':2083/execute/SubDomain/addsubdomain?domain='.$slug.'&rootdomain='.$c->dominio.'&dir='.$template->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    
    
            $headers = array();
            $headers[] = 'Authorization: cpanel '.$c->cpanel_user.':'.$c->cpanel_token.'';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
            $result = curl_exec($ch);
    

            $res=json_decode($result);


           // dd($res->status);
           ## dd($res->errors[0]);

            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
    
            return $res;

        }else{

            return 0;
        }

        
    
    }

    
    public function crearSubdomainReload($slug){

        $c=Credenciales::where('id', '1')->first();

        $empresa=Empresa::where('slug', $slug)->first();

        if(isset($empresa->id) || !is_null($empresa->agente)){

        $template=UserTemplate::select('templates.url')
        ->join('templates', 'user_template.id_template', '=', 'templates.id')
        ->where('user_template.id_user', $empresa->agente)->first();


        $t=Template::first();

        if(isset($template->url)){

        }else{

            if(is_null($empresa->agente)){

                return 0;

            }else{

                UserTemplate::create([
                    'id_template'=>$t->id,
                    'id_user'=>$empresa->agente,
                ]);

            }

           

            $template=UserTemplate::select('templates.url')
            ->join('templates', 'user_template.id_template', '=', 'templates.id')
            ->where('user_template.id_user', $empresa->agente)->first();

         #   return 'No posee template seleccionado';
        }


        $ch = curl_init();

       ## dd('https://'.$c->dominio.':2083/execute/SubDomain/addsubdomain?domain='.$slug.'&rootdomain='.$c->dominio.'&dir='.$template->url);

        curl_setopt($ch, CURLOPT_URL, 'https://'.$c->dominio.':2083/execute/SubDomain/addsubdomain?domain='.$slug.'&rootdomain='.$c->dominio.'&dir='.$template->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers = array();
        $headers[] = 'Authorization: cpanel '.$c->cpanel_user.':'.$c->cpanel_token.'';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

       // dd($result);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

       // $empresa->update(['id_dominio'=>json_encode($result)]);

       return 1;
        


        }else{

            return null;
        }

        
       
        
      
    
    }

      


}
