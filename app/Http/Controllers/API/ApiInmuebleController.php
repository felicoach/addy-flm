<?php

namespace App\Http\Controllers\API;

use id;

use App\Models\InmueblesViews;
use App\Models\City;
use App\Models\Inmueble;
use App\Models\Inmuebles;
use App\Models\TipoNegocio;
use App\Models\TipoInmueble;
use Illuminate\Http\Request;
use App\Models\InmuebleStrato;
use App\Models\EstadoPropiedad;
use App\Models\InmuebleImagenes;
use App\Models\InmuebleImagenesTemporal;
use App\Models\SegmentoMercados;
use App\Models\TipoPeriodoAdmon;
use App\Models\StatePublications;
use App\Models\InmuebleTipoPrecio;
use Illuminate\Support\Facades\DB;
use App\Models\InmuebleExternaMany;
use App\Models\InmuebleInternaMany;
use App\Models\InmueblePropietario;
use App\Models\InmuebleStadoFisico;
use App\Models\InmuebleInternaaMany;
use App\Models\InmuebleTipoParqueadero;
use Illuminate\Support\Facades\Validator;
use App\Models\InmuebleTipoTiempoAlquiler;
use App\Http\Controllers\API\ApiController;
use App\Imports\InmuebleExport;
use App\Models\Empresa;
use App\Models\InmuebleAntiguedad;
use App\Models\InmueblesCaracteristicasExterna;
use App\Models\InmueblesCaracteristicasInternas;
use App\Models\Role;
use App\Models\User;
use App\Models\Userdata;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

use Cloudinary;

use function GuzzleHttp\json_decode;

class ApiInmuebleController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function searchPaginate()
    {
        // $inmuebles = Inmuebles::paginate(1);
        // return response()->json($inmuebles);
    }


    public function getTipoInmueble()
    {

        $tipo_inmuebles = TipoInmueble::all();

        $data_json = json_decode(json_encode($tipo_inmuebles));

        for ($i = 0; $i < count($data_json); $i++) {

            $tI = DB::table('portales_tipo_inmuebles')
                ->where("tI_id", $data_json[$i]->id)
                ->get();

            $data_json[$i]->portales_tipo_inmueble = $tI;
        }


        $data['tipo_inmuebles'] = $data_json;

        return $this->sendResponse($data, "Listado de inmuebles");
        // }

        // return $this->sendError("Error", ["El usuario no existe"], 404);
    }

    public function getTipoPrecio()
    {

        $precio = InmuebleTipoPrecio::all();

        $data['precios'] = $precio;

        return $this->sendResponse($data, "Listado de precios");
        // }

        // return $this->sendError("Error", ["El usuario no existe"], 404);
    }

    public function getAntiguedad()
    {

        $antiguedad = InmuebleAntiguedad::all();

        $data['antiguedad'] = $antiguedad;

        return $this->sendResponse($data, "Listado de antiguedad");
        // }

        // return $this->sendError("Error", ["El usuario no existe"], 404);
    }

    public function getTipoNegocio()
    {

        $tipo_inmuebles = TipoNegocio::all();

        $data_json = json_decode(json_encode($tipo_inmuebles));

        for ($i = 0; $i < count($data_json); $i++) {

            $carac = DB::table('portales_inmuebles_tipo_negocio')
                ->where("tipo_negocio_id", $data_json[$i]->id)
                ->first();

            $data_json[$i]->portales_inmuebles_tipo_negocio = $carac;
        }
        $data['tipo_negocio'] = $data_json;

        return $this->sendResponse($data, "Listado de negocios");
        // }

        // return $this->sendError("Error", ["El usuario no existe"], 404);
    }


    public function getTipoContrato()
    {

        $tipo_inmuebles = EstadoPropiedad::all();

        $data['tipo_negocio'] = $tipo_inmuebles;

        return $this->sendResponse($data, "Listado de negocios");
        // }

        // return $this->sendError("Error", ["El usuario no existe"], 404);
    }

    public function getCaracteristicasInternas()
    {

        $cara = InmueblesCaracteristicasInternas::all();

        $data_json = json_decode(json_encode($cara));

        for ($i = 0; $i < count($data_json); $i++) {

            $carac = DB::table('portales_caracteristicas_intarnas')
                ->where("ctin_id", $data_json[$i]->id)
                ->get();

            $data_json[$i]->portales_caracteristicas_internas = $carac;
        }

        $data['caracteristicas_internas'] = $data_json;;

        return $this->sendResponse($data, "Listado de caracteriticas internas");
    }



    public function getCaracteristicasExternas()
    {

        $cara = InmueblesCaracteristicasExterna::all();
        $data_json = json_decode(json_encode($cara));

        for ($i = 0; $i < count($data_json); $i++) {

            $carac = DB::table('portales_caracteristicas_externas')
                ->where("ctex_id", $data_json[$i]->id)
                ->get();

            $data_json[$i]->portales_caracteristicas_externas = $carac;
        }

        $data['caracteristicas_externas'] = $data_json;

        return $this->sendResponse($data, "Listado de caracteriticas externas");
    }

    public function filterMultiple(Request $request)
    {
        $auth = Auth::user();

        $user = getUsersRoles($auth);

        $filter = Inmuebles::query();

        $filter->with(
            'InmuebleImagenes',
            'userId',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'propietario',
            'parqueadero',
            'segmento'
        );

        $filter->where('user_id', $user);

        if ($request->state_id != null) {
            $filter->where('estado_id', $request->state_id['id']);
        }

        if ($request->barrio_id != null) {
            $filter->where('barrio_id', $request->barrio_id['id']);
        }
        if ($request->city_id != null) {
            $filter->where('ciudad_id', $request->city_id['id']);
        }

        if ($request->zona_id != null) {
            $filter->where('zona_id', $request->zona_id['id']);
        }

        if ($request->agente) {
            $filter->where('user_id', $request->agente['id']);
        }

        if ($request->tipo_inmueble) {
            $filter->where('tipo_inmueble', $request->tipo_inmueble['id']);
        }

        if ($request->tipo_negocio) {
            $filter->where('tipo_negocio', $request->tipo_negocio['id']);
        }

        if ($request->state_fisico) {
            $filter->where('state_fisico', $request->state_fisico['id']);
        }

        $data['inmuebles']  = $filter->get();

        return $this->sendResponse($data, "Listado de inmuebles");
    }

    public function calculatePorcentage()
    {
        $auth = Auth::user();

        $data = Inmuebles::with("tipoInmueble")
            ->where("user_id", $auth->id)
            ->get();

        return $this->sendResponse($data, "registro de inmuebles");
    }

    public function getInmuebles()
    {

        $c_internas = [];

        $auth = Auth::user();

        $user = getUsersRoles($auth);
        //   return $role;
        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'propietario',
            'barrioId',
            'tipo_negocio',
            'antiguedad',
            'estadoPublicacion',
            'stateFisico',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento',
            'PortalesCodigoResponse',
            'PortaleStateInmueble',
            'PortalesUrls'
        )->where("user_id", $user)
            ->orderBy('id', 'desc')
            ->get();

        $data['inmuebles'] = $inmuebles;

        for ($i = 0; $i < count($data['inmuebles']); $i++) {
            $rangue_price = DB::table('inmueble_range_price')->where('id', $data['inmuebles'][$i]['rangue_price'])->first();

            $c_internas = InmuebleInternaMany::with('caracteristicasInternas')
                ->where('inmueble', $data['inmuebles'][$i]['id'])->get();

            $c_externas = InmuebleExternaMany::with('caracteristicasExternas')
                ->where('inmueble', $data['inmuebles'][$i]['id'])->get();

            $for_id_localidad = DB::table('portales_inmuebles_localidad')->where("inm_id", $data['inmuebles'][$i]['id'])->first();

            $localidad = null;

            if ( $for_id_localidad ) {
                $localidad =  DB::table('ciencuadra_localidades')->where("id", $for_id_localidad->id)->first();
            }
        
            $data['inmuebles'][$i]['caracteristicas_internas'] = $c_internas;
            $data['inmuebles'][$i]['caracteristicas_externas'] = $c_externas;
            $data['inmuebles'][$i]['rangue_price'] = $rangue_price;
            $data['inmuebles'][$i]['localidad'] = $localidad;
        }

        return $this->sendResponse($data, "Listado de inmuebles");
    }
    public function getInmueblesForIdFilter($id, Request $request)
    {
        $user = User::where("referral_code", $id)->first();

        $filter = Inmuebles::query();

        $filter->with(
            'InmuebleImagenes',
            'userId',
            'tipoInmueble',
            'antiguedad',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'propietario',
            'estadoPublicacion',
            'stateFisico',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'PortalesCodigoResponse',
            'PortaleStateInmueble',
            'segmento'
        );

        $filter->where("user_id", $user->id);
        if ($request->search != null) {
            $filter->where('titulo_inmueble', 'LIKE', "%{$request->search}%");
        }
        if ($request->selected_dep != null) {
            $filter->where('estado_id', $request->selected_dep['id']);
        }
        if ($request->selected_ciu != null) {
            $filter->where('ciudad_id', $request->selected_ciu['id']);
        }
        if ($request->selected_tn != null) {
            $filter->where('tipo_negocio', $request->selected_tn['id']);
        }
        if ($request->selected_tinm != null) {
            $filter->where('tipo_inmueble', $request->selected_tinm['id']);
        }

        $data['inmuebles']  = $filter->get();

        return $this->sendResponse($data, "Listado de inmuebles");
    }
    public function getInmueblesForId($id)
    {

        InmueblesViews::create([
            'id_inmueble'=>$id,
            'id_user'=>1,
        ]);

        $data["inmueble"] = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'tipoInmueble',
            'antiguedad',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'propietario',
            'estadoPublicacion',
            'stateFisico',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'PortalesCodigoResponse',
            'PortaleStateInmueble',
            'segmento'
        )->where("id", $id)
            ->first();


        $c_internas = InmuebleInternaMany::with('caracteristicasInternas')
            ->where('inmueble', $data['inmueble']['id'])->get();

        $c_externas = InmuebleExternaMany::with('caracteristicasExternas')
            ->where('inmueble', $data['inmueble']['id'])->get();

        $data['inmueble']['caracteristicas_internas'] = $c_internas;
        $data['inmueble']['caracteristicas_externas'] = $c_externas;


        return $this->sendResponse($data, "inmueble");
    }

    public function getInmueblesUser(Request $request, $referer)
    {

      #  return $referer;

        $c_internas = [];

        $empresa=Empresa::where('slug', '=', $referer)->first();

       if(isset($empresa->id)){

       # return json_encode($empresa);

        $auth = User::where("id", $empresa->agente)->first();

        $data["configuracion"] = Empresa::with(
            "empresaEmails",
            "empresaNumbers",
            "empresaRedes",
            "id_pais",
            "id_estado",
            "id_ciudad"
        )->where("agente", $auth->id)->first();

        $data["userdata"] = User::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
            'userdata.segundo_nombre',
            'userdata.primer_apellido',
            'userdata.segundo_apellido',
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.id_estado',
            'userdata.descripcion',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.genero',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente',
            'userdata.descripcion'
        )
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->where('users.id', $auth->id)
            ->first();

        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'tipoInmueble',
            'paisId',
            'antiguedad',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'propietario',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento',
            'PortalesCodigoResponse',
            'PortaleStateInmueble',
            'PortalesUrls'
        )->where("user_id", $auth->id)
            ->get();

        $data['inmuebles'] = $inmuebles;

        $ciudades=Inmuebles::select(
        'estado_id', 
        'states.name as name', 
        DB::raw('count(inmuebles.estado_id)  as cantidad'))
        ->join('states', 'inmuebles.estado_id', '=', 'states.id')
        ->where("user_id", $auth->id)
        ->groupBy('inmuebles.estado_id')
        ->groupBy('states.name')
        ->get();

        $data['ciudades']=$ciudades;

        for ($i = 0; $i < count($data['inmuebles']); $i++) {


            $c_internas = InmuebleInternaMany::with('caracteristicasInternas')
                ->where('inmueble', $data['inmuebles'][$i]['id'])->get();

            $c_externas = InmuebleExternaMany::with('caracteristicasExternas')
                ->where('inmueble', $data['inmuebles'][$i]['id'])->get();


            $data['inmuebles'][$i]['caracteristicas_internas'] = $c_internas;
            $data['inmuebles'][$i]['caracteristicas_externas'] = $c_externas;
        }


       }else{

        return $this->sendResponse([], "Listado de inmuebles");

       }

        return $this->sendResponse($data, "Listado de inmuebles");
    }




    public function getInmueblesAll()
    {

        $c_internas = [];

        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'antiguedad',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'estrato',
            'propietario',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento',
            'PortalesCodigoResponse',
            'PortaleStateInmueble'
        )->get();

        $data['inmuebles'] = $inmuebles;

        for ($i = 0; $i < count($data['inmuebles']); $i++) {


            $c_internas = InmuebleInternaMany::with('caracteristicasInternas')
                ->where('inmueble', $data['inmuebles'][$i]['id'])->get();

            $c_externas = InmuebleExternaMany::with('caracteristicasExternas')
                ->where('inmueble', $data['inmuebles'][$i]['id'])->get();



            $data['inmuebles'][$i]['caracteristicas_internas'] = $c_internas;
            $data['inmuebles'][$i]['caracteristicas_externas'] = $c_externas;
        }

        return $this->sendResponse($data, "Listado de inmuebles");
    }

    public function inventarioInmuebleArrendo()
    {
        $user = Auth::user();

        $data["inventario_arriendo"] =  DB::table('inmuebles')
            ->select("inmuebles_tipo_inmuebles.*")
            ->join('inmuebles_tipo_inmuebles', 'inmuebles.tipo_inmueble', '=', 'inmuebles_tipo_inmuebles.id')
            ->join('inmuebles_tipo_negocio', 'inmuebles.tipo_negocio', '=', 'inmuebles_tipo_negocio.id')
            ->where('inmuebles_tipo_negocio.tipo', '=', "Alquiler")
            ->where('user_id', $user->id)
            ->get();

        return $this->sendResponse($data, "inventario de inmueble ARRIENDO");
    }


    public function inventarioInmuebleVenta()
    {
        $user = Auth::user();

        $data["inventario_venta"] =  DB::table('inmuebles')
            ->select("inmuebles_tipo_inmuebles.*")
            ->join('inmuebles_tipo_inmuebles', 'inmuebles.tipo_inmueble', '=', 'inmuebles_tipo_inmuebles.id')
            ->join('inmuebles_tipo_negocio', 'inmuebles.tipo_negocio', '=', 'inmuebles_tipo_negocio.id')
            ->where('inmuebles_tipo_negocio.tipo', '=', "Venta")
            ->where('user_id', $user->id)
            ->get();

        return $this->sendResponse($data, "inventario de inmueble VENTA");
    }

    public function orderImagen(Request $request)
    {
        // return $request;
        $id = $request->codigo;
        $imagenes = $request->imagenes;

        for ($i = 0; $i < count($imagenes); $i++) {

            InmuebleImagenes::where("inmueble", $id)->where("id", $imagenes[$i]['id'])->update([
                "order" => $imagenes[$i]['order']
            ]);
        }

        $data['data'] = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'tipoInmueble',
            'antiguedad',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'propietario',
            'estadoPublicacion',
            'stateFisico',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento',
            'PortalesCodigoResponse',
            'PortaleStateInmueble',
            'PortalesUrls'
        )->where("id", $id)
            ->first();

        return $this->sendResponse($data, "Se ordenaron las imagenes correctamente");
    }



    public function getInmueblesForSlug($slug)
    {

        $inmuebles =  Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'created_by',
            'propietario',
            'barrioId',
            'antiguedad',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento',
            'PortalesCodigoResponse',
            'PortaleStateInmueble',
            'PortalesUrls'
        )->where('slug', $slug)
            ->first();

        if ($inmuebles) {

            //return $inmuebles;
            $data['inmueble'] = $inmuebles;

            $agente = User::with('userdata')->where('id', $inmuebles->user_id)->first();

            $empresa = Empresa::with(
                "empresaEmails",
                "empresaNumbers",
                "empresaRedes",
                "id_pais",
                "id_estado",
                "id_ciudad",

            )->where("agente", $inmuebles->user_id)->first();

            $c_internas = InmuebleInternaMany::with('caracteristicasInternas')
                ->where('inmueble', $inmuebles->id)->get();

            $c_externas = InmuebleExternaMany::with('caracteristicasExternas')
                ->where('inmueble', $inmuebles->id)->get();

            $rangue_price = DB::table('inmueble_range_price')->where('id', $inmuebles->rangue_price)->first();

            $data['inmueble']['caracteristicas_internas'] = $c_internas;
            $data['inmueble']['caracteristicas_externas'] = $c_externas;
            $data['inmueble']['agente'] = $agente;
            $data['inmueble']['empresa'] = $empresa;
            $data['inmueble']['rangue_price'] = $rangue_price;

            return $this->sendResponse($data, "Inmueble encontrado");
        }

        return $this->sendError('', "Inmueble encontrado");
    }

    public function getInmueblesForSlugView($slug)
    {
        $inmuebles =  Inmuebles::where('slug', $slug)->first();

        $description = Str::limit(strip_tags($inmuebles->descripcion), 200);

        $data['tags'] = [
            'og:app_id' => '4549589748545',
            'og:image' =>  env('APP_URL') . Storage::url($inmuebles->InmuebleImagenes[0]['url']),
            'og:description' => $description,
            'description' => $description,
            'og:title' => $inmuebles->titulo_inmueble,
            'og:url' =>  env('APP_URL') . '/inmuebles/' . $inmuebles->slug,

        ];

        return view('application', compact('data'));
    }



    public function getStateTipePublication()
    {
        $cara = StatePublications::all();

        $data['tipe_publication'] = $cara;

        return $this->sendResponse($data, "Listado de tipo de pubicacion");
    }


    public function getSegmentoMercado()
    {
        $segmento = SegmentoMercados::all();

        $data['segmento_mercado'] = $segmento;

        return $this->sendResponse($data, "Listado de segmento de mercado");
    }

    public function getStateFisico()
    {
        $segmento = InmuebleStadoFisico::all();
        $data_json = json_decode(json_encode($segmento));

        for ($i = 0; $i < count($data_json); $i++) {

            $carac = DB::table('portales_inmuebles_estado_fisico')
                ->where("state_fisico", $data_json[$i]->id)
                ->get();

            $data_json[$i]->portales_inmuebles_estado_fisico = $carac;
        }

        $data['estado_fisico'] = $data_json;;

        return $this->sendResponse($data, "Listado de estado fisicos");
    }

    public function getEstratos()
    {
        $estrato = InmuebleStrato::all();

        $data['estratos'] = $estrato;

        return $this->sendResponse($data, "Listado de estratos");
    }

    public function getTipoParqueadero()
    {
        $parqueadero = InmuebleTipoParqueadero::all();

        $data['parqueaderos'] = $parqueadero;

        return $this->sendResponse($data, "Listado de parqueaderos");
    }


    public function getTipoTiempoAlquiler()
    {
        $parqueadero = InmuebleTipoTiempoAlquiler::all();

        $data['tiempo_alquiler'] = $parqueadero;

        return $this->sendResponse($data, "Listado de tipos de tiempos alquileres");
    }

    public function getTipoPeriodoAdmon()
    {
        $admon = TipoPeriodoAdmon::all();

        $data['periodo_admon'] = $admon;

        return $this->sendResponse($data, "Listado de periodos admon");
    }



    public function downloadPdf(Request $request)
    {
        $file = public_path() . Storage::url($request[0]);

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, 'document.pdf', $headers);
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'titulo_inmueble' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
        }

        $condicion = "null";
        $resulCondicion = null;

        $auth = Auth::user();


        $json = json_decode($request->propietario);
        $rangue_price = json_decode($request->rangue_price);


        if (
            $json->primer_nombre == null && $json->segundo_nombre == null && $json->primer_apellido == null
            && $json->segundo_apellido == null && $json->email == null && $json->celular == null && $json->telefono_fijo == null
        ) {
            $propietario = $condicion;
        } else {

            $propietario = InmueblePropietario::create([
                'primer_nombre' => $json->primer_nombre,
                'segundo_nombre' => $json->segundo_nombre,
                'primer_apellido' => $json->primer_apellido,
                'segundo_apellido' => $json->segundo_apellido,
                'email' => $json->email,
                'celular' => $json->celular,
                'telefono_fijo' => $json->telefono_fijo,

            ]);
        }
        $user = getUsersRoles($auth);

        $path_cedula = "";
        $path_certificado_tradicion = "";
        $path_predial = "";

        if ($request->file('cedula')) {
            $file = $request->file("cedula");
            $nombre = "pdf_" . time() . "." . $file->guessExtension();
            if ($file->guessExtension() == "pdf") {
                $upload = $request->file('cedula')->storeAs('public/inmuebles/documentos/cedula', $nombre);
                $path_cedula = $upload;
            } else {
                dd("NO ES UN PDF");
            }
        }


        if ($request->file('certificado_tradicion')) {
            $file = $request->file("certificado_tradicion");
            $nombre = "pdf_" . time() . "." . $file->guessExtension();
            if ($file->guessExtension() == "pdf") {
                $upload = $request->file('certificado_tradicion')->storeAs('public/inmuebles/documentos/certificado_tradicion', $nombre);
                $path_certificado_tradicion = $upload;
            } else {
                dd("NO ES UN PDF");
            }
        }

        if ($request->file('predial')) {
            $file = $request->file("predial");
            $nombre = "pdf_" . time() . "." . $file->guessExtension();
            if ($file->guessExtension() == "pdf") {
                $upload = $request->file('predial')->storeAs('public/inmuebles/documentos/predial/', $nombre);
                $path_predial = $upload;
            } else {
                dd("NO ES UN PDF");
            }
        }

        $register = Inmuebles::create([
            'user_id' => $user,
            'created_by' => $auth->id,
            "cedula" => $path_cedula,
            'rangue_price' => $rangue_price->id,
            "certificado_tradicion" => $path_certificado_tradicion,
            "predial" => $path_predial,
            'propietario_id' => $propietario != $condicion ? $propietario->id : $resulCondicion,
            'titulo_inmueble' => $request->titulo_inmueble,
            'slug' => str_replace(" ", "-", $request->titulo_inmueble),
            'tipo_inmueble' => $request->tipo_inmueble == $condicion ? $resulCondicion : intval($request->tipo_inmueble),
            'tipo_negocio' => $request->tipo_negocio == $condicion ? $resulCondicion : intval($request->tipo_negocio),
            'segmento' => $request->segmento_mercado == $condicion ? $resulCondicion : intval($request->segmento_mercado),
            'estado_publicacion' => $request->estado_publicacion == $condicion ? $resulCondicion : intval($request->estado_publicacion),
            'state_fisico' => $request->state_fisico == $condicion ? $resulCondicion : intval($request->state_fisico),
            'matricula_inmobiliaria' => $request->matricula_inmobiliaria,
            'url_video' => $request->url_video == $condicion ? "" : $request->url_video,
            'estrato' => $request->estrato == $condicion ? $resulCondicion : intval($request->estrato),
            'ano_construcion' => $request->ano_construcion,
            'antiguedad' =>  $request->antiguedad == $condicion ? $resulCondicion : intval($request->antiguedad),
            'banos' => $request->banos,
            'habitaciones' => $request->habitaciones == $condicion ? 1 : $request->habitaciones,
            'garaje' => $request->garaje,
            'pisos' => $request->pisos,
            'state_inmueble' => 2,
            'cantidad_parqueadero' => $request->cantidad_parqueadero,
            'parqueadero' => $request->parqueadero == $condicion ? $resulCondicion : intval($request->parqueadero),
            'area_lote' => $request->area_lote,
            'area_contruida' => $request->area_contruida,
            'frente' => $request->frente,
            'fondo' => $request->fondo,
            'area_total' => $request->area_total,
            'descripcion' => $request->descripcion,
            'precio_venta' =>  str_replace('.', '', $request->precio_venta),
            'precio_alquiler' =>  str_replace('.', '', $request->precio_alquiler),
            'precio_administracion' => str_replace('.', '', $request->precio_administracion),
            'periodo_admon' => $request->periodo_admon == $condicion ? $resulCondicion : intval($request->periodo_admon),
            'tiempo_alquiler' => $request->tiempo_alquiler == $condicion ? $resulCondicion : intval($request->tiempo_alquiler),
            'tipo_precio' => $request->tipo_precio == $condicion ? $resulCondicion : intval($request->tipo_precio),
            'pais_id' => $request->pais_id == $condicion ? $resulCondicion : intval($request->pais_id),
            'estado_id' => $request->estado_id  == $condicion ? $resulCondicion : intval($request->estado_id),
            'ciudad_id' => $request->ciudad_id == $condicion ? $resulCondicion : intval($request->ciudad_id),
            'zona_id' => $request->zona_id == $condicion ? $resulCondicion : intval($request->zona_id),
            'barrio_id' => $request->barrio_id == $condicion ? $resulCondicion : intval($request->barrio_id),
            'latitud' => $request->latitud,
            'longitud' => $request->longitud,
            'direccion' => $request->direccion,
            //'precio_alquiler' => $request->precio_alquiler
        ]);
        if ($register) {

            $inmueble_updated = Inmuebles::where('id', $register->id)->update(["slug" => $register->slug . '_' . $register->id]);

            $carInt = json_decode($request->caracteristicas_internas);
            $carExt = json_decode($request->caracteristicas_externas);

            $imagenes = json_decode($request->json);

            $inmueble = null;

            foreach ($imagenes as $imagen) {

                $i = InmuebleImagenesTemporal::where('id', $imagen->id)->first();

                if (isset($i->id)) {

                    InmuebleImagenes::create([
                        'url' => $i->url,
                        'orden' => $i->orden,
                        'inmueble' => $register->id
                    ]);
                }
            }


            if (count($carInt) != 0) {
                foreach ($carInt as $value) {
                    InmuebleInternaMany::create([
                        'caracteristicas_internas' => $value->id,
                        'inmueble' => $register->id,

                    ]);
                }
            }

            if (count($carExt) != 0) {

                foreach ($carExt as $value) {
                    InmuebleExternaMany::create([
                        'caracteristicas_externas' => $value->id,
                        'inmueble' => $register->id,

                    ]);
                }
            }
            return $this->sendResponse($inmueble_updated, "Se registro el inmueble correctamente");
        }

        return $this->sendError([], "Ocurrio un error al registrar el inmueble");
    }



    public function deleteImage(Request $request)
    {


        $image = InmuebleImagenes::where('id', $request->id_inmueble)
            ->delete();


        if ($image) {

            if (Storage::disk('public')->exists($request->url)) {

                Storage::delete('public/' . $request->url);
            }

            $inmueble = InmuebleImagenes::where('inmueble', $request->id)
                ->orderBy("id", "desc")
                ->get();

            return $this->sendResponse($inmueble, "Se elimino la imagen");
        }

        return $this->sendError('No se pudo eliminar la imagen', [], 500);
    }

    public function editImageInmueble(Request $request)
    {


        if ($request->file('files')) {

            $files = $request->file('files');

            if (!is_array($files)) {
                $files = [$files];
            }

            for ($i = 0; $i < count($files); $i++) {

                $uploadedFileUrl = Cloudinary::upload($files[$i]->getRealPath())->getSecurePath();

                #  $file = $files[$i];
                # $filename = $file->getClientOriginalName();
                # $filename = str_replace(' ', '', $filename);
                $path = $uploadedFileUrl;
                # $path = $file->storeAs('inmuebles/' . $request->id, $filename, 'public');


                InmuebleImagenes::create([
                    'url' => $path,
                    'inmueble' => $request->id
                ]);
            }

            $inmueble = InmuebleImagenes::where('inmueble', $request->id)
                ->orderBy("id", "desc")
                ->get();


            return $this->sendResponse($inmueble, "Se registro la imagen");
        }
        return $this->sendError('No se pudo registrar la imagen', [], 500);
    }


    public function addImageInmueble(Request $request)
    {

        if ($request->file('files')) {

            $files = $request->file('files');

            if (!is_array($files)) {
                $files = [$files];
            }

            for ($i = 0; $i < count($files); $i++) {

                $uploadedFileUrl = Cloudinary::upload($files[$i]->getRealPath())->getSecurePath();


                #  $file = $files[$i];
                # $filename = $file->getClientOriginalName();
                # $filename = str_replace(' ', '', $filename);
                $path =  $uploadedFileUrl;

                $data = array([
                    'url' => $path,
                    'inmueble' => $request->id,
                    'orden' => $i + 1
                ]);

                #dd($data);

                InmuebleImagenesTemporal::create([
                    'url' => $path,
                    'inmueble' => $request->id,
                    'orden' => $i + 1
                ]);
            }

            $inmueble = InmuebleImagenesTemporal::where('inmueble', $request->id)
                ->orderBy("id", "desc")
                ->get();


            return $this->sendResponse($inmueble, "Se registro la imagen");
        }
        return $this->sendError('No se pudo registrar la imagen', [], 500);
    }


    public function updImageInmueble(Request $request)
    {

        $imagenes = json_decode($request->json);

        $inmueble = null;

        foreach ($imagenes as $imagen) {

            $i = InmuebleImagenesTemporal::where('id', $imagen->id)->first();

            if (isset($i->id)) {
                $inmueble = $imagen->inmueble;
                $i->update(['orden' => $imagen->order]);
            }
        }

        $inmueble = InmuebleImagenesTemporal::where('inmueble', $i->inmueble)
            ->orderBy("orden", "asc")
            ->get();

        return $this->sendResponse($inmueble, "Se registro la imagen");

        // return $this->sendError('No se pudo registrar la imagen', [], 500);
    }


    public function delImageInmueble(Request $request)
    {

        $imagenes = json_decode($request->json);

        $inmueble = null;


        $i = InmuebleImagenesTemporal::where('id', $imagenes->id)->first();

        if (isset($i->id)) {

            # $inmueble=$imagen->inmueble;

            $i->delete();
        }


        $inmueble = InmuebleImagenesTemporal::where('inmueble', $imagenes->inmueble)
            ->orderBy("orden", "asc")
            ->get();

        return $this->sendResponse($inmueble, "Se registro la imagen");

        // return $this->sendError('No se pudo registrar la imagen', [], 500);
    }







    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function exportInmueble()
    {
        $auth = Auth::user();

        $user = getUsersRoles($auth);


        $inmueble = DB::table("inmuebles")
            ->select(
                "inmuebles.*",
                "inmuebles_tipo_inmuebles.descripcion AS tipo_inmueble",
                "inmuebles_tipo_negocio.descripcion AS tipo_negocio",
                // "inmuebles_tipo_contrato.descripcion AS tipo_contrato",
                "countries.name AS paises",
                "states.name AS departamento",
                "cities.name AS ciudades"
            )
            ->join('inmuebles_tipo_inmuebles', 'inmuebles_tipo_inmuebles.id', '=', 'inmuebles.tipo_inmueble')
            ->join('inmuebles_tipo_negocio', 'inmuebles_tipo_negocio.id', '=',  'inmuebles.tipo_negocio')
            // ->join('inmuebles_tipo_contrato', 'inmuebles_tipo_contrato.id', '=',   'inmuebles.tipo_contrato')
            ->join('countries',  'countries.id', '=', 'inmuebles.pais_id')
            ->join('states', 'states.id', '=', 'inmuebles.estado_id')
            ->join('cities', 'cities.id', '=', 'inmuebles.ciudad_id')
            ->where("user_id", 8)
            ->get();


        return Excel::download(new InmuebleExport($inmueble), 'inmueble.xlsx');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editInmueble(Request $request)
    {

        $antiguedad = json_decode($request->antiguedad);
        $barrio_id = json_decode($request->barrio_id);
        $ciudad_id = json_decode($request->ciudad_id);
        $estado_id = json_decode($request->estado_id);
        $estrato = json_decode($request->estrato);
        $pais_id = json_decode($request->pais_id);
        $parqueadero = json_decode($request->parqueadero);
        $state_fisico = json_decode($request->state_fisico);
        $tipo_inmueble = json_decode($request->tipo_inmueble);
        $tipo_negocio = json_decode($request->tipo_negocio);
        $tipo_precio = json_decode($request->tipo_precio);
        $zona_id = json_decode($request->zona_id);
        $segmento_mercado = json_decode($request->segmento_mercado);
        $estado_publicacion = json_decode($request->estado_publicacion);
        $periodo_admon = json_decode($request->periodo_admon);
        $tiempo_alquiler = json_decode($request->tiempo_alquiler);
        $rangue_price = json_decode($request->rangue_price);

        $validator = Validator::make($request->all(), [
            'titulo_inmueble' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
        }

        $auth = Auth::user();
        $condicion = null;
        $resulCondicion = null;
        $propietario = null;

        $json = json_decode($request->propietario);

        $array = [
            'primer_nombre' => $json->primer_nombre,
            'segundo_nombre' => $json->segundo_nombre,
            'primer_apellido' => $json->primer_apellido,
            'segundo_apellido' => $json->segundo_apellido,
            'email' => $json->email,
            'celular' => $json->celular,
            'telefono_fijo' => $json->telefono_fijo,
        ];

        if ($json->id != null) {
            $pro = InmueblePropietario::where('id', $json->id)->first();

            $propietario = null;

            if ($pro) {
                $propietario_update = InmueblePropietario::where('id', $json->id)
                    ->update($array);

                $propietario = $propietario_update;
            }
        } else {
            $propietario_create = InmueblePropietario::create($array);
            $propietario = $propietario_create->id;
        }

        $path_cedula = $request->cedula;
        $path_certificado_tradicion = $request->certificado_tradicion;
        $path_predial = $request->predial;

        if ($request->file('cedula')) {
            if (Storage::exists($request->cedula)) {
                Storage::delete($request->cedula);
            }

            $file = $request->file("cedula");
            $nombre = "pdf_" . time() . "." . $file->guessExtension();
            if ($file->guessExtension() == "pdf") {
                $upload = $request->file('cedula')->storeAs('public/inmuebles/documentos/cedula', $nombre);
                $path_cedula = $upload;
            } else {
                dd("NO ES UN PDF");
            }
        }


        if ($request->file('certificado_tradicion')) {

            if (Storage::exists($request->certificado_tradicion)) {
                Storage::delete($request->certificado_tradicion);
            }

            $file = $request->file("certificado_tradicion");
            $nombre = "pdf_" . time() . "." . $file->guessExtension();
            if ($file->guessExtension() == "pdf") {
                $upload = $request->file('certificado_tradicion')->storeAs('public/inmuebles/documentos/certificado_tradicion', $nombre);
                $path_certificado_tradicion = $upload;
            } else {
                dd("NO ES UN PDF");
            }
        }

        if ($request->file('predial')) {

            if (Storage::exists($request->predial)) {
                Storage::delete($request->predial);
            }

            $file = $request->file("predial");
            $nombre = "pdf_" . time() . "." . $file->guessExtension();
            if ($file->guessExtension() == "pdf") {
                $upload = $request->file('predial')->storeAs('public/inmuebles/documentos/predial', $nombre);
                $path_predial = $upload;
            } else {
                dd("NO ES UN PDF");
            }
        }

        $update = DB::table("inmuebles")->where('id', intval($request->id))
            ->update([
                'propietario_id' => $propietario,
                'updated_by' => $auth->id,
                "cedula" => $path_cedula,
                "certificado_tradicion" => $path_certificado_tradicion,
                "predial" => $path_predial,
                'titulo_inmueble' => $request->titulo_inmueble,
                'slug' => str_replace(" ", "-", $request->titulo_inmueble),
                'tipo_inmueble' => $tipo_inmueble == $condicion ? $resulCondicion : $tipo_inmueble->id,
                'tipo_negocio' => $tipo_negocio == $condicion ? $resulCondicion : $tipo_negocio->id,
                'segmento' => $segmento_mercado == $condicion ? $resulCondicion : $segmento_mercado->id,
                'estado_publicacion' => $estado_publicacion == $condicion ? $resulCondicion : $estado_publicacion->id,
                'state_fisico' => $state_fisico == $condicion ? $resulCondicion : $state_fisico->id,
                'matricula_inmobiliaria' => $request->matricula_inmobiliaria,
                'estrato' => $estrato == $condicion ? $resulCondicion : $estrato->id,
                'ano_construcion' => $request->ano_construcion,
                'habitaciones' => $request->habitaciones == $condicion ? 1 : $request->habitaciones,
                'banos' => $request->banos,
                'garaje' => $request->garaje,
                'pisos' => $request->pisos,
                'antiguedad' =>  $antiguedad == $condicion ? $resulCondicion : $antiguedad->id,
                'cantidad_parqueadero' => $request->cantidad_parqueadero,
                'parqueadero' => $parqueadero == $condicion ? $resulCondicion : $parqueadero->id,
                'area_lote' => $request->area_lote,
                'area_contruida' => $request->area_contruida,
                'frente' => $request->frente,
                'fondo' => $request->fondo,
                'area_total' => $request->area_total,
                'descripcion' => $request->descripcion,
                'url_video' => $request->url_video == $condicion || $request->url_video == "null"  ? "" : $request->url_video,
                'precio_venta' =>  str_replace('.', '', $request->precio_venta),
                'precio_alquiler' =>  str_replace('.', '', $request->precio_alquiler),
                'precio_administracion' => str_replace('.', '', $request->precio_administracion),
                'periodo_admon' => $periodo_admon == $condicion ? $resulCondicion : $periodo_admon->id,
                'tiempo_alquiler' => $tiempo_alquiler == $condicion ? $resulCondicion : $tiempo_alquiler->id,
                'tipo_precio' => $tipo_precio == $condicion ? $resulCondicion : $tipo_precio->id,
                'pais_id' => $pais_id == $condicion ? $resulCondicion : $pais_id->id,
                'estado_id' => $estado_id  == $condicion ? $resulCondicion : $estado_id->id,
                'ciudad_id' => $ciudad_id == $condicion ? $resulCondicion : $ciudad_id->id,
                'zona_id' => $zona_id == $condicion ? $resulCondicion : $zona_id->id,
                'barrio_id' => $barrio_id == $condicion ? $resulCondicion : $barrio_id->id,
                'latitud' => $request->latitud,
                'longitud' => $request->longitud,
                'rangue_price' => $rangue_price->id,
            ]);

        if ($update == 1 || $update == 0) {

            $find_car_int = InmuebleInternaMany::where('inmueble', $request->id)->get();
            $find_car_ext = InmuebleExternaMany::where('inmueble', $request->id)->get();

            $carInt = json_decode($request->caracteristicas_internas);
            $carExt = json_decode($request->caracteristicas_externas);

            if (count($carInt) != 0) {
                if (count($find_car_int) != 0) {
                    InmuebleInternaMany::where('inmueble', $request->id)->delete();
                    foreach ($carInt as $value) {

                        InmuebleInternaMany::create([
                            'caracteristicas_internas' => $value->id,
                            'inmueble' => $request->id,
                        ]);
                    }
                } else {
                    foreach ($carInt as $value) {
                        InmuebleInternaMany::create([
                            'caracteristicas_internas' => $value->id,
                            'inmueble' => $request->id,
                        ]);
                    }
                }
            }

            if (count($carExt) != 0) {
                if (count($find_car_ext) != 0) {
                    InmuebleExternaMany::where('inmueble', $request->id)->delete();
                    foreach ($carExt as $value) {
                        InmuebleExternaMany::create([
                            'caracteristicas_externas' => $value->id,
                            'inmueble' => $request->id,
                        ]);
                    }
                } else {

                    foreach ($carExt as $value) {
                        InmuebleExternaMany::create([
                            'caracteristicas_externas' => $value->id,
                            'inmueble' => $request->id,

                        ]);
                    }
                }
            }

            $data = DB::table("inmuebles")->where('id', intval($request->id))->first();

            return $this->sendResponse($data, "Se actualzo el inmueble correctamente");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPdf($ruta)
    {
        return response(Storage::disk('local')->get('temppdf/' . $ruta), 200)
            ->header('Content-Type', 'application/pdf');
    }


    public function getInmuebleRanguePrice()
    {
        $data['rangue_price'] = DB::table('inmueble_range_price')->get();

        return $this->sendResponse($data, "Listado de rango de precio");
    }
}
