<?php

namespace App\Http\Controllers\API;

use App\Jobs\SendMailJob;
use App\Mail\EmailPrivateMecadeo;
use App\Mail\NewArrivals;
use App\Models\Cliente;
use App\Models\Country;
use App\Models\Empresa;
use App\Models\Mercadeo;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;

class MessageController extends ApiController
{




    

    public function getUsers()
    {
        $auth = Auth::user();
        $data['users'] = User::with('userdata')->where("referred_by", $auth->id)->get();
        return $this->sendResponse($data, "Listado de usuarios");
    }

    public function getMessages()
    {
        $auth = Auth::user();
        $data['message'] = Message::with('mercadeo_id')->where('user_id', $auth->id)->orderBy('created_at', 'desc')->get();
        return $this->sendResponse($data, "Listado de mensajes");
    }

    public static function getUser()
    {
        $auth = Auth::user();

        $data = User::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
            'userdata.segundo_nombre',
            'userdata.primer_apellido',
            'userdata.segundo_apellido',
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.descripcion',
            'userdata.genero',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.id_estado',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente'
        )
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->where('users.id', $auth->id)
            ->first();

        $user_json = json_decode(json_encode($data));
        $user_admin = getUsersRoles($auth);

        $empresa = Empresa::with(
            "empresaEmails",
            "empresaNumbers",
            "empresaRedes",
            "id_pais",
            "id_estado",
            "id_ciudad"
        )->where("agente", $user_admin)->first();

        $user_json->roles = [];

        $pais = Country::where("id", $user_json->id_pais)->first();
        $state = DB::table("states")->where("id", $user_json->id_estado)->first();
        $ciudad = DB::table("cities")->where("id", $user_json->id_ciudad)->first();


        $rol = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $user_json->id)
            ->first();

        $user_json->id_pais = $pais;
        $user_json->id_estado = $state;
        $user_json->id_ciudad = $ciudad;
        $user_json->roles = $rol;
        $user_json->empresa = $empresa;


        return $user_json;
    }

    public function sendMail(Request $request)
    {

        $users = $request->selectedCliente;

        $auth = Auth::user();

        $message = new Message();
        $message->title = $request->title;
        $message->body = "No tiene";
        $message->user_id = $auth->id;
        if ($request->mercadeo_id["type"] == "public") {
            $message->mercadeo_id =  $request->mercadeo_id["mercadeo"];
        } else {
            $message->mercadeo_private =  $request->mercadeo_id["mercadeo"];
        }
        $message->save();


        if ($request->item == "now") {

            $message->delivered = 'Enviado';
            $message->send_date = Carbon::now();
            $message->save();


            if (empty($users)) {
                $users =  Cliente::with('correos')->where("user_id", $auth->id)->get();
            }

            // return $users;
            if ($request->mercadeo_id["type"] == "public") {
                $messages = Message::with('mercadeo_id')->where('id', $message->id)->first();
                $mercadeo = Mercadeo::where('id', $messages->mercadeo_id)->first();
                $template = $mercadeo->html;

                for ($i = 0; $i < count($users); $i++) {
                    $userdata = $this->getUser();

                    for ($j = 0; $j < count($users[$i]["correos"]); $j++) {
                        dispatch(new SendMailJob($users[$i]["correos"][$j]["email"], new NewArrivals($users[$i], $message, $template, $userdata)));
                    }
                }
            } else {

                $template = DB::table("mercadeos_admin")->where("id", $message->mercadeo_private)->first();
                $template_json = json_decode(json_encode($template));
                $mercadeo_type = DB::table("mercadeos_type")->where("id", $template_json->mercadeo_type)->first();
                $template_json->mercadeo_type = $mercadeo_type;

                $inmueble = null;
                if (array_key_exists('selected', $request->mercadeo_id)) {
                    $inmueble = $request->mercadeo_id['selected'][0];
                }
                //   $inmueble = $request->mercadeo_id['selected'][0];

                $userdata = $this->getUser();


                for ($i = 0; $i < count($users); $i++) {
                    for ($j = 0; $j < count($users[$i]["correos"]); $j++) {
                        dispatch(new SendMailJob($users[$i]["correos"][$j]["email"], new EmailPrivateMecadeo($users[$i], $message, $template_json, $userdata, $inmueble)));
                    }
                }
            }

            return response()->json('Correo eviado.', 201);
        } else {

            $message->date_string = date("Y-m-d H:i", strtotime($request->send_date));

            $message->save();

            return response()->json('Tu correo se envia en el tiempo programado.', 201);
        }
    }
}
