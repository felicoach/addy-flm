<?php

namespace App\Http\Controllers\API;

use App\Models\Inmuebles;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClasificadosPaisController extends ApiController
{

    public function publicInmuieble($id)
    {

        $find = $this->getCredentials();

        $data_info = $this->getInfo($id);

        $client = new Client();

        $res = $client->request('POST',  env('API_URL_EL_PAIS_TEST') . '1/bulk/advice_importer.json', [
            'auth' => [
                $find['credentials']->username,
                $find['credentials']->password
            ],
            'json' => $data_info['array']
        ]);

        $rest_body = json_decode($res->getBody());

        if ($rest_body->result->code == 200) {

            $data =  json_decode($res->getBody());
            $array_data = (array) $data->result;
            $codigo =  $array_data['RBM ID'];

            $data = $this->getResponse($codigo, $data_info, $id);

            return $this->sendResponse($data, "Se publico el inmueble");
        } else {
            return $this->sendError($rest_body, "Intentalo de nuevo");
        }
    }

    public function updateInmuieble($id)
    {

        $find = $this->getCredentials();

        $data = $this->getInfo($id);

        $info_c_p  = $this->getPortalCodigoPublication($id);

        $data['array']['adviceId'] = $info_c_p['codigo'];

        $client = new Client();

        $res = $client->request('PUT',  env('API_URL_EL_PAIS_TEST') . '1/bulk/advice_update.json', [
            'auth' => [
                $find['credentials']->username,
                $find['credentials']->password
            ],
            'json' => $data['array']
        ]);

        $data =  json_decode($res->getBody());

        if ($data->result->code == 200) {
            return $this->sendResponse($data, "Se actualizo el inmueble");
        } else {
            return $this->sendResponse($data, "Intentalo de nuevo");
        }
    }



    public function cancelarAvizo($id)
    {

        $find = $this->getCredentials();

        $info_c_p  = $this->getPortalCodigoPublication($id);

        if ($info_c_p['codigo']) {
            $client = new Client();
            $res = $client->request('PUT',  env('API_URL_EL_PAIS_TEST') . '1/bulk/cancel_advice.json', [

                'auth' => [
                    $find['credentials']->username,
                    $find['credentials']->password
                ],

                'json' => [
                    "advice_id" =>  $info_c_p['codigo']
                ]
            ]);

            $data_res = json_decode($res->getBody());

            if ($data_res->result->code == 200) {

                DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal',  $info_c_p['portal'])->delete();
                DB::table('portales_state_inmuebles')->where('id_inmueble', $id)->where('id_portal', $info_c_p['portal'])->delete();
                DB::table('portales_urls')->where('inmueble_id', $id)->where('portal_id',  $info_c_p['portal'])->delete();

                return $this->sendResponse($data_res,  "Se desactivo el inmueble");
            } else {
                return $this->sendError($res->getBody(), "error al desactivar el inmueble");
            }
        } else {
            return $this->sendError($id, "No existe codigo para este inmueble");
        }
    }


    public static function getCredentials()
    {
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $find = DB::table('portales_credentials_clasificados_pais')
            ->where('user_id', $aut_user)
            ->first();

        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%clasificado%")->first();

        return ["credentials" => $find, "portal" => $portal];
    }

    public function getPaises()
    {
        try {
            $find = $this->getCredentials();

            $client = new Client();

            $res = $client->request('GET',  env('API_URL_EL_PAIS_TEST') . 'País/attributes_list.json', [
                'auth' => [
                    $find['credentials']->username,
                    $find['credentials']->password
                ],
            ]);

            $data_res = json_decode($res->getBody());

            $darr = [];

            for ($i = 0; $i < count($data_res->result->attribute_options); $i++) {

                $find_country = DB::table('countries')->where('name', 'LIKE', "%" . $data_res->result->attribute_options[$i]->attribute_option_name . "%")->first();

                if ($find_country) {
                    array_push($darr, $find_country);

                    $find = DB::table('portales_countrie')->where('countrie_id',  $find_country->id)->where('portale_id', $find['portal']->id)->first();
                    $find_object = json_decode(json_encode($find));

                    if (!$find) {
                        DB::table('portales_countrie')->insert(
                            [
                                'countrie_id' => $find_country->id,
                                'portale_id' => $find['portal']->id,
                                'codigo' => $data_res->result->attribute_options[$i]->attribute_option_id
                            ],
                        );
                    } else {
                        DB::table('portales_countrie')->where('id', $find_object->id)->update(
                            ['codigo' => $data_res->result->attribute_options[$i]->attribute_option_id],
                        );
                    }
                }
            }

            return $darr;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function getDepartamento()
    {
        try {
            $find = $this->getCredentials();

            $client = new Client();

            $res = $client->request('GET', env('API_URL_EL_PAIS_TEST') . 'Departamento/attributes_list.json', [
                'auth' => [
                    $find['credentials']->username,
                    $find['credentials']->password
                ]
            ]);

            $data_res = json_decode($res->getBody());

            for ($i = 0; $i < count($data_res->result->attribute_options); $i++) {

                $attr = $data_res->result->attribute_options[$i];

                $find_state = DB::table('states')->where('country_id', 48)->where('name', 'LIKE', "%" . $attr->attribute_option_name . "%")->first();

                if ($find_state) {

                    $find_portal_state = DB::table('portales_states')->where('state_id',  $find_state->id)->where('portale_id', $find['portal']->id)->first();
                    $find_object = json_decode(json_encode($find_portal_state));

                    if (!$find_portal_state) {
                        DB::table('portales_states')->insert(
                            [
                                'state_id' => $find_state->id,
                                'portale_id' => $find['portal']->id,
                                'codigo' => $attr->attribute_option_id
                            ],
                        );
                    } else {
                        DB::table('portales_states')->where('id', $find_object->id)->update(
                            ['codigo' => $attr->attribute_option_id],
                        );
                    }
                }
            }
        } catch (\Throwable $th) {
            return $th;
        }
    }


    public function getCities()
    {
        try {
            $find = $this->getCredentials();

            $client = new Client();

            $res = $client->request('GET', env('API_URL_EL_PAIS_TEST') . 'Ciudad/attributes_list.json', [
                'auth' => [
                    $find['credentials']->username,
                    $find['credentials']->password
                ]
            ]);

            $data_res = json_decode($res->getBody());

            for ($i = 0; $i < count($data_res->result->attribute_options); $i++) {

                $find_city = DB::table('cities')->where('name', 'LIKE', "%" . $data_res->result->attribute_options[$i]->attribute_option_name . "%")->first();

                if ($find_city) {

                    $find_city_portal = DB::table('portales_cities')->where('city_id',  $find_city->id)->where('portale_id', $find['portal']->id)->first();
                    $find_object = json_decode(json_encode($find_city_portal));

                    if (!$find_city_portal) {
                        DB::table('portales_cities')->insert(
                            [
                                'city_id' => $find_city->id,
                                'portale_id' => $find['portal']->id,
                                'codigo' => $data_res->result->attribute_options[$i]->attribute_option_id
                            ],
                        );
                    } else {
                        DB::table('portales_cities')->where('id', $find_object->id)->update(
                            ['codigo' => $data_res->result->attribute_options[$i]->attribute_option_id],
                        );
                    }
                }
            }
        } catch (\Throwable $th) {
            return $th;
        }
    }


    public static function getInfo($id, $data = [])
    {
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%clasificado%")->first();


        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'created_by',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'antiguedad',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento'
        )->where("id", $id)
            ->first();

        $imagenes = json_decode(json_encode($inmuebles));      
       
        $rangue_price = DB::table('inmueble_range_price')
            ->where('id', $imagenes->rangue_price)
            ->first();
       
        $consult = DB::table('portales_credenciales')->where(
            'user_id',
            $aut_user
        )->where('portale_id', $portal->id)->first();

        if (!$consult) {
            $consult = DB::table('portales_credenciales')->where(
                'user_id',
                $auth->create_by
            )->where('portale_id', $portal->id)->first();
        }

        $tipo_negocio = DB::table('portales_inmuebles_tipo_negocio')->where('tipo_negocio_id',  $imagenes->tipo_negocio->id)->where('portale_id', $portal->id)->first();

        $leasingFee = 0;
        $tipo_inmueble = null;
        $type = 'otros';
        if ($imagenes->tipo_negocio->tipo == "Alquiler") {
            $type = "alquiler";
            $leasingFee =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_alquiler));
            $tipo_inmueble = DB::table('portales_tipo_inmueble_pais_alquiler')->where('tI_id', $imagenes->tipo_inmueble->id)->first();
        } else {
            $leasingFee  =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_venta));
            $tipo_inmueble = DB::table('portales_tipo_inmueble_pais_venta')->where('tI_id', $imagenes->tipo_inmueble->id)->first();
        }

        $img_array = [];

        for ($i = 0; $i < count($imagenes->inmueble_imagenes); $i++) {
            if (!isValidURL($imagenes->inmueble_imagenes[$i]->url)) {
                array_push($img_array, env('APP_URL') . "/storage/" . $imagenes->inmueble_imagenes[$i]->url);
            } else {
                array_push($img_array, $imagenes->inmueble_imagenes[$i]->url);
            }
        }

        $img = implode(' ', $img_array);

        $array = [
            "attributes" => [
                "País" => $imagenes->pais_id->name,
                "Área" =>  $imagenes->area_total,
                "Texto adicional del anuncio internet" => $imagenes->descripcion,
                "Barrio" => $imagenes->barrio_id->name,
                "Ciudad" => $imagenes->ciudad_id->name,
                "Departamento" => $imagenes->estado_id->name,
                "Precio" => $leasingFee,
                "Zona" => $imagenes->zona_id->name,
                "Condición" => "Sin datos",
                "Rango de precio FR $type" => $rangue_price->value,
                "Estrato" => str_replace('estrato', '', $imagenes->estrato->name),
                "No. de Alcobas" => $imagenes->habitaciones,
                "No. de Baños" => $imagenes->banos,
                "Parqueadero-Garaje" => "No",
                "No. de Plantas" => $imagenes->pisos,
                "Tiempo Construido" => $imagenes->antiguedad->name,
                "Semi-integral" => "Si",
                "Alcoba Servicio" => "No",
                "Baño Servicio" => "No",
                "Zona oficios" => "No",
                "Patio" =>  "No",
                "Baño Social" => "No",
                "Ascensor" => "No",
                "No. de Piso" => $imagenes->pisos,
                "Piscina" => "No",
                "Balcón" => "No",
                "Zonas Verdes" => "No"
            ],
            "contactPhone" => $imagenes->created_by->userdata->celular_movil,
            "contactEmail" => $imagenes->created_by->email,
            "contactAddress" => "",
            "contactWeb" => env('APP_URL'),
            "external_reference" => "",
            "customerId" => "352473",
            "operationType" => $tipo_negocio->codigo,
            "adviceType" => $tipo_inmueble->codigo,
            "images" => $img,
            "videos" => $imagenes->url_video,
            "geo_latitud" => $imagenes->latitud,
            "geo_longitud" => $imagenes->longitud,
            "portalconfigurator_id" => "3",
            "productId" => 173

        ];


        return ['array' => $array,  'portal' => $portal->id];
    }


    public static function getResponse($status, $data, $id)
    {

        $find_code = DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->first();

        if (!$find_code) {

            $data_cr = DB::table('portales_codigo_response')->insert([
                'id_portal' => $data['portal'],
                'id_inmueble' =>  $id,
                'codigo' => $status
            ]);

            if ($data_cr) {

                $find_p_url =  DB::table('portales_urls')->where('portal_id', $data['portal'])->where('inmueble_id', $id)->first();

                if (!$find_p_url) {
                    DB::table('portales_urls')->insert([
                        'portal_id' => $data['portal'],
                        'inmueble_id' => $id,
                        'url' => env('API_URL_PAIS') . "aviso/$status"
                    ]);

                    return self::statePublication($data, $id, "publicado");
                } else {
                    return "Ya se registro la url inmueble";
                }
            } else {
                return 'Error al insertar el codigo del inmueble';
            }
        }

        return "Ya se publico el inmueble";
    }



    public static function statePublication($data, $id, $state)
    {
        $message = '';
        $find_state = DB::table('portales_state_inmuebles')
            ->where('id_portal', $data['portal'])->where('id_inmueble', $id)->first();

        if ($find_state) {
            $update = DB::table('portales_state_inmuebles')->where('id', $find_state->id)->update([
                'id_portal' => $data['portal'],
                'id_inmueble' =>  $id,
                'state' => $state
            ]);

            if ($update) {
                $message = 'Se actualzo el estado del inmueble';
            }
        } else {
            $insert = DB::table('portales_state_inmuebles')->insert([
                'id_portal' => $data['portal'],
                'id_inmueble' =>  $id,
                'state' => $state
            ]);

            if ($insert) {
                $message = 'Se inserto el estado del inmueble';
            }
        }

        return $message;
    }

    public static function getPortalCodigoPublication($id)

    {
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%clasificado%")->first();

        $find_code = DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal', $portal->id)->first();

        return ['portal' => $portal->id, 'codigo' => $find_code->codigo];
    }
}
