<?php

namespace App\Http\Controllers\API;

use App\Models\Agenda;
use App\Models\AgendaEstadoCita;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AgendaController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $auth = Auth::user();

        if ($request->exists('calendar')) {
            if (count($request->calendar) > 0) {
                $data['agenda']  = Agenda::with("t_cita", "cliente_id", "inmueble_id")
                    ->where("user_id", $auth->id)
                    ->whereIn('t_cita', $request->calendar)
                    ->get();
            }
        } else {
            $data['agenda'] = Agenda::with("t_cita", "cliente_id",  "inmueble_id")->where("user_id", $auth->id)->get();
        }

        if (count($data) > 0) {
            return $this->sendResponse($data, "Listado de eventos");
        }

        return $this->sendResponse($data, "No hay eventos registrados por el momento");
    }

    public function getTodayAgenda()
    {

        $date = Agenda::all();
        $data['agenda'] = [];

        for ($i = 0; $i < count($date); $i++) {
            if (Carbon::createFromFormat('Y-m-d H:i:s', $date[$i]["end"])->format('d-m-Y') == date('d-m-Y')) {
                array_push($data['agenda'], $date[$i]);
            }
        }

        if (count($data['agenda']) > 0) {
            return $this->sendResponse($data, "Listado de hoy");
        }

        return $this->sendResponse($data, "No hay eventos registrados por el momento");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $auth = Auth::user();
        try {
            $data = Agenda::create([
                "start" => Carbon::parse($request->event['start']),
                "end" =>  Carbon::parse($request->event['end']),
                "t_cita" => $request->event['extendedProps']['t_cita']['id'],
                "cliente_id" => $request->event['extendedProps']['cliente_id']['id'],
                "inmueble_id" => $request->event['extendedProps']['inmueble_id']['id'],
                "nota" => array_key_exists('nota', $request->event['extendedProps']) ? $request->event['extendedProps']['nota'] : '',
                "direccion" =>  array_key_exists('direccion', $request->event['extendedProps']) ? $request->event['extendedProps']['direccion'] : '',
                "user_id" => $auth->id,
                "created_by" => $auth->id,
                "updated_by" => $auth->id,

            ]);

            return $this->sendResponse($data, "Se guardo bien!");
        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $auth = Auth::user();

        $agenda = Agenda::where("id", $id)->update([
            "start" =>  Carbon::parse($request->event['start']),
            "end" =>  Carbon::parse($request->event['end']),
            "t_cita" => $request->event['extendedProps']['t_cita']['id'],
            "cliente_id" => $request->event['extendedProps']['cliente_id']['id'],
            "inmueble_id" => $request->event['extendedProps']['inmueble_id']['id'],
            "nota" => $request->event['extendedProps']['nota'],
            "direccion" => $request->event['extendedProps']['direccion'],
            "updated_by" => $auth->id,
        ]);
        if ($agenda) {
            $data['agenda'] = Agenda::where("id", $id)->first();

            return $this->sendResponse($data, "Se Actualizo correctamente!");
        } else {
            return $this->sendError($agenda, "error!");
        }
    }

    public function getCitasForInmuebles( $id ) {
        $data = Agenda::where('inmueble_id', $id)->get();
        return $this->sendResponse($data, "Listado reporte citas");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Agenda::where("id", $id)->delete();
        return $this->sendResponse($data, "Eliminado");
    }

    public function getTiposCitas()
    {
        $data["tipo_citas"] = DB::table("tipo_citas")->orderBy("label")->get();

        return $this->sendResponse($data, "listado de tipos de citas");
    }


    public function getStateAgendCita()
    {
        $data['agendas_estado_citas'] = DB::table('agendas_estado_citas')->orderBy("name")->get();

        return $this->sendResponse($data, "listado de estados de citas");
    }

    public function getStateAgendCitaForCita($id)
    {

        $estado = AgendaEstadoCita::where('id_agenda', $id)->first();
        if ($estado) {

            $estado['state_cita'] = DB::table('agendas_estado_citas')->where("id", $estado->state_cita)->first();
            $data['estado'] = $estado;

            return $this->sendResponse($data, "Estado agenda");
            
        } else {

            return $this->sendError($estado, "No encontrado");
            
        }
    }

    public function registrarStateAgendCita(Request $request)
    {

        $array = [
            "id_agenda" => $request->id_agenda,
            "state_cita" => $request->state_cita['id'],
            "comentario_cita" => $request->comentario_cita
        ];

        $estado = AgendaEstadoCita::where("id", $request->id)->first();

        if ($estado) {
            $data = AgendaEstadoCita::where("id", $request->id)->update($array);
            if ($data) {
                return $this->sendResponse($data, "Se actualizo correctamente");
            } else {
                return $this->sendError($data, "Error al actualizar el inmueble", 500);
            }
        } else {

            $data = AgendaEstadoCita::create($array);
            if ($data) {
                return $this->sendResponse($data, "Se registro correctamente");
            } else {
                return $this->sendError($data, "Error al registrar el inmueble");
            }
        }
    }

    public function updatedStateAgendCita(Request $request, $id)
    {
        $array = [
            "id_agenda" => $request->agenda_id,
            "state_cita" => $request->state_cita['id'],
            "comentario_cita" => $request->comentario_cita
        ];

        $estado = AgendaEstadoCita::where("id", $id)->first();

        if ($estado) {
            $data = AgendaEstadoCita::where("data", $id)->update($array);
            if ($data) {
                return $this->sendResponse($data, "Se actualizo correctamente");
            } else {
                return $this->sendError($data, "Error al actualizar el inmueble", 500);
            }
        } else {
            $data = AgendaEstadoCita::create($array);
            if ($data) {
                return $this->sendResponse($data, "Se registro correctamente");
            } else {
                return $this->sendError($data, "Error al registrar el inmueble");
            }
        }
    }
}
