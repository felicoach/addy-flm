<?php

namespace App\Http\Controllers\API;

use App\Models\Perfiles;
use App\Models\UserPerfiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image as Image;
use Cloudinary;


class ApiPerfilesController extends ApiController
{
    public function store(Request $request)
    {

        $auth = Auth::user();

       // dd($request->all());

        $path='';


        $file = $request->file('files');

        $uploadedFileUrl = Cloudinary::upload($file[0]->getRealPath())->getSecurePath();

    #    $filename = $file[0]->getClientOriginalName();
     #   $filename = str_replace(' ', '', $filename);

     #   $filename=time().$filename;
      #  $path = $file[0]->storeAs('paginaweb/'.$auth->id.'/templates', $filename, 'public');
        $path = $uploadedFileUrl;

     


        $data = Perfiles::create([
            "id_user" => $auth->id,
            "nombre" => $request->nombre,
            "descripcion" => $request->descripcion,
            "imagen" => $path,
            "url" => $request->url,
            "posicion" => $request->posicion
        ]);

        if ($data) {
            return $this->sendResponse($data, "Registro exitoso");
        }
    }


    public function upd(Request $request)
    {

        $auth = Auth::user();

        #dd($request->all());

        $path='';

        $perfil=Perfiles::where('id', $request->id)->first();

        if(!is_null($request->file('files'))){

            $file = $request->file('files');
            $uploadedFileUrl = Cloudinary::upload($file[0]->getRealPath())->getSecurePath();

           # $filename = $file[0]->getClientOriginalName();
          #  $filename = str_replace(' ', '', $filename);
            $path = $uploadedFileUrl;

            

            $perfil->update([
                "imagen" => $path,
            ]);

        }

        $perfil->update([
            "id_user" => $auth->id,
            "nombre" => $request->nombre,
            "descripcion" => $request->descripcion,
            "url" => $request->url,
            "posicion" => $request->posicion
        ]);

        if ($template) {
            return $this->sendResponse($template, "El registro de actualizo exitosamente");
        }
    }


    public function del(Request $request)
    {

        $auth = Auth::user();



        $p=Perfiles::where('id', $request->id)->first();

        if(isset($p->id)){

            $p->delete();

        }

        return $this->sendResponse($t, "Registro exitoso");
    }



    public function list()
    {

        $auth = Auth::user();

        $p=USerPerfiles::where('id_user', $auth->id)->first();

        $perfiles=Perfiles::get();

        if(isset($p->id)){

            foreach($perfiles as $per){

                if($per->id == $p->id_template){

                    $per->selected=1;

                }else{

                    $per->selected=0;

                }

            }
    
        }else{

            foreach($perfiles as $per){
                    $per->selected=0;
            }
        }

       
        $data["perfiles"] = $perfiles;

        return $this->sendResponse($data, "Listado de Perfiles");
    }


    public function settemplate($id)
    {

        $auth = Auth::user();

        $p=UserPerfiles::where('id_user', $auth->id)->delete();

        UserPerfiles::create([
                'id_user'=>$auth->id,
                'id_template'=>$id,
            ]);

        $t=UserPerfiles::where('id_user', $auth->id)->first();

       # dd($t);
        $perfiles=Perfiles::get();

        if(isset($p->id)){

            foreach($perfiles as $per){

                if($per->id == $p->id_perfil){

                    $per->selected=1;

                }else{

                    $per->selected=0;

                }

            }
    
        }else{

            foreach($perfiles as $tem){
                    $tem->selected=0;
            }
        }

       
        $data["perfiles"] = $perfiles;

        return $this->sendResponse($data, "Listado de perfiles");
    }




    public function get(Request $request)
    {
        $auth = Auth::user();

        $perfiles=Perfiles::where('id', $request->id)->first();
        
        $data["perfiles"] = $template;

        return $this->sendResponse($data, "Listado de perfiles");
    }


    public function index()
    {
       
        $data["perfiles"] = Perfiles::get();

        return $this->sendResponse($data, "Listado de perfiles");
    }



}
