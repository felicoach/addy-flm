<?php

namespace App\Http\Controllers\API;

use App\Models\Tareas;
use App\Models\EtiquetasCrm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CrmController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $auth = Auth::user();
        $descriptio_consult = "user_id";

        $role = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

        if ($role->slug != "inmobiliaria") {
            $descriptio_consult = "assignee";
        }

        $task = Tareas::with('assignee', 'created_by', 'inmueble')
            ->where($descriptio_consult, $auth->id)
            ->get();

        // return $task;

        $task_json = json_decode(json_encode($task));

        for ($i = 0; $i < count($task_json); $i++) {
            $task_json[$i]->tags = [];
            $task_etiquetas = DB::table("task_etiquetas")->where("task_id", $task_json[$i]->id)->get();
            $task_etiquetas_json = json_decode(json_encode($task_etiquetas));

            for ($j = 0; $j < count($task_etiquetas_json); $j++) {
                $etiquetas = DB::table("etiquetas")->where("id", $task_etiquetas_json[$j]->etiqueta_id)->first();

                array_push(
                    $task_json[$i]->tags,
                    $etiquetas
                );
            }
        }

        return $this->sendResponse($task_json, "task");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $auth = Auth::user();

        $crm = new Tareas();

        $crm->title = $request->title;
        $crm->dueDate =  Carbon::parse($request->dueDate);
        $crm->user_id = $auth->id;
        $crm->created_by = $request->created_by;
        $crm->updated_by = $request->updated_by;
        $crm->assignee = $request->assignee == null ? null : $request->assignee["id"];
        $crm->inmueble = $request->inmueble == null ? null : $request->inmueble["id"];
        $crm->isCompleted = $request->isCompleted;
        $crm->description = $request->description;
        $crm->isDeleted = $request->isDeleted;
        $crm->isImportant = $request->isImportant;
        $crm->save();


        if ($crm) {
            for ($i = 0; $i < count($request->tags); $i++) {
                DB::table("task_etiquetas")->insert(
                    ['etiqueta_id' => $request->tags[$i]["id"], 'task_id' => $crm->id],
                );
            }

            return response()->json([
                "success" => true,
                "message" => "Creados correctamente",
                "data" => $crm
            ], 200);
        } else {
            return response()->json([
                "success" => true,
                "message" => $crm,
            ], 500);
        }
    }

    public function getEtiquetas()
    {
        $auth = Auth::user();

        $data["etiquetas"] = DB::table("etiquetas")->where("type", "public")
            ->orWhere("created_by", $auth->id)->get();

        return $this->sendResponse($data, "Listado de etiquetas");
    }

    public function addEtiquetas(Request $request)
    {
        //   return $request;
        $auth = Auth::user();

        $etiquetas = DB::table("etiquetas")->insert(
            ['label' => $request->label, 'value' => strtolower(str_replace(' ', '-',  $request->label)), 'created_by' => $auth->id],
        );

        if ($etiquetas) {
            return $this->sendResponse($etiquetas, "Regiostro la etiqueta");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $crm =  Tareas::with('assignee')
            ->whereId($id)
            ->get();

        if (count($crm) != 0) {
            return response()->json([
                "success" => true,
                "message" => "referido",
                "data" => $crm
            ], 200);
        } else {
            return response()->json([
                "success" => true,
                "message" => "La tarea no se encuentra registrada en la base de datos",
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $crm = Tareas::find($id);
        $auth = Auth::user();


        if ($crm) {
            $crm->title = $request->title;
            $crm->dueDate = $request->dueDate;
            $crm->updated_by = $auth->id;

            $crm->assignee = $request->assignee == null ? null : $request->assignee["id"];
            $crm->inmueble = $request->inmueble == null ? null : $request->inmueble["id"];
            $crm->isCompleted = $request->isCompleted;
            $crm->description = $request->description;
            $crm->isDeleted = $request->isDeleted;
            $crm->isImportant = $request->isImportant;

            $crm->save();

            if ($crm) {

                DB::table("task_etiquetas")->where("task_id", $crm->id)->delete();

                for ($i = 0; $i < count($request->tags); $i++) {

                    $etiquetas = DB::table("etiquetas")->where("id", $request->tags[$i]['id'])->first();

                    DB::table("task_etiquetas")->insert(
                        [
                            'etiqueta_id' => $etiquetas->id,
                            'task_id' => $crm->id
                        ],
                    );
                }
            }


            return response()->json([
                "success" => true,
                "message" => "Se actualizo correctamente",
                "data" => $crm
            ], 200);
        } else {

            return response()->json([
                "success" => true,
                "message" => "La tarea no se encuentra registrada en la base de datos",
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crm = Tareas::find($id);
        if ($crm) {
            Tareas::where('id', $id)->update(['isDeleted' => true]);

            $crm = Tareas::find($id);

            if ($crm->isDeleted == 1) {
                return response()->json([
                    "success" => true,
                    "message" => "Se elimino correctamente",
                ], 200);
            } else {
                return response()->json([
                    "success" => true,
                    "message" => "Error al eliminar la tarea" . $id,
                    "error" => $crm
                ], 500);
            }
        } else {
            return response()->json([
                "success" => true,
                "message" => "La tarea no se encuentra registrada en la base de datos",
            ], 404);
        }
    }

    public function getEtiquetasCrm($id_user)
    {

        $data = EtiquetasCrm::where("id_user", $id_user)->get();

        return response()->json([
            "success" => true,
            "message" => "listado de etiquetas",
            "data" => $data
        ], 200);
    }

    public function addEtiquetasCrm(Request $request)
    {


        $data = new EtiquetasCrm();

        $data->id_user = $request->user_id;
        $data->nombre = $request->titulo;
        $data->save();

        return response()->json([
            "success" => true,
            "message" => "Guardar etiquetas",
            "data" => $data
        ], 200);
    }
}
