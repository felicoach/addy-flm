<?php

namespace App\Http\Controllers\API;

use App\Models\Module;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ModuleController extends ApiController
{
    public function getModules()
    {
        $data['modules'] = Permission::with('action')
            ->whereNotExists(function ($q) {
                $q->where('subject', "all");
            })->get();

        return $this->sendResponse($data, "Listado de modulos");
    }
}
