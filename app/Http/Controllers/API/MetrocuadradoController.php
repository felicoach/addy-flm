<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Inmuebles;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class MetrocuadradoController extends ApiController
{

    public function dataApiRestMetrocuadrado($id)
    {
        $this->getToken();

        $data = $this->getInfo($id);

        $client = new Client();
        $res = $client->request('POST', env('API_KEY_METROCUADRADO') . 'rest-api/realestate/publish', [
            'json' => $data['array'],
            'headers' => [
                'x-api-key' =>  $data['credenciales']->xapikey,
                'token' => $data['token']
            ]
        ]);

        if ($res->getStatusCode() == 200) {
            $data_res = json_decode($res->getBody());

       
            $code = strval($data_res->transactionId);

            $client = new Client();

            sleep(7);

            $res1 = $client->request('GET', env('API_KEY_METROCUADRADO') . "rest-api/transactions/$code", [
                'headers' => [
                    'x-api-key' =>  $data['credenciales']->xapikey,
                    'token' => $data['token']
                ]
            ]);

      

            if ($res1->getStatusCode() == 200) {
               
                $data_res1 = json_decode($res1->getBody());

                $data = $this->getResponse($data_res1, $data, $id);

                return $this->sendResponse($data, "Se publico el inmueble");
            }
        } else {
            return $this->sendError(json_decode($res->getBody()), "Error en el servidor");
        }



        // return $this->getResponse($status, $data, $id);
    }

    public function updateApiCiencuadra($id)
    {
        $this->getToken();

        $data = $this->getInfo($id);

        $pcr = DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->first();

        if ($pcr) {

            $data['array']["realEstateId"] = $pcr->codigo;

            $client = new Client();
            $res = $client->request('PUT', env('API_KEY_METROCUADRADO') . 'rest-api/realestate/update', [
                'json' => $data['array'],
                'headers' => [
                    'x-api-key' =>  $data['credenciales']->xapikey,
                    'token' => $data['token']
                ]
            ]);

            if ($res->getStatusCode() == 200) {

                $data_res = json_decode($res->getBody());

                sleep(7);

                $res1 = $client->request('GET', env('API_KEY_METROCUADRADO') . "rest-api/transactions/$data_res->transactionId", [
                    'headers' => [
                        'x-api-key' =>  $data['credenciales']->xapikey,
                        'token' => $data['token']
                    ]
                ]);

                if ($res->getStatusCode() == 200) {

                    $data_res1 = json_decode($res1->getBody());

                    return $this->sendResponse($data, $data_res1->messageList[0]->message);
                }

                return $this->sendResponse($data, "Se actualizo el inmueble en Metrocuadrado");
            } else {
                return $this->sendError($data, "Error al actualizar el inmueble en metrocuadrado");
            }
        }
    }


    public function despublicarMetrocuadrado($id)
    {
        $this->getToken();

        $data = $this->getInfo($id);

        $pcr = DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->first();

        if ($pcr) {

            $client = new Client();
            $res = $client->request('PATCH', env('API_KEY_METROCUADRADO') . 'rest-api/realestate/unpublish', [
                'json' => [
                    "realEstateId" => $pcr->codigo,
                    "responseUrl" => env('API_KEY_METROCUADRADO')
                ],
                'headers' => [
                    'x-api-key' =>   $data['credenciales']->xapikey,
                    'token' => $data['token']
                ]
            ]);

            if ($res->getStatusCode() == 200) {

                $data_res = json_decode($res->getBody());

                DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->delete();
                DB::table('portales_state_inmuebles')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->delete();

                sleep(7);

                $res1 = $client->request('GET', env('API_KEY_METROCUADRADO') . "rest-api/transactions/$data_res->transactionId", [
                    'headers' => [
                        'x-api-key' =>  $data['credenciales']->xapikey,
                        'token' => $data['token']
                    ]
                ]);

                $data_res1 = json_decode($res1->getBody());

                if ($res1->getStatusCode() == 200) {
                    DB::table('portales_urls')->where('inmueble_id', $id)->where('portal_id', $data['portal'])->delete();

                    return $this->sendResponse($data_res1,  $data_res1->messageList[0]->message);
                } else {
                    return $this->sendResponse($data_res1, "Error al obtener la respuesta del inmueble");
                }
            } else {
                return $this->sendError($res->getBody(), "error al despublicar el inmueble");
            }
        }
    }

    public static function getResponse($status, $data, $id)
    {

        $find_code = DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->first();

        if (!$find_code) {

            $data_cr = DB::table('portales_codigo_response')->insert([
                'id_portal' => $data['portal'],
                'id_inmueble' =>  $id,
                'codigo' => $status->resourceId
            ]);

            if ($data_cr) {

                $find_p_url =  DB::table('portales_urls')->where('portal_id', $data['portal'])->where('inmueble_id', $id)->first();

                if (!$find_p_url) {
                    DB::table('portales_urls')->insert([
                        'portal_id' => $data['portal'],
                        'inmueble_id' => $id,
                        'url' => env('API_RESPONSE_CODE_METROCUADRADO') . $status->resourceId
                    ]);

                    return self::statePublication($data, $id, "publicado");
                } else {
                    return "Ya se registro la url inmueble";
                }
            } else {
                return 'Error al insertar el codigo del inmueble';
            }
        }

        return "Ya se publico el inmueble";
    }

    public static function statePublication($data, $id, $state)
    {
        $message = '';
        $find_state = DB::table('portales_state_inmuebles')
            ->where('id_portal', $data['portal'])->where('id_inmueble', $id)->first();

        if ($find_state) {
            $update = DB::table('portales_state_inmuebles')->where('id', $find_state->id)->update([
                'id_portal' => $data['portal'],
                'id_inmueble' =>  $id,
                'state' => $state
            ]);

            if ($update) {
                $message = 'Se actualzo el estado del inmueble';
            }
        } else {
            $insert = DB::table('portales_state_inmuebles')->insert([
                'id_portal' => $data['portal'],
                'id_inmueble' =>  $id,
                'state' => $state
            ]);

            if ($insert) {
                $message = 'Se inserto el estado del inmueble';
            }
        }

        return $message;
    }

    public static function getToken()
    {

        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%metro%")->first();


        $find = DB::table('portales_credentials_metro_cuadrado')
            ->where('user_id', $aut_user)
            ->first();


        $client = new Client();
        $res = $client->request('POST', 'https://' . env('METROCUADRADO') . '.b2clogin.com/' . env('METROCUADRADO') . '.onmicrosoft.com/oauth2/v2.0/token?p=b2c_1_ropc_iniciar_sesion', [
            'form_params' => [
                'client_id' => $find->client_id,
                'scope' => "openid $find->client_id offline_access",
                'response_type' => 'id_token',
                'grant_type' => 'password',
                'username' => $find->username,
                'password' => $find->password
            ]
        ]);

        $data = json_decode($res->getBody());

        $if_exits_pt = DB::table('portales_token')->where("portal_id", $portal->id)->where("user_id", $aut_user)->first();

        if ($if_exits_pt) {
            DB::table('portales_token')->where("portal_id", $portal->id)->where("user_id", $aut_user)->update([
                "token" => $data->access_token,
                "created_by" => $auth->id,
                "updated_at" => date('Y-m-d H:i:s')
            ]);
        } else {
            DB::table('portales_token')->insert([
                "portal_id" => $portal->id,
                "created_by" => $aut_user,
                "user_id" => $aut_user,
                "token" => $data->access_token,
                "created_at" =>  date('Y-m-d H:i:s')
            ]);
        }
    }




    public function getDepartamento()
    {
        try {
            $find = $this->getCredentials();

            $client = new Client();

            $res = $client->request('GET', env('API_KEY_METROCUADRADO') . 'rest-catalogue/regions/', [
                'headers' => [
                    'x-api-key' =>  $find->xapikey,
                ]
            ]);

            $data_res = json_decode($res->getBody());

            $portal = DB::table("portales")->where('portales.name', 'LIKE', "%metro%")->first();

            if (count($data_res) > 0) {

                for ($i = 0; $i < count($data_res); $i++) {

                    $find_state = DB::table('states')->where('country_id', 48)->where('name', 'LIKE', "%" . $data_res[$i]->name . "%")->first();

                    if ($find_state) {

                        $find = DB::table('portales_states')->where('state_id',  $find_state->id)->where('portale_id', $portal->id)->first();
                        $find_object = json_decode(json_encode($find));

                        if (!$find) {
                            DB::table('portales_states')->insert(
                                ['state_id' => $find_state->id, 'portale_id' => $portal->id, 'codigo' => $data_res[$i]->id],
                            );
                        } else {
                            DB::table('portales_states')->where('id', $find_object->id)->update(
                                ['codigo' => $data_res[$i]->id],
                            );
                        }
                    }
                }
            }
        } catch (\Throwable $th) {
            return $th;
        }
    }


    public function getCiudades()
    {
        try {
            $find = $this->getCredentials();
            $xapikey = $find->xapikey;
            $find_state = DB::table('states')->where('country_id', 48)->get();
            $portal = DB::table("portales")->where('portales.name', 'LIKE', "%metro%")->first();

            for ($st = 0; $st < count($find_state); $st++) {

                $find_portal_state = DB::table('portales_states')->where('state_id', $find_state[$st]->id)->where('portale_id', $portal->id)->first();

                if ($find_portal_state) {

                    $client = new Client();

                    $res = $client->request('GET', env('API_KEY_METROCUADRADO') . 'rest-catalogue/cities/', [
                        'query' => [
                            'regionId' => intval($find_portal_state->codigo)
                        ],
                        'headers' => [
                            'x-api-key' => $xapikey,
                        ]
                    ]);

                    $data_res = json_decode($res->getBody());

                    if (count($data_res) > 0) {

                        for ($i = 0; $i < count($data_res); $i++) {

                            $find_cities = DB::table('cities')->where('state_id', $find_state[$st]->id)->where('name', 'LIKE', "%" . $data_res[$i]->name . "%")->first();

                            if ($find_cities) {

                                $find = DB::table('portales_cities')
                                    ->where('city_id',  $find_cities->id)
                                    ->where('portale_id', $portal->id)->first();

                                if (!$find) {

                                    DB::table('portales_cities')->insert(
                                        ['city_id' => $find_cities->id, 'portale_id' => $portal->id, 'codigo' => $data_res[$i]->id],
                                    );
                                } else {
                                    DB::table('portales_cities')->where('id', $find->id)->update(
                                        ['codigo' => $data_res[$i]->id],
                                    );
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function getZonas()
    {
        ini_set('max_execution_time', 120); // 5 minutes

        try {
            $client = new Client();
            $find = $this->getCredentials();

            $find_state = DB::table('states')->where('country_id', 48)->get();
            $portal = DB::table("portales")->where('portales.name', 'LIKE', "%metro%")->first();

            for ($st = 0; $st < count($find_state); $st++) {

                $find_cities = DB::table('cities')->where('state_id', $find_state[$st]->id)->get();

                for ($ct = 0; $ct < count($find_cities); $ct++) {

                    $find_portal_city = DB::table('portales_cities')->where('city_id', $find_cities[$ct]->id)->where('portale_id', $portal->id)->first();

                    if ($find_portal_city) {


                        $res = $client->request('GET', env('API_KEY_METROCUADRADO') . 'rest-catalogue/zones/', [
                            'query' => [
                                'cityId' => intval($find_portal_city->codigo)
                            ],
                            'headers' => [
                                'x-api-key' =>  $find->xapikey,
                            ]
                        ]);

                        $data_res = json_decode($res->getBody());


                        if (count($data_res) > 0) {

                            for ($i = 0; $i < count($data_res); $i++) {

                                $find_zona = DB::table('zona')->where('ciudad_id', $find_portal_city->id)->where('name', 'LIKE', "%" . $data_res[$i]->name . "%")->first();

                                if ($find_zona) {

                                    $find = DB::table('portales_zona')->where('zona_id',  $find_zona->id)->where('portale_id', $portal->id)->first();

                                    if (!$find) {
                                        DB::table('portales_zona')->insert(
                                            ['zona_id' => $find_zona->id, 'portale_id' => $portal->id, 'codigo' => $data_res[$i]->id],
                                        );
                                    } else {
                                        DB::table('portales_zona')->where('id', $find->id)->update(
                                            ['codigo' => $data_res[$i]->id],
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public static function getCredentials()
    {
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $find = DB::table('portales_credentials_metro_cuadrado')
            ->where('user_id', $aut_user)
            ->first();

        return $find;
    }


    public function getTiposInmuebles()
    {

        try {

            $find = $this->getCredentials();

            $client = new Client();

            $res = $client->request('GET', env('API_KEY_METROCUADRADO') . 'rest-catalogue/realestatetypes/', [
                'headers' => [
                    'x-api-key' =>  $find->xapikey,
                ]
            ]);

            $data_res = json_decode($res->getBody());

            $portal = DB::table("portales")->where('portales.name', 'LIKE', "%metro%")->first();

            if (count($data_res) > 0) {

                for ($i = 0; $i < count($data_res); $i++) {

                    $find_tinm = DB::table('inmuebles_tipo_inmuebles')->where('tipo', 'LIKE', "%" . $data_res[$i]->text . "%")->first();

                    if ($find_tinm) {

                        $find = DB::table('portales_tipo_inmuebles')->where('tI_id',  $find_tinm->id)->where('portale_id', $portal->id)->first();

                        if (!$find) {
                            DB::table('portales_tipo_inmuebles')->insert(
                                ['tI_id' => $find_tinm->id, 'portale_id' => $portal->id, 'codigo' => $data_res[$i]->id],
                            );
                        } else {
                            DB::table('portales_tipo_inmuebles')->where('id', $find_tinm->id)->update(
                                ['codigo' => $data_res[$i]->id],
                            );
                        }
                    }
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function getTipoNegocio()
    {

        try {

            $find = $this->getCredentials();

            $client = new Client();

            $res = $client->request('GET', env('API_KEY_METROCUADRADO') . 'rest-catalogue/businesstypes/', [
                'headers' => [
                    'x-api-key' =>  $find->xapikey,
                ]
            ]);

            $data_res = json_decode($res->getBody());


            $portal = DB::table("portales")->where('portales.name', 'LIKE', "%metro%")->first();

            if (count($data_res) > 0) {

                for ($i = 0; $i < count($data_res); $i++) {

                    $find_tng = DB::table('inmuebles_tipo_negocio')->where('tipo', 'LIKE', "%" . $data_res[$i]->text . "%")->first();

                    if ($find_tng) {

                        $find = DB::table('portales_inmuebles_tipo_negocio')->where('tipo_negocio_id',  $find_tng->id)->where('portale_id', $portal->id)->first();

                        if (!$find) {
                            DB::table('portales_inmuebles_tipo_negocio')->insert(
                                ['tipo_negocio_id' => $find_tng->id, 'portale_id' => $portal->id, 'codigo' => $data_res[$i]->id],
                            );
                        } else {
                            DB::table('portales_inmuebles_tipo_negocio')->where('id', $find_tng->id)->update(
                                ['codigo' => $data_res[$i]->id],
                            );
                        }
                    }
                }
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }




    public static function getInfo($id)
    {
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $portal = DB::table("portales")->where('portales.slug', 'LIKE', "%metro_cuadrado%")->first();

        $find = DB::table('portales_credentials_metro_cuadrado')
            ->where('user_id', $aut_user)
            ->first();

        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'created_by',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'antiguedad',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento'
        )->where("id", $id)
            ->first();

        $imagenes = json_decode(json_encode($inmuebles));

        $tipo_inmueble = DB::table('portales_tipo_inmuebles')
            ->where('portale_id', $portal->id)
            ->where('tI_id', $imagenes->tipo_inmueble->id)
            ->first();

        $tipo_negocio = DB::table('portales_inmuebles_tipo_negocio')
            ->where('portale_id', $portal->id)
            ->where('tipo_negocio_id', $imagenes->tipo_negocio->id)
            ->first();

        $ciudad = DB::table('portales_cities')
            ->where('portale_id', $portal->id)
            ->where('city_id', $imagenes->ciudad_id->id)
            ->first();

        $consult = DB::table('portales_credenciales')->where(
            'user_id',
            $aut_user
        )->where('portale_id', $portal->id)->first();

        if (!$consult) {
            $consult = DB::table('portales_credenciales')->where(
                'user_id',
                $auth->create_by
            )->where('portale_id', $portal->id)->first();
        }

        $token_p = DB::table('portales_token')->where('portal_id', $portal->id)
            ->where('user_id', $aut_user)->first();

        $precio = 0;
        $precio_admin = 0;

        if ($imagenes->tipo_negocio->tipo == "Alquiler o Arriendo") {
            $precio =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_alquiler));
            $precio_admin =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_alquiler));
        } else {
            $precio  =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_venta));
        }

        $img_array = [];

        for ($i = 0; $i < count($imagenes->inmueble_imagenes); $i++) {
            if (!isValidURL($imagenes->inmueble_imagenes[$i]->url)) {
                array_push($img_array, env('APP_URL') . "/storage/" . $imagenes->inmueble_imagenes[$i]->url);
            } else {
                array_push($img_array, $imagenes->inmueble_imagenes[$i]->url);
            }
        }

        $array = [
            "realEstateType" => $tipo_inmueble->codigo,
            "realEstateOffer" => $tipo_negocio->codigo,
            "price" => $precio,
            "integratorId" => 4820,
            "city" => $ciudad->codigo,
            "neighborhood" => $imagenes->barrio_id->name,
            "address" => $imagenes->direccion,
            "comments" => $imagenes->descripcion,
            "amenities" => [
                "rooms" => $imagenes->habitaciones,
                "bathrooms" => $imagenes->banos,
                "builtTime" => "Más de 20 años",
                "garages" => $imagenes->cantidad_parqueadero,
                "builtArea" => $imagenes->area_total,
                "area" => $imagenes->area_contruida,
                "lotArea" => $imagenes->area_lote,
                "dinningRoomType" => "Sala Comedor",
                "floorNumber" => $imagenes->pisos,
            ],
            "images" => $img_array,
            "stratum" => preg_replace("/[^0-9]/", "", $imagenes->estrato->name),
            "responseUrl" => env('APP_URL') . '/api',
        ];
        if ($imagenes->url_video != null || $imagenes->url_video != "") {
            $video = $imagenes->url_video;
            $array["video"] = $video;
        }

        return ['credenciales' => $find, 'array' => $array, 'token' => $token_p->token, 'portal' => $portal->id];
    }
}
