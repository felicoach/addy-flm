<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiProfileController extends Apicontroller
{
    public function getProfile() {

        $auth = Auth::user();

        $data['profile'] =  User::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
            'userdata.segundo_nombre',
            'userdata.primer_apellido',
            'userdata.segundo_apellido',
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.descripcion',
            'userdata.genero',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.id_estado',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente'
          )
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->where('users.email', $auth->email)
            ->first();
        
            return $this->sendData($data, "Perfil de usuario");
    }
}
