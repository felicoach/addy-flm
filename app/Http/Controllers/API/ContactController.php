<?php

namespace App\Http\Controllers\API;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactController extends ApiController
{
    public function store(Request $request)
    {

        $data = Contact::create([
            "user_id" => $request->user_id,
            "full_name" => $request->full_name,
            "email" => $request->email,
            "celular" => $request->celular,
            "message" => $request->message
        ]);

        if ($data) {
            return $this->sendResponse($data, "Registro exitoso");
        }
        
    }

    public function listContact()
    {
        $auth = Auth::user();
        
        $role = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

      /*  if ($role->slug == "agentes") {
            $user = $auth->id;
        } else {
            $user = $auth->create_by;
        }*/

        $data["contact"] = Contact::where("user_id", $auth->id)->get();

        return $this->sendResponse($data, "Listado de contactos");
    }
}
