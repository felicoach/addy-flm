<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Inmuebles;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PortalMercadoLibreController extends Controller
{

    public function publicarInmueble()
    {

        $client = new Client();

        $this->getTokenMercadoLibre();

        $rest = $client->request('GET', env('API_MERCADOLIBRE') . "items", [
            'headers' => [
                'Authorization' => 'Bearer ' . ''
            ]
        ]);

        $data_res = json_decode($rest->getBody());

        if ($rest->getStatusCode() == 200) {
        }
    }



    public static function getTokenMercadoLibre()
    {
        $auth = Auth::user();

        $aut_user = getUsersRoles($auth);

        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%mercado%")->first();

        $find = DB::table('portales_credentials_ciencuadra')
            ->where('user_id', $aut_user)
            ->first();

        $client = new Client();
        $res = $client->request('GET', env('API_KEY_CIENCUADRA') . 'login', [
            'json' => [
                'username' => $find->username,
                'password' => $find->password
            ]
        ]);

        $data = json_decode($res->getBody());

        $if_exits_pt = DB::table('portales_token')->where("portal_id", $portal->id)->first();

        if ($if_exits_pt) {
            $token = DB::table('portales_token')->where("portal_id", $portal->id)->update([
                "token" => $data->token,
                "created_by" => $auth->id
            ]);
        } else {
            $token = DB::table('portales_token')->insert([
                "portal_id" => $portal->id,
                "user_id" => $aut_user,
                "token" => $data->token
            ]);
        }
        return [$token, "token creado"];
    }


    public static function getInfo($id)
    {
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%cien%")->first();

        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'created_by',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'antiguedad',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento'
        )->where("id", $id)
            ->first();

        $requestedplatform = "mercado";

        $imagenes = json_decode(json_encode($inmuebles));


        $tipo_negocio = DB::table('portales')
            ->select('portales_inmuebles_tipo_negocio.codigo', 'portales.id')
            ->join('portales_inmuebles_tipo_negocio', 'portales.id', '=', 'portales_inmuebles_tipo_negocio.portale_id')
            ->where('portales.name', 'LIKE', "%{$requestedplatform}%")
            ->where('portales_inmuebles_tipo_negocio.tipo_negocio_id', $imagenes->tipo_negocio->id)
            ->first();


        $consult = DB::table('portales_credenciales')->where(
            'user_id',
            $aut_user
        )->where('portale_id', $portal->id)->first();

        if (!$consult) {
            $consult = DB::table('portales_credenciales')->where(
                'user_id',
                $auth->create_by
            )->where('portale_id', $portal->id)->first();
        }

        $token_p = DB::table('portales_token')->where('portal_id', $portal->id)
            ->where('user_id', $aut_user)->first();


        $ciudad = DB::table('portales')
            ->select('portales_cities.codigo')
            ->join('portales_cities', 'portales.id', '=', 'portales_cities.portale_id')
            ->where('portales.name', 'LIKE', "%{$requestedplatform}%")
            ->where('portales_cities.city_id', $imagenes->ciudad_id->id)
            ->first();

        $estratos = DB::table('portales_estratos_inmuebles')
            ->where('portale_id', $portal->id)
            ->where('estrato_id', $imagenes->estrato->id)
            ->first();

        $sellingPrice = 10000000;
        $leasingFee = 100000;

        if ($imagenes->tipo_negocio->tipo == "Alquiler") {
            $leasingFee =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_alquiler));
        } else {
            $sellingPrice  =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_venta));
        }

        $img_array = [];


        for ($i = 0; $i < count($imagenes->inmueble_imagenes); $i++) {
            array_push($img_array, array("url" =>  env('APP_URL') . "/storage/" . $imagenes->inmueble_imagenes[$i]->url, "type" => "I"));
        }

        $array = [
            "cityId" => intval($ciudad->codigo),
            "localityId" => intval($imagenes->barrio_id->local_id),
            "neighborhoodName" => $imagenes->barrio_id->name,
            "propertyTypeId" => 10,
            "transactionTypeId" => intval($tipo_negocio->codigo),
            "address" => $imagenes->direccion,
            "showAddress" => 1,
            "stratum" => intval($estratos->codigo),
            "propertyCode" => $imagenes->id,
            "latitude" =>  floatval($imagenes->latitud),
            "longitude" => floatval($imagenes->longitud),
            "sellingPrice" => intval($sellingPrice),
            "leasingFee" => intval($leasingFee),
            "additionalInfo" => $imagenes->descripcion,
            "status" => "A",
            "integrator" => "ADDY",
            "advisorName" => $imagenes->titulo_inmueble,
            "advisorPhone" => str_replace('+57', '',  str_replace(' ', '', $imagenes->created_by->userdata->celular_movil)),
            "features" => [
                "numBedRooms" => $imagenes->habitaciones,
                "numBathrooms" => $imagenes->banos,
                "numParking" => intval($imagenes->cantidad_parqueadero),
                "parkingType" => 1,
                "privatePool" => false,
                "antiquity" => 2,
                "propertyName" => $inmuebles->titulo_inmueble,
                "remodeled" => false,
                "project" => false,
                "projectName" => "",
                "administrationValue" => 0,
                "serviceRoom" => true,
                "serviceBathroom" => true,
                "laundryZone" => true,
                "floorTypes" => 8,
                "homeAppliances" => false,
                "chimney" => false,
                "airConditioner" => false,
                "terracesNumber" => "",
                "terraceArea" => "",
                "balconiesNumber" => 2,
                "depositsNumber" => 2,
                "lotUse" => 1,
                "floorsNumber" => 2,
                "apartmentsNumber" => 0,
                "elevatorsNumber" => 0,
                "vigilance" => 0,
                "numParkingVisitors" => 0,
                "reception" => false,
                "closedCircuitTv" => false,
                "electricPlant" => false,
                "communalLiving" => false,
                "childrenZone" => false,
                "greenZones" => false,
                "communalPool" => false,
                "gym" => false,
                "lotLocation" => 0,
                "socialVenue" => false,
                "brandNew" => false,
                "addressComplement" => "ninguna",
                "realestateLogo" => env('FRONT_APP') . "images/_/_/_/_/addy-flm/frontend/src/assets/images/logo/logo_addy.png",
                "furnished" => false,
                "inConstruction" => false,
                "builtArea" => $imagenes->area_contruida,
                "privateArea" => 450,
                "area" => $imagenes->area_total,
                "photospropertyData" => [
                    [
                        "url" => "https://palacioasesores.com/wp-content/uploads/2018/07/apartmento_turistico-1030x664.jpeg",
                        "type" => "I"
                    ],
                ]
            ]
        ];

        return ['array' => $array, 'token' => $token_p->token, 'portal' => $portal->id];
    }

    public function getCiudadesM()
    {
        $client = new Client();

        $rest = $client->request('GET', env('API_MERCADOLIBRE') . "classified_locations/countries");
        $data_res = json_decode($rest->getBody());

        if ($rest->getStatusCode() == 200) {

            $portal = DB::table("portales")->where('portales.name', 'LIKE', "%mercado%")->first();

            for ($i = 0; $i < count($data_res); $i++) {

                $find_state = DB::table('countries')->where('name', 'LIKE', "%" . $data_res[$i]->name . "%")->first();

                if ($find_state) {

                    $find = DB::table('portales_countrie')->where('countrie_id',  $find_state->id)->where('portale_id', $portal->id)->first();
                    $find_object = json_decode(json_encode($find));

                    if (!$find) {
                        DB::table('portales_countrie')->insert(
                            ['countrie_id' => $find_state->id, 'portale_id' => $portal->id, 'codigo' => $data_res[$i]->id],
                        );
                    } else {
                        DB::table('portales_countrie')->where('id', $find_object->id)->update(
                            ['codigo' => $data_res[$i]->id],
                        );
                    }
                }
            }
        } else {
            return $this->sendResponse("", "Error al obtener la respuesta del inmueble");
        }
    }

    public function getDepartamentoM()
    {
        $client = new Client();
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%mercado%")->first();

        $rest = $client->request('GET', env('API_MERCADOLIBRE') . "classified_locations/countries/CO");

        $data_res = json_decode($rest->getBody());

        if ($rest->getStatusCode() == 200) {

            for ($i = 0; $i <  count($data_res->states); $i++) {

                if ($rest->getStatusCode() == 200) {
                    $find_state = DB::table('states')->where('country_id', 48)->where('name', 'LIKE', "%" . $data_res->states[$i]->name . "%")->first();

                    if ($find_state) {

                        $find = DB::table('portales_states')->where('state_id',  $find_state->id)->where('portale_id', $portal->id)->first();
                        $find_object = json_decode(json_encode($find));

                        if (!$find) {
                            DB::table('portales_states')->insert(
                                ['state_id' => $find_state->id, 'portale_id' => $portal->id, 'codigo' => $data_res->states[$i]->id],
                            );
                        } else {
                            DB::table('portales_states')->where('id', $find_object->id)->update(
                                ['codigo' => $data_res->states[$i]->id],
                            );
                        }
                    }
                }
            }
        }
    }

    public function getCitiesM()
    {
        $client = new Client();
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%mercado%")->first();

        $data_state = DB::table('portales_states')->where('portale_id', $portal->id)->get();

        for ($dt = 0; $dt < count($data_state); $dt++) {

            $rest = $client->request('GET', env('API_MERCADOLIBRE') . "classified_locations/states/" . $data_state[$dt]->codigo);

            $data_res = json_decode($rest->getBody());

            if ($rest->getStatusCode() == 200) {

                for ($i = 0; $i <  count($data_res->cities); $i++) {

                    if ($rest->getStatusCode() == 200) {
                        $find_state = DB::table('cities')->where('name', 'LIKE', "%" . $data_res->cities[$i]->name . "%")->first();

                        if ($find_state) {

                            $find = DB::table('portales_cities')->where('city_id',  $find_state->id)->where('portale_id', $portal->id)->first();
                            $find_object = json_decode(json_encode($find));

                            if (!$find) {
                                DB::table('portales_cities')->insert(
                                    ['city_id' => $find_state->id, 'portale_id' => $portal->id, 'codigo' => $data_res->cities[$i]->id],
                                );
                            } else {
                                DB::table('portales_cities')->where('id', $find_object->id)->update(
                                    ['codigo' => $data_res->cities[$i]->id],
                                );
                            }
                        }
                    }
                }
            }
        }
    }


    public function getBarriosM()
    {
        sleep(10);

        $client = new Client();
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%mercado%")->first();

        $data_cities = DB::table('portales_cities')->where('portale_id', $portal->id)->get();

        for ($dc = 0; $dc < count($data_cities); $dc++) {

            $rest = $client->request('GET', env('API_MERCADOLIBRE') . "classified_locations/cities/" . $data_cities[$dc]->codigo);

            $data_res = json_decode($rest->getBody());

            if ($rest->getStatusCode() == 200) {


                for ($i = 0; $i <  count($data_res->neighborhoods); $i++) {

                    if ($rest->getStatusCode() == 200) {
                        $find_barrio = DB::table('barrio')->where('name', 'LIKE', "%" . $data_res->neighborhoods[$i]->name . "%")->first();

                        if ($find_barrio) {

                            $find = DB::table('portales_barrio')->where('barrio_id',  $find_barrio->id)->where('portale_id', $portal->id)->first();

                            if (!$find) {
                                DB::table('portales_barrio')->insert(
                                    ['barrio_id' => $find_barrio->id, 'portale_id' => $portal->id, 'codigo' => $data_res->neighborhoods[$i]->id],
                                );
                            } else {
                                DB::table('portales_barrio')->where('id', $find->id)->update(
                                    ['codigo' => $data_res->neighborhoods[$i]->id],
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}
