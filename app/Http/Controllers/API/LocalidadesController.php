<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Models\AddyCiudade;
use App\Models\AddyEstado;
use App\Models\AddyPaise;
use App\Models\Barrio;
use App\Models\City;
use App\Models\Country;
use App\Models\InmueblesCaracteristicasInternas;
use App\Models\State;
use App\Models\Zona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocalidadesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getPaisesforId($id)
    {

        $paises = Country::where('id', $id)->first();

        $data['pais'] = $paises;

        return $this->sendResponse($data, "Pais");
    }

    public function getPaises()
    {

        $data = Country::all();

        $data_json = json_decode(json_encode($data));

        for ($i = 0; $i < count($data_json); $i++) {

            $datas = DB::table('portales_countrie')
                ->where("countrie_id", $data_json[$i]->id)
                ->get();

            $data_json[$i]->portales_countrie = $datas;
        }

        $pais['paises'] = $data_json;

        return $this->sendResponse($pais, "Listado de paises");
    }

    public function getStates($id)
    {

        $estados = State::where('country_id', $id)->get();
        $data_json = json_decode(json_encode($estados));

        for ($i = 0; $i < count($data_json); $i++) {


            $states = DB::table('portales_states')
                ->where("state_id", $data_json[$i]->id)
                ->get();


            $data_json[$i]->portales_states = $states;
        }
        $data['estados'] = $data_json;

        return $this->sendResponse($data, "Listado de estados");
    }

    public function getState($id)
    {

        $estado = State::where('id', $id)->first();

        $data['estado'] = $estado;

        return $this->sendResponse($data, "estado");
    }


    public function getCiudades($id)
    {

        $ciudades = City::where('state_id', $id)->get();
        $data_json = json_decode(json_encode($ciudades));

        for ($i = 0; $i < count($data_json); $i++) {


            $states = DB::table('portales_cities')
                ->where("city_id", $data_json[$i]->id)
                ->get();


            $data_json[$i]->portales_cities = $states;
        }
        $data['ciudades'] = $data_json;

        return $this->sendResponse($data, "Listado de ciudades");
    }


    public function getZona($id)
    {
        $zona = Zona::where('ciudad_id', $id)->get();
        $data_json = json_decode(json_encode($zona));

        for ($i = 0; $i < count($data_json); $i++) {


            $zonas = DB::table('portales_zona')
                ->where("zona_id", $data_json[$i]->id)
                ->get();


            $data_json[$i]->portales_zona = $zonas;
        }
        $data['zonas'] = $data_json;

        return $this->sendResponse($data, "Listado de zonas");
    }


    public function getBarrio($id)
    {

        $barrio = Barrio::where('zona_id', $id)->get();
        $data_json = json_decode(json_encode($barrio));

        for ($i = 0; $i < count($data_json); $i++) {

            $zonas = DB::table('portales_barrio')
                ->where("barrio_id", $data_json[$i]->id)
                ->get();

            $data_json[$i]->portales_barrio = $zonas;
        }
        $data['barrios'] = $data_json;

        return $this->sendResponse($data, "Listado de barrios");
    }

    public function agregarZona(Request $request)
    {
        $zona = new Zona();

        $zona->name = $request->name;
        $zona->ciudad_id = $request->ciudad_id;

        $zona->save();

        $data['zona'] = $zona;

        if ($zona) {
            return $this->sendResponse($data, "Registro exitoso");
        }
    }


    public function agregarBarrio(Request $request)
    {
        $barrio = new Barrio();

        $barrio->name = $request->name;
        $barrio->zona_id = $request->zona_id;

        $barrio->save();

        $data['barrio'] = $barrio;

        if ($barrio) {
            return $this->sendResponse($data, "Registro exitoso");
        }
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
