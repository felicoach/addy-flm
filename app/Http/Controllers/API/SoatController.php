<?php

namespace App\Http\Controllers\API;

use App\Imports\BarriosImport;
use App\Imports\FincaraizDepartamentosImport;
use App\Imports\FincarraizBarriosImport;
use App\Imports\FincarraizCiudadesImport;
use App\Imports\FincarraizZonasImport;
use App\Imports\ZonasImport;
use App\Models\InmuebleExternaMany;
use App\Models\InmuebleInternaMany;
use App\Models\Inmuebles;
use App\Models\User;
use GuzzleHttp\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

use function GuzzleHttp\json_decode;

class SoatController extends ApiController
{
    public function actionService(Request $request)
    {
        return $this->dataSoatFincaRaiz($request->id);
    }

    public static function dataSoatFincaRaiz($id)
    {
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'created_by',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'antiguedad',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento'
        )->where("id", $id)
            ->first();

        $requestedplatform = "finca";

        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%finca%")->first();

        $imagenes = json_decode(json_encode($inmuebles));

        $c_internas = InmuebleInternaMany::with('caracteristicasInternas')
            ->where('inmueble', $imagenes->id)->get();

        $c_externas = InmuebleExternaMany::with('caracteristicasExternas')
            ->where('inmueble', $imagenes->id)->get();

        $imagenes->caracteristicas_internas = $c_internas;
        $imagenes->caracteristicas_externas = $c_externas;

        $ceIn = json_decode(json_encode($imagenes->caracteristicas_internas));
        $ceEx = json_decode(json_encode($imagenes->caracteristicas_externas));

        //
        // return $imagenes->tipo_inmueble->id_fincarraiz;

        $states = DB::table('portales_states')
            ->where('portale_id', $portal->id)
            ->where('state_id', $imagenes->estado_id->id)
            ->first();

        $tipo_negocio = DB::table('portales_inmuebles_tipo_negocio')
            ->where('portale_id', $portal->id)
            ->where('tipo_negocio_id', $imagenes->tipo_negocio->id)
            ->first();


        $state_fisico = DB::table('portales_inmuebles_estado_fisico')
            ->where('portale_id', $portal->id)
            ->where('state_fisico', $imagenes->state_fisico->id)
            ->first();

        $consult = DB::table('portales_credenciales')->where(
            'user_id',
            $aut_user
        )->where('portale_id', $portal->id)->first();

        if (!$consult) {
            $consult = DB::table('portales_credenciales')->where(
                'user_id',
                $auth->create_by
            )->where('portale_id', $portal->id)->first();
        }

        $ciudad = DB::table('portales_cities')
            ->where('portale_id', $portal->id)
            ->where('city_id', $imagenes->ciudad_id->id)
            ->first();


        $soapUrl = env('SOAP_URL'); // asmx URL of WSDL
        $soapUser = $consult->user;  //  username
        $soapPassword = $consult->token;
        $emailUsuario = $consult->email;

        // xml post structure

        $xml_post_string = '<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
        <soap12:Header>
            <AuthHeader xmlns="http://tempuri.org/">
                <UserName>' . $soapUser . '</UserName>
                <Password>' . $soapPassword . '</Password>
            </AuthHeader>
        </soap12:Header>
        <soap12:Body>
            <PublishAd_V8 xmlns="http://tempuri.org/">
                <EmailUsuario>' . $emailUsuario . '</EmailUsuario>
                <NroDeRef>' . $inmuebles->id . '</NroDeRef>
                <Barrio>' .  $imagenes->barrio_id->name . '</Barrio>
                <BarrioComun></BarrioComun>
                <Direccion>' . $inmuebles->direccion . '</Direccion>
                <Telefono1>' . str_replace('+57', '',  str_replace(' ', '', $imagenes->created_by->userdata->celular_movil)) . '</Telefono1>
                <Telefono2></Telefono2>
                <Telefono3></Telefono3>
                <Email>' . $imagenes->created_by->email . '</Email>
                <Email2></Email2>';

        if ($imagenes->tipo_negocio->tipo == "Alquiler") {
            $xml_post_string .=   '<Precio>' . str_replace('.', '',  str_replace(',', '', $inmuebles->precio_alquiler)) . '</Precio>';
            if ($inmuebles->precio_administracion != null ||  $inmuebles->precio_administracion != "") {
                '<PrecioAdm>' . str_replace('.', '', str_replace(',', '', $inmuebles->precio_administracion)) . '</PrecioAdm>';
            }
        } else {
            $xml_post_string .=   '<Precio>' . str_replace('.', '',  str_replace(',', '', $inmuebles->precio_venta)) . '</Precio>';
        }

        $xml_post_string .=  '<Area>' . $imagenes->area_total . '</Area>
                <Habitaciones>' . $imagenes->habitaciones . '</Habitaciones>
                <Banos>' . $imagenes->banos . '</Banos>
                <IdTipoInmueble>' . $imagenes->tipo_inmueble->id_fincarraiz . '</IdTipoInmueble>
                <IdNivelUbicacion1>' . $states->codigo . '</IdNivelUbicacion1>
                <IdNivelUbicacion2>' . $ciudad->codigo . '</IdNivelUbicacion2>
                <IdNivelUbicacion3>0</IdNivelUbicacion3>
                <IdNivelUbicacion4>0</IdNivelUbicacion4>
                <IdNivelUbicacion5>0</IdNivelUbicacion5>
                <IdTipoOferta>' . $tipo_negocio->codigo . '</IdTipoOferta>
                <IdEstadoConservacionInmueble>' . $state_fisico->codigo . '</IdEstadoConservacionInmueble>
                <IdAntiguedad>' . $imagenes->antiguedad->id_fincarraiz . '</IdAntiguedad>
                <IdNroPiso>0</IdNroPiso>
                <IdEstado>2</IdEstado>
                <Comentarios>' . $imagenes->descripcion . '</Comentarios>
                <Extras>
                   ';


        foreach ($ceIn as $value) {
            $caracteristicas_internas = DB::table('portales')
                ->select('portales_caracteristicas_intarnas.codigo', 'portales.id')
                ->join('portales_caracteristicas_intarnas', 'portales.id', '=', 'portales_caracteristicas_intarnas.portale_id')
                ->where('portales.name', 'LIKE', "%{$requestedplatform}%")
                ->where('portales_caracteristicas_intarnas.ctin_id', $value->caracteristicas_internas->id)
                ->first();

            if ($caracteristicas_internas) {
                $xml_post_string .=  '<objExtras>
                <Id>' . $caracteristicas_internas->codigo . '</Id>
                </objExtras>';
            }
        }


        foreach ($ceEx as $value) {

            $caracteristicas_externas = DB::table('portales')
                ->select('portales_caracteristicas_externas.codigo', 'portales.id')
                ->join('portales_caracteristicas_externas', 'portales.id', '=', 'portales_caracteristicas_externas.portale_id')
                ->where('portales.name', 'LIKE', "%{$requestedplatform}%")
                ->where('portales_caracteristicas_externas.ctex_id', $value->caracteristicas_externas->id)
                ->first();
            if ($caracteristicas_externas) {
                $xml_post_string .=  '<objExtras><Id>' . $caracteristicas_externas->codigo . '</Id></objExtras>';
            }
        }

        $xml_post_string .= '
                </Extras>
                <Imagenes>';

        for ($i = 0; $i < count($imagenes->inmueble_imagenes); $i++) {
            if (!isValidURL($imagenes->inmueble_imagenes[$i]->url)) {
                $xml_post_string .= '<objImagenes>
                    <URL>' . env('APP_URL') . '/storage/' . $imagenes->inmueble_imagenes[$i]->url . '</URL>
                    <tipoMultimedia>' . $i . '</tipoMultimedia>
                </objImagenes>';
            } else {
                $xml_post_string .= '<objImagenes>
                    <URL>' . $imagenes->inmueble_imagenes[$i]->url . '</URL>
                    <tipoMultimedia>' . $i . '</tipoMultimedia>
                </objImagenes>';
            }
        }
        $xml_post_string .= '</Imagenes>
                        <IdTipoClima>0</IdTipoClima>
                        <IdEstrato>' . $imagenes->estrato->id . '</IdEstrato>
                        <IdAddressViewType>2</IdAddressViewType>
                        <Latitud>' . $inmuebles->latitud . '</Latitud>
                        <Longitud>' . $inmuebles->longitud . '</Longitud>
                        <AreaPrivada>0</AreaPrivada>
                        <ParkingSpaces>0</ParkingSpaces>
                        <TelefonoWhatsapp>' . str_replace('+57', '',  str_replace(' ', '', $imagenes->created_by->userdata->celular_whatsapp)) . '</TelefonoWhatsapp>
                        <TelefonoClickToCall>' . str_replace('+57', '',  str_replace(' ', '', $imagenes->created_by->userdata->celular_movil)) . '</TelefonoClickToCall>
                        <ProyectoNuevo>0</ProyectoNuevo>';
        if ($inmuebles->precio_administracion != null ||  $inmuebles->precio_administracion != "") {
            $xml_post_string .= '<IncludesAdministration>false</IncludesAdministration>';
        } else {
            $xml_post_string .= '<IncludesAdministration>true</IncludesAdministration>';
        }
        $xml_post_string .= '</PublishAd_V8>
                </soap12:Body>
            </soap12:Envelope>';   // data from the form, e.g. some ID number


        //return $xml_post_string;
        $headers = array(
            "Content-type: text/xml",
            "Content-length: " . strlen($xml_post_string),
        ); //SOAPAction: your op URL

        $url = $soapUrl;

        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);

        curl_close($ch);

        // converting
        $response1 = str_replace('<?xml version="1.0" encoding="utf-8"?>', "", $response);

        $response2 = str_replace('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">', "", $response1);
        $response3 = str_replace('<PublishAd_V8Response xmlns="http://tempuri.org/">', '', $response2);
        $response4 = str_replace('<PublishAd_V8Result>', '', $response3);
        $response5 = str_replace('<AdvertId>', '', $response4);
        $response6 = str_replace('<ErrorCode>', '', $response5);
        $response7 = str_replace('</ErrorCode>', '', $response6);
        $response8 = str_replace('</AdvertId>', '', $response7);
        $response9 = str_replace('</PublishAd_V8Result>', '', $response8);
        $response10 = str_replace('</PublishAd_V8Response>', '', $response9);
        $response11 = str_replace('</soap:Envelope>', '', $response10);
        $response12 = str_replace('<soap:Body>', '', $response11);
        $response13 = str_replace('</soap:Body>', '', $response12);

        $num = intval($response13);

        $data = "";

        if ($num > 0) {
            $data = '<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                    <soap12:Header>
                        <AuthHeader xmlns="http://tempuri.org/">
                        <UserName>' . $soapUser . '</UserName>
                        <Password>' . $soapPassword . '</Password>
                        </AuthHeader>
                    </soap12:Header>
                    <soap12:Body>
                        <GetIdPortalByIdOv xmlns="http://tempuri.org/">
                            <idOv>' . $response13 . '</idOv>
                            <EmailUsuario>' . $imagenes->created_by->email . '</EmailUsuario>
                        </GetIdPortalByIdOv>
                    </soap12:Body>
                </soap12:Envelope>';

            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch1, CURLOPT_URL, $url);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch1, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc
            curl_setopt($ch1, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch1, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch1, CURLOPT_POST, true);
            curl_setopt($ch1, CURLOPT_POSTFIELDS, $data); // the SOAP request
            curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($ch1);

            $find = DB::table('portales_codigo_response')
                ->where('id_portal', $portal->id)
                ->where('id_inmueble', $imagenes->id)
                ->where('codigo', $response13)
                ->first();

            if (!$find) {
                DB::table('portales_state_inmuebles')->insert([
                    'id_portal' => $portal->id,
                    'id_inmueble' => $imagenes->id,
                ]);

                DB::table('portales_codigo_response')->insert([
                    'id_portal' => $portal->id,
                    'id_inmueble' => $imagenes->id,
                    'codigo' => $response13
                ]);

                $data = "Se sincronizo el inmueble";
            } else {
                $data = "Se actualizo la sincronizacion";
            }
        } else {
            $data = "Hay un problema en el servidor";
        }

        return $data;
        // user $parser to get your data out of XML response and to display it. 

    }

    public function despublicarFincarraiz($id)
    {

        $requestedplatform = "finca";
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%{$requestedplatform}%")->first();
        //    return $portal;
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);


        $consult = DB::table('portales_credenciales')->where(
            'user_id',
            $aut_user
        )->where('portale_id', $portal->id)->first();


        $soapUser = $consult->user;  //  username
        $soapPassword = $consult->token;
        $emailUsuario = $consult->email;

        $soapUrl = env('SOAP_URL'); // asmx URL of WSDL

        $xml_post_string = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
                                <soap:Header>
                                    <tem:AuthHeader>
                                    <!--Optional:-->
                                    <tem:UserName>' . $soapUser . '</tem:UserName>
                                    <tem:Password>' . $soapPassword . '</tem:Password>
                                    </tem:AuthHeader>
                                </soap:Header>
                                <soap:Body>
                                    <tem:DeleteAd>
                                    <!--Optional:-->
                                    <tem:NroDeRef>' . $id . '</tem:NroDeRef>
                                    <tem:UserEmail>' . $emailUsuario . '</tem:UserEmail>
                                    </tem:DeleteAd>
                                </soap:Body>
                            </soap:Envelope>';
        $url = $soapUrl;

        $headers = array(
            "Content-type: text/xml",
            "Content-length: " . strlen($xml_post_string),
        ); //SOAPAction: your op URL

        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);

        $response1 = str_replace('<?xml version="1.0" encoding="utf-8"?>', "", $response);

        $response2 = str_replace('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">', "", $response1);
        $response3 = str_replace('<PublishAd_V8Response xmlns="http://tempuri.org/">', '', $response2);
        $response4 = str_replace('<soap:Body>', '', $response3);
        $response5 = str_replace('<DeleteAdResponse xmlns="http://tempuri.org/">', '', $response4);
        $response6 = str_replace('<DeleteAdResult>', '', $response5);
        $response7 = str_replace('</DeleteAdResult>', '', $response6);
        $response8 = str_replace('</DeleteAdResponse>', '', $response7);
        $response9 = str_replace('</soap:Body>', '', $response8);
        $response10 = str_replace('</soap:Envelope>', '', $response9);


        if (boolval($response10)) {

            DB::table('portales_state_inmuebles')->where('id_portal', $portal->id,)
                ->where('id_inmueble', $id)
                ->delete();

            DB::table('portales_codigo_response')
                ->where('id_portal', $portal->id,)
                ->where('id_inmueble', $id)
                ->delete();

            return $this->sendResponse(true, 'Se despublico el inmueble');
        } else {
            return $this->sendError(false, 'Error al despublicar el inmueble', 500);
        }

        curl_close($ch);
    }

    public function getStatusPortalFincaRaizInmuebles(Request $request)
    {

        $requestedplatform = "finca";
        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%{$requestedplatform}%")->first();
        //    return $portal;
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);


        $consult = DB::table('portales_credenciales')->where(
            'user_id',
            $aut_user
        )->where('portale_id', $portal->id)->first();


        $soapUser = $consult->user;  //  username
        $soapPassword = $consult->token;
        $emailUsuario = $consult->email;

        $soapUrl = env('SOAP_URL'); // asmx URL of WSDL

        $xml_post_string = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
        <soap:Header>
           <tem:AuthHeader>
              <tem:UserName>' . $soapUser . '</tem:UserName>
              <tem:Password>' . $soapPassword . '</tem:Password>
           </tem:AuthHeader>
        </soap:Header>
        <soap:Body>
           <tem:GetAdStatus>
              <tem:NroDeRef>' . $request->id_inmueble . '</tem:NroDeRef>
              <tem:UserEmail>' . $emailUsuario . '</tem:UserEmail>
           </tem:GetAdStatus>
        </soap:Body>
     </soap:Envelope>';
        $url = $soapUrl;

        $headers = array(
            "Content-type: text/xml",
            "Content-length: " . strlen($xml_post_string),
        ); //SOAPAction: your op URL

        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);

        //return $response;
        curl_close($ch);



        return $this->sendResponse($response, "data");
    }




    public function importDepartamentos(Request $request)
    {
        //return $request;
        $request->validate([
            'file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('file');
        $data = Excel::import(new FincaraizDepartamentosImport, $path);

        return $this->sendResponse($data, "Se importo el archivo");
    }

    public function importCiudades(Request $request)
    {
        try {

            $request->validate([
                'file' => 'required|file|mimes:xls,xlsx'
            ]);

            $path = $request->file('file');
            $data = Excel::import(new FincarraizCiudadesImport, $path);

            return $this->sendResponse($data, "Se importo el archivo");
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    public function importFincarraizZonas(Request $request)
    {
        try {

            $request->validate([
                'file' => 'required|file|mimes:xls,xlsx'
            ]);

            $path = $request->file('file');
            $data = Excel::import(new FincarraizZonasImport, $path);

            return $this->sendResponse($data, "Se importo el archivo");
        } catch (\Throwable $th) {
            throw $th;
        }
    }



    public function importZonas(Request $request)
    {
        try {

            $request->validate([
                'file' => 'required|file|mimes:xls,xlsx'
            ]);

            $path = $request->file('file');
            $data = Excel::import(new ZonasImport, $path);

            return $this->sendResponse($data, "Se importo el archivo");
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function importFincarraizBarrios(Request $request)
    {
        try {
            $request->validate([
                'file' => 'required|file|mimes:xls,xlsx'
            ]);

            $path = $request->file('file');
            $data = Excel::import(new FincarraizBarriosImport, $path);

            return $this->sendResponse($data, "Se importo el archivo");
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function importBarrios(Request $request)
    {
        try {
            $request->validate([
                'file' => 'required|file|mimes:xls,xlsx'
            ]);

            $path = $request->file('file');
            $data = Excel::import(new BarriosImport, $path);

            return $this->sendResponse($data, "Se importo el archivo");
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
