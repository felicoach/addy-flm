<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\InmuebleExternaMany;
use App\Models\InmuebleInternaMany;
use App\Models\Inmuebles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PortalPuntoPropiedadController extends Controller
{
    public function dataXmlPuntoPropiedad($slug)
    {

        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'created_by',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'antiguedad',
            'estrato',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento'
        )->where("slug", $slug)
            ->first();

        $imagenes = json_decode(json_encode($inmuebles));

        $user = User::select(
            'users.*',
            'userdata.primer_nombre',
            'userdata.segundo_nombre',
            'userdata.primer_apellido',
            'userdata.segundo_apellido',
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.descripcion',
            'userdata.genero',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.id_estado',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente'
        )
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->where('users.id', $imagenes->created_by->id)
            ->first();



        $c_internas = InmuebleInternaMany::with('caracteristicasInternas')
            ->where('inmueble', $imagenes->id)->get();

        $c_externas = InmuebleExternaMany::with('caracteristicasExternas')
            ->where('inmueble', $imagenes->id)->get();

        $imagenes->caracteristicas_internas = $c_internas;
        $imagenes->caracteristicas_externas = $c_externas;

        $xml_post_string = '<?xml version="1.0"?>
        <ads>
          <ad>
            <id><![CDATA[ 123 ]]></id>
            <agency_id><![CDATA[ 456 ]]></agency_id>
            <title><![CDATA[ ' . $imagenes->titulo_inmueble . ' ]]></title>
            <description><![CDATA[ ' . $imagenes->descripcion . ' ]]></description>';
        if ($imagenes->tipo_negocio->tipo == "Alquiler") {
            $xml_post_string .=   '<price>' . str_replace('.', '',  str_replace(',', '', $inmuebles->precio_alquiler)) . '</price>';
        } else {
            $xml_post_string .=   '<price>' . str_replace('.', '',  str_replace(',', '', $inmuebles->precio_venta)) . '</price>';
        }
        $xml_post_string .= '<currency><![CDATA[ COP ]]></currency>
            <rooms><![CDATA[ ' . $imagenes->habitaciones . ' ]]></rooms>
            <bathrooms><![CDATA[ ' . $imagenes->banos . ' ]]></bathrooms>
            <bathroom_half><![CDATA[ 0 ]]></bathroom_half>';
        if ($imagenes->tipo_negocio->tipo == "Alquiler") {
            $xml_post_string .=   '<operation_type><![CDATA[ ALQUILER ]]></operation_type>';
        } else {
            $xml_post_string .=   '<operation_type><![CDATA[ VENTA ]]></operation_type>';
        }
        $xml_post_string .= '
            <property_type><![CDATA[ CASA ]]></property_type>
            <property_type_land><![CDATA[]]></property_type_land>
            <contact_phone><![CDATA[]]></contact_phone>
            <contact_cellphone><![CDATA[ +57 ' . $user->celular_movil . ' ]]></contact_cellphone>
            <whatsapp><![CDATA[ TRUE ]]></whatsapp>
            <contact_mail><![CDATA[ ' . $user->email . ' ]]></contact_mail>
            <contact_name><![CDATA[ ' . $user->primer_nombre . ' ]]></contact_name>
            <location>
              <location_region><![CDATA[ ' . $imagenes->estado_id->name . ' ]]></location_region>
              <location_city><![CDATA[  ' . $imagenes->ciudad_id->name . '  ]]></location_city>
              <location_neighbourhood><![CDATA[ ' . $imagenes->barrio_id->name . ' ]]></location_neighbourhood>
              <location_address><![CDATA[ ' . $imagenes->direccion . ' ]]></location_address>
              <location_postcode><![CDATA[0]]></location_postcode>
              <location_visibility><![CDATA[ accurate ]]></location_visibility>
              <latitude><![CDATA[ ' . $imagenes->latitud . ' ]]></latitude>
              <longitude><![CDATA[ ' . $imagenes->longitud . ' ]]></longitude>
              <country_code><![CDATA[ CO ]]></country_code>
            </location>
            <floor><![CDATA[ ' . $imagenes->pisos . ' ]]></floor>
            <floor_area_unit><![CDATA[0]]></floor_area_unit>
            <floor_area><![CDATA[0]]></floor_area>
            <plot_area_unit><![CDATA[0]]></plot_area_unit>
            <plot_area><![CDATA[0]]></plot_area>
            <photos>';
        for ($i = 0; $i < count($imagenes->inmueble_imagenes); $i++) {
            $xml_post_string .= '<url_photo><![CDATA[' . env('APP_URL') . '/storage/' . $imagenes->inmueble_imagenes[$i]->url . ']]></url_photo>';
        }
        $xml_post_string .= '</photos>
            <multimedia_url><![CDATA[ ' . $imagenes->url_video . ' ]]></multimedia_url>
            <virtualtour_url><![CDATA[0]]></virtualtour_url>
            <community_fee><![CDATA[0]]></community_fee>
            <community_fee_currency><![CDATA[ COP ]]></community_fee_currency>
            <condition><![CDATA[0]]></condition>
            <year><![CDATA[0]]></year>
            <is_furnished><![CDATA[0]]></is_furnished>
            <amenities>
              <air_conditioning><![CDATA[0]]></air_conditioning>
              <alarm><![CDATA[0]]></alarm>
              <balcony><![CDATA[0]]></balcony>
              <build-in_wardrobe><![CDATA[0]]></build-in_wardrobe>
              <parking><![CDATA[0]]></parking>
              <cellar><![CDATA[0]]></cellar>
              <children_area><![CDATA[0]]></children_area>
              <concierge><![CDATA[0]]></concierge>
              <disabled_access><![CDATA[0]]></disabled_access>
              <electricity><![CDATA[0]]></electricity>
              <equipped_kitchen><![CDATA[0]]></equipped_kitchen>
              <fireplace><![CDATA[0]]></fireplace>
              <garden><![CDATA[0]]></garden>
              <bbq><![CDATA[0]]></bbq>
              <guardhouse><![CDATA[0]]></guardhouse>
              <gym><![CDATA[0]]></gym>
              <heating><![CDATA[0]]></heating>
              <integral_kitchen><![CDATA[0]]></integral_kitchen>
              <internet><![CDATA[0]]></internet>
              <jacuzzi><![CDATA[0]]></jacuzzi>
              <library><![CDATA[0]]></library>
              <lift><![CDATA[0]]></lift>
              <natural_gas><![CDATA[0]]></natural_gas>
              <office><![CDATA[0]]></office>
              <outside_view><![CDATA[0]]></outside_view>
              <panoramic_view><![CDATA[0]]></panoramic_view>
              <roof_garden><![CDATA[0]]></roof_garden>
              <sauna><![CDATA[0]]></sauna>
              <security><![CDATA[0]]></security>
              <service_room><![CDATA[0]]></service_room>
              <swimming_pool><![CDATA[0]]></swimming_pool>
              <tennis_court><![CDATA[0]]></tennis_court>
              <terrace><![CDATA[0]]></terrace>
              <video_cable><![CDATA[0]]></video_cable>
              <water><![CDATA[0]]></water>
              <water_tank><![CDATA[0]]></water_tank>
              <natural_gas><![CDATA[ 1 ]]></natural_gas>
              <yard><![CDATA[0]]></yard>
            </amenities>
            <parkings><![CDATA[' . $imagenes->cantidad_parqueadero . ']]></parkings>
            <ground_type><![CDATA[0]]></ground_type>
            <near_mainstreet><![CDATA[0]]></near_mainstreet>
            <near_park><![CDATA[0]]></near_park>
            <near_schools><![CDATA[0]]></near_schools>
            <near_sea><![CDATA[0]]></near_sea>
            <near_shopping_mall><![CDATA[0]]></near_shopping_mall>
            <near_train_station><![CDATA[0]]></near_train_station>
          </ad>
        </ads>';

        header('Content-type: text/xml');
        echo $xml_post_string;
    }
}
