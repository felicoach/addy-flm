<?php

namespace App\Http\Controllers\API;

use App\Models\TelefonosClientes;
use App\Models\TipoCliente;
use App\Models\TipoDocumento;
use Illuminate\Http\Request;

class DocumentosController extends ApiController
{

    public function getTipoDocumento()
    {

        $data['tipo_documento'] = TipoDocumento::all();

        return $this->sendResponse($data, "tipos de documentos");
    }

    public function getTipoCliente()
    {

        $data['tipo_cliente'] = TipoCliente::all();

        return $this->sendResponse($data, "tipos de clientes");
    }

    public function registerCelularCliente(Request $request)
    {

        $data['numeros'] = TelefonosClientes::create([
            'numero' => $request->numero,
            'cliente' => $request->cliente
        ]);

        return $this->sendResponse($data, "números de clientes");
    }

    public function getCelularCliente($id)
    {
        $data['numero'] = TelefonosClientes::where('cliente', $id)->get();

        return $this->sendResponse($data, "números de cliente");
    }

    public function eliminarNumeroCliente($id)
    {

        $data = TelefonosClientes::find($id);

        $data->delete();

        return $this->sendResponse($data, "El número se elimino correctamente");
    }

    public function editarNumeroCliente(Request $request, $id)
    {

        $data['telefono'] = TelefonosClientes::where('id', $id)->update([
            'numero' => $request->numero
        ]);

        

        return $this->sendResponse($data, "Se edito correctamente");
    }
}
