<?php

namespace App\Http\Controllers\API;

use App\Imports\ClienteImport;
use App\Models\Cliente;
use App\Models\Correos;
use App\Models\TelefonosClientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ClienteController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth = Auth::user();

        $user = getUsersRoles($auth);

        $data['clientes'] = Cliente::with('userId', 'pais', 'ciudad', 'departamento', 'tipo_identificacion', 'tipo_cliente', 'genero', 'numeros', 'correos')
            ->where("user_id", $user)
            ->orderBy('created_at', 'desc')
            ->get();

        return $this->sendResponse($data, "Listado de clientes");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth = Auth::user();
        $user_admin = getUsersRoles($auth);

        //return $request;
        $data = Cliente::create([
            'user_id' => $user_admin,
            'created_by' => $auth->id,
            'nombre' => $request->nombre,
            'apellido' => $request->apellido,
            'telefono' => $request->telefono,
            'tipo_cliente' => $request->tipo_cliente['id'],
            'genero' => $request->genero
        ]);

        if ($data) {

            for ($i = 0; $i < count($request->celulares); $i++) {
                TelefonosClientes::create([
                    'number' => $request->celulares[$i]['numero'],
                    'cliente' => $data->id
                ]);
            }


            for ($i = 0; $i < count($request->correos); $i++) {
                Correos::create([
                    'email' => $request->correos[$i]['correo'],
                    'cliente' => $data->id
                ]);
            }
        }

        return $this->sendResponse($data, "Registro correctamente");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['cliente'] = Cliente::with('userId', 'pais', 'ciudad', 'departamento', 'tipo_identificacion', 'tipo_cliente', 'numeros', 'correos')->where('id', $id)->first();


        return $this->sendResponse($data, "cliente");
    }


    public function filterCliente(Request $request)
    {
        $auth = Auth::user();

        $filter = Cliente::query();

        $filter->with(
            'userId',
            'pais',
            'ciudad',
            'departamento',
            'tipo_identificacion',
            'tipo_cliente',
            'genero',
            'numeros',
            'correos'
        );

        $filter->where("user_id", $auth->id);

        if ($request->tipo_cliente != null) {
            $filter->where('tipo_cliente', $request->tipo_cliente['id']);
        }
        if ($request->genero != null) {
            $filter->where('genero', $request->genero['id']);
        }
        $data['clientes']  = $filter->get();
        return $this->sendResponse($data, "Listado de clientes");
    }

    public function saveCliente()
    {
    }


    public function importCliente(Request $request)
    {
        //return $request;
        $request->validate([
            'file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('file');
        $data = Excel::import(new ClienteImport, $path);

        return $this->sendResponse($data, "Se importo el archivo");
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;

        $data['cliente'] = Cliente::where('id', $id)
            ->update([
                'nombre' => $request->nombre,
                'apellido' => $request->apellido,
                'telefono' => $request->telefono,
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'n_identificacion' => $request->n_identificacion,
                'adicional' => $request->adicional,
                'pais' => $request->pais == null ? null : $request->pais['id'],
                'tipo_identificacion' => $request->tipo_identificacion == null ? null : $request->tipo_identificacion['id'],
                'tipo_cliente' => $request->tipo_cliente == null ? null : $request->tipo_cliente['id'],
                'departamento' =>   $request->departamento == null ? null :  $request->departamento['id'],
                'ciudad' => $request->ciudad == null ? null : $request->ciudad['id'],
                'genero' => $request->genero == null ? null : $request->genero,
                'direccion' => $request->direccion,
                'observaciones' => $request->observaciones,
                'clasificacion' => $request->clasificacion
            ]);



        return $this->sendResponse($data, "Se edito el cliente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
