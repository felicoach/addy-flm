<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function verify(Request $request) {
        $user = User::findOrFail($request->id);

        if (! hash_equals((string) $request->hash, sha1($user->getEmailForVerification()))) {
            return response()->json([
                "message" => "No estas autorizado",
                "success" => false
            ], 401);
        }

        if ($user->hasVerifiedEmail()) {
            return response()->json([
                "message" => "Tu email ya se confirmo, inicia sesion con tu correo y contraseña!",
                "success" => false
            ], 400);
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        return response()->json([
            "message" => "Bienvenido a Addy, tu correo ha sido confirmado. Ahora puedes iniciar sesion!",
            "success" => true
        ]);
    }

    public function resendVerificationEmail (Request $request) {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                "message" => "Correo no existente!",
                "success" => false
            ]);
        }

        $user->sendEmailVerificationNotification();

        return response()->json([
            "message" => "Chekea tu email!",
            "success" => true
        ]);
    }
}