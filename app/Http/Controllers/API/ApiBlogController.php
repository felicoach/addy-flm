<?php

namespace App\Http\Controllers\API;

use App\Models\Blog;
use App\Models\Empresa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image as Image;
use Cloudinary;

class ApiBlogController extends ApiController
{
    public function store(Request $request)
    {

        $auth = Auth::user();

       # dd($auth);

        $path='';


        $file = $request->file('files');

        if(isset($file[0])){

            $uploadedFileUrl = Cloudinary::upload($file[0]->getRealPath())->getSecurePath();

            $path = $uploadedFileUrl;

        }

        $data = Blog::create([
            "id_user" => $auth->id,
            "titulo" => $request->titulo,
            "slug" => $request->slug,
            "descripcion" => $request->descripcion,
            "contenido" => $request->contenido,
            "image" => $path,
            "estado_registro" => '1'
        ]);

        if ($data) {
            return $this->sendResponse($data, "Registro exitoso");
        }
    }


    public function upd(Request $request)
    {

        $auth = Auth::user();

        #dd($request->all());

        $path='';

        $blog=Blog::where('id', $request->id)->first();

        if(!is_null($request->file('files'))){

            if(isset($file[0])){

                $uploadedFileUrl = Cloudinary::upload($file[0]->getRealPath())->getSecurePath();
    
                $path = $uploadedFileUrl;

                $blog->update([
                    "imagen" => $path,
                ]);
    
            }

        }

        $blog->update([
            "id_user" => $auth->id,
            "titulo" => $request->titulo,
            "descripcion" => $request->descripcion,
            "contendio" => $request->contendio,
            "slug" => $request->slug
        ]);

        if ($blog) {
            return $this->sendResponse($blog, "El campose actualizo exitosamente");
        }
    }


    public function del(Request $request)
    {

        $auth = Auth::user();

        $blog=Blog::where('id', $request->id)->first();

        if(isset($blog->id)){

            $blog->delete();

        }

        return $this->sendResponse($blog, "Registro exitoso");
    }



    public function list()
    {
        $auth = Auth::user();
       
        $data["blogs"] = Blog::where("id_user", $auth->id)->get();

        return $this->sendResponse($data, "Listado de Blogs");
    }


    public function get(Request $request)
    {
        $auth = Auth::user();

        $blog=Blog::where('id', $request->id)->first();
        
        $data["blog"] = $blog;

        return $this->sendResponse($data, "Listado de Blogs");
    }


    public function index(Request $request)
    {

        $empresa=Empresa::where('slug', '=', $request->code)->first();


      #  $auth = User::where("referral_code", $request->code)->first();

        if(isset($empresa->id)){

            $auth = User::where("id", $empresa->agente)->first();


            $data["blogs"] = Blog::where('id_user', $auth->id)->get();

            return $this->sendResponse($data, "Listado de Blogs");

        }else{

            return $this->sendResponse([], "Listado de Blogs");

        }
       
      
    }


    public function detalle($id)
    {
       
        $data["blogs"] = Blog::where('id', $id)->first();

        return $this->sendResponse($data, "Listado de Blogs");
    }



}
