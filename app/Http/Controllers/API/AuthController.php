<?php

namespace App\Http\Controllers\API;

use App\Actions\Auth\LoginAction;
use App\Http\Requests\UserLoginRequest;
use App\Models\Empresa;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\Userdata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Socialite;

use AWS;

class AuthController extends ApiController
{

  public function signup(Request $request)
  {

    $validator = Validator::make($request->all(), [
      'username' => 'required|string|unique:users,username',
      'email' => 'required|string|email|unique:users,email',
      'password' => 'required|string|confirmed',
    ]);

    if ($validator->fails()) {
      return response(['error' => $validator->errors()->all(), 'Validation Error'], 400);
    }

    $code = '';
    if ($request->referral_code != null || $request->referral_code != '') {
      $usercode =  User::where('referral_code', $request->referral_code)->first();
      $code = $usercode->id;
    } else {
      $code = 1;
    }

    $user = new User([
      'username' => $request->username,
      'email' => $request->email,
      'password' => bcrypt($request->password),
      'referred_by' => $code
    ]);

    $user->save();

    if ($user) {
      $role = new RoleUser();

      $role->role_id = 4;
      $role->user_id = $user->id;

      $role->save();

      if ($role) {

       $user->sendEmailVerificationNotification();

        return response()->json([
          'message' => 'Se ha enviado un email de confirmación, por favor revisa tu correo electrónico',
        ], 200);
      } else {
        return response()->json([
          'message' => 'Error al registrar el rol', 'status' => 400
        ], 400);
      }
    } else {
      return response()->json([
        'message' => 'Error al registrar el usuario', 'status' => 400
      ], 400);
    }
  }

  public function login(UserLoginRequest $request, LoginAction $loginAction)
  {
    $passportRequest = $loginAction->run($request->all());

    #dd($passportRequest);

    $tokenContent = $passportRequest["content"];

    if (!empty($tokenContent['access_token'])) {

      $user =  User::select('users.*')
        ->where('users.email', $request->email)
        ->first();

      $userdata = Userdata::where('id_usuario', $user->id)->first();

      if ($userdata != null) {

        $user = User::select(
          'users.*',
          'userdata.tipo_identificacion',
          'userdata.foto_persona',
          'userdata.foto_portada_persona',
          'userdata.cedula_persona',
          'userdata.primer_nombre',
          'userdata.id_usuario',
          'userdata.segundo_nombre',
          'userdata.primer_apellido',
          'userdata.segundo_apellido',
          'userdata.fecha_nacimiento',
          'userdata.telefono_fijo',
          'userdata.celular_movil',
          'userdata.celular_whatsapp',
          'userdata.direccion_persona',
          'userdata.id_pais',
          'userdata.descripcion',
          'userdata.genero',
          'userdata.facebook',
          'userdata.instagram',
          'userdata.linkedin',
          'userdata.id_estado',
          'userdata.id_ciudad',
          'userdata.id_perfil',
          'userdata.estado_persona',
          'userdata.color_fondo',
          'userdata.color_fuente',
          'userdata.tipo_cliente'
        )
          ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
          ->where('users.email', $request->email)
          ->first();
      }
      $user_empresa = getUsersRoles($user);

      $rol = DB::table('role_user')
        ->select('roles.*')
        ->join('roles', 'role_user.role_id', '=', 'roles.id')
        ->join('users', 'role_user.user_id', '=', 'users.id')
        ->where('role_user.user_id', $user->id)
        ->first();

      $user['role'] = $rol;


      if ($rol->slug == 'administrator') {


        $permisos = $this->permisos($rol);
        $user['ability'] = $permisos;
      } else {


          $user["empresa"] = Empresa::with(
            "empresaEmails",
            "empresaNumbers",
            "empresaRedes",
            "id_pais",
            "id_estado",
            "id_ciudad"
          )->where("agente", $user_empresa)->first();
        


        $permisos = $this->permisos($rol);

        $user['ability'] = $permisos;
      }


      return response()->json([
        'token' => $passportRequest["content"],
        'userData' => $user,
      ]);

    }



    if (empty($tokenContent)) {
      return response()->json([
        'message' => 'Verifica tu correo electronico'
      ], 400);
    }

    return response()->json([
      'message' => 'No estas autorizado'
    ], 400);
  }

  public static function permisos($rol = null)
  {

    $permisos = DB::table('roles')
      ->select(
        'permissions.subject',
        'actions.action'
      )
      ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
      ->join('active_permission_actions', 'role_permissions.id', '=', 'active_permission_actions.role_permission')
      ->join('permissions', 'role_permissions.permission', '=', 'permissions.id')
      ->join('actions', 'active_permission_actions.action', '=', 'actions.id')

      ->where('roles.slug', $rol->slug)
      ->get();

    return $permisos;
  }




  public function authAndRedirect($user)
  {
    Auth::login($user);

    return redirect($this->redirectTo);
  }


  public function logout(Request $request)
  {
    $request->user()->token()->revoke();
    return response()->json(['message' =>
    'Successfully logged out']);
  }

  public function user(Request $request)
  {
    return response()->json($request->user());
  }

  public function recovery(Request $request)
  {


    $validator = Validator::make($request->all(), [
      'email' => 'required|string|email',
    ]);

    if ($validator->fails()) {
      return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
    }

    //Password::sendResetLink($credentials);

    return response()->json(["msg" => 'Reset password link sent on your email id.']);
  }

  public function crearSubdomain($slug){



    $route = AWS::createClient('route53', [
      'version'     => 'latest',
      'region'      => 'us-east-1a',
      'credentials' => [
          'key'    => 'AKIAXT7PCHUDELVU5EDY',
          'secret' => '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',
      ],
  ]);

    $nhz=$slug.'.crmaddy.online';
    $nhzp=$slug.'.crmaddy.online.';
    $ohz='crmaddy.online';
    $hzp='crmaddy.online.';

    $result = $route->createHostedZone(array(
        // Name is required
        'Name' => $nhz,
        // CallerReference is required
        'CallerReference' => $nhz,
        'HostedZoneConfig' => array(
            'Comment' => 'zona para '.$nhz,
            'PrivateZone' => false,
        )
    ));

    $hz=$route->listHostedZones([]);

    $HostedZones = $hz->get('HostedZones');



    $idhz='0';
    $idhzp='0';
    
   # dd($HostedZones);
    foreach($HostedZones as $hz){

       

        if($hz['Name']==$nhz.'.'){
           # dd($hz['Id']);
            $idhz=$hz['Id'];
        }

        if($hz['Name']==$ohz.'.'){
            $idhzp=$hz['Id'];
        }

    }

   # dd($idhz);

    if($idhz=='0'){
        dd('Nose encontro zona ');

    }else{

        $result = $route->changeResourceRecordSets(array(
            // HostedZoneId is required
            'HostedZoneId' => $idhz,
            // ChangeBatch is required
            'ChangeBatch' => array(
                'Comment' => 'creacion de registro A para '.$nhz,
                // Changes is required
                'Changes' => array(
                    array(
                        // Action is required
                        'Action' => 'CREATE',
                        // ResourceRecordSet is required
                        'ResourceRecordSet' => array(
                            // Name is required
                            'Name' => $nhz,
                            // Type is required
                            'Type' => 'A',
                           
                            'TTL' => 300,
                            'ResourceRecords' => array(
                                array(
                                    // Value is required
                                    'Value' => '34.196.48.41',
                                ),
                                // ... repeated
                            )
                        ),
                    ),
                    // ... repeated
                ),
            ),
        ));



        $result = $route->listResourceRecordSets(array(
            // HostedZoneId is required
            'HostedZoneId' => $idhz,
        ));

        $ResourceRecordSets=$result->get('ResourceRecordSets');

        $rrhz='0';

        foreach($ResourceRecordSets as $rrs){

            if($rrs['Type']=='NS'){
    
                //dd($rrs['ResourceRecords']);
    
                $rrhz=$rrs['ResourceRecords'];
            }
    
        }

        if($rrhz=='0'){
            dd('No se encontraron registros de NS');
        }else{

           # dd($idhzp);

            $result = $route->changeResourceRecordSets(array(
                // HostedZoneId is required
                'HostedZoneId' => $idhzp,
                // ChangeBatch is required
                'ChangeBatch' => array(
                    'Comment' => 'creacion de registro NS ',
                    // Changes is required
                    'Changes' => array(
                        array(
                            // Action is required
                            'Action' => 'CREATE',
                            // ResourceRecordSet is required
                            'ResourceRecordSet' => array(
                                // Name is required
                                'Name' => $nhz,
                                // Type is required
                                'Type' => 'NS',
                               
                                'TTL' => 300,
                                'ResourceRecords' => $rrhz
                                    // ... repeated
                            ),
                        ),
                        // ... repeated
                    ),
                ),
            ));
        }
    }


    $ResourceRecordSets = $result->get('ResourceRecordSets');

  }


  
}
