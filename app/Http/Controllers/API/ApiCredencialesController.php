<?php

namespace App\Http\Controllers\API;

use App\Models\Credenciales;
use App\Models\UserTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image as Image;
use Cloudinary;


class ApiCredencialesController extends ApiController
{
    public function store(Request $request)
    {

        $auth = Auth::user();

        $c=Credenciales::where('id', '1')->first();

        if(isset($c->id)){

            $data = $c->update([
                "dominio" => $request->dominio,
                "cpanel_user" => $request->cpanel_user,
                "cpanel_token" => $request->cpanel_token,
                "id_user" => $auth->id,
            ]);


        }else{

            $data = Credenciales::create([
                "dominio" => $request->dominio,
                "cpanel_user" => $request->cpanel_user,
                "cpanel_token" => $request->cpanel_token,
                 "id_user" => $auth->id,
            ]);

        }
        

        if ($data) {
            return $this->sendResponse($data, "Actualizacion exitoso");
        }
    }


    public function upd(Request $request)
    {

        $auth = Auth::user();

        $path='';

        $credenciales=Credenciales::where('id', $request->id)->first();

        $credenciales->update([
            "id_user" => $auth->id,
            "dominio" => $request->dominio,
            "cpanel_user" => $request->cpanel_user,
            "cpanel_token" => $request->cpanel_token,
        ]);

        if ($credenciales) {
            return $this->sendResponse($credenciales, "El registro de actualizo exitosamente");
        }
    }


    public function del(Request $request)
    {

        $auth = Auth::user();

        $t=Credenciales::where('id', $request->id)->first();

        if(isset($t->id)){

            $t->delete();

        }

        return $this->sendResponse($t, "Registro exitoso");
    }



    public function list()
    {

        $auth = Auth::user();

        $credenciales=Credenciales::get();
       
        $data["credenciales"] = $credenciales;

        return $this->sendResponse($data, "Listado de Templates");
    }



    public function get(Request $request)
    {
        $auth = Auth::user();

        $credenciales=Credenciales::where('id', '1')->first();
        
        $data["credenciales"] = $credenciales;

        return $this->sendResponse($data, "Listado de Credenciales");
    }


    public function index()
    {
       
        $data["credenciales"] = Credenciales::get();

        return $this->sendResponse($data, "Listado de Credenciales");
    }



}
