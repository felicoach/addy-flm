<?php

namespace App\Http\Controllers\API;

use App\Imports\CiencuadraBarriosImport;
use App\Imports\CiencuadraCiuImport;
use App\Imports\ciencuadraDepImport;
use App\Imports\ciencuadraLocalidadImport;
use App\Models\Inmuebles;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class CiencuadraController extends ApiController
{
    public function dataApiRestCienCuadra(Request $request)
    {

        $this->getTokenCiencuadra();

        $data = $this->getInfo($request->inmueble, $request->localidad);


        DB::table('portales_inmuebles_localidad')->insert(["inm_id" => $request->immueble, "loc_id" => $request->localidad['id']]);

        $client = new Client();
        $res = $client->request('POST', env('API_KEY_CIENCUADRA') . 'api/insert', [
            'json' => [$data['array']],
            'headers' => [
                'Authorization' => 'Bearer ' . $data['token']
            ]
        ]);

        $data_res = json_decode($res->getBody());

        if ($data_res->statusCode == 100) {

            $res_status = $client->request('POST', env('API_KEY_CIENCUADRA') . 'api/consult-status', [
                'json' => [
                    'idRequest' => $data_res->idRequest,
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $data['token']
                ]
            ]);

            $status = json_decode($res_status->getBody());

            $response = $this->getResponse($status, $data, $request->inmueble);

            return $this->sendResponse($response, $response['message']);
        } else {
            return $this->sendError($data_res, "Error al registrar");
        }
    }

    public function updateApiCiencuadra(Request $request)
    {

        $this->getTokenCiencuadra();

        $data = $this->getInfo($request->inmueble, $request->localidad);

        $client = new Client();
        $res = $client->request('POST', env('API_KEY_CIENCUADRA') . 'api/update', [
            'json' => [$data['array']],
            'headers' => [
                'Authorization' => 'Bearer ' . $data['token']
            ]
        ]);

        $data_res = json_decode($res->getBody());


        if ($data_res->statusCode == 100) {

            $res_status = $client->request('POST', env('API_KEY_CIENCUADRA') . 'api/consult-status', [
                'json' => [
                    'idRequest' => $data_res->idRequest,
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $data['token']
                ]
            ]);

            $status = json_decode($res_status->getBody());

            $response = $this->getResponse($status, $data, $request->inmueble);

            return $this->sendResponse($response, $response['message']);
        } else {
            return $this->sendError($data_res, "Error al acualizar");
        }
    }



    public function desactivarInmuebleCiencuadra(Request $request)
    {
        $this->getTokenCiencuadra();
        $id = $request->inmueble;


        $data = $this->getInfo($id, $request->localidad);

        $find_code = DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->first();

        $client = new Client();
        $res = $client->request('POST', env('API_KEY_CIENCUADRA') . 'api/update', [
            'json' => [[
                "propertyTypeId" => $data['array']['propertyTypeId'],
                "propertyCode" => $find_code->codigo,
                "status" => "I",
                "features" => []
            ]],
            'headers' => [
                'Authorization' => 'Bearer ' . $data['token']
            ]
        ]);

        $data_res = json_decode($res->getBody());

        if ($res->getStatusCode() == 200) {

            $res_status = $client->request('POST', env('API_KEY_CIENCUADRA') . 'api/consult-status', [
                'json' => [
                    'idRequest' => $data_res->idRequest,
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $data['token']
                ]
            ]);

            $status = json_decode($res_status->getBody());

            if ($res_status->getStatusCode() == 200) {

                DB::table('portales_codigo_response')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->where('id_inmueble', $id)->delete();
                DB::table('portales_state_inmuebles')->where('id_inmueble', $id)->where('id_portal', $data['portal'])->delete();
                DB::table('portales_urls')->where('inmueble_id', $id)->where('portal_id', $data['portal'])->delete();

                return $this->sendResponse($status, "Se desactivo el inmueble");
            }

            return $this->sendError($data_res, "Error al desactivar el inmueble", 500);
        }
        return $this->sendError($data_res, "No se encontro el codigo del inmueble", 400);
    }

    public static function getResponse($status, $data, $id)
    {

        $message = "";

        if (is_object($status[0]->message)) {
            if ($status[0]->message->statusCode == 100) {

                $find_p_url =  DB::table('portales_urls')->where('portal_id', $data['portal'])->where('inmueble_id', $id)->first();
                $find_p_codigo =  DB::table('portales_codigo_response')->where('id_portal', $data['portal'])->where('id_inmueble', $id)->first();


                if (!$find_p_url) {

                    DB::table('portales_urls')->insert([
                        'portal_id' => $data['portal'],
                        'inmueble_id' => $id,
                        'url' => $status[0]->message->propertyDetailUrl
                    ]);
                }

                if (!$find_p_codigo) {
                    DB::table('portales_codigo_response')->insert([
                        'id_portal' => $data['portal'],
                        'id_inmueble' =>  $id,
                        'codigo' => $status[0]->propertyCode
                    ]);
                } else {
                    DB::table('portales_codigo_response')->where('id_portal',  $data['portal'])->where('id_inmueble', $id)->update([
                        'codigo' => $status[0]->propertyCode
                    ]);
                }

                $message = $status[0]->message->message;
                self::statePublication($data, $id, env('PUBLICADO'));
            } else {
                $message = "Hay un error en el servidor, por favor intentelo mas tarde";
            }
        }

        if ($status[0]->message == 'Pending to be processed') {
            $find = DB::table('portales_codigo_response')
                ->where('id_portal', $data['portal'])
                ->where('id_inmueble', $id)
                ->where('codigo', $status[0]->propertyCode)
                ->first();

            if (!$find) {

                self::statePublication($data, $id,  env('PENDIENTE'));

                DB::table('portales_codigo_response')->insert([
                    'id_portal' => $data['portal'],
                    'id_inmueble' =>  $id,
                    'codigo' => $status[0]->propertyCode
                ]);

                $message = "Inmueble en estado pendiente para sincronizar";
            } else {
                self::statePublication($data, $id, env('ACTUALIZADO'));
                $message = "Se actualizo la sincronizacion";
            }
        }

        return ["message" => $message, "status" => $status];
    }

    public static function statePublication($data, $id, $state)
    {

        $find_state = DB::table('portales_state_inmuebles')
            ->where('id_portal', $data['portal'])->where('id_inmueble', $id)->first();

        if ($find_state) {
            DB::table('portales_state_inmuebles')->where('id', $find_state->id)->update([
                'id_portal' => $data['portal'],
                'id_inmueble' =>  $id,
                'state' => $state
            ]);
        } else {
            DB::table('portales_state_inmuebles')->insert([
                'id_portal' => $data['portal'],
                'id_inmueble' =>  $id,
                'state' => $state
            ]);
        }
    }

    public static function getTokenCiencuadra()
    {

        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $portal = DB::table("portales")->where('portales.slug', 'LIKE', "%ciencuadras%")->first();

        $find = DB::table('portales_credentials_ciencuadra')
            ->where('user_id', $aut_user)
            ->first();

        $client = new Client();
        $res = $client->request('GET', env('API_KEY_CIENCUADRA') . 'login', [
            'json' => [
                'username' => $find->username,
                'password' => $find->password
            ]
        ]);

        $data = json_decode($res->getBody());

        $if_exits_pt = DB::table('portales_token')->where("portal_id", $portal->id)->where('user_id',  $aut_user)->first();

        if ($if_exits_pt) {
            $token = DB::table('portales_token')->where("portal_id", $portal->id)->where('user_id',  $aut_user)->update([
                "token" => $data->token,
                "created_by" => $auth->id
            ]);
        } else {
            $token = DB::table('portales_token')->insert([
                "portal_id" => $portal->id,
                "user_id" => $aut_user,
                "token" => $data->token
            ]);
        }
        return [$token, "token creado"];
    }

    public static function getInfo($id, $data)
    {
        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $portal = DB::table("portales")->where('portales.slug', 'LIKE', "%ciencuadras%")->first();

        $inmuebles = Inmuebles::with(
            'InmuebleImagenes',
            'userId',
            'created_by',
            'tipoInmueble',
            'paisId',
            'ciudadId',
            'zonaId',
            'estadoId',
            'barrioId',
            'tipo_negocio',
            'estadoPublicacion',
            'stateFisico',
            'antiguedad',
            'estrato',
            'periodoAdmon',
            'tipoPrecio',
            'tiempoAlquiler',
            'parqueadero',
            'segmento'
        )->where("id", $id)
            ->first();

        $imagenes = json_decode(json_encode($inmuebles));

        $tipo_negocio = DB::table('portales_inmuebles_tipo_negocio')
            ->where('portales_inmuebles_tipo_negocio.portale_id', $portal->id)
            ->where('portales_inmuebles_tipo_negocio.tipo_negocio_id', $imagenes->tipo_negocio->id)
            ->first();

        $tipo_inmueble = DB::table('portales_tipo_inmuebles')->where('portale_id', $portal->id)->where('tI_id', $imagenes->tipo_inmueble->id)->first();

        $consult = DB::table('portales_credenciales')->where(
            'user_id',
            $aut_user
        )->where('portale_id', $portal->id)->first();

        if (!$consult) {
            $consult = DB::table('portales_credenciales')->where(
                'user_id',
                $auth->create_by
            )->where('portale_id', $portal->id)->first();
        }

        $token_p = DB::table('portales_token')->where('portal_id', $portal->id)
            ->where('user_id', $aut_user)->first();


        $ciudad = DB::table('portales_cities')
            ->where('portale_id', $portal->id)
            ->where('city_id', $imagenes->ciudad_id->id)
            ->first();

        $sellingPrice = 10000000;
        $leasingFee = 100000;

        if ($imagenes->tipo_negocio->tipo == "Alquiler") {
            $leasingFee =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_alquiler));
        } else {
            $sellingPrice  =   str_replace('.', '',  str_replace(',', '', $inmuebles->precio_venta));
        }

        $img_array = [];

        for ($i = 0; $i < count($imagenes->inmueble_imagenes); $i++) {
            if (!isValidURL($imagenes->inmueble_imagenes[$i]->url)) {
                array_push($img_array, array("url" =>  env('APP_URL') . "/storage/" . $imagenes->inmueble_imagenes[$i]->url, "type" => "I"));
            } else {
                array_push($img_array, array("url" => $imagenes->inmueble_imagenes[$i]->url, "type" => "I"));
            }
        }

        $array = [
            "cityId" => intval($ciudad->codigo),
            "localityId" => intval($data['codigo']),
            "neighborhoodName" => $imagenes->barrio_id->name,
            "propertyTypeId" => intval($tipo_inmueble->codigo),
            "transactionTypeId" => intval($tipo_negocio->codigo),
            "address" => $imagenes->direccion,
            "showAddress" => 1,
            "stratum" => intval($imagenes->estrato->id),
            "propertyCode" => $imagenes->id,
            "latitude" =>  floatval($imagenes->latitud),
            "longitude" => floatval($imagenes->longitud),
            "sellingPrice" => intval($sellingPrice),
            "leasingFee" => intval($leasingFee),
            "additionalInfo" => $imagenes->descripcion,
            "status" => "A",
            "integrator" => "ADDY",
            "advisorName" => $imagenes->titulo_inmueble,
            "advisorPhone" => str_replace('+57', '',  str_replace(' ', '', $imagenes->created_by->userdata->celular_movil)),
            "features" => [
                "numBedRooms" => $imagenes->habitaciones,
                "numBathrooms" => $imagenes->banos,
                "numParking" => intval($imagenes->cantidad_parqueadero),
                "parkingType" => 1,
                "privatePool" => false,
                "antiquity" => 2,
                "propertyName" => $inmuebles->titulo_inmueble,
                "remodeled" => false,
                "project" => false,
                "projectName" => "",
                "administrationValue" => 0,
                "serviceRoom" => false,
                "serviceBathroom" => false,
                "laundryZone" => false,
                "floorTypes" => 8,
                "homeAppliances" => false,
                "chimney" => false,
                "airConditioner" => false,
                "terracesNumber" => "",
                "terraceArea" => "",
                "balconiesNumber" => 2,
                "depositsNumber" => 2,
                "lotUse" => 1,
                "floorsNumber" => 2,
                "apartmentsNumber" => 0,
                "elevatorsNumber" => 0,
                "vigilance" => 0,
                "numParkingVisitors" => 0,
                "reception" => false,
                "closedCircuitTv" => false,
                "electricPlant" => false,
                "communalLiving" => false,
                "childrenZone" => false,
                "greenZones" => false,
                "communalPool" => false,
                "gym" => false,
                "lotLocation" => 0,
                "socialVenue" => false,
                "brandNew" => false,
                "addressComplement" => "ninguna",
                "realestateLogo" => env('FRONT_APP') . "images/_/_/_/_/addy-flm/frontend/src/assets/images/logo/logo_addy.png",
                "furnished" => false,
                "inConstruction" => false,
                "builtArea" => $imagenes->area_contruida,
                "privateArea" => 450,
                "area" => $imagenes->area_total,
                "photospropertyData" => $img_array
            ]
        ];


        return ['array' => $array, 'token' => $token_p->token, 'portal' => $portal->id];
    }

    public function importDepartamentos(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('file');
        $data = Excel::import(new ciencuadraDepImport, $path);

        return $this->sendResponse($data, "Se importo el archivo");
    }

    public function importCiudades(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('file');
        $data = Excel::import(new CiencuadraCiuImport, $path);

        return $this->sendResponse($data, "Se importo el archivo");
    }

    public function importLocalidades(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('file');
        $data = Excel::import(new ciencuadraLocalidadImport, $path);

        return $this->sendResponse($data, "Se importo el archivo");
    }

    public function importBarrios(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:xls,xlsx'
        ]);

        $path = $request->file('file');
        $data = Excel::import(new CiencuadraBarriosImport, $path);

        return $this->sendResponse($data, "Se importo el archivo");
    }

    public function getLocalidades()
    {
        $data['localidades'] = DB::table('ciencuadra_localidades')->get();
        return $this->sendResponse($data, 'listados de localidades ciencuadra');
    }
}
