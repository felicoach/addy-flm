<?php

namespace App\Http\Controllers\API;

use App\Models\Template;
use App\Models\UserTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image as Image;
use Cloudinary;


class ApiTemplateController extends ApiController
{
    public function store(Request $request)
    {

        $auth = Auth::user();

       // dd($request->all());

        $path='';


        $file = $request->file('files');

        $uploadedFileUrl = Cloudinary::upload($file[0]->getRealPath())->getSecurePath();

    #    $filename = $file[0]->getClientOriginalName();
     #   $filename = str_replace(' ', '', $filename);

     #   $filename=time().$filename;
      #  $path = $file[0]->storeAs('paginaweb/'.$auth->id.'/templates', $filename, 'public');
        $path = $uploadedFileUrl;

     


        $data = Template::create([
            "id_user" => $auth->id,
            "nombre" => $request->nombre,
            "descripcion" => $request->descripcion,
            "imagen" => $path,
            "url" => $request->url,
            "posicion" => $request->posicion
        ]);

        if ($data) {
            return $this->sendResponse($data, "Registro exitoso");
        }
    }


    public function upd(Request $request)
    {

        $auth = Auth::user();

        #dd($request->all());

        $path='';

        $template=Template::where('id', $request->id)->first();

        if(!is_null($request->file('files'))){

            $file = $request->file('files');
            $uploadedFileUrl = Cloudinary::upload($file[0]->getRealPath())->getSecurePath();

           # $filename = $file[0]->getClientOriginalName();
          #  $filename = str_replace(' ', '', $filename);
            $path = $uploadedFileUrl;

            

            $template->update([
                "imagen" => $path,
            ]);

        }

        $template->update([
            "id_user" => $auth->id,
            "nombre" => $request->nombre,
            "descripcion" => $request->descripcion,
            "url" => $request->url,
            "posicion" => $request->posicion
        ]);

        if ($template) {
            return $this->sendResponse($template, "El registro de actualizo exitosamente");
        }
    }


    public function del(Request $request)
    {

        $auth = Auth::user();



        $t=Template::where('id', $request->id)->first();

        if(isset($t->id)){

            $t->delete();

        }

        return $this->sendResponse($t, "Registro exitoso");
    }



    public function list()
    {

        $auth = Auth::user();

        $t=UserTemplate::where('id_user', $auth->id)->first();

        $templates=Template::get();

        if(isset($t->id)){

            foreach($templates as $tem){

                if($tem->id == $t->id_template){

                    $tem->selected=1;

                }else{

                    $tem->selected=0;

                }

            }
    
        }else{

            foreach($templates as $tem){
                    $tem->selected=0;
            }
        }

       
        $data["templates"] = $templates;

        return $this->sendResponse($data, "Listado de Templates");
    }


    public function settemplate($id)
    {

        $auth = Auth::user();

        $t=UserTemplate::where('id_user', $auth->id)->delete();

        UserTemplate::create([
                'id_user'=>$auth->id,
                'id_template'=>$id,
            ]);

        $t=UserTemplate::where('id_user', $auth->id)->first();

       # dd($t);
        $templates=Template::get();

        if(isset($t->id)){

            foreach($templates as $tem){

                if($tem->id == $t->id_template){

                    $tem->selected=1;

                }else{

                    $tem->selected=0;

                }

            }
    
        }else{

            foreach($templates as $tem){
                    $tem->selected=0;
            }
        }

       
        $data["templates"] = $templates;

        return $this->sendResponse($data, "Listado de Templates");
    }




    public function get(Request $request)
    {
        $auth = Auth::user();

        $template=Template::where('id', $request->id)->first();
        
        $data["template"] = $template;

        return $this->sendResponse($data, "Listado de Templates");
    }


    public function index()
    {
       
        $data["templates"] = Template::get();

        return $this->sendResponse($data, "Listado de templates");
    }



}
