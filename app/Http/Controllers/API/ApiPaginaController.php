<?php

namespace App\Http\Controllers\API;

use App\Models\Banners;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiPaginaController extends ApiController
{
    public function store(Request $request)
    {

        $auth = Auth::user();

       // dd($request->all());

        $path='';

        $file = $request->file('files');
        $filename = $file[0]->getClientOriginalName();
        $filename = str_replace(' ', '', $filename);
        $path = $file[0]->storeAs('paginaweb/'.$auth->id.'/banners', $filename, 'public');



        $data = Banners::create([
            "id_user" => $auth->id,
            "titulo" => $request->titulo,
            "descripcion" => $request->descripcion,
            "imagen" => $path,
            "enlace" => $request->enlace,
            "posicion" => $request->posicion
        ]);

        if ($data) {
            return $this->sendResponse($data, "Registro exitoso");
        }
    }


    public function upd(Request $request)
    {

        $auth = Auth::user();

        #dd($request->all());

        $path='';

        $banner=Banners::where('id', $request->id)->first();

        if(!is_null($request->file('files'))){

            $file = $request->file('files');
            $filename = $file[0]->getClientOriginalName();
            $filename = str_replace(' ', '', $filename);
            $path = $file[0]->storeAs('paginaweb/'.$auth->id.'/banners', $filename, 'public');

            $banner->update([
                "imagen" => $path,
            ]);

        }

        $banner->update([
            "id_user" => $auth->id,
            "titulo" => $request->titulo,
            "descripcion" => $request->descripcion,
            "enlace" => $request->enlace,
            "posicion" => $request->posicion
        ]);

        if ($banner) {
            return $this->sendResponse($banner, "El campose registro exitosamente");
        }
    }


    public function del(Request $request)
    {

        $auth = Auth::user();

        $banner=Banners::where('id', $request->id)->first();

        if(isset($banner->id)){

            $banner->delete();

        }

        return $this->sendResponse($banner, "Registro exitoso");
    }



    public function list()
    {
        $auth = Auth::user();
        
        $role = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

        if ($role->slug == "agentes") {
            $user = $auth->id;
        } else {
            $user = $auth->create_by;
        }

        $data["banners"] = Banners::where("id_user", $auth->id)->get();

        return $this->sendResponse($data, "Listado de Banners");
    }


    public function get(Request $request)
    {
        $auth = Auth::user();

        $banner=Banners::where('id', $request->id)->first();
        
        $data["banner"] = $banner;

        return $this->sendResponse($data, "Listado de Banners");
    }



}
