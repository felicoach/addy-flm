<?php

namespace App\Http\Controllers\API;

use App\Models\Action;
use App\Models\ActivePermissionActions;
use App\Models\Permission;
use App\Models\PermissionAction;
use App\Models\PermissionRole;
use App\Models\PermissionsAction;
use App\Models\Role;
use App\Models\RolePermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RolesController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth = Auth::user();
        $roles = [];
        $role = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

        if ($role->slug == "administrator") {
            $roles = Role::where('type', 'public')->whereNotExists(function ($q) {
                $q->where('slug', "administrator");
            })->get();
        } else {
            $roles = Role::where('type', 'public')->whereNotExists(function ($q) {
                $q->where('slug', "administrator");
            })->whereNotExists(function ($q) {
                $q->where('slug', "visitantes");
            })->whereNotExists(function ($q) {
                $q->where('slug', "socio_referidor");
            })->whereNotExists(function ($q) {
                $q->where('slug', "inmobliaria");
            })->get();
        }

        $roles_json = json_decode(json_encode($roles));

        $json = $this->forJsonRole($roles_json);

        $data["roles"] = $json;

        return $this->sendResponse($data, "Listado de roles");
    }



    public function getRolesForUser()
    {
        $auth = Auth::user();

        $roles = Role::where("create_by", $auth->id)
            ->get();

        $roles_json = json_decode(json_encode($roles));
        $json = $this->forJsonRole($roles_json);

        $data["roles"] = $json;

        return $this->sendResponse($data, "Listado de roles");
    }

    public function getRolesForUserAll()
    {
        $auth = Auth::user();

        $roles = Role::where("create_by", $auth->id)
            ->orWhere('type', 'public')->whereNotExists(function ($q) {
                $q->where('slug', "administrator");
            })->whereNotExists(function ($q) {
                $q->where('slug', "visitantes");
            })->whereNotExists(function ($q) {
                $q->where('slug', "socio_referidor");
            })->whereNotExists(function ($q) {
                $q->where('slug', "inmobiliaria");
            })

            ->orderBy('id', 'DESC')
            ->get();

        $roles_json = json_decode(json_encode($roles));

        $json = $this->forJsonRole($roles_json);

        $data["roles"] = $json;

        return $this->sendResponse($data, "Listado de roles");
    }




    public static function forJsonRole($roles_json)
    {
        for ($i = 0; $i < count($roles_json); $i++) {
            $roles_json[$i]->permissions = [];

            $permissions =  DB::table('roles')
                ->select(
                    'permissions.id',
                    'permissions.subject',
                )
                ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
                ->join('permissions', 'role_permissions.permission', '=', 'permissions.id')
                ->where("roles.id", $roles_json[$i]->id)
                ->get();

            $roles_json[$i]->permissions = $permissions;

            for ($j = 0; $j < count($roles_json[$i]->permissions); $j++) {

                $roles_json[$i]->permissions[$j]->actions = [];

                $actions = DB::table('roles')
                    ->select(
                        'actions.id',
                        'actions.action'
                    )
                    ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
                    ->join('active_permission_actions', 'role_permissions.id', '=', 'active_permission_actions.role_permission')
                    ->join('actions', 'active_permission_actions.action', '=', 'actions.id')
                    ->where("role_permissions.permission",  $roles_json[$i]->permissions[$j]->id)
                    ->get();


                for ($k = 0; $k < count($actions); $k++) {
                    array_push(
                        $roles_json[$i]->permissions[$j]->actions,
                        $actions[$k]
                    );
                }
            }
        }

        return $roles_json;
    }





    public function getPermissionRole($role)
    {

        $roles = Role::where("id", $role)->first();

        $roles["permissions"] = DB::table('roles')
            ->select(
                'permissions.id',
                'permissions.subject',

            )
            ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
            ->join('permissions', 'role_permissions.permission', '=', 'permissions.id')
            ->where('roles.id', $role)
            ->where('type', 'public')
            ->get();

        $roles_json = json_decode(json_encode($roles["permissions"]));

        for ($i = 0; $i < count($roles_json); $i++) {

            $roles_json[$i]->actions = [];

            $actions = DB::table('roles')
                ->select(
                    'actions.id',
                    'actions.action'
                )
                ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
                ->join('role_permission_actions', 'role_permissions.id', '=', 'role_permission_actions.role_permission')
                ->join('actions', 'role_permission_actions.action', '=', 'actions.id')
                ->where("role_permissions.permission", $roles_json[$i]->id)
                ->get();

            for ($j = 0; $j < count($actions); $j++) {
                array_push(
                    $roles_json[$i]->actions,
                    $actions[$j]
                );
            }
        }

        $roles["permissions"] = $roles_json;

        return $this->sendResponse($roles, "Listado de permisos");
    }

    public function addPermissionRole(Request $request)
    {
        foreach ($request as $value) {
            PermissionRole::create([
                "role_id" => $value->role_id,
                "permission_id" => $value->permission_id
            ]);
        }

        return $this->sendResponse("", "Registro de permisos");
    }

    function arrayUnique($array, $keep_key_assoc = false)
    {
        $duplicate_keys = array();
        $tmp = array();

        foreach ($array as $key => $val) {

            if (is_object($val))
                $val = (array)$val;

            if (!in_array($val, $tmp))
                $tmp[] = $val;
            else
                $duplicate_keys[] = $key;
        }

        foreach ($duplicate_keys as $key)
            unset($array[$key]);

        return $keep_key_assoc ? $array : array_values($array);
    }

    public function getActionsForId($code)
    {
        $data["actions_id"] =  Action::where("id", $code)->get();

        return $this->sendResponse($data, "Listado");
    }

    public function getPermissionsActions()
    {

        $permissions_data = Permission::all();

        $data_json = json_decode(json_encode($permissions_data));

        for ($i = 0; $i < count($data_json); $i++) {
            $data_json[$i]->actions = [];

            $consult = DB::table("permissions_actions")
                ->select("actions.*")
                ->where("permission", $data_json[$i]->id)
                ->join("actions", "permissions_actions.action", "actions.id")
                ->get();

            array_push($data_json[$i]->actions, $consult);
        }

        $data["permissions"] = $data_json;

        return $this->sendResponse($data, "Listado de permisos");
    }

    public function addPermissions(Request $request)
    {
        $message = "";
        try {
            if ($request->id) {
                $data = Permission::where('id', $request->id)->update([
                    "title" => $request->title,
                    "subject" => $request->subject
                ]);
                $message = "Se edito";
            } else {
                $data = Permission::create([
                    "subject" => $request->subject,
                    "title" => $request->title
                ]);

                $message = "Se registro correctamente";
            }

            return $this->sendResponse($data, $message);
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function addActions(Request $request)
    {
        $message = "";
        try {
            if ($request->id) {
                $data = Action::where('id', $request->id)->update([
                    "title" => $request->title,
                    "action" => $request->action
                ]);
                $message = "Se edito";
            } else {
                $data = Action::create([
                    "title" => $request->title,
                    "action" => $request->action
                ]);

                $message = "Se registro correctamente";
            }

            return $this->sendResponse($data, $message);
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function addpermissionsActions(Request $request)
    {
        if ($request->permission != null) {

            PermissionsAction::where("permission", $request->permission)
                ->delete();

            if (!empty($request->actions)) {
                for ($i = 0; $i < count($request->actions); $i++) {
                    $permission_action = DB::table("permissions_actions")
                        ->where("permission", $request->permission)
                        ->where("action", $request->actions[$i])
                        ->first();

                    if (!$permission_action) {
                        DB::table('permissions_actions')->insert([
                            'permission' => $request->permission,
                            'action' => $request->actions[$i]
                        ]);
                    }
                }
                return $this->sendResponse("Se egistro", "Bien!!");
            } else {
                return $this->sendResponse("Valores vacios", "Mal!!");
            }
        }
    }


    public function getPermissions()
    {
        $auth = Auth::user();

        $role = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

        if ($role->slug != "administrator") {
            $data["permissions"] = Permission::where('type', 'public')->orderBy('id', 'DESC')->get();
        } else {
            $data["permissions"] = Permission::orderBy('id', 'DESC')->get();
        }

        return $this->sendResponse($data, "Listado de permisos");
    }


    public function edictPermissions(Request $request, $code)
    {

        $data = Permission::where('id', $request->id)->update([
            "title" => $request->title,
            "subject" => $request->subject
        ]);

        if ($data) {
            return $this->sendResponse($data, "Se edito correctamente");
        }
    }


    public function getActions()
    {
        $data["actions"] = Action::orderBy('id', 'DESC')->get();

        return $this->sendResponse($data, "Listado de actiions");
    }

    public function getActivePermissionsActions(Request $request)
    {
        $role_permission = RolePermission::where("role", $request->role)->where("permission", $request->permission)->first();
        $data["active_permissions"] = [];
        if ($role_permission) {
            $data["active_permissions"] = ActivePermissionActions::where("role_permission", $role_permission->id)->get();
        }

        return $this->sendResponse($data, "Listado");
    }


    public function addActivePermissionsActions(Request $request)
    {

        $role_permission = RolePermission::where("role", $request->role)->where("permission", $request->permission)->first();

        ActivePermissionActions::where("role_permission", $role_permission->id)->delete();

        for ($i = 0; $i < count($request->actions); $i++) {
            ActivePermissionActions::create([
                "role_permission" => $role_permission->id,
                "action" => $request->actions[$i]
            ]);
        }

        return $this->sendResponse($role_permission, "Role permissions");
    }


    public function getRolePermissionActionForIdRole($role)
    {
        $permission = DB::table('roles')
            ->select(
                'permissions.*',
            )
            ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
            ->join('permissions', 'role_permissions.permission', '=', 'permissions.id')
            ->where('roles.id', $role)
            ->get();

        $data_json = json_decode(json_encode($permission));

        for ($i = 0; $i < count($data_json); $i++) {
            $data_json[$i]->actions = [];

            $consult = DB::table("permissions_actions")
                ->select("actions.*")
                ->where("permission", $data_json[$i]->id)
                ->join("actions", "permissions_actions.action", "actions.id")
                ->get();

            array_push($data_json[$i]->actions, $consult);
        }

        $data["permissions"] = $data_json;

        return $this->sendResponse($data, "Listado de permisos");
    }

    public function getRolePermissionForIdRole($code)
    {
        $role_permission = RolePermission::with("permission")->where("role", $code)->get();

        $role_permission_json = json_decode(json_encode($role_permission));

        $permissions["permissions"] = [];

        for ($i = 0; $i < count($role_permission_json); $i++) {
            array_push($permissions["permissions"], $role_permission_json[$i]->permission);
        }

        return $this->sendResponse($permissions, "List of permissions");
    }

    public static function existsInArrayJson($entry, $array)
    {

        if (count($array) > 0) {
            for ($i = 0; $i < count($array); $i++) {
                if ($array[$i]->permission->id == $entry["id"]) {
                    return true;
                }
                return false;
            }
            return false;
        } else {
            return false;
        }
    }

    public static function existsInArray($entry, $array)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i]["id"] == $entry) {
                return false;
            }
            return true;
        }
    }


    public function addPermissionsForId(Request $request)
    {

        $permission_register = RolePermission::with("permission")->where("role", $request->role)->get();

        $json_permission = json_decode(json_encode($permission_register));

        if (count($request->deleted_permissions) > 0) {

            for ($i = 0; $i < count($request->deleted_permissions); $i++) {

                ActivePermissionActions::where("role_permission", $request->deleted_permissions[$i]["id"])->delete();

                RolePermission::where("role", $request->role)->where("permission", $request->deleted_permissions[$i]["id"])->delete();
            }
        }
        // return  $permissions;
        //  return $this->sendResponse($count, "List of permissions");


        for ($k = 0; $k < count($request->permissions); $k++) {

            if (!$this->existsInArrayJson($request->permissions[$k],  $json_permission)) {
                $role = RolePermission::where("role", $request->role)
                    ->where("permission", $request->permissions[$k]["id"])->first();
                if (!$role) {
                    RolePermission::create([
                        "role" => $request->role,
                        "permission" => $request->permissions[$k]["id"],
                        "create_by" => $request->user
                    ]);
                }
            }
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth = Auth::user();

        $user = getUsersRoles($auth);

        $rol = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

        if ($request->id) {
            $data['roles'] = Role::where('id', $request->id)->update([
                "nombre" => $request->nombre,
                "descripcion" => $request->descripcion,
                "slug" => $request->nombre,
                "type" => $request->type,
                "create_by" => $auth->id
            ]);

            if ($data['roles']) {
                return $this->sendResponse($data, "Se edito correctamente");
            }
        } else {
            $type = 1;
            if ($rol->slug == "administrator") {
                $type = 1;
            } else {
                $type = 2;
            }

            $datas = Role::create([
                "nombre" => $request->titulo,
                "descripcion" => $request->descripcion,
                "slug" => $request->titulo,
                "type" => $type,
                "create_by" => $user
            ]);

            $permissions = Permission::where("subject", "perfil_public")->first();

            $role_permision = RolePermission::create([
                "role" => $datas->id,
                "permission" => $permissions->id,
                "create_by" => $request->user
            ]);

            $actions = Action::all();
            $user_json = json_decode(json_encode($actions));

            for ($i = 0; $i < count($user_json); $i++) {
                ActivePermissionActions::create([
                    "role_permission" => $role_permision->id,
                    "action" => $user_json[$i]->id
                ]);
            }
            $data["roles"] = $datas;

            if ($data['roles']) {
                return $this->sendResponse($data, "Se registro correctamente");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
