<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;

class PortalesCredentialsController extends ApiController
{

    public function getTokenCiencuadra()
    {

        $auth = Auth::user();
        $aut_user = getUsersRoles($auth);

        $portal = DB::table("portales")->where('portales.name', 'LIKE', "%cien%")->first();


        $find = DB::table('portales_credentials_ciencuadra')
            ->where('user_id', $aut_user)
            ->first();


        $client = new Client();
        $res = $client->request('GET', env('API_KEY_CIENCUADRA') . 'login', [
            'json' => [
                'username' => $find->username,
                'password' => $find->password
            ]
        ]);

        $data = json_decode($res->getBody());

        $if_exits_pt = DB::table('portales_token')->where("portal_id", $portal->id)->first();

        if ($if_exits_pt) {
            $token = DB::table('portales_token')->where("portal_id", $portal->id)->update([
                "token" => $data->token,
                "created_by" => $auth->id
            ]);
        } else {
            $token = DB::table('portales_token')->insert([
                "portal_id" => $portal->id,
                "user_id" => $aut_user,
                "token" => $data->token
            ]);
        }
        return $this->sendResponse($token, "token creado");
    }


    public function getCodigoResponsePortal(Request $request)
    {

        $data['codigo_response'] = DB::table('portales_codigo_response')
            ->where('id_portal', $request->id_portal)
            ->where('id_inmueble', $request->id_inmueble)->first();

        
        return $this->sendResponse($data, "codigo portal response");
            
    }
}
