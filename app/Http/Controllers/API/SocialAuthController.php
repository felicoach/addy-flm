<?php

namespace App\Http\Controllers\API;

use App\Actions\Auth\RegisterAction;
use App\Models\Empresa;
use App\Models\RoleUser;
use App\Models\SocialAccount;
use App\Models\User;
use App\Models\Userdata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Socialite;
use Str;

class SocialAuthController extends ApiController
{
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

        return response()->json([
            "url" => $url
        ]);
    }

    public function socialLoginApp(Request $request)
    {

        $provider = 'google';
        $accessToken = $request->token;
        $user = Socialite::driver('google')->stateless()->userFromToken($accessToken);
        // $user = Socialite::driver('google')->userFromToken($request->token);
        //return $this->sendResponse($userSocial, "Listado de roles");

        $appUser = User::whereEmail($user->email)->first();

        if (!$appUser) {

            User::create([
                'name' => $user->name,
                'password' => Str::random(7),
                'email' => $user->email,
                'username' => $user->name
            ]);

            Userdata::create([
                'foto_persona'          => $user->avatar,
                'primer_nombre'         => $user->user['given_name'],
                'primer_apellido'     =>    $user->user['family_name'],
                'id_usuario'            => $appUser->id, //Crypt::decrypt($request->id),
            ]);

            $socialAccount = SocialAccount::create([
                'provider' => 'google',
                'provider_user_id' => $user->id,
                'user_id' => $appUser->id
            ]);
        } else {
            // means that we have already this user
            $userdata = Userdata::where('id_usuario', $appUser->id)->first();

            if ($userdata != null) {
                if ($userdata->foto_persona != null) {
                    Userdata::updated([
                        'foto_persona'          => $user->avatar,
                        'primer_nombre'         => $user->user['given_name'],
                        'primer_apellido'     =>    $user->user['family_name'],
                    ]);
                }
            } else {
                Userdata::create([
                    'foto_persona'          => $user->avatar,
                    'primer_nombre'         => $user->user['given_name'],
                    'primer_apellido'     =>    $user->user['family_name'],
                ]);
            }
        }
        $userdata = Userdata::where('id_usuario', $appUser->id)->first();


        if ($userdata != null) {
            $appUser = User::select(
                'users.*',
                'userdata.tipo_identificacion',
                'userdata.foto_persona',
                'userdata.foto_portada_persona',
                'userdata.cedula_persona',
                'userdata.primer_nombre',
                'userdata.id_usuario',
                'userdata.segundo_nombre',
                'userdata.primer_apellido',
                'userdata.segundo_apellido',
                'userdata.fecha_nacimiento',
                'userdata.telefono_fijo',
                'userdata.celular_movil',
                'userdata.celular_whatsapp',
                'userdata.direccion_persona',
                'userdata.id_pais',
                'userdata.descripcion',
                'userdata.genero',
                'userdata.facebook',
                'userdata.instagram',
                'userdata.linkedin',
                'userdata.id_estado',
                'userdata.id_ciudad',
                'userdata.id_perfil',
                'userdata.estado_persona',
                'userdata.tipo_cliente'
            )
                ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
                ->where('users.email', $user->email)
                ->first();
        }
        // login our user and get the token
        $passportToken = $appUser->createToken('Login token')->accessToken;
        $role = new RoleUser();

        $role->role_id = 4;
        $role->user_id = $appUser->id;

        $role->save();

        if ($role) {
            $rol = DB::table('role_user')
                ->select('roles.*')
                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.user_id', $appUser->id)
                ->first();

            $appUser['role'] = $rol;

            if ($rol->slug == 'administrator') {

                $permisos = $this->permisos($rol);

                $appUser['ability'] = $permisos;
            } else {


                $appUser["empresa"] = Empresa::with(
                    "empresaEmails",
                    "empresaNumbers",
                    "empresaRedes",
                    "id_pais",
                    "id_estado",
                    "id_ciudad"
                )->where("agente", $appUser->id)->first();


                $permisos = $this->permisos($rol);

                $appUser['ability'] = $permisos;
            }

            return $this->sendResponse([
                'token' => $passportToken,
                'userData' => $appUser,
                'user' => $user
            ], "Datos de usuario");
            
        }
    }

    public function handleProviderCallback($provider, RegisterAction $registerAction)
    {

        $user = Socialite::driver($provider)->stateless()->user();


        if (!$user->token) {
            // return json
            return response()->json([
                "success" => false,
                "message" => "Failed to login"
            ], 401);
        }

        $appUser = User::whereEmail($user->email)->first();

        if (!$appUser) {
            // create user and add the provider

            $appUser = $registerAction->run([
                'name' => $user->name,
                'password' => Str::random(7),
                'email' => $user->email,
                'username' => $user->name
            ]);

            Userdata::create([
                'foto_persona'          => $user->avatar,
                'primer_nombre'         => $user->user['given_name'],
                'primer_apellido'     =>    $user->user['family_name'],
                'id_usuario'            => $appUser->id, //Crypt::decrypt($request->id),

            ]);

            $socialAccount = SocialAccount::create([
                'provider' => $provider,
                'provider_user_id' => $user->id,
                'user_id' => $appUser->id
            ]);
        } else {
            // means that we have already this user
            $socialAccount = $appUser->socialAccounts()->where('provider', $provider)->first();

            if (!$socialAccount) {
                // create social account
                $socialAccount = SocialAccount::create([
                    'provider' => $provider,
                    'provider_user_id' => $user->id,
                    'user_id' => $appUser->id
                ]);
            }

            $userdata = Userdata::where('id_usuario', $appUser->id)->first();

            if ($userdata != null) {
                if ($userdata->foto_persona != null) {
                    Userdata::updated([
                        'foto_persona'          => $user->avatar,
                        'primer_nombre'         => $user->user['given_name'],
                        'primer_apellido'     =>    $user->user['family_name'],
                    ]);
                }
            } else {
                Userdata::create([
                    'foto_persona'          => $user->avatar,
                    'primer_nombre'         => $user->user['given_name'],
                    'primer_apellido'     =>    $user->user['family_name'],
                ]);
            }
        }

        $userdata = Userdata::where('id_usuario', $appUser->id)->first();


        if ($userdata != null) {
            $appUser = User::select(
                'users.*',
                'userdata.tipo_identificacion',
                'userdata.foto_persona',
                'userdata.foto_portada_persona',
                'userdata.cedula_persona',
                'userdata.primer_nombre',
                'userdata.id_usuario',
                'userdata.segundo_nombre',
                'userdata.primer_apellido',
                'userdata.segundo_apellido',
                'userdata.fecha_nacimiento',
                'userdata.telefono_fijo',
                'userdata.celular_movil',
                'userdata.celular_whatsapp',
                'userdata.direccion_persona',
                'userdata.id_pais',
                'userdata.descripcion',
                'userdata.genero',
                'userdata.facebook',
                'userdata.instagram',
                'userdata.linkedin',
                'userdata.id_estado',
                'userdata.id_ciudad',
                'userdata.id_perfil',
                'userdata.estado_persona',
                'userdata.tipo_cliente'
            )
                ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
                ->where('users.email', $user->email)
                ->first();
        }
        // login our user and get the token
        $passportToken = $appUser->createToken('Login token')->accessToken;
        $role = new RoleUser();

        $role->role_id = 4;
        $role->user_id = $appUser->id;

        $role->save();

        if ($role) {
            $rol = DB::table('role_user')
                ->select('roles.*')
                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.user_id', $appUser->id)
                ->first();

            $appUser['role'] = $rol;

            if ($rol->slug == 'administrator') {

                $permisos = $this->permisos($rol);

                $appUser['ability'] = $permisos;
            } else {


                $appUser["empresa"] = Empresa::with(
                    "empresaEmails",
                    "empresaNumbers",
                    "empresaRedes",
                    "id_pais",
                    "id_estado",
                    "id_ciudad"
                )->where("agente", $appUser->id)->first();


                $permisos = $this->permisos($rol);

                $appUser['ability'] = $permisos;
            }
            return response()->json([
                'token' => $passportToken,
                'userData' => $appUser,
                'user' => $user
            ]);
        }
    }

    public static function permisos($rol = null)
    {
        $permisos  = DB::table('roles')
            ->select(
                'permissions.subject',
                'actions.action'
            )
            ->join('role_permissions', 'roles.id', '=', 'role_permissions.role')
            ->join('active_permission_actions', 'role_permissions.id', '=', 'active_permission_actions.role_permission')
            ->join('permissions', 'role_permissions.permission', '=', 'permissions.id')
            ->join('actions', 'active_permission_actions.action', '=', 'actions.id')

            ->where('roles.slug', $rol->slug)
            ->get();

        return $permisos;
    }
}
