<?php

namespace App\Http\Controllers\API;

use App\Models\City;
use App\Models\Country;
use App\Models\Empresa;
use App\Models\RoleUser;
use App\Models\State;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Userdata;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth = Auth::user();
        $user = getUsersRoles($auth);
        $data["users"] = User::with("userdata")->where("create_by", $user->id)->get();

        return $this->sendResponse($data, "Listado de usuarios");
    }


    public function getUsers()
    {
        $auth = Auth::user();

        $user = getUsersRoles($auth);

        $data = User::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
            'userdata.segundo_nombre',
            'userdata.primer_apellido',
            'userdata.segundo_apellido',
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.descripcion',
            'userdata.genero',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.id_estado',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente'
        )
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->where('users.create_by', $user->id)
            ->get();

        $user_json = json_decode(json_encode($data));

        for ($i = 0; $i < count($user_json); $i++) {
            $user_json[$i]->roles = [];

            $pais = Country::where("id", $user_json[$i]->id_pais)->first();
            $state = DB::table("states")->where("id", $user_json[$i]->id_estado)->first();
            $ciudad = DB::table("cities")->where("id", $user_json[$i]->id_ciudad)->first();

            $user_json[$i]->id_pais = [];
            $user_json[$i]->id_estado = [];
            $user_json[$i]->id_ciudad = [];


            $rol = DB::table('role_user')
                ->select('roles.*')
                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.user_id', $user_json[$i]->id)
                ->first();

            $user_json[$i]->id_pais = $pais;
            $user_json[$i]->id_estado = $state;
            $user_json[$i]->id_ciudad = $ciudad;
            $user_json[$i]->roles = $rol;
        }

        $users["users"] = $user_json;

        return $this->sendResponse($users, "Listado de usuarios");
    }
    public static function getRoleUser($auth)
    {
        $role = DB::table('role_user')
            ->select('roles.*')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $auth->id)
            ->first();

        if ($role->slug == "agentes") {
            return $auth->id;
        } else {
            return $auth->create_by;
        }
    }

    public function getUsersCode()
    {
        $auth = Auth::user();

        $user = getUsersRoles($auth);

        $data = User::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
            'userdata.segundo_nombre',
            'userdata.primer_apellido',
            'userdata.segundo_apellido',
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.descripcion',
            'userdata.genero',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.id_estado',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente'
        )
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')
            ->where('users.create_by', $user)
            ->get();

        $user_json = json_decode(json_encode($data));

        for ($i = 0; $i < count($user_json); $i++) {

            $empresa = Empresa::with(
                "empresaEmails",
                "empresaNumbers",
                "empresaRedes",
                "id_pais",
                "id_estado",
                "id_ciudad"
            )->where("agente", $user_json[$i]->id)->first();

            $user_json[$i]->roles = [];

            $pais = Country::where("id", $user_json[$i]->id_pais)->first();
            $state = DB::table("states")->where("id", $user_json[$i]->id_estado)->first();
            $ciudad = DB::table("cities")->where("id", $user_json[$i]->id_ciudad)->first();

            $user_json[$i]->id_pais = [];
            $user_json[$i]->id_estado = [];
            $user_json[$i]->id_ciudad = [];


            $rol = DB::table('role_user')
                ->select('roles.*')
                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.user_id', $user_json[$i]->id)
                ->first();

            $user_json[$i]->id_pais = $pais;
            $user_json[$i]->id_estado = $state;
            $user_json[$i]->id_ciudad = $ciudad;
            $user_json[$i]->roles = $rol;
            $user_json[$i]->empresa = $empresa;
        }

        $users["users"] = $user_json;

        return $this->sendResponse($users, "Listado de usuarios");
    }


    public function getUsersAllAdmin()
    {

        $role = RoleUser::where("role_id", 2)->get();

        $data = RoleUser::select(
            'users.*',
            'userdata.tipo_identificacion',
            'userdata.foto_persona',
            'userdata.foto_portada_persona',
            'userdata.cedula_persona',
            'userdata.primer_nombre',
            'userdata.id_usuario',
            'userdata.segundo_nombre',
            'userdata.primer_apellido',
            'userdata.segundo_apellido',
            'userdata.fecha_nacimiento',
            'userdata.telefono_fijo',
            'userdata.celular_movil',
            'userdata.celular_whatsapp',
            'userdata.direccion_persona',
            'userdata.id_pais',
            'userdata.descripcion',
            'userdata.genero',
            'userdata.facebook',
            'userdata.instagram',
            'userdata.linkedin',
            'userdata.id_estado',
            'userdata.id_ciudad',
            'userdata.id_perfil',
            'userdata.estado_persona',
            'userdata.tipo_cliente'
        )
            ->leftJoin('users', 'role_user.user_id', '=', 'users.id')
            ->join('userdata', 'users.id', '=', 'userdata.id_usuario')

            ->where("role_user.role_id", 2)

            ->get();

        $user_json = json_decode(json_encode($data));

        for ($i = 0; $i < count($user_json); $i++) {
            $user_json[$i]->roles = [];

            $pais = Country::where("id", $user_json[$i]->id_pais)->first();

            $empresa = Empresa::with(
                "empresaEmails",
                "empresaNumbers",
                "empresaRedes",
                "id_pais",
                "id_estado",
                "id_ciudad"
            )->where("agente", $user_json[$i]->id)->first();

            $state = DB::table("states")->where("id", $user_json[$i]->id_estado)->first();
            $ciudad = DB::table("cities")->where("id", $user_json[$i]->id_ciudad)->first();

            $user_json[$i]->id_pais = [];
            $user_json[$i]->empresa = [];
            $user_json[$i]->id_estado = [];
            $user_json[$i]->id_ciudad = [];


            $rol = DB::table('role_user')
                ->select('roles.*')
                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->where('role_user.user_id', $user_json[$i]->id)
                ->first();

            $user_json[$i]->id_pais = $pais;
            $user_json[$i]->id_estado = $state;
            $user_json[$i]->id_ciudad = $ciudad;
            $user_json[$i]->roles = $rol;
            $user_json[$i]->empresa = $empresa;
        }

        $users["users"] = $user_json;

        return $this->sendResponse($users, "Listado de usuarios");
    }



    public function addUsers(Request $request)
    {
        $auth = Auth::user();
        $user = getUsersRoles($auth);
        $user_register = User::where('create_by', $auth->id)->get();
        // return count($user_register);
        if (count($user_register) < 5) {
            try {
                $this->saveUser($user, $request);
                return $this->sendResponse("registrado", "Se registro el usuario");
            } catch (\Throwable $th) {
                return $this->sendError("No se pudo registrar", $th, 500);
            }
        } else {
            return $this->sendError("Solicita mas usuarios", [], 400);
        }
    }

    public static function saveUser($auth, $request)
    {
        $user = new User();

        $user->username = 'user-' . $request->cedula_persona;
        $user->email = $request->email;
        $user->password = bcrypt($request->cedula_persona);
        $user->referred_by = 1;
        $user->create_by = $auth;

        $user->save();


        if ($user) {

            User::where('id', $user->id)->update([
                'email_verified_at'   => now()
            ]);

            Userdata::create([
                "primer_nombre" => $request->primer_nombre,
                "direccion_persona" => $request->direccion_persona,
                "celular_movil" => $request->celular_movil,
                "celular_whatsapp" => $request->celular_whatsapp,
                "id_pais" => $request->id_pais,
                "id_estado" => $request->id_estado,
                "id_ciudad" => $request->id_ciudad,
                "cedula_persona" => $request->cedula_persona,
                "genero" => $request->genero,
                "id_usuario" => $user->id
            ]);

            RoleUser::create([
                "role_id" => $request->roles,
                "user_id" => $user->id
            ]);

            return User::with("userdata")->where("id", $user->id);
        }
    }

    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($referral_code)
    {
        $data['user'] =  User::with('userdata')->where('referral_code', $referral_code)->first();

        return $this->sendResponse($data, "users");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $roles = null;
        // return $request;
        if (is_array($request->roles)) {
            $roles = $request->roles["id"];
        } else {
            $roles = $request->roles;
        }

        $array = [
            'primer_nombre' => $request->primer_nombre,
            'celular_movil'     =>  $request->celular_movil,
            'celular_whatsapp'  =>  $request->celular_whatsapp,
            'direccion_persona' => $request->direccion_persona,
            'genero' => $request->genero,
            'id_pais' => is_array($request->id_pais) ? $request->id_pais["id"] : $request->id_pais,
            'id_estado' => is_array($request->id_estado) ? $request->id_estado["id"]  : $request->id_estado,
            'id_ciudad' => is_array($request->id_ciudad) ? $request->id_ciudad["id"]  : $request->id_ciudad,
        ];

        RoleUser::where("user_id", $request->id)->update([
            "role_id" => $roles,
        ]);

        $user_update = Userdata::where('id_usuario', $request->id)->update($array);

        return $this->sendResponse($user_update, "usuario editado correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
