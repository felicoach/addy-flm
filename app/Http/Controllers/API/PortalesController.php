<?php


namespace App\Http\Controllers\API;

use App\Models\Portales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;

class PortalesController extends ApiController
{
    public function getPortales()
    {
        $auth = Auth::user();
        $portales = Portales::where('state', 'active')->get();

        $portales_json = json_decode(json_encode($portales));

        for ($i = 0; $i < count($portales_json); $i++) {

            $crp = DB::table("portales_credenciales")
                ->where("user_id", $auth->id)
                ->where('portale_id', $portales_json[$i]->id)
                ->first();

            $tp = DB::table("portales_type_services")
                ->where('id', $portales_json[$i]->type_service)
                ->first();

            $portales_json[$i]->type_service = $tp;
            $portales_json[$i]->credenciales_portales = $crp;
        }

        $data['portales'] = $portales_json;

        return $this->sendResponse($data, "Listado de portales 1");
    }

    public function getTipoInmuebles()
    {
        $data["tipo_inmuebles"] = DB::table("portales_tipo_inmuebles")->get();
        return $this->sendResponse($data, "Listado de portales tipo inmuebles");
    }

    public function getTypeService()
    {
        $data["type_service"] = DB::table("portales_type_services")->get();
        return $this->sendResponse($data, "Listado de servicios");
    }

    public function getCredentialPortal($portal)
    {

        $auth = Auth::user();

        $data["credential"] = DB::table("portales_credenciales")
            ->where("user_id", $auth->id)
            ->where('portale_id', $portal)
            ->first();



        return $this->sendResponse($data, "Listado de portales 1");
    }

    public function getCredencialesClasificadosPais($portal)
    {
        $auth = Auth::user();

        $find = DB::table('portales_credentials_clasificados_pais')
            ->where('portale_id', $portal)
            ->where('user_id', $auth->id)
            ->first();

        $find_object['credential'] = json_decode(json_encode($find));

        return $this->sendResponse($find_object, "Credenciales");
    }

    public function getCredencialesMetroCuadrado($portal)
    {

        $auth = Auth::user();

        $find = DB::table('portales_credentials_metro_cuadrado')
            ->where('portale_id', $portal)
            ->where('user_id', $auth->id)
            ->first();

        $find_object['credential'] = json_decode(json_encode($find));

        return $this->sendResponse($find_object, "Credenciales");
    }

    public function  getCredencialesCiencuadra($portal)
    {

        $auth = Auth::user();

        $find = DB::table('portales_credentials_ciencuadra')
            ->where('portale_id', $portal)
            ->where('user_id', $auth->id)
            ->first();

        $find_object['credential'] = json_decode(json_encode($find));

        return $this->sendResponse($find_object, "Credenciales");
    }

    public function registerPortales(Request $request)
    {
        // return $request;

        $imagen = "No tiene";

        if ($request->file('image')) {
            $avatarPath = $request->file('image');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('image')->storeAs('portales/' . $request->numero_cedula, $avatarName, 'public');
            $imagen =  $path;
        }

        $data = Portales::create([
            "name" => $request->name,
            "slug" => preg_replace('/\s+/', '_', strtolower($request->name)),
            "image" => $imagen,
            "description" => $request->description,
            "type_service" => $request->type_service
        ]);

        return $this->sendResponse($data, "Listado de portales");
    }

    public function editarPortales(Request $request)
    {

        $imagen = $request->image;

        if ($request->file('image')) {
            $avatarPath = $request->file('image');
            $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();

            $path = $request->file('image')->storeAs('portales/' . $request->numero_cedula, $avatarName, 'public');
            $imagen =  $path;
        }
        // return eliminar_tildes($request->name);

        $data = Portales::where("id", $request->id)->update([
            "name" => $request->name,
            "slug" => preg_replace('/\s+/', '_', strtolower(eliminar_tildes($request->name))),
            "image" => $imagen,
            "description" => $request->description,
            "type_service" => $request->type_service
        ]);

        return $this->sendResponse($data, "Listado de portales");
    }


    public function registerPortalesCountries(Request $request)
    {

        $data = [];

        for ($i = 0; $i < count($request->portales); $i++) {
            $find = DB::table('portales_countrie')->where('portale_id', $request->portales[$i]["id"])->where('countrie_id', $request->id)->first();
            $find_object = json_decode(json_encode($find));
            if (array_key_exists('codigo', $request->portales[$i])) {

                if (!$find) {
                    $consult = DB::table('portales_countrie')->insert(
                        ['countrie_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_countrie')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }

                array_push($data, $consult);
            }
        }

        return $this->sendResponse($data, "Registrado");
    }

    public function registerPortalesTipoInmueble(Request $request)
    {

        $data = [];

        for ($i = 0; $i < count($request->portales); $i++) {


            if (array_key_exists('codigo', $request->portales[$i])) {

                $find = DB::table('portales_tipo_inmuebles')->where('portale_id', $request->portales[$i]["id"])->where('tI_id', $request->id)->first();
                $find_object = json_decode(json_encode($find));

                if (!$find) {
                    $consult = DB::table('portales_tipo_inmuebles')->insert(
                        ['tI_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_tipo_inmuebles')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }

                array_push($data, $consult);
            }
        }

        return $this->sendResponse($data, "Registrado");
    }

    public function registerPortalesStates(Request $request)
    {
        //   return $request;
        $data = [];
        for ($i = 0; $i < count($request->portales); $i++) {
            if (array_key_exists("codigo", $request->portales[$i])) {
                $find = DB::table('portales_states')->where('state_id', $request->id)->where('portale_id', $request->portales[$i]["id"])->first();
                $find_object = json_decode(json_encode($find));

                if (!$find) {
                    $consult = DB::table('portales_states')->insert(
                        ['state_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_states')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }
                array_push($data, $consult);
            }
        }
        return $this->sendResponse($data, "Registrado");
    }

    public function registerPortalesCities(Request $request)
    {
        $data = [];
        for ($i = 0; $i < count($request->portales); $i++) {
            if (array_key_exists("codigo", $request->portales[$i])) {
                $find = DB::table('portales_cities')->where('city_id', $request->id)->where('portale_id', $request->portales[$i]["id"])->first();
                $find_object = json_decode(json_encode($find));

                if (!$find) {
                    $consult = DB::table('portales_cities')->insert(
                        ['city_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_cities')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }
                array_push($data, $consult);
            }
        }
        return $this->sendResponse($data, "Registrado");
    }

    public function registerPortalesCaracteristicasInternas(Request $request)
    {
        // return $request;
        $data = [];
        for ($i = 0; $i < count($request->portales); $i++) {

            $find = DB::table('portales_caracteristicas_intarnas')
                ->where('portale_id', $request->portales[$i]["id"])
                ->where('ctin_id', $request->id)
                ->first();

            $find_object = json_decode(json_encode($find));

            if (array_key_exists('codigo', $request->portales[$i])) {

                if (!$find) {
                    $consult = DB::table('portales_caracteristicas_intarnas')->insert(
                        ['ctin_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_caracteristicas_intarnas')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }
                array_push($data, $consult);
            }
        }
        return $this->sendResponse($data, "Registrado");
    }


    public function registerPortalesTipoNegocio(Request $request)
    {

        $data = [];
        for ($i = 0; $i < count($request->portales); $i++) {

            $find = DB::table('portales_inmuebles_tipo_negocio')
                ->where('portale_id', $request->portales[$i]["id"])
                ->where('tipo_negocio_id', $request->id)
                ->first();

            $find_object = json_decode(json_encode($find));

            if (array_key_exists('codigo', $request->portales[$i])) {
                if (!$find) {
                    $consult = DB::table('portales_inmuebles_tipo_negocio')->insert(
                        ['tipo_negocio_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_inmuebles_tipo_negocio')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }
                array_push($data, $consult);
            }
        }
        return $this->sendResponse($data, "Registrado");
    }

    public function registerPortalesEstadoFisico(Request $request)
    {
        // return $request;
        $data = [];
        for ($i = 0; $i < count($request->portales); $i++) {

            $find = DB::table('portales_inmuebles_estado_fisico')
                ->where('portale_id', $request->portales[$i]["id"])
                ->where('state_fisico', $request->id)
                ->first();

            $find_object = json_decode(json_encode($find));
            if (array_key_exists('codigo', $request->portales[$i])) {
                if (!$find) {
                    $consult = DB::table('portales_inmuebles_estado_fisico')->insert(
                        ['state_fisico' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_inmuebles_estado_fisico')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }
                array_push($data, $consult);
            }
        }
        return $this->sendResponse($data, "Registrado");
    }

    public function registerPortalesZona(Request $request)
    {
        // return $request;
        $data = [];
        for ($i = 0; $i < count($request->portales); $i++) {

            $find = DB::table('portales_zona')
                ->where('portale_id', $request->portales[$i]["id"])
                ->where('zona_id', $request->id)
                ->first();

            $find_object = json_decode(json_encode($find));
            if (array_key_exists('codigo', $request->portales[$i])) {

                if (!$find) {
                    $consult = DB::table('portales_zona')->insert(
                        ['zona_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_zona')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }
                array_push($data, $consult);
            }
        }
        return $this->sendResponse($data, "Registrado");
    }

    public function registerPortalesBarrio(Request $request)
    {
        // return $request;
        $data = [];
        for ($i = 0; $i < count($request->portales); $i++) {

            $find = DB::table('portales_barrio')
                ->where('portale_id', $request->portales[$i]["id"])
                ->where('barrio_id', $request->id)
                ->first();

            $find_object = json_decode(json_encode($find));

            if (array_key_exists('codigo', $request->portales[$i])) {

                if (!$find) {
                    $consult = DB::table('portales_barrio')->insert(
                        ['barrio_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_barrio')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }
            }
            array_push($data, $consult);
        }
        return $this->sendResponse($data, "Registrado");
    }


    public function registerPortalesCaracteristicasExternas(Request $request)
    {
        // return $request;
        $data = [];
        for ($i = 0; $i < count($request->portales); $i++) {

            $find = DB::table('portales_caracteristicas_externas')
                ->where('portale_id', $request->portales[$i]["id"])
                ->where('ctex_id', $request->id)
                ->first();

            $find_object = json_decode(json_encode($find));

            if (array_key_exists('codigo', $request->portales[$i])) {

                if (!$find) {
                    $consult = DB::table('portales_caracteristicas_externas')->insert(
                        ['ctex_id' => $request->id, 'portale_id' => $request->portales[$i]["id"], 'codigo' => $request->portales[$i]["codigo"]],
                    );
                } else {
                    $consult = DB::table('portales_caracteristicas_externas')->where('id', $find_object->id)->update(
                        ['codigo' => $request->portales[$i]["codigo"]],
                    );
                }
                array_push($data, $consult);
            }
        }
        return $this->sendResponse($data, "Registrado");
    }


    public function registerCredencialesPortales(Request $request)
    {
        $auth = Auth::user();

        $find = DB::table('portales_credenciales')
            ->where('portale_id', $request->portal)
            ->where('user_id', $auth->id)
            ->first();

        $find_object = json_decode(json_encode($find));


        if (!$find) {
            $consult = DB::table('portales_credenciales')->insert(
                ['user_id' => $auth->id, 'portale_id' => $request->portal, 'token' => $request->token, 'email' => $request->email, 'user' => $request->user],
            );
        } else {
            $consult = DB::table('portales_credenciales')->where('id', $find_object->id)->update(
                ['token' => $request->token, 'email' => $request->email, 'user' => $request->user],
            );
        }

        return $this->sendResponse($consult, "Registrado");
    }

    public function getCredencialesPortales(Request $request)
    {

        $find = DB::table('portales_credenciales')
            ->where('portale_id', $request->portales_id)
            ->where('user_id', $request->user_id)
            ->first();

        $find_object = json_decode(json_encode($find));

        return $this->sendResponse($find_object, "Registrado");
    }





    public function registerCredencialesClasificadosPais(Request $request)
    {
        $auth = Auth::user();

        $find = DB::table('portales_credentials_clasificados_pais')
            ->where('portale_id', $request->portal)
            ->where('user_id', $auth->id)
            ->first();

        $find_object = json_decode(json_encode($find));

        if (!$find) {
            $consult = DB::table('portales_credentials_clasificados_pais')->insert(
                ['user_id' => $auth->id, 'portale_id' => $request->portal, 'username' => $request->username, 'password' => $request->password, 'authorization' => $request->authorization],
            );
        } else {
            $consult = DB::table('portales_credentials_clasificados_pais')->where('id', $find_object->id)->update(
                ['username' => $request->username, 'password' => $request->password, 'authorization' => $request->authorization],
            );
        }

        return $this->sendResponse($consult, "Registrado");
    }

    public function registerCredencialesMetroCuadrado(Request $request)
    {

        $auth = Auth::user();

        $find = DB::table('portales_credentials_metro_cuadrado')
            ->where('portale_id', $request->portal)
            ->where('user_id', $auth->id)
            ->first();

        $find_object = json_decode(json_encode($find));
        $mensage = '';

        if (!$find) {
            $consult = DB::table('portales_credentials_metro_cuadrado')->insert(
                ['xapikey' => $request->xapikey, 'user_id' => $auth->id, 'portale_id' => $request->portal, 'username' => $request->username, 'password' => $request->password, 'client_id' => $request->client_id],
            );
            if ($consult) {
                $mensage = "registrado";
            } else {
                $mensage = "ocurrio un error al registrar las credenciales";
            }
        } else {
            $consult = DB::table('portales_credentials_metro_cuadrado')->where('id', $find_object->id)->update(
                ['xapikey' => $request->xapikey, 'username' => $request->username, 'password' => $request->password, 'client_id' => $request->client_id],
            );
            if ($consult) {
                $mensage = "actualizado";
            } else {
                $mensage = "ocurrio un error al actuaizar las credenciales";
            }
        }

        return $this->sendResponse($consult, $mensage);
    }

    public function registerCredencialesCienCuadra(Request $request)
    {
        $auth = Auth::user();

        $find = DB::table('portales_credentials_ciencuadra')
            ->where('portale_id', $request->portal)
            ->where('user_id', $auth->id)
            ->first();

        $find_object = json_decode(json_encode($find));

        if (!$find) {
            $consult = DB::table('portales_credentials_ciencuadra')->insert(
                ['user_id' => $auth->id, 'portale_id' => $request->portal, 'username' => $request->username, 'password' => $request->password],
            );
        } else {
            $consult = DB::table('portales_credentials_ciencuadra')->where('id', $find_object->id)->update(
                ['username' => $request->username, 'password' => $request->password],
            );
        }

        return $this->sendResponse($consult, "Registrado");
    }
}
