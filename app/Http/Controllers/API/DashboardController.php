<?php

namespace App\Http\Controllers\API;

use App\Models\Cliente;
use App\Models\Contact;
use App\Models\Inmuebles;
use App\Models\InmueblesViews;
use App\Models\User;
use App\Models\Userdata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends ApiController
{

    public function countDashboard()
    {
        $user = Auth::user();

        $visitas=array();
        $c=array();
        $inmuebles=array();
        $cli=array();
        $referidos=array();

        $year=date("Y");
        $m=date("M");

        $datavisitas=array();
        $datacontactos=array();
        $datainmuebles=array();
        $dataclientes=array();
        $datareferidos=array();

        for ($i=1; $i <=12 ; $i++) { 

            $cantidad=InmueblesViews::join('inmuebles', 'inmuebles_views.id_inmueble', 'inmuebles.id')
            ->where('inmuebles.user_id', $user->id)
            ->whereYear('inmuebles_views.created_at', '=', $year)
            ->whereMonth('inmuebles_views.created_at', '=', $i)
            ->count();

            $datavisitas[]=$cantidad;

            $cantidadcontacto=Contact::where('contacts_inmueble.user_id', $user->id)
                ->whereYear('contacts_inmueble.created_at', '=', $year)
                ->whereMonth('contacts_inmueble.created_at', '=', $i)
                ->count();

            $datacontactos[]=$cantidadcontacto;

            $cantidadinmuebles=Inmuebles::where('inmuebles.user_id', $user->id)
                ->whereYear('inmuebles.created_at', '=', $year)
                ->whereMonth('inmuebles.created_at', '=', $i)
                ->count();

            $datainmuebles[]=$cantidadinmuebles;


            $cantidadclientes=Cliente::where('clientes.user_id', $user->id)
                ->whereYear('clientes.created_at', '=', $year)
                ->whereMonth('clientes.created_at', '=', $i)
                ->count();

            $dataclientes[]=$cantidadclientes;

            $cantreferidos=User::where('users.referred_by', $user->id)
            ->whereYear('users.created_at', '=', $year)
            ->whereMonth('users.created_at', '=', $i)
            ->count();

            $datareferidos[]=$cantreferidos;
            
        }


        $contactos=Contact::where('user_id', $user->id)->count();

        $visitas['options']['chart']['id']='Visitas x mes';
        $visitas['options']['xaxis']['categories']=['Enero', 'Febrero'];
        $visitas['series']['name']='Total';
        $visitas['series']['data']=$datavisitas;


        $c['options']['chart']['id']='Contactos x mes';
        $c['options']['xaxis']['categories']=['Enero', 'Febrero'];
        $c['series']['name']='Total';
        $c['series']['data']=$datacontactos;

        $inmuebles['options']['chart']['id']='Inmuebles x mes';
        $inmuebles['options']['xaxis']['categories']=['Enero', 'Febrero'];
        $inmuebles['series']['name']='Total';
        $inmuebles['series']['data']=$datainmuebles;

        $cli['options']['chart']['id']='cli x mes';
        $cli['options']['xaxis']['categories']=['Enero', 'Febrero'];
        $cli['series']['name']='Total';
        $cli['series']['data']=$dataclientes;

        $referidos['options']['chart']['id']='referidos x mes';
        $referidos['options']['xaxis']['categories']=['Enero', 'Febrero'];
        $referidos['series']['name']='Total';
        $referidos['series']['data']=$datareferidos;

        $data['visitas']=$visitas;
        $data['c']=$c;
        $data['inmuebles']=$inmuebles;
        $data['cli']=$cli;
        $data['referidos']=$referidos;

        $data['contactos'] = $contactos;

        $data['clientes']  = Cliente::where('user_id', $user->id)->count();

        $data['inmuebles_ventas'] =  DB::table('inmuebles')
            ->leftJoin('inmuebles_tipo_negocio', 'inmuebles.tipo_negocio', '=', 'inmuebles_tipo_negocio.id')
            ->where('inmuebles_tipo_negocio.tipo', '=', "Venta")
            ->where('user_id', $user->id)
            ->count();

        $data['inmuebles_arrendo'] = DB::table('inmuebles')
            ->leftJoin('inmuebles_tipo_negocio', 'inmuebles.tipo_negocio', '=', 'inmuebles_tipo_negocio.id')
            ->where('inmuebles_tipo_negocio.tipo', '=', "Alquiler")
            ->where('user_id', $user->id)
            ->count();


        $data['casa'] = DB::table('inmuebles')
        ->where('inmuebles.tipo_inmueble', '=', "2")
        ->where('user_id', $user->id)
        ->count();

        $data['apartamento'] = DB::table('inmuebles')
        ->where('inmuebles.tipo_inmueble', '=', "3")
        ->where('user_id', $user->id)
        ->count();

        // Inmuebles::with(['tipo_negocio' => function ($query) {
        //     $query->where('tipo', 'Alquiler')->get();
        // }])->count();

        return $this->sendResponse($data, "Dashboard");
    }
}
