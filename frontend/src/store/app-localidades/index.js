import axios from "@axios";

export default {
  namespaced: true,
  state: {
    ciudades: [],
    estados: [],
    paises: [],
    zonas: [],
    barrios: [],
  },
  getters: {},
  mutations: {
    GET_CIUDADES(state, val) {
      state.ciudades = val;
    },

    GET_STATES(state, val) {
      state.estados = val;
    },

    GET_PAISES(state, val) {
      state.paises = val;
    },

    GET_BARRIOS(state, val) {
      state.barrios = val;
    },

    GET_ZONAS(state, val) {
      state.zonas = val;
    },
  },
  actions: {
    getPaises(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/localidad/paises")
          .then((response) => {
            ctx.commit("GET_PAISES", response.data.data.paises);
            resolve(response.data.data.paises);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getStates(ctx, id_country) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/localidad/states/" + id_country.codigo)
          .then((response) => {
            ctx.commit("GET_STATES", response.data.data.estados);
            resolve(response.data.data.estados);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getState(ctx, id_estado) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/localidad/state/" + id_estado.codigo)
          .then((response) => {
            resolve(response.data.data.estado);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    

    getCiudades(ctx, id_state) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/localidad/ciudades/" + id_state.codigo)
          .then((response) => {
            ctx.commit("GET_CIUDADES", response.data.data.ciudades);
            resolve(response.data.data.ciudades);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getZona(ctx, id_ciudad) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/localidad/zona/" + id_ciudad.codigo)
          .then((response) => {
            ctx.commit("GET_ZONAS", response.data.data.zonas);
            resolve(response.data.data.zonas);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getBarrio(ctx, id_zona) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/localidad/barrio/" + id_zona.codigo)
          .then((response) => {
            ctx.commit("GET_BARRIOS", response.data.data.barrios);
            resolve(response.data.data.barrios);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    saveZona(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`/api/auth/localidad/zona`, data)
          .then((response) => {
            resolve(response.data.data.zona);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    saveBarrio(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`/api/auth/localidad/barrio`, data)
          .then((response) => {
            resolve(response.data.data.barrio);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
};
