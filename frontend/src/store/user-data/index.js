

export default {
  namespaced: true,
  state: {
    userData: JSON.parse(localStorage.getItem('userData')),
    user: JSON.parse(localStorage.getItem('user')),
  },
  getters: {},
  mutations: {
    UPDATE_USER_DATA(state, val) {
      state.userData = val
    },

    UPDATE_USER(state, val) {
      state.user = val
    },

    DELETE(state, val) {
      state.userData = val
      state.user = val
    },
  },
  actions: {},
}
