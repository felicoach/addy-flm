import Vue from 'vue'
import Vuex from 'vuex'

// Modules
//import ecommerceStoreModule from '@/views/apps/e-commerce/eCommerceStoreModule'
import empresaStoreModule from '@/views/apps/empresas/empresaStoreModule'
import referidoStoreModule from '@/views/apps/referidos/referidoStoreModule'
import inmuebleStoreModule from "@/views/apps/public/inmuebleStoreModule";
import templateStoreModule from "@/views/templates/templateStoreModule";

import app from './app'
import appConfig from './app-config'
import verticalMenu from './vertical-menu'
import userData from './user-data'
import dashboardData from './dashboard-data'
import appLocalidades from './app-localidades'
import appConfiguracion from "./app-configuration";
import appDocumentos from './app-documentos';
import clienteStoreModule from '@/views/apps/clientes/clienteStoreModule';
import userStoreModule from '@/views/apps/user/userStoreModule';
import MercadeoStoreModule from '@/views/apps/mercadeo/MercadeoStoreModule';

import DashboardStoreModule from '@/views/apps/dashboard/DashboardStoreModule';
import todoStoreModule from '@/views/apps/tareas/todoStoreModule';
import calendarStoreModule from '@/views/apps/calendar/calendarStoreModule';
import roleStoreModule from '@/views/apps/roles/roleStoreModule';
import InmuebleStoreModule from '@/views/apps/inmuebles/InmuebleStoreModule';
import inmobiliariaStoreModule from '@/views/apps/inmobiliarias/inmobiliariaStoreModule';
import portalesStoreModule from '@/views/apps/portales/portalesStoreModule';
import contactStoreModule from '@/views/apps/contactos/contactStoreModule';
import mercadeoAdminStoreModule from '@/views/admin/mercadeo/mercadeoAdminStoreModule';


Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    appConfig,
    verticalMenu,
    referidoStoreModule,
    userData,
    'banners':templateStoreModule,
    'empresa': empresaStoreModule,
    'appInmueble': inmuebleStoreModule,
    'apiInmueblePrivate': InmuebleStoreModule,
    'appCliente': clienteStoreModule,
    'appMercadeo': MercadeoStoreModule,
    'appUser': userStoreModule,
    'appDashboard': DashboardStoreModule,
    'appTareas': todoStoreModule,
    'appCalendario': calendarStoreModule,
    'appRole': roleStoreModule,
    'appInmobiliaria': inmobiliariaStoreModule,
    'appTask': todoStoreModule,
    'appPortales': portalesStoreModule,
    "appContact": contactStoreModule,
    "appMercadeoAdmin": mercadeoAdminStoreModule,
    "appCalendar": calendarStoreModule,
    appConfiguracion,
    appDocumentos,
    dashboardData,
    appLocalidades
  },
  strict: process.env.DEV,
})
