import axios from "@axios";

export default {
  namespaced: true,
  state: {
    tipo_documento: [],

  },
  getters: {},
  mutations: {
    GET_TIPO_DOCUMENTO(state, val) {
      state.tipo_documento = val;
    },


  },
  actions: {
    getTipoDocumento(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/documento/tipo_docuemnto")
          .then((response) => {
            ctx.commit("GET_TIPO_DOCUMENTO", response.data.data.tipo_documento);
            resolve(response.data.data.tipo_documento);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getTipoCliente(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/documento/tipo_cliente")
          .then((response) => {
            resolve(response.data.data.tipo_cliente);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getNumerosClientes(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/documento/celulares")
          .then((response) => {
            resolve(response.data.data.numero);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    registerNumerosClientes(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/documento/celulares", data)
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    updateNumerosClientes(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .put("api/auth/documento/celulares")
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    deleteNumerosClientes(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .delete("api/auth/documento/celulares")
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    






  },
};
