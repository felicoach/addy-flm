import axios from "@axios";

export default {
    namespaced: true,

    state: {
        empresas: [],
        configuracion: [],
        dominio: [],
    },

    getters: {},

    mutations: {
        SET_EMPRESAS(state, val) {
            state.empresas = val;
        },

        SET_CONFIGURATION(state, val) {
            state.configuracion = val
        },

        SET_DOMINIO(state, val) {
            state.dominio = val
        }
    },

    actions: {

        
        getEmpresas(ctx) {
            return new Promise((resolve, reject) => {
                axios.get("api/auth/agente/empresas")
                    .then((response) => {
                        ctx.commit("SET_EMPRESAS", response.data.data.empresas);
                        
                        resolve(response.data.data.empresas);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        addEmpresa(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post("api/auth/agente/empresas", data)
                    .then((response) => {
                        ctx.commit("SET_CONFIGURATION", response.data.data.empresa);
                        
                        resolve(response);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        updateEmpresa(data) {
            return new Promise((resolve, reject) => {
                axios.put(`api/auth/agente/empresas/${data.id}`, data)
                    .then((response) => {
                        ctx.commit("SET_CONFIGURATION", response.data.data.empresa);
                        resolve(response.data.data.empresa);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        getEmpresa(ctx, agente) {
            return new Promise((resolve, reject) => {
                axios.get(`api/auth/agente/empresa`)
                    .then((response) => {
                        ctx.commit("SET_CONFIGURATION", response.data.data.empresa);
                        ctx.commit("SET_DOMINIO", response.data.data.dominio);
                        console.log(response.data.data.dominio);
                        resolve(response.data.data.empresa);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        getDominio(ctx, agente) {
            return new Promise((resolve, reject) => {
                axios.get(`api/auth/agente/empresa`)
                    .then((response) => {
                        ctx.commit("SET_DOMINIO", response.data.data.dominio);
                        console.log('getdominio'+response.data.data.dominio);
                        resolve(response.data.data.dominio);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },


        getEmpresasForId(ctx) {
            return new Promise((resolve, reject) => {
                axios.get("api/auth/agente/empresas")
                    .then((response) => {
                        ctx.commit("SET_EMPRESAS", response.data.data.empresas);
                        resolve(response.data.data.empresas);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        addEmpresasEmails(agente) {
            return new Promise((resolve, reject) => {
                axios.post(`api/auth/agente/empresas/emails/${agente}`)
                    .then((response) => {
                        resolve(response.data.data.emails);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        getEmpresasEmails(agente) {
            return new Promise((resolve, reject) => {
                axios.get(`api/auth/agente/empresas/emails${agente}`)
                    .then((response) => {
                        resolve(response.data.data.emails);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        addEmpresasNumbers(agente) {
            return new Promise((resolve, reject) => {
                axios.post(`api/auth/agente/empresas/numbers/${agente}`)
                    .then((response) => {
                        resolve(response.data.data.number);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        getEmpresasNumbers(agente) {
            return new Promise((resolve, reject) => {
                axios.get(`api/auth/agente/empresas/numbers/${agente}`)
                    .then((response) => {
                        resolve(response.data.data.number);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        addEmpresasRedes(agente) {
            return new Promise((resolve, reject) => {
                axios.post(`api/auth/agente/empresas/redes/${agente}`)
                    .then((response) => {
                        resolve(response.data.data.redes);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },

        getEmpresasRedes(agente) {
            return new Promise((resolve, reject) => {
                axios.get(`api/auth/agente/empresas/redes/${agente}`)
                    .then((response) => {
                        resolve(response.data.data.redes);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        }
    },
};
