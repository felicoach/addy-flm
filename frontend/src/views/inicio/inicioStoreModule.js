import axios from "@axios";

export default {
  namespaced: true,

  state: {
  
  },
  getters: {},
  mutations: {
   
  },
  actions: {
    getMonyCountReferidos(ctx, users) {
  
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/users/referido/money/"+ users.id)
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
};
