import axios from '@axios'


export default {
  namespaced: true,
  state: {
    banners:[]
  },
  getters: {},
    mutations: {
        SET_BANNERS: (state, payload) => {
        state.banners = payload;
    },
  },

  actions: {

    getBanners(ctx, queryParams) {

      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/banners/list', {})
          .then(response => {
            ctx.commit('SET_BANNERS', response.data)
            resolve(response.data)
          })
          .catch(error => reject(error))
      })
    },
    fetchBanner(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    
   
    addBanners(ctx, banners) {
      console.log(banners)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/banners/store', banners)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    delBanners(ctx, banners) {
      console.log(banners)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/banners/del', banners)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    updBanners(ctx, banners) {
      console.log(banners)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/banners/upd', banners)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    getBanner(ctx, banners) {
      console.log(banners)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/banners/get', banners)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    /** Rutas de blogs  */

    getBlogs(ctx, queryParams) {

      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/blogs/list', {})
          .then(response => {
            ctx.commit('SET_BANNERS', response.data)
            resolve(response.data)
          })
          .catch(error => reject(error))
      })
    },
    
    
   
    addblog(ctx, blog) {
      console.log(blog)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/blogs/store', blog)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    delBlog(ctx, blog) {
      console.log(blog)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/blogs/del', blog)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    updblog(ctx, blog) {
      console.log(blog)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/blogs/upd', blog)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    getblog(ctx, blogs) {
      console.log(blogs)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/blogs/get', blogs)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },



    gettemplates(ctx, queryParams) {

      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/template/list', {})
          .then(response => {
            ctx.commit('SET_BANNERS', response.data)
            resolve(response.data)
          })
          .catch(error => reject(error))
      })
    },

    settemplates(ctx, id) {
      console.log(id)
      return new Promise((resolve, reject) => {
        axios
          .post('/api/auth/template/settemplate/'+id)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    
    
   
    addtemplate(ctx, template) {
      console.log(template)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/template/store', template)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    deltemplate(ctx, template) {
      console.log(template)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/template/del', template)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    updtemplate(ctx, template) {
      console.log(template)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/template/upd', template)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    gettemplate(ctx, template) {
      console.log(template)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/template/get', template)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },



    addperfiles(ctx, perfiles) {
      console.log(perfiles)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/perfiles/store', perfiles)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    delperfiles(ctx, perfiles) {
      console.log(perfiles)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/perfiles/del', perfiles)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    updperfiles(ctx, perfiles) {
      console.log(perfiles)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/perfiles/upd', perfiles)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    getperfiles(ctx, perfiles) {
      console.log(perfiles)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/perfiles/get', perfiles)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },



    
    updcredenciales(ctx, credenciales) {
      console.log(credenciales)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/credenciales/store', credenciales)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    getcredenciales(ctx, credenciales) {
      console.log(credenciales)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/credenciales/get', credenciales)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },








  },
}
