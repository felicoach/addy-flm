import axios from "@axios";


export default {
  namespaced: true,

  state: {
    users: [],
  },

  getters: {},

  mutations: {
    SET_USERS(state, payload) {
      state.users = payload;
    },
  },
  actions: {
    fetchUsers(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/usuarios", { params: queryParams })
          .then((response) => {
            ctx.commit("SET_USERS", response.data.data.users);

            resolve(response.data.data.users);
          })
          .catch((error) => reject(error));
      });
    },

    getUserSession(ctx, code) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/users/informacion/${code}`)
          .then((response) => {
            resolve(response.data.data.user);
          })
          .catch((error) => reject(error));
      });
    },

    fetchUser(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/apps/user/users/${id}`)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      });
    },

    updatedUser(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .put(`/api/auth/usuarios/${userData.id}`, userData)
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => reject(error));
      });
    },

    addUser(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post("/api/auth/usuarios", userData)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      });
    },
  },
};
