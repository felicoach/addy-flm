import axios from '@axios'
import { paginateArray, sortCompare } from '@/@fake-db/utils'

export default {
    namespaced: true,
    state: {
        contact: [],
    },
    getters: {},
    mutations: {
        SET_CONTACT: (state, payload) => {
            state.contact = payload;
        },
    },
    actions: {
        fetchContact(ctx) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/api/auth/contact`)
                    .then((response) => {
                        console.log(response)
                        ctx.commit("SET_CONTACT", response.data.data.contact);
                        resolve(response.data.data.contact)
                    })
                    .catch(error => reject(error))
            })
        },

    },


}
