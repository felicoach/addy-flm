import axios from "@axios";

export default {
  namespaced: true,

  state: {
    referido: [],
  },
  getters: {},
  mutations: {
    SET_REFERIDOS: (state, payload) => {
      state.referido = payload;
    },
  },
  actions: {
    getReferidos(ctx, referral_code) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/users/referidos/" + referral_code.data)
          .then((response) => {
            ctx.commit("SET_REFERIDOS", response.data.data.referidos);
            resolve(response.data.data.referidos);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    fetchCreateReferido(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/referidos/create")
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getReferidoForId(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/users/${id}`)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      });
    },

    updateReferido(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .put("api/auth/users/" + data.id_referido, data)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    fetchReferidosAdicional(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/referidos/adicional", data)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    addReferido(ctx, referido) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/users", referido)
          .then((response) => {
            console.log(response);
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    validateCampos(ctx, referido) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/users/referido/validacion", referido)
          .then((response) => {
            console.log(response);
            resolve(response);
          })
          .catch((error) => {
            console.log(error);
            reject(error);
          });
      });
    },
  },
};
