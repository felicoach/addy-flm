import axios from '@axios'

export default {
  namespaced: true,
  state: {
    roles: [],
    permissions: [],
    actions: [],
    permissions_actions: [],
    role_users: [],
    role_users_all: []
  },
  getters: {},
  mutations: {

    SET_ROLES(state, payload) {
      state.roles = payload
    },

    SET_ROLES_USER(state, payload) {
      state.role_users = payload
    },

    SET_ROLES_USER_ALL(state, payload) {
      state.role_users_all = payload
    },

    SET_PERMISSIONS(state, payload) {
      state.permissions = payload
    },

    SET_ACTIONS(state, payload) {
      state.actions = payload
    },

    SET_PERMISSIONS_ACTIONS(state, payload) {
      state.permissions_actions = payload
    }

  },
  actions: {
    getRoles(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/roles')
          .then(response => {
            ctx.commit("SET_ROLES", response.data.data.roles)
            resolve(response.data.data.roles)
          })
          .catch(error => reject(error))
      })
    },

    getRolesForUser(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/roles-users`)
          .then(response => {
            ctx.commit("SET_ROLES_USER", response.data.data.roles)
            resolve(response.data.data.roles)
          })
          .catch(error => reject(error))
      })
    },


    getRolesForUserAll(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/roles-users-all`)
          .then(response => {
            ctx.commit("SET_ROLES_USER_ALL", response.data.data.roles)
            resolve(response.data.data.roles)
          })
          .catch(error => reject(error))
      })
    },

  

    getModules(ctx, id) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/permission/${id}`)
          .then(response => {
            resolve(response.data.data)
          })
          .catch(error => reject(error))
      })
    },

    getPermissions(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/permission`)
          .then(response => {
            ctx.commit("SET_PERMISSIONS", response.data.data.permissions)
            resolve(response.data.data.permissions)
          })
          .catch(error => reject(error))
      })
    },

    addPermissions(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`api/auth/permissions-module`, data)
          .then(response => {
            resolve(response.data.data)
          })
          .catch(error => reject(error))
      })
    },



    getPermissionsActions(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/permissions-actions`)
          .then(response => {

            ctx.commit("SET_PERMISSIONS_ACTIONS", response.data.data.permissions)
            resolve(response.data.data.permissions)
          })
          .catch(error => reject(error))
      })

    },

    getActionsForId(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/actions/${data.id}`)
          .then(response => {
            resolve(response.data.data.actions_id)
          })
          .catch(error => reject(error))
      })

    },
    addActions(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`api/auth/actions-modules`, data)
          .then(response => {
            resolve(response.data.data)
          })
          .catch(error => reject(error))
      })

    },

    getActions(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/actions`)
          .then(response => {
            ctx.commit("SET_ACTIONS", response.data.data.actions)
            resolve(response.data.data.actions)
          })
          .catch(error => reject(error))
      })

    },

    addPermissionAction(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`api/auth/actions`, data)
          .then(response => {
            resolve(response.data.data)
          })
          .catch(error => reject(error))
      })
    },

    addPermissionsForId(cxt, data) {
      console.log(data)
      return new Promise((resolve, reject) => {
        axios
          .post(`api/auth/permission`, data)
          .then(response => {
            resolve(response.data.data)
          })
          .catch(error => reject(error))
      })
    },


    getPermissionsForRoleId(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/permission/role/${data.role}`)
          .then(response => {
            resolve(response.data.data.permissions)
          })
          .catch(error => reject(error))
      })
    },

    getPermissionsActionsForRoleId(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/permission/role-permission/${data.role}`)
          .then(response => {
            resolve(response.data.data.permissions)
          })
          .catch(error => reject(error))
      })
    },

    getPermissionActiveRole(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`api/auth/permissions-active-actions`, data)
          .then(response => {
            console.log(response)
            resolve(response.data.data.active_permissions)
          })
          .catch(error => reject(error))
      })
    },



    addActivePermissionsActions(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`api/auth/permission/permissions-active-role`, data)
          .then(response => {
            resolve(response.data.data)
          })
          .catch(error => reject(error))
      })
    },


    getPermissionRole(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/roles/permission/${data.roles}`)
          .then(response => {
            resolve(response.data.data)
          })
          .catch(error => reject(error))
      })
    },

    addPermissionRole(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`api/auth/roles/permission/`, data)
          .then(response => {
            resolve()
          })
          .catch(error => reject(error))
      })
    },

    edictRoles(ctx, roles) {
      return new Promise((resolve, reject) => {
        axios
          .post('/api/auth/roles', roles.roles)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addRoles(ctx, roles) {
      return new Promise((resolve, reject) => {
        axios
          .post('/api/auth/roles', roles.roles)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
