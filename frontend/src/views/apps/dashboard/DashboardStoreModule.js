import axios from '@axios'
import { paginateArray, sortCompare } from '@/@fake-db/utils'

export default {
    namespaced: true,
    state: {
        modal: true,
        visitas: 0,
        contactos: 0,
        clientes: 0,
        inmuebles_vendas: 0,
        inmuebles_arriendo: 0
    },
    getters: {},
    mutations: {
        SET_CONTACTOS: (state, payload) => {
            state.contactos = payload;
        },

        SET_CLIENTE: (state, payload) => {
            state.clientes = payload;
        },

        SET_INMUEBLES_VENTAS: (state, payload) => {
            state.inmuebles_vendas = payload;
        },

        SET_INMUEBLES_ARRIENDO: (state, payload) => {
            state.inmuebles_arriendo = payload;
        },

        SET_VISITAS: (state, payload) => {
            state.visitas = payload;
        },

        SET_MODAL: (state, payload) => {
            state.modal = payload;
        },
    },
    actions: {
        setModal(ctx, data) {
            ctx.commit("SET_MODAL", data);
        },
        fetchDashboard(ctx, data) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/api/auth/dashboard`)
                    .then((response) => {

                        let data = response.data.data
                        ctx.commit("SET_CONTACTOS", data.contactos);
                        ctx.commit("SET_CLIENTE", data.clientes);
                        ctx.commit("SET_INMUEBLES_VENTAS", data.inmuebles_ventas);
                        ctx.commit("SET_INMUEBLES_ARRIENDO", data.inmuebles_arrendo);
                        ctx.commit("SET_VISITAS", data.visitas);

                        resolve(response.data.data);

                    })
                    .catch(error => reject(error))
            })
        },

        getPorcentageArrendo(ctx) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/api/auth/api-inmueble/arrendo`)
                    .then((response) => {
                      
                        let data = response.data.data.inventario_arriendo

                        let tipo_inmueble = [];

                        for (let i = 0; i < data.length; i++) {
                            tipo_inmueble.push(data[i])
                        }

                        resolve(tipo_inmueble);

                    })
                    .catch(error => reject(error))
            })
        },

        getPorcentageVenta(ctx) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/api/auth/api-inmueble/venta`)
                    .then((response) => {
                      
                        let data = response.data.data.inventario_venta

                        let tipo_inmueble = [];

                        for (let i = 0; i < data.length; i++) {
                            tipo_inmueble.push(data[i])
                        }

                        resolve(tipo_inmueble);

                    })
                    .catch(error => reject(error))
            })
        },

    }
}
