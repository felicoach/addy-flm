import axios from "@axios";

export default {
  namespaced: true,

  state: {
    inmuebles: [],
    inmueblesAll: [],
    antiguedad: [],
  },
  getters: {},
  mutations: {
    SET_INMUEBLES: (state, payload) => {
      state.inmuebles = payload;
    },

    UPDATE_INMUEBLE: (state, payload) => {
      state.inmuebles = state.inmuebles.map(inm => {
        if (inm.id === payload.id) {
          return Object.assign({}, inm, payload)
        }
        return inm
      })
   
    },

    SET_INMUEBLES_ALL: (state, payload) => {
      state.inmueblesAll = payload;
    },
    SET_ANTIGUEDAD(state, payload) {
      state.antiguedad = payload;
    }
  },
  actions: {
    getInmueblesAll(ctx, payload) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api-inmueble/inmuebles")
          .then((response) => {
            ctx.commit("SET_INMUEBLES_ALL", response.data.data.inmuebles);
            let data = response.data.data;
            const { q = '', sortBy = 'featured', perPage = 9, page = 1 } = payload

            const queryLowered = q.toLowerCase()

            const filteredData = data.inmuebles.filter(product => product.titulo_inmueble.toLowerCase().includes(queryLowered))

            let sortDesc = false
            const sortByKey = (() => {
              if (sortBy === 'price-desc') {
                sortDesc = true
                return 'price'
              }
              if (sortBy === 'price-asc') {
                return 'price'
              }
              sortDesc = true
              return 'id'
            })()

            const sortedData = filteredData.sort(sortCompare(sortByKey))

            if (sortDesc) sortedData.reverse()

            const paginatedData = JSON.parse(JSON.stringify(paginateArray(sortedData, perPage, page)))

            paginatedData.forEach(product => {
              /* eslint-disable no-param-reassign */
              product.isInWishlist = data.userWishlist.findIndex(p => p.productId === product.id) > -1
              product.isInCart = data.userCart.findIndex(p => p.productId === product.id) > -1
              /* eslint-enable */
            })

            return [
              200,
              {
                products: paginatedData,
                total: filteredData.length,
                userWishlist: data.userWishlist,
                userCart: data.userCart,
              },
            ]

          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    orderImage(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("/api/auth/api-inmueble/order-image",  data )
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },


    getInmuebles(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/inmuebles")
          .then((response) => {
            ctx.commit("SET_INMUEBLES", response.data.data.inmuebles);
            resolve(response.data.data.inmuebles);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    downloadPdf(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/api-inmueble/download_pdf`, { params: data.data, responseType: 'arraybuffer' })
          .then((response) => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data], { type: 'application/pdf' }));
            var fileLink = document.createElement('a');
            fileLink.href = fileURL;

            let mensaje = "documento";

            if (data.type == 'cedula') {
              mensaje = 'cedula'
            }
            if (data.type == 'predial') {
              mensaje = 'predial'
            }
            if (data.type == 'certificado_tradiccion') {
              mensaje = 'certificado de tradiccion'
            }

            fileLink.setAttribute('download', `${mensaje}.pdf`);
            document.body.appendChild(fileLink);
            fileLink.click();

            resolve(true)

          })
          .catch((error) => reject(error));
      });
    },


    showPDF(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/api-inmueble/download_pdf`, { params: data.data, responseType: 'arraybuffer' })
          .then((response) => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data], { type: 'application/pdf' }));
            var fileLink = document.createElement('a');
            fileLink.href = fileURL;
            //fileLink.setAttribute('download', `${mensaje}.pdf`);
            document.body.appendChild(fileLink);
            //fileLink.click();

            resolve(fileLink)

          })
          .catch((error) => reject(error));
      });
    },

    exportInmueble(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/api-inmueble/export`, { responseType: 'arraybuffer' })
          .then((response) => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data], { type: 'application/pdf' }));
            var fileLink = document.createElement('a');
            fileLink.href = fileURL;
            fileLink.setAttribute('download', 'inmuebles.xlsx');
            document.body.appendChild(fileLink);
            fileLink.click();
            resolve(true)

          })
          .catch((error) => reject(error));
      });
    },

    soatAction(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`/api/auth/service`, data)
          .then((response) => resolve(response.data))
          .catch((error) => reject(error));
      });

    },

    fetchInmuebles(ctx, payload) {
      return new Promise((resolve, reject) => {
        axios
          .get("/apps/ecommerce/products", { params: payload })
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      });
    },


    getAntiguedad(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/antiguedad")
          .then((response) => {
            ctx.commit("SET_ANTIGUEDAD", response.data.data.antiguedad);

            resolve(response.data.data.antiguedad);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },


    fetchInmueble(ctx, productId) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/api-inmueble/inmuebles/url/${productId.productSlug}`)
          .then((response) => {
            resolve(response.data.data.inmueble);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    filterInmueble(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("/api/auth/api-inmueble/filter", data)
          .then((response) => {

            resolve(response.data.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
};
