import axios from "@axios";
import { promiseTimeout } from "@vueuse/shared";

export default {
  namespaced: true,

  state: {
    inmuebles: [],
    tipo_inmuebles: [],
    country: [],
    states: [],
    city: [],
    tipo_negocio: [],
    state_fisico: [],
    caracteristicas_internas: [],
    caracteristicas_externas: [],
    segmento_mercado: [],
    state_publication: [],
    estratos: [],
    tipo_parqueadero: [],
    rangue_price: []
  },
  getters: {},
  mutations: {

    SET_INMUEBLES: (state, payload) => {
      state.inmuebles = payload;
    },

    SET_STATE_PUBLICACION(state, payload) {
      state.state_publication = payload
    },

    SET_SEGMENTO_MERCADO(state, payload) {
      state.segmento_mercado = payload
    },

    SET_TIPO_NEGOCIO: (state, payload) => {
      state.tipo_negocio = payload
    },

    SET_STATE_FISICO: (state, payload) => {
      state.state_fisico = payload
    },

    SET_TIPO_INMUEBLES: (state, payload) => {
      state.tipo_inmuebles = payload;
    },

    GET_COUNTRY: (state, payload) => {
      state.country = payload;
    },

    SET_TIPO_PARQUEADERO(state, payload) {
      state.tipo_parqueadero = payload;
    },

    SET_ESTRATOS(state, payload) {
      state.estratos = payload
    },

    SET_CARACTERISTICAS_INTERNAS: (state, payload) => {
      state.caracteristicas_internas = payload
    },

    SET_CARACTERISTICAS_EXTERNAS: (state, payload) => {
      state.caracteristicas_externas = payload
    },

    RANGUE_PRICE: (state, payload) => {
      state.rangue_price = payload
    },

    GET_STATES: (state, payload) => {
      state.states = payload;
    },

    GET_CITY: (state, payload) => {
      state.city = payload;
    },


  },
  actions: {
    getInmuebles(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/inmuebles/" + userData, {})
          .then((response) => {
            ctx.commit("SET_INMUEBLES", response.data.data.inmuebles);
            resolve(response.data.data.inmuebles);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    fetchInmueble(ctx, productId) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/api-inmueble/inmuebles/${productId.productSlug}`)
          .then((response) => {
            resolve(response.data.data.inmueble);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    removeImage(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/api-inmueble/delete-image", data)
          .then((response) => {
            resolve(response.data.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    editImage(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/api-inmueble/edit-image", data)
          .then((response) => {
            resolve(response.data.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    addImage(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/api-inmueble/add-image", data)
          .then((response) => {
            resolve(response.data.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    updImage(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/api-inmueble/upd-image", data)
          .then((response) => {
            resolve(response.data.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    delImage(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/api-inmueble/del-image", data)
          .then((response) => {
            resolve(response.data.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },


    getReferidoForId(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/users/${id}`)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      });
    },

    updateReferido(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .put("api/auth/users/" + data.id_referido, data)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getTipoInmueble(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/api-inmueble/tipo_inmuebles")
          .then((response) => {
            ctx.commit("SET_TIPO_INMUEBLES", response.data.data.tipo_inmuebles);
            resolve(response.data.data.tipo_inmuebles);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getPorcentageArrendo(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/api-inmueble/arrendo`)
          .then((response) => {
            let data = response.data.data.inventario_arriendo

            chartInfo = {
              icon: null,
              name: null,
              iconColor: 'text-primary',
              usage: 0,
              upDown: 0,
            };

            let tipo_inmueble = [];

            for (let i = 0; i < data.length; i++) {
              tipo_inmueble.push(data[i].tipo_inmueble)
            }

            resolve(tipo_inmueble);

          })
          .catch(error => reject(error))
      })
    },

    getPorcentageVenta() {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/api-inmueble/arrendo`)
          .then((response) => {
            console.log(response)
          })
          .catch(error => reject(error))
      })
    },

    getTipoNegocio(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/api-inmueble/tipo_negocio")
          .then((response) => {
            ctx.commit('SET_TIPO_NEGOCIO', response.data.data.tipo_negocio)
            resolve(response.data.data.tipo_negocio);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    fetchReferidosAdicional(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/referidos/adicional", data)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    validateCampos(ctx, referido) {
      return new Promise((resolve, reject) => {
        axios
          .post("api/auth/referidos/validacion", referido)
          .then((response) => {
            console.log(response);
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getCaracteristicasInternas(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/api-inmueble/caracteristicas_internas")
          .then((response) => {
            ctx.commit('SET_CARACTERISTICAS_INTERNAS', response.data.data.caracteristicas_internas)
            resolve(response.data.data.caracteristicas_internas);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getCaracteristicasExternas(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("api/auth/api-inmueble/caracteristicas_externas")
          .then((response) => {
            ctx.commit('SET_CARACTERISTICAS_EXTERNAS', response.data.data.caracteristicas_externas)
            resolve(response.data.data.caracteristicas_externas);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getStatePublicacion(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/stete_publication")
          .then((response) => {
            ctx.commit('SET_STATE_PUBLICACION', response.data.data.tipe_publication)
            resolve(response.data.data.tipe_publication);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getSegmentoMercado(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/segmento_mercado")
          .then((response) => {
            ctx.commit('SET_SEGMENTO_MERCADO', response.data.data.segmento_mercado)
            resolve(response.data.data.segmento_mercado);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getStateFisico(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/estado_fisico")
          .then((response) => {
            ctx.commit('SET_STATE_FISICO', response.data.data.estado_fisico)
            resolve(response.data.data.estado_fisico);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getEstratoFisico(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/estratos")
          .then((response) => {
            ctx.commit('SET_ESTRATOS', response.data.data.estratos)

            resolve(response.data.data.estratos);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getTipoParqueadero(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/parqueaderos")
          .then((response) => {
            ctx.commit('SET_TIPO_PARQUEADERO', response.data.data.parqueaderos)

            resolve(response.data.data.parqueaderos);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getTipoPrecios() {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/precios")
          .then((response) => {
            resolve(response.data.data.precios);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getTipoTiempoAlquler() {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/tiempo_alquiler")
          .then((response) => {
            resolve(response.data.data.tiempo_alquiler);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getPeridoAdmon() {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/periodo_admon")
          .then((response) => {
            resolve(response.data.data.periodo_admon);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    getRanguePrice(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/api-inmueble/rangue-price")
          .then((response) => {
            ctx.commit('RANGUE_PRICE', response.data.data.rangue_price)
            resolve(response.data.data.rangue_price);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },


    saveInmuebles(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("/api/auth/api-inmueble", data)
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error.data);
          });
      });
    },

    getPdf(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("/api/auth/api-inmueble/get_pdf/" + data)
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    edictInmuebles(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("/api/auth/api-inmueble/editar_inmueble", data)
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error.data);
          });
      });
    },
  },
};
