import { ref, watch } from '@vue/composition-api'
 import store from '@/store'

export default function useTaskHandler(props, emit) {
  // ------------------------------------------------
  // taskLocal
  // ------------------------------------------------
  const taskLocal = ref(JSON.parse(JSON.stringify(props.task.value)))
  const resetTaskLocal = () => {
    taskLocal.value = JSON.parse(JSON.stringify(props.task.value))
  }


  watch(props.task, () => {
    resetTaskLocal()
  })



  const onSubmit = () => {
    const taskData = JSON.parse(JSON.stringify(taskLocal))

    // * If event has id => Edit Event
    // Emit event for add/update event
    if (props.task.value.id) emit('update-task', taskData.value)
    else emit('add-task', taskData.value)

    // Close sidebar
    emit('update:is-task-handler-sidebar-active', false)
  }

  /* eslint-disable global-require */
  const assigneeOptions = []
  /* eslint-enable global-require */

  const resolveAvatarVariant = tags => {
    if (tags.includes('alto')) return 'primary'
    if (tags.includes('medio')) return 'warning'
    if (tags.includes('bajo')) return 'success'
    if (tags.includes('actualizado')) return 'danger'
    if (tags.includes('equipo')) return 'info'
    return 'primary'
  }

  // Lista de etiquetas formulario
  
  const tagOptions = store.state.appTask.etiquetas

  return {
    taskLocal,
    resetTaskLocal,
    assigneeOptions,
    resolveAvatarVariant,
    tagOptions,
    onSubmit,
  }
}
