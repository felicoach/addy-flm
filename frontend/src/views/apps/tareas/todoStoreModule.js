import axios from "@axios";
const userData = JSON.parse(localStorage.getItem("userData"))
export default {
  namespaced: true,
  state: {
    tareas: [],
    etiquetas: [],
    etiquetas_local: [],

  },
  getters: {},
  mutations: {
    SET_TAREAS: (state, payload) => {
      state.tareas = payload;
    },

    SET_ETIQUETAS: (state, payload) => {
      state.etiquetas = payload;
    },

    SET_ETIQUETAS_LOCAL: (state, payload) => {
      state.etiquetas_local = payload;
    }

  },
  actions: {
    fetchTasks(ctx, payload) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/crm", { params: payload })
          .then((response) => {
            console.log(response)
            // Filtro 
            const { q = '', filter, tag, sortBy: sortByParam = 'latest' } = payload
            /* eslint-enable */

            // ------------------------------------------------
            // Get Sort by and Sort Direction
            // ------------------------------------------------
            let sortDesc = true

            const sortBy = (() => {
              if (sortByParam === 'title-asc') {
                sortDesc = false
                return 'title'
              }
              if (sortByParam === 'title-desc') return 'title'
              if (sortByParam === 'assignee') {
                sortDesc = false
                return 'assignee'
              }
              if (sortByParam === 'due-date') {
                sortDesc = false
                return 'dueDate'
              }
              return 'id'
            })()

            // ------------------------------------------------
            // Filtering
            // ------------------------------------------------
            const queryLowered = q.toLowerCase()

            const hasFilter = task => {
              if (filter === 'important') return task.isImportant && !task.isDeleted
              if (filter === 'completed') return task.isCompleted && !task.isDeleted
              if (filter === 'deleted') return task.isDeleted
              return !task.isDeleted
            }
            /* eslint-disable no-confusing-arrow, implicit-arrow-linebreak, arrow-body-style */
            const filteredData = response.data.data.filter(task => {
              return task.title.toLowerCase().includes(queryLowered) && hasFilter(task) && (tag ? task.tags.includes(tag) : true)
            })
            /* eslint-enable d */

            // ------------------------------------------------
            // Perform sorting
            // ------------------------------------------------
            const sortTasks = key => (a, b) => {
              let fieldA
              let fieldB

              // If sorting is by dueDate => Convert data to date
              if (key === 'dueDate') {
                fieldA = new Date(a[key])
                fieldB = new Date(b[key])
                // eslint-disable-next-line brace-style
              }

              // If sorting is by assignee => Use `fullName` of assignee
              else if (key === 'assignee') {
                fieldA = a.assignee ? a.assignee.fullName : null
                fieldB = b.assignee ? b.assignee.fullName : null
              } else {
                fieldA = a[key]
                fieldB = b[key]
              }

              let comparison = 0

              if (fieldA === fieldB) {
                comparison = 0
              } else if (fieldA === null) {
                comparison = 1
              } else if (fieldB === null) {
                comparison = -1
              } else if (fieldA > fieldB) {
                comparison = 1
              } else if (fieldA < fieldB) {
                comparison = -1
              }

              return comparison
            }

            // Sort Data
            const sortedData = filteredData.sort(sortTasks(sortBy))
            if (sortDesc) sortedData.reverse()

            resolve(sortedData);
          })
          .catch((error) => reject(error));
      });
    },

    getTareas(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/auth/crm")
          .then((response) => {
            ctx.commit("SET_TAREAS", response.data);
            resolve(response.data)
          })
      })
    },



    addTask(ctx, taskData) {
      taskData.created_by = userData.id
      return new Promise((resolve, reject) => {
        axios
          .post("/api/auth/crm", taskData)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => reject(error));
      });
    },

    fetchEtiquetas(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/etiquetas`)
          .then((response) => {
           var data = [];
            let res = response.data.data.etiquetas
            ctx.commit("SET_ETIQUETAS", res);

            for (let i = 0; i < res.length; i++) {

              data.push({
                title: res[i].label,
                color: "primary",
                route: { name: "apps-todo-tag", params: { tag: res[i].value } },
              })

            }
            ctx.commit("SET_ETIQUETAS_LOCAL", data);
            resolve(response.data.data.etiquetas)
          })
          .catch((error) => reject(error));
      });
    },

    addEtiquetasRegister(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post(`/api/auth/etiquetas`, data)
          .then((response) => {
            resolve(response)
          })
          .catch((error) => reject(error));
      });
    },

    getEtiquetas(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/crm/etiquetas/${userData.id}`)
          .then((response) => {
            ctx.commit("SET_ETIQUETAS", response.data.data);
            resolve(response.data.data)
          })
          .catch((error) => reject(error));
      });
    },

    addEtiquetas(ctx, etiquetas) {
      etiquetas.user_id = userData.id
      return new Promise((resolve, reject) => {
        axios
          .post(`/api/auth/crm/etiquetas`, etiquetas)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => reject(error));
      });
    },

    updateTask(ctx, { task }) {
      return new Promise((resolve, reject) => {
        axios
          .put(`/api/auth/crm/${task.id}`, task)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      });
    },
    removeTask(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .delete(`/api/auth/crm/${id}`)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      });
    },
  },
};
