import axios from '@axios'
import { paginateArray, sortCompare } from '@/@fake-db/utils'
import { reject } from 'core-js/fn/promise';

export default {
    namespaced: true,
    state: {
        portales: [],
        localidades_ciencuadra: []
    },
    getters: {},
    mutations: {
        SET_PORTALES: (state, payload) => {
            state.portales = payload;
        },

        SET_TOKEN_CIENCUADRA: (state, payload) => {
            localStorage.setItem("token_ciencuadra", payload);
        },

        SET_LOCALIDADES_CIENCUADRA: (state, payload) => {
            state.localidades_ciencuadra = payload
        }
    },
    actions: {
        fetchPortales(ctx) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/api/auth/portales`)
                    .then((response) => {
                        ctx.commit("SET_PORTALES", response.data.data.portales);
                        resolve(response.data.data.portales)
                    })
                    .catch(error => reject(error))
            })
        },

        despublicarFincaRaiz(ctx, data) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/api/auth/fincarraiz/despublicar/${data}`)
                    .then((response) => {

                        resolve(response.data)
                    })
                    .catch(error => reject(error))
            })
        },


        getCredentialesPortales(ctx, portal) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/api/auth/portales/credential/${portal.portal}`)
                    .then((response) => {
                        resolve(response.data.data.credential)
                    })
                    .catch(error => reject(error))
            })
        },

        getCredencialesClasificadosPais(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/portales/credenciales_clasificados_pais/${data.portal}`).then((response) => {
                    resolve(response.data.data.credential)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        getCredencialesMetroCuadrado(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/portales/credenciales_metro_cuadrado/${data.portal}`).then((response) => {
                    resolve(response.data.data.credential)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        getCredencialesCienCuadra(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/portales/credenciales_ciencuadra/${data.portal}`).then((response) => {
                    resolve(response.data.data.credential)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        getTypeService(ctx) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/api/auth/portales/type-services`)
                    .then((response) => {
                        resolve(response.data.data.type_service)
                    })
                    .catch(error => reject(error))
            })
        },

        addPortales(ctx, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/api/auth/portales', data)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },

        editarPortales(ctx, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post(`/api/auth/portales/edit`, data)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },

        addCountryPortal(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/contries_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addCitiesPortal(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/cities_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addStatePortal(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/states_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addCaracteristicasInternas(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/caracteristicas_internas_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addCaracteristicasExternas(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/caracteristicas_externas_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addCredencialesPortalesUser(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/credenciales_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },


        addCredencialesClasificadosPais(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/credenciales_regster_portal_clasificados_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addCredencialesMetroCuadrado(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/credenciales_regster_portal_metro_cuadrado`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addCredencialesCiencuadra(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/credenciales_regster_portal_ciencuadra`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },


        getCredencialesPortalesUser(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/credenciales_get_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },



        addTipoNegocioPortales(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/tipo_negocio_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addEstadoFisicoPortales(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/estado_fisico_portal`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addTipoInmueblePortal(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/tipo_inmueble_portal`, data).then((response) => {
                    resolve(response.data.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        addZonaPortales(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/zonas_portales`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },


        addBarrioPortales(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/portales/barrios_portales`, data).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        getTokenCiencuadra(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/portales_credentials_ciencuadra`, data).then((response) => {
                    ctx.commit("SET_TOKEN_CIENCUADRA", response.data.data.portales);
                    resolve(response.data.data.portales)
                }).catch((error) => {
                    reject(error)
                })
            })
        },




        getCodigoResponsePortal(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/portales_codigo_response`, data).then((response) => {
                    resolve(response.data.data.codigo_response)
                }).catch((error) => {
                    reject(error)
                })
            })
        },


        sincronizarCiencuadra(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/service/cienciuadra`, data).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        updateApiCiencuadra(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/service/cienciuadra/update`, data).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        desactivarCiencuadra(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.post(`/api/auth/service/cienciuadra/desactivar`, data).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },


        sincronizarMetrocuadrado(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/service/metro_cuadrado/${data}`).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        updateApiMetrocuadrado(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/service/metro_cuadrado/update/${data}`).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        despublicarMetrocuadrado(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/service/metro_cuadrado/despublicar/${data}`).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },



        sincronizarPais(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/service/pais/sincronizar/${data}`).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        desactivarPais(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/service/pais/desactivar/${data}`).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

        updateApiPais(ctx, data) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/auth/service/pais/update/${data}`).then((response) => {
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        },

       
        getLocalidadesCiencuadra(ctx) {
            return new Promise((resolve, reject) => {
                axios.get(`api/auth/ciencuadra/import/localidades`).then((response) => {
                    ctx.commit("SET_LOCALIDADES_CIENCUADRA", response.data.data.localidades);
                    resolve(response.data)
                }).catch((error) => {
                    reject(error)
                })
            })
        }



    },


}
