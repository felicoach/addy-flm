import axios from '@axios'
import { paginateArray, sortCompare } from '@/@fake-db/utils'

export default {
  namespaced: true,
  state: {
    clientes: []
  },
  getters: {},
  mutations: {
    SET_CLIENTES: (state, payload) => {
      state.clientes = payload;
    },
  },
  actions: {
    fetchClients(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/clientes')
          .then((response) => {

            ctx.commit("SET_CLIENTES", response.data.data.clientes);
            resolve(response.data.data.clientes)
          })
          .catch(error => reject(error))
      })
    },
    fetchClient(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/auth/clientes/${id}`)
          .then((response) => {
            resolve(response.data.data.cliente)
          })
          .catch(error => reject(error))
      })
    },
    addClient(ctx, clienteData) {

      return new Promise((resolve, reject) => {
        axios
          .post('/api/auth/clientes', clienteData)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },

    updateCliente(ctx, clienteData) {
      return new Promise((resolve, reject) => {
        axios
          .put(`/api/auth/clientes/${clienteData.id}`, clienteData)
          .then((response) => {
            resolve(response.data)
          })
          .catch(error => reject(error))
      })
    },

    filterCliente(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("/api/auth/clientes/filter", data)
          .then((response) => {

            resolve(response.data.data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
}
