import axios from "@axios";
import { promiseTimeout } from "@vueuse/shared";

export default {
    namespaced: true,

    state: {
        users: [],
    },

    getters: {},

    mutations: {
        SET_USERS: (state, payload) => {
            state.users = payload;
        },
    },

    actions: {
        getInmobiliaria(ctx, queryParams) {
            return new Promise((resolve, reject) => {
                axios
                  .get("/api/auth/usuarios/admin-all", { params: queryParams })
                  .then((response) => {
                    ctx.commit("SET_USERS", response.data.data.users);
        
                    resolve(response.data.data.users);
                  })
                  .catch((error) => reject(error));
              });
        }
    }
};
