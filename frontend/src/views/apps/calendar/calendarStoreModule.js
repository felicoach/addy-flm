import axios from '@axios'

export default {
  namespaced: true,
  state: {
    dataGoogle: [],
    agenda: [],
    calendarOptions: [],
    calendarOptionsGoogle: [{
      color: 'primary',
      label: 'Personal',
    },],
    selectedCalendars: [],
    state_agenda_cita: []
  },
  getters: {},
  mutations: {
    SET_DATA_EVENT(state, val) {
      state.selectedCalendars = val
    },

    SET_SELECTED_EVENTS(state, val) {
      state.selectedCalendars = val
    },

    SET_DATA_GOOGLE(state, val) {
      state.dataGoogle = val
    },

    SET_DATA_AGENDA(state, val) {
      state.agenda = val
    },

    SET_TIPO_CITAS(state, val) {
      state.calendarOptions = val
    },

    SET_ESTADO_AGENDA_CITA(state, val) {
      state.state_agenda_cita = val
    }
  },
  actions: {
    fetchEvents(ctx, { calendars }) {

      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/api-agenda', {
            params: calendars
          })
          .then((response) => {

            let res = response.data.data.agenda
            resolve(res)
          }
          ).catch(error => reject(error))
      })
    },

    fetchReporteAgente(ctx, data){
      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/api-agenda-reporte/'+ data)
          .then((response) => {
            resolve(response.data.data)
          })
          .catch(error => reject(error))
      })
    },

    fetchTipoCitas(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/api-agenda-tipo/tipo_citas')
          .then((response) => {
            ctx.commit("SET_TIPO_CITAS", response.data.data.tipo_citas);
            resolve(response.data.data.tipo_citas)
          })
          .catch(error => reject(error))
      })
    },

    fetchEstadoAgendaCitas(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/api-agenda-state/agenda_cita')
          .then((response) => {
            ctx.commit("SET_ESTADO_AGENDA_CITA", response.data.data.agendas_estado_citas);
            resolve(response.data.data.agendas_estado_citas)
          })
          .catch(error => reject(error))
      })
    },

    fetchEstadoAgendaCitasForInmueble(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/api-agenda-state/agenda_cita/' + data.id)
          .then((response) => {
            resolve(response.data.data.estado)
          })
          .catch(error => reject(error))
      })
    },

    addEstadoAgendaReport(ctx,  data) {
      return new Promise((resolve, reject) => {
        axios
          .post('/api/auth/api-agenda-state/register', data)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },

    updateEstadoAgendaReport(ctx, { event }) {
      return new Promise((resolve, reject) => {
        axios
          .put(`/api/auth/api-agenda-state/update/${event.id}`, { event })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },


    getEventos(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/api-agenda-hoy/today')
          .then((response) => {
            let state = [];
            let res = response.data.data.agenda
            console.log(res)
            ctx.commit("SET_DATA_AGENDA", res);
            resolve(res)
          }
          ).catch(error => reject(error))
      })
    },

    addEvent(ctx, { event }) {
      return new Promise((resolve, reject) => {
        axios
          .post('/api/auth/api-agenda', { event })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateEvent(ctx, { event }) {
      return new Promise((resolve, reject) => {
        axios
          .put(`/api/auth/api-agenda/${event.id}`, { event })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    removeEvent(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .delete(`/api/auth/api-agenda/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
