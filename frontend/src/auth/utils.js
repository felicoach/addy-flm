import useJwt from '@/auth/jwt/useJwt'

/**
 * Return if user is logged in
 * This is completely up to you and how you want to store the token in your frontend application
 * e.g. If you are using cookies to store the application please update this function
 */
// eslint-disable-next-line arrow-body-style
export const isUserLoggedIn = () => {
  return localStorage.getItem('userData')
}

export const getUserData = () => JSON.parse(localStorage.getItem('userData'))

/**
 * In real app you won't need this function because your app will navigate to same route for each users regardless of ability
 * Please note role field is just for showing purpose it's not used by anything in frontend
 * We are checking role just for ease
 * NOTE: If you have different pages to navigate based on user ability then this function can be useful. However, you need to update it.
 * @param {String} userRole Role of user
 */
export const getHomeRouteForLoggedInUser = userRole => {

  if (userRole == 'inmobiliaria') return { name: 'dashboard-agente' }
  if (userRole == 'agente_inmobiliario') return { name: 'dashboard-agente' }
  if (userRole == 'administrator') return { name: 'dashboard-agente' }

  if (userRole == 'visitantes') return { name: 'perfil-editar' }

  if (userRole != 'socio_referidor') return { name: 'apps-public-perfil' }

  if (userRole === 'socio_referidor') return { name: 'inicio-home' }

  return { name: 'auth-login' }
}
