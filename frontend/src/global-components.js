import Vue from "vue";
import FeatherIcon from "@core/components/feather-icon/FeatherIcon.vue";
import VuePhoneNumberInput from "vue-phone-number-input";
import UploadImages from "vue-upload-drop-images";
import VueSocialSharing from "vue-social-sharing";
import 'vue-phone-number-input/dist/vue-phone-number-input.css';
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';

Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);


Vue.component(FeatherIcon.name, FeatherIcon);

Vue.component("vue-phone-number-input", VuePhoneNumberInput);

Vue.component("UploadImages", UploadImages);

Vue.component("VueSocialSharing", VueSocialSharing);

