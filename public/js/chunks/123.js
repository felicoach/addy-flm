(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[123],{

/***/ "./frontend/src/@core/components/b-card-code/index.js":
/*!************************************************************!*\
  !*** ./frontend/src/@core/components/b-card-code/index.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BCardCode_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BCardCode.vue */ "./frontend/src/@core/components/b-card-code/BCardCode.vue");

/* harmony default export */ __webpack_exports__["default"] = (_BCardCode_vue__WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./frontend/src/@core/components/toastification/ToastificationContent.vue":
/*!********************************************************************************!*\
  !*** ./frontend/src/@core/components/toastification/ToastificationContent.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ToastificationContent_vue_vue_type_template_id_2fedfe59_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ToastificationContent.vue?vue&type=template&id=2fedfe59&scoped=true& */ "./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=template&id=2fedfe59&scoped=true&");
/* harmony import */ var _ToastificationContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ToastificationContent.vue?vue&type=script&lang=js& */ "./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ToastificationContent_vue_vue_type_style_index_0_id_2fedfe59_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true& */ "./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ToastificationContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ToastificationContent_vue_vue_type_template_id_2fedfe59_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ToastificationContent_vue_vue_type_template_id_2fedfe59_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2fedfe59",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/src/@core/components/toastification/ToastificationContent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ToastificationContent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true&":
/*!******************************************************************************************************************************************!*\
  !*** ./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true& ***!
  \******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_style_index_0_id_2fedfe59_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/sass-loader/dist/cjs.js??ref--11-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_style_index_0_id_2fedfe59_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_style_index_0_id_2fedfe59_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_style_index_0_id_2fedfe59_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_style_index_0_id_2fedfe59_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=template&id=2fedfe59&scoped=true&":
/*!***************************************************************************************************************************!*\
  !*** ./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=template&id=2fedfe59&scoped=true& ***!
  \***************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_template_id_2fedfe59_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ToastificationContent.vue?vue&type=template&id=2fedfe59&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=template&id=2fedfe59&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_template_id_2fedfe59_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ToastificationContent_vue_vue_type_template_id_2fedfe59_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./frontend/src/@core/utils/validations/validations.js":
/*!*************************************************************!*\
  !*** ./frontend/src/@core/utils/validations/validations.js ***!
  \*************************************************************/
/*! exports provided: required, email, min, max, confirmed, regex, between, alpha, integer, digits, alphaDash, alphaNum, length, positive, credit, password, url */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "required", function() { return required; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "email", function() { return email; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "min", function() { return min; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "max", function() { return max; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirmed", function() { return confirmed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "regex", function() { return regex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "between", function() { return between; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha", function() { return alpha; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "integer", function() { return integer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "digits", function() { return digits; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaDash", function() { return alphaDash; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaNum", function() { return alphaNum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "length", function() { return length; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "positive", function() { return positive; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "credit", function() { return credit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "password", function() { return password; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "url", function() { return url; });
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vee-validate */ "./frontend/node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vee-validate/dist/rules */ "./frontend/node_modules/vee-validate/dist/rules.js");
/* harmony import */ var vee_validate_dist_locale_ar_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vee-validate/dist/locale/ar.json */ "./frontend/node_modules/vee-validate/dist/locale/ar.json");
var vee_validate_dist_locale_ar_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! vee-validate/dist/locale/ar.json */ "./frontend/node_modules/vee-validate/dist/locale/ar.json", 1);
/* harmony import */ var vee_validate_dist_locale_en_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vee-validate/dist/locale/en.json */ "./frontend/node_modules/vee-validate/dist/locale/en.json");
var vee_validate_dist_locale_en_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! vee-validate/dist/locale/en.json */ "./frontend/node_modules/vee-validate/dist/locale/en.json", 1);
/* harmony import */ var _validators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./validators */ "./frontend/src/@core/utils/validations/validators.js");



 // eslint-disable-next-line object-curly-newline

 // ////////////////////////////////////////////////////////
// General
// ////////////////////////////////////////////////////////

var required = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('required', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["required"]); // export const required = (val) => {
//   return (val && val.length > 0) || '*Field is required'
// }

var email = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('email', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["email"]);
var min = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('min', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["min"]);
var max = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('max', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["max"]);
var confirmed = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('confirmed', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["confirmed"]);
var regex = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('regex', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["regex"]);
var between = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('between', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["between"]);
var alpha = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('alpha', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["alpha"]);
var integer = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('integer', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["integer"]);
var digits = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('digits', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["digits"]);
var alphaDash = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('alpha-dash', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["alpha_dash"]);
var alphaNum = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('alpha-num', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["alpha_num"]);
var length = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('length', vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_1__["length"]);
var positive = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('positive', {
  validate: _validators__WEBPACK_IMPORTED_MODULE_4__["validatorPositive"],
  message: 'Please enter positive number!'
});
var credit = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('credit-card', {
  validate: _validators__WEBPACK_IMPORTED_MODULE_4__["validatorCreditCard"],
  message: 'It is not valid credit card!'
});
var password = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('password', {
  validate: _validators__WEBPACK_IMPORTED_MODULE_4__["validatorPassword"],
  message: 'Your {_field_} must contain at least one uppercase, one lowercase, one special character and one digit'
});
var url = Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["extend"])('url', {
  validate: _validators__WEBPACK_IMPORTED_MODULE_4__["validatorUrlValidator"],
  message: 'URL is invalid'
}); // Install English and Arabic localizations.

Object(vee_validate__WEBPACK_IMPORTED_MODULE_0__["localize"])({
  en: {
    messages: vee_validate_dist_locale_en_json__WEBPACK_IMPORTED_MODULE_3__.messages,
    names: {
      email: 'Email',
      password: 'Password'
    },
    fields: {
      password: {
        min: '{_field_} es demaciado corta'
      }
    }
  },
  ar: {
    messages: vee_validate_dist_locale_ar_json__WEBPACK_IMPORTED_MODULE_2__.messages,
    names: {
      email: 'البريد الإلكتروني',
      password: 'كلمة السر'
    },
    fields: {
      password: {
        min: 'كلمة السر قصيرة جداً سيتم اختراقك'
      }
    }
  }
}); // ////////////////////////////////////////////////////////
// NOTE:
// Quasar validation for reference only
// Remove this note once development is finished and make sure to
// to convert all of them in veevalidate version
// ////////////////////////////////////////////////////////
// export const required = (val) => {
//   return (val && val.length > 0) || '*Field is required'
// }
// export const required_obj = (obj) => {
//   if (obj === null || obj === undefined) return '*Field is required'
//   return (Object.entries(obj).length > 0 && obj.constructor === Object) || '*Field is required'
// }
// export const no_blank_spaces_arr = (arr) => {
//   return arr.every(val => (val.trim() && val.trim().length > 0)) || 'Blank Spaces are not allowed'
// }
// export const url = val => {
//   // If blank return
//   if (val === undefined || val === null || val.length === 0) return true
//   // Used
//   // https://stackoverflow.com/questions/4314741/url-regex-validation
//   // Other
//   // https://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-a-url
//   // https://www.w3resource.com/javascript-exercises/javascript-regexp-exercise-9.php
//   // https://www.geeksforgeeks.org/how-to-validate-url-using-regular-expression-in-javascript/
//   /* eslint-disable no-useless-escape */
//   const re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/
//   /* eslint-enable no-useless-escape */
//   return re.test(val) || 'URL is invalid'
// }
// export const date = val => {
//   // If blank return
//   if (val === undefined || val === null || val.length === 0) return true
//   // https://github.com/quasarframework/quasar/blob/dev/ui/src/utils/patterns.js
//   return /^-?[\d]+\/[0-1]\d\/[0-3]\d$/.test(val) || 'Date is invalid'
// }
// export const max = (val, max) => {
//   // If blank return
//   if (val === undefined || val === null) return true
//   return val.length <= max || `More than ${max} characters are not allowed`
// }
// export const max_arr = (val, max) => {
//   return val.length <= max || `More than ${max} values are not allowed`
// }
// export const min = (val, min) => {
//   // If blank return
//   if (val === undefined || val === null || val.length === 0) return true
//   return val.length >= min || `Minimum ${min} characters are required`
// }
// export const num_range = (val, min, max) => {
//   // If blank return
//   if (val === undefined || val === null || val.length === 0) return true
//   const msg = 'Value is invalid'
//   if (min === null) return val <= max || msg
//   else if (max === null) return val >= min || msg
//   else return (val >= min && val <= max) || msg
// }

/***/ }),

/***/ "./frontend/src/@core/utils/validations/validators.js":
/*!************************************************************!*\
  !*** ./frontend/src/@core/utils/validations/validators.js ***!
  \************************************************************/
/*! exports provided: validatorPositive, validatorPassword, validatorCreditCard, validatorUrlValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validatorPositive", function() { return validatorPositive; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validatorPassword", function() { return validatorPassword; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validatorCreditCard", function() { return validatorCreditCard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validatorUrlValidator", function() { return validatorUrlValidator; });
var validatorPositive = function validatorPositive(value) {
  if (value >= 0) {
    return true;
  }

  return false;
};
var validatorPassword = function validatorPassword(password) {
  /* eslint-disable no-useless-escape */
  var regExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{8,}/;
  /* eslint-enable no-useless-escape */

  var validPassword = regExp.test(password);
  return validPassword;
};
var validatorCreditCard = function validatorCreditCard(creditnum) {
  /* eslint-disable no-useless-escape */
  var cRegExp = /^(?:3[47][0-9]{13})$/;
  /* eslint-enable no-useless-escape */

  var validCreditCard = cRegExp.test(creditnum);
  return validCreditCard;
};
var validatorUrlValidator = function validatorUrlValidator(val) {
  if (val === undefined || val === null || val.length === 0) {
    return true;
  }
  /* eslint-disable no-useless-escape */


  var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
  /* eslint-enable no-useless-escape */

  return re.test(val);
};

/***/ }),

/***/ "./frontend/src/views/apps/perfil/code.js":
/*!************************************************!*\
  !*** ./frontend/src/views/apps/perfil/code.js ***!
  \************************************************/
/*! exports provided: codeIcon, codeIconInfo, codeBasic, codeValidation, codeVueBasic, codeIndeterminate, codeVertical */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "codeIcon", function() { return codeIcon; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "codeIconInfo", function() { return codeIconInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "codeBasic", function() { return codeBasic; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "codeValidation", function() { return codeValidation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "codeVueBasic", function() { return codeVueBasic; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "codeIndeterminate", function() { return codeIndeterminate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "codeVertical", function() { return codeVertical; });
var codeIcon = "\n<template>\n  <form-wizard\n    color=\"#5A8DEE\"\n    :title=\"null\"\n    :subtitle=\"null\"\n    finish-button-text=\"Submit\"\n    @on-complete=\"formSubmitted\"\n  >\n    <tab-content icon=\"bx bx-file-blank\">\n      <b-row>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Enter Your Personal Details\n          </h6>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"firstName\">First Name</label>\n            <b-form-input\n              id=\"firstName\"\n              type=\"text\"\n              placeholder=\"Enter Your First Name\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"lastName\">Last Name</label>\n            <b-form-input\n              id=\"lastName\"\n              type=\"text\"\n              placeholder=\"Enter Your Last Name\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"email\">Email</label>\n            <b-form-input\n              id=\"email\"\n              type=\"email\"\n              placeholder=\"Enter Your Email\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"phone\">Phone</label>\n            <b-form-input\n              id=\"phone\"\n              type=\"number\"\n              placeholder=\"Enter Your Phone Number\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"age\">Age</label>\n            <b-form-input\n              id=\"age\"\n              type=\"number\"\n              placeholder=\"Enter Your Age\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label>Gender</label>\n            <div class=\"radio\">\n              <b-form-radio\n                v-model=\"gender\"\n                class=\"p-0\"\n                name=\"some-radios\"\n                value=\"A\"\n                plain\n                inline\n              >\n                Male\n              </b-form-radio>\n              <b-form-radio\n                v-model=\"gender\"\n                name=\"some-radios\"\n                value=\"B\"\n                plain\n                inline\n              >\n                Female\n              </b-form-radio>\n            </div>\n          </b-form-group>\n        </b-col>\n      </b-row>\n    </tab-content>\n    <tab-content icon=\"bx bxs-truck\">\n      <b-row>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Enter Your Location\n          </h6>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"AddresLine1\">Address Line 1</label>\n            <b-form-input\n              id=\"AddresLine1\"\n              type=\"text\"\n              placeholder=\"Enter House no./ Flate no.\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"AddresLine2\">Address Line 2</label>\n            <b-form-input\n              id=\"AddresLine2\"\n              type=\"text\"\n              placeholder=\"Enter Society name/ Area name\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"landmark\">Landmark</label>\n            <b-form-input\n              id=\"landmark\"\n              type=\"text\"\n              placeholder=\"Enter A Landmark\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"town-city\">Town/City</label>\n            <b-form-input\n              id=\"town-city\"\n              type=\"text\"\n              placeholder=\"Enter Town/City\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"pincode\">Pincode</label>\n            <b-form-input\n              id=\"pincode\"\n              type=\"number\"\n              placeholder=\"Enter Your Pincode\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"state\">State</label>\n            <b-form-input\n              id=\"state\"\n              type=\"text\"\n              placeholder=\"Enter Your State\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label for=\"country\">Country</label>\n            <b-form-select\n              v-model=\"selected\"\n              :options=\"options\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col\n          md=\"6\"\n          class=\"d-flex align-items-center\"\n        >\n          <b-form-group>\n            <div class=\"checkbox\">\n              <b-form-checkbox\n                plain\n                value=\"A\"\n                class=\"ml-0\"\n              >\n                Permanent Delivery address\n              </b-form-checkbox>\n            </div>\n          </b-form-group>\n        </b-col>\n      </b-row>\n    </tab-content>\n    <tab-content icon=\"bx bx-home\">\n      <b-row>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Enter Your Payment Methods\n          </h6>\n        </b-col>\n        <b-col cols=\"12\">\n          <b-form-group>\n            <div class=\"d-flex justify-content-between flex-wrap align-items-center\">\n              <div class=\"vs-radio-con vs-radio-primary\">\n                <b-img\n                  :src=\"require('@/assets/images/images/pages/bank.png')\"\n                  height=\"40\"\n                  class=\"d-inline-block\"\n                />\n                <span>Card 12XX XXXX XXXX 0000</span>\n              </div>\n              <div class=\"card-holder-name\">\n                John Doe\n              </div>\n              <div class=\"card-expiration-date\">\n                11/2020\n              </div>\n              <div>\n                <label>Enter CVV</label>\n                <b-form-input\n                  type=\"password\"\n                  placeholder=\"Enter Your CVV no.\"\n                />\n              </div>\n            </div>\n          </b-form-group>\n          <hr>\n        </b-col>\n        <b-col cols=\"12 pl-0\">\n          <b-form-group>\n            <div class=\"radio\">\n              <b-form-radio-group\n                :options=\"radioOption\"\n                name=\"radios-stacked\"\n                plain\n                stacked\n              />\n            </div>\n          </b-form-group>\n          <hr class=\"ml-1\">\n        </b-col>\n        <b-col\n          cols=\"12\"\n          class=\"d-flex align-items-center\"\n        >\n          <div class=\"paypal cursor-pointer d-flex align-items-center\">\n            <div class=\"radio\">\n              <b-form-radio\n                class=\"p-0\"\n                name=\"some-radios\"\n                value=\"A\"\n                plain\n              >\n                <b-img\n                  :src=\"require('@/assets/images/images/pages/PayPal_logo.png')\"\n                  alt=\"PayPal Logo\"\n                />\n              </b-form-radio>\n            </div>\n          </div>\n          <div class=\"googlepay cursor-pointer pl-1 d-flex align-items-center\">\n            <div class=\"radio\">\n              <b-form-radio\n                class=\"p-0\"\n                name=\"some-radios\"\n                value=\"B\"\n                plain\n              >\n                <b-img\n                  :src=\"require('@/assets/images/images/pages/google-pay.png')\"\n                  height=\"30\"\n                  alt=\"google Logo\"\n                />\n              </b-form-radio>\n            </div>\n          </div>\n        </b-col>\n        <b-col md=\"6\">\n          <hr>\n\n          <label>Enter Your Promocode</label>\n          <b-form-input\n            type=\"text\"\n            placeholder=\"Enter Your Promocode\"\n          />\n        </b-col>\n      </b-row>\n    </tab-content>\n  </form-wizard>\n</template>\n\n<script>\nimport AppCard from '@core/components/app-card/AppCard.vue'\nimport { FormWizard, TabContent } from 'vue-form-wizard'\nimport 'vue-form-wizard/dist/vue-form-wizard.min.css'\nimport {\n  BRow,\n  BCol,\n  BFormGroup,\n  BFormInput,\n  BFormRadio,\n  BFormCheckbox,\n  BFormSelect,\n  BImg,\n  BFormRadioGroup,\n} from 'bootstrap-vue'\n\nexport default {\n  components: {\n    FormWizard,\n    TabContent,\n    AppCard,\n    BRow,\n    BCol,\n    BFormGroup,\n    BFormInput,\n    BFormRadio,\n    BFormCheckbox,\n    BFormSelect,\n    BImg,\n    BFormRadioGroup,\n  },\n  data: () => ({\n    selected: null,\n    gender: 'A',\n    options: [\n      { value: null, text: 'Please select an option' },\n      { value: 'a', text: 'This is First option' },\n      { value: 'b', text: 'Selected Option' },\n      { value: { C: '3PO' }, text: 'This is an option with object value' },\n      { value: 'd', text: 'This one is disabled', disabled: true },\n    ],\n    radioOption: [\n      { text: 'Credit / Debit / ATM Card', value: 'first' },\n      { text: 'Net Banking', value: 'second' },\n      { text: 'EMI (Easy Installment)', value: 'third' },\n      { text: 'Cash On Delivery', value: 'fourth' },\n    ],\n    radioOption2: [\n      { text: '', value: 'paypal' },\n      { text: '', value: 'gpay' },\n    ],\n  }),\n  methods: {\n    formSubmitted() {\n      // eslint-disable-next-line\n      alert('Form submitted!')\n    },\n  },\n}\n</script>\n";
var codeIconInfo = "\n<template>\n  <form-wizard\n    color=\"#5A8DEE\"\n    :title=\"null\"\n    :subtitle=\"null\"\n    finish-button-text=\"Submit\"\n    @on-complete=\"formSubmitted\"\n   >\n    <tab-content\n      title=\"BASIC DETAILS\"\n      icon=\"bx bx-user\"\n    >\n      <b-row>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Basic Details\n          </h6>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label>First Name</label>\n            <b-form-input placeholder=\"Enter Your First Name\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label>Last Name</label>\n            <b-form-input placeholder=\"Enter Your Last Name\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label>Email</label>\n            <b-form-input\n              type=\"email\"\n              placeholder=\"Enter Your Email\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"6\">\n          <b-form-group>\n            <label>Password</label>\n            <b-form-input\n              type=\"password\"\n              placeholder=\"Enter Your Password\"\n            />\n          </b-form-group>\n        </b-col>\n      </b-row>\n      <hr>\n      <b-row>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Contact Details\n          </h6>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>Address Line 1</label>\n            <b-form-input placeholder=\"Enter Your House no./ Flate no.\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>Address Line 2</label>\n            <b-form-input placeholder=\"Enter Your Society name / Area name\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>Landmark</label>\n            <b-form-input placeholder=\"Enter a Landmark\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>City</label>\n            <b-form-input placeholder=\"Enter Your City\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>State</label>\n            <b-form-input placeholder=\"Enter Your State\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>Country</label>\n            <b-form-input placeholder=\"Enter Your Country\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>Age</label>\n            <b-form-input\n              type=\"number\"\n              placeholder=\"Enter Your Current Age\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>Phone</label>\n            <b-form-input\n              type=\"number\"\n              placeholder=\"+123456789\"\n            />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"4\">\n          <b-form-group>\n            <label>Gender</label>\n            <div class=\"radio\">\n              <b-form-radio\n                v-model=\"gender\"\n                class=\"p-0\"\n                name=\"some-radios\"\n                value=\"A\"\n                plain\n                inline\n              >\n                Male\n              </b-form-radio>\n              <b-form-radio\n                v-model=\"gender\"\n                name=\"some-radios\"\n                value=\"B\"\n                plain\n                inline\n              >\n                Female\n              </b-form-radio>\n            </div>\n          </b-form-group>\n        </b-col>\n      </b-row>\n      <hr>\n      <b-row>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Id Proof\n          </h6>\n        </b-col>\n        <b-col md=\"3\">\n          <b-form-group>\n            <label>Passport</label>\n            <b-form-file placeholder=\"Choose File\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"3\">\n          <b-form-group>\n            <label>National ID</label>\n            <b-form-file placeholder=\"Choose File\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"3\">\n          <b-form-group>\n            <label>BIRTH CERTIFICATE </label>\n            <b-form-file placeholder=\"Choose File\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"3\">\n          <b-form-group>\n            <label>DRIVING LICENCE </label>\n            <b-form-file placeholder=\"Choose File\" />\n          </b-form-group>\n        </b-col>\n      </b-row>\n    </tab-content>\n    <tab-content\n      title=\"TERM AND CONDITIONS\"\n      icon=\"bx bx-briefcase\"\n    >\n      <b-row>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Terms and conditions\n          </h6>\n        </b-col>\n        <b-col cols=\"12\">\n          <p>\n            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum culpa repellendus laborum maxime\n            dignissimos dolor excepturi iusto nemo aspernatur? Qui modi inventore reprehenderit, nostrum quaerat\n            libero maiores consequuntur illo veritatis.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, culpa obcaecati. Qui accusantium sit\n            error, dolorem alias incidunt est. Laborum, atque ipsum debitis obcaecati dolor illo magni provident harum\n            vitae?\n          </p>\n          <p>\n            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reprehenderit omnis, doloribus autem, non quam\n            quibusdam harum accusamus voluptatem in perspiciatis consequuntur sint nam debitis sapiente ex, optio\n            totam delectus quis. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facilis placeat in quisquam\n            dolorum numquam, rerum expedita corporis eveniet quas nostrum, quia veritatis neque quos sint sit\n            exercitationem obcaecati perferendis magnam!\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam consequatur laudantium voluptatibus\n            impedit unde. Error eius consequuntur tenetur unde molestias esse doloribus animi, temporibus placeat\n            eaque laborum, maiores, ex quo! Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae,\n            suscipit placeat accusamus voluptas odio est ea accusantium dignissimos et officia, cupiditate aperiam\n            atque facilis? Adipisci earum fuga illo odit reiciendis. Lorem ipsum dolor sit amet consectetur\n            adipisicing elit. Asperiores nihil necessitatibus sequi placeat tenetur, perspiciatis, excepturi dolor,\n            consectetur assumenda amet accusantium velit fuga numquam tempore repellendus voluptatem vitae.\n            Voluptatem, hic.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde perspiciatis vero, reprehenderit beatae\n            necessitatibus dignissimos animi distinctio illo porro fuga maxime nemo voluptate aspernatur tempore?\n            Incidunt consectetur dignissimos blanditiis. Corporis. Lorem ipsum dolor sit amet consectetur, adipisicing\n            elit. At, dolore omnis! Architecto dolorem non, earum pariatur, molestias voluptatem saepe voluptatibus\n            praesentium expedita cum quae et assumenda. Voluptas debitis praesentium quis. Lorem ipsum dolor sit amet\n            consectetur adipisicing elit. Cumque veniam ipsa saepe sint necessitatibus incidunt nihil totam delectus,\n            dolor omnis itaque libero sed molestiae assumenda non repellat, rerum, eius quia. lorem\n          </p>\n          <strong>Read all term and conditions carefully.</strong>\n        </b-col>\n        <b-col cols=\"12\">\n          <b-form-group>\n            <b-form-radio\n              name=\"terms\"\n              stacked\n            >\n              <span class=\"text-success\">I read all term and conditions and i Agree.</span>\n            </b-form-radio>\n            <b-form-radio\n              name=\"terms\"\n              stacked\n            >\n              <span class=\"text-danger\">I am not Agree with it.</span>\n            </b-form-radio>\n          </b-form-group>\n        </b-col>\n      </b-row>\n    </tab-content>\n    <tab-content\n      title=\"NOMINEE\"\n      icon=\"bx bx-group\"\n    >\n      <b-row>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Enter Nominate Details\n          </h6>\n        </b-col>\n        <b-col cols=\"12\">\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa ad, consectetur animi magni magnam nihil\n            error, quaerat pariatur dolores unde quod sequi modi temporibus libero eos consequuntur ab maxime alias!\n          </p>\n        </b-col>\n        <b-col md=\"3\">\n          <b-form-group>\n            <label>NOMINATION NAME</label>\n            <b-form-input placeholder=\"Nomination Name\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"3\">\n          <b-form-group>\n            <label>NOMINEE'S RELATION</label>\n            <b-form-input placeholder=\"Relation\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"3\">\n          <b-form-group>\n            <label>Nominee Age</label>\n            <b-form-input placeholder=\"Age\" />\n          </b-form-group>\n        </b-col>\n        <b-col md=\"3\">\n          <b-form-group>\n            <label>Nominee Documents</label>\n            <b-form-file placeholder=\"Choose File\" />\n          </b-form-group>\n        </b-col>\n        <b-col cols=\"12\">\n          <h6 class=\"py-50\">\n            Terms and conditions for nominee\n          </h6>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci odit sunt facilis, exercitationem\n            placeat in maiores, ullam doloribus aperiam sint culpa, quo ducimus tempore perferendis ipsum laborum\n            officia ut dignissimos!\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. In beatae quibusdam enim neque animi fugiat harum\n            tempora ipsum excepturi, cupiditate illum hic dignissimos, quaerat dolore! Minus rem sed accusamus\n            corrupti?\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. In beatae quibusdam enim neque animi fugiat harum\n            tempora ipsum excepturi, cupiditate illum hic dignissimos, quaerat dolore! Minus rem sed accusamus\n            corrupti? Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci odit sunt facilis,\n            exercitationem placeat in maiores, ullam doloribus aperiam sint culpa, quo ducimus tempore perferendis\n            ipsum laborum officia ut dignissimos!\n          </p>\n        </b-col>\n      </b-row>\n    </tab-content>\n  </form-wizard>\n</template>\n\n<script>\nimport AppCard from '@core/components/app-card/AppCard.vue'\nimport { FormWizard, TabContent } from 'vue-form-wizard'\nimport 'vue-form-wizard/dist/vue-form-wizard.min.css'\nimport {\n  BRow, BCol, BFormGroup, BFormInput, BFormRadio, BFormFile,\n} from 'bootstrap-vue'\n\nexport default {\n  components: {\n    FormWizard,\n    TabContent,\n    AppCard,\n    BRow,\n    BCol,\n    BFormGroup,\n    BFormInput,\n    BFormRadio,\n    BFormFile,\n  },\n  data: () => ({\n    gender: 'A',\n  }),\n  methods: {\n    formSubmitted() {\n      // eslint-disable-next-line\n      alert('Form submitted!')\n    },\n  },\n}\n</script>\n";
var codeBasic = "\n<template>\n  <div>\n    <label for=\"example-datepicker\">Choose a date</label>\n    <b-form-datepicker\n      id=\"example-datepicker\"\n      v-model=\"value\"\n      class=\"mb-1\"\n    />\n    <p>Value: '{{ value }}'</p>\n  </div>\n</template>\n\n<script>\nimport { BFormDatepicker } from 'bootstrap-vue'\n\nexport default {\n  components: {\n    BFormDatepicker,\n  },\n  data: () => ({\n    value: '',    codeBasic,    codeBasic,\n  }),\n}\n</script>\n";
var codeValidation = "\n<template>\n  <form-wizard\n    color=\"#5A8DEE\"\n    :title=\"null\"\n    :subtitle=\"null\"\n    finish-button-text=\"Submit\"\n    @on-complete=\"formSubmitted\"\n    >\n    <tab-content\n      title=\"Baisc Information\"\n      icon=\"bx bx-user\"\n      :before-change=\"validateStep1\"\n    >\n      <ValidationObserver ref=\"ruleStep1\">\n        <b-form>\n          <b-row>\n            <b-col md=\"6\">\n              <b-form-group>\n                <label>First Name</label>\n                <ValidationProvider\n                  v-slot=\"{ errors }\"\n                  name=\"First Name\"\n                  rules=\"required\"\n                >\n                  <b-form-input\n                    v-model=\"firstName\"\n                    placeholder=\"Enter Your First Name\"\n                  />\n                  <small class=\"text-danger\">{{ errors[0] }}</small>\n                </ValidationProvider>\n              </b-form-group>\n            </b-col>\n            <b-col md=\"6\">\n              <b-form-group>\n                <label>Last Name</label>\n                <ValidationProvider\n                  v-slot=\"{ errors }\"\n                  name=\"Last Name\"\n                  rules=\"required\"\n                >\n                  <b-form-input\n                    v-model=\"lastName\"\n                    placeholder=\"Enter Your Last Name\"\n                  />\n                  <small class=\"text-danger\">{{ errors[0] }}</small>\n                </ValidationProvider>\n              </b-form-group>\n            </b-col>\n            <b-col md=\"6\">\n              <b-form-group>\n                <label>Email</label>\n                <ValidationProvider\n                  v-slot=\"{ errors }\"\n                  name=\"Email\"\n                  rules=\"required\"\n                >\n                  <b-form-input\n                    v-model=\"email\"\n                    placeholder=\"Enter Your Email\"\n                  />\n                  <small class=\"text-danger\">{{ errors[0] }}</small>\n                </ValidationProvider>\n              </b-form-group>\n            </b-col>\n            <b-col md=\"6\">\n              <b-form-group>\n                <label>City</label>\n                <b-form-select\n                  v-model=\"selected\"\n                  :options=\"options\"\n                />\n              </b-form-group>\n            </b-col>\n          </b-row>\n        </b-form>\n      </ValidationObserver>\n    </tab-content>\n    <tab-content\n      title=\"Job Details\"\n      icon=\"bx bx-briefcase\"\n      :before-change=\"validateStep2\"\n    >\n      <ValidationObserver ref=\"ruleStep2\">\n        <b-form>\n          <b-row>\n            <b-col md=\"6\">\n              <b-form-group>\n                <label>PROPOSAL TITLE</label>\n                <ValidationProvider\n                  v-slot=\"{ errors }\"\n                  name=\"Proposal\"\n                  rules=\"required\"\n                >\n                  <b-form-input\n                    v-model=\"proposal\"\n                    placeholder=\"Enter Your Proposel Title\"\n                  />\n                  <small class=\"text-danger\">{{ errors[0] }}</small>\n                </ValidationProvider>\n              </b-form-group>\n              <b-form-group>\n                <label>JOB TITLE</label>\n                <ValidationProvider\n                  v-slot=\"{ errors }\"\n                  name=\"Job\"\n                  rules=\"required\"\n                >\n                  <b-form-input\n                    v-model=\"job\"\n                    placeholder=\"Enter Your Job Title\"\n                  />\n                  <small class=\"text-danger\">{{ errors[0] }}</small>\n                </ValidationProvider>\n              </b-form-group>\n            </b-col>\n            <b-col md=\"6\">\n              <b-form-group>\n                <label>\n                  SHORT DESCRIPTION\n                </label>\n                <b-form-textarea\n                  rows=\"5\"\n                  placeholder=\"Please Enter Short Description\"\n                />\n              </b-form-group>\n            </b-col>\n          </b-row>\n        </b-form>\n      </ValidationObserver>\n    </tab-content>\n    <tab-content\n      title=\"Event Details\"\n      icon=\"bx bx-group\"\n      :before-change=\"validateStep3\"\n    >\n      <ValidationObserver ref=\"ruleStep3\">\n        <b-form>\n          <b-row>\n            <b-col md=\"6\">\n              <label>Event Name</label>\n              <ValidationProvider\n                v-slot=\"{ errors }\"\n                name=\"Event\"\n                rules=\"required\"\n              >\n                <b-form-input\n                  v-model=\"event\"\n                  placeholder=\"Enter Event Name\"\n                />\n                <small class=\"text-danger\">{{ errors[0] }}</small>\n              </ValidationProvider>\n            </b-col>\n            <b-col md=\"6\">\n              <b-form-group>\n                <label>EVENT LOCATION </label>\n                <b-form-select\n                  v-model=\"selected\"\n                  :options=\"options\"\n                />\n              </b-form-group>\n            </b-col>\n            <b-col md=\"6\">\n              <b-form-group>\n                <label>EVENT STATUS</label>\n                <b-form-select\n                  v-model=\"selected2\"\n                  :options=\"eventStatus\"\n                />\n              </b-form-group>\n            </b-col>\n            <b-col md=\"6\">\n              <label>REQUIREMENTS </label>\n              <b-form-group>\n                <div class=\"checkbox\">\n                  <b-form-checkbox\n                    plain\n                    value=\"Staffing\"\n                    class=\"ml-0\"\n                    inline\n                  >\n                    Staffing\n                  </b-form-checkbox>\n                  <b-form-checkbox\n                    plain\n                    value=\"Catering\"\n                    class=\"ml-0\"\n                    inline\n                  >\n                    Catering\n                  </b-form-checkbox>\n                </div>\n              </b-form-group>\n            </b-col>\n          </b-row>\n        </b-form>\n      </ValidationObserver>\n    </tab-content>\n  </form-wizard>\n</template>\n\n<script>\nimport AppCard from '@core/components/app-card/AppCard.vue'\nimport { FormWizard, TabContent } from 'vue-form-wizard'\nimport 'vue-form-wizard/dist/vue-form-wizard.min.css'\nimport { ValidationProvider, ValidationObserver } from 'vee-validate'\nimport {\n  BRow, BCol, BFormGroup, BFormInput, BFormSelect, BForm, BFormTextarea, BFormCheckbox,\n} from 'bootstrap-vue'\nimport { required } from '@validations'\n\nexport default {\n  components: {\n    FormWizard,\n    TabContent,\n    AppCard,\n    BRow,\n    BCol,\n    BFormGroup,\n    BFormInput,\n    BFormSelect,\n    BForm,\n    BFormTextarea,\n    BFormCheckbox,\n\n    ValidationProvider,\n    ValidationObserver,\n  },\n  data: () => ({\n    selected: 'new york',\n    selected2: 'planning',\n    firstName: '',\n    lastName: '',\n    emailValue: '',\n    proposal: '',\n    job: '',\n    email: '',\n    event: '',\n    required,\n    options: [\n      { value: 'new york', text: 'New York' },\n      { value: 'chicago', text: 'Chicago' },\n      { value: 'san francisco', text: 'San Francisco' },\n      { value: 'Boston', text: 'Boston' },\n    ],\n    eventStatus: [\n      { value: 'planning', text: 'Planning' },\n      { value: 'inprogress', text: 'In Progress' },\n      { value: 'Finished', text: 'Finished' },\n    ],\n  }),\n  methods: {\n    formSubmitted() {\n      // eslint-disable-next-line\n      alert('Form submitted!')\n    },\n    validateStep1() {\n      return new Promise((resolve, reject) => {\n        this.$refs.ruleStep1.validate().then((success) => {\n          if (success) {\n            resolve(true)\n          } else {\n            reject(new Error('Currect all value'))\n          }\n        })\n      })\n    },\n    validateStep2() {\n      return new Promise((resolve, reject) => {\n        this.$refs.ruleStep2.validate().then((success) => {\n          if (success) {\n            resolve(true)\n          } else {\n            reject(new Error('Currect all value'))\n          }\n        })\n      })\n    },\n    validateStep3() {\n      return new Promise((resolve, reject) => {\n        this.$refs.ruleStep3.validate().then((success) => {\n          if (success) {\n            resolve(true)\n          } else {\n            reject(new Error('Currect all value'))\n          }\n        })\n      })\n    },\n  },\n}\n</script>\n";
var codeVueBasic = "\n<template>\n   <b-row>\n    <!-- basic select -->\n    <b-col md=\"6\">\n      <h6>Basic Select</h6>\n      <b-card-text>\n        <code>vue-select</code>\n        <span> accepts arrays of primitive values or objects to use as </span>\n        <code>options</code>\n        <span> through the options prop:</span>\n      </b-card-text>\n\n      <b-form-group>\n        <v-select\n          v-model=\"selected\"\n          :dir=\"$store.state.appConfig.isRTL ? 'rtl' : 'ltr'\"\n          label=\"title\"\n          :options=\"option\"\n        />\n      </b-form-group>\n    </b-col>\n\n    <!-- select with icon -->\n    <b-col md=\"6\">\n      <h6>Select with Icon</h6>\n      <b-card-text>\n        <span>The current option within the dropdown, contained within </span><code>&lt;li&gt;</code>\n        <span>.</span>\n        <code>option {Object}</code>\n        <span> - The currently iterated option from </span>\n        <code>filteredOptions</code>\n      </b-card-text>\n\n      <b-form-group>\n        <v-select\n          v-model=\"selected1\"\n          :dir=\"$store.state.appConfig.isRTL ? 'rtl' : 'ltr'\"\n          :options=\"books\"\n          label=\"title\"\n        >\n          <template #option=\"{ title, icon }\">\n            <feather-icon\n              :icon=\"icon\"\n              size=\"16\"\n              class=\"align-middle mr-50\"\n            />\n            <span> {{ title }}</span>\n          </template>\n        </v-select>\n      </b-form-group>\n    </b-col>\n  </b-row>\n</template>\n\n<script>\nimport { BRow, BCol, BFormGroup } from 'bootstrap-vue'\nimport vSelect from 'vue-select'\n\nexport default {\n  components: {\n    BRow,\n    BCol,\n    BFormGroup,\n    vSelect,\n  },\n  data() {\n    return {\n      selected: { title: 'Square' },\n      selected1: {\n        title: 'Aperture',\n        icon: 'ApertureIcon',\n      },\n      option: [{ title: 'Square' }, { title: 'Rectangle' }, { title: 'Rombo' }, { title: 'Romboid' }],\n      books: [\n        {\n          title: 'Aperture',\n          icon: 'ApertureIcon',\n        },\n        {\n          title: 'Codepen',\n          icon: 'CodepenIcon',\n        },\n        {\n          title: 'Globe ',\n          icon: 'GlobeIcon',\n        },\n        {\n          title: 'Instagram ',\n          icon: 'InstagramIcon',\n        },\n      ],\n    }\n  },\n}\n</script>\n\n<style lang=\"scss\">\n@import '@core/scss/vue/libs/vue-select.scss';\n</style>\n";
var codeIndeterminate = "\n<template>\n  <div>\n    <!-- checkbox -->\n    <b-form-checkbox\n      v-model=\"checked\"\n      :indeterminate.sync=\"indeterminate\"\n    >\n      Click me to see what happens\n    </b-form-checkbox>\n\n    <!-- button -->\n    <b-button\n      v-ripple.400=\"'rgba(113, 102, 240, 0.15)'\"\n      class=\"mt-1\"\n      variant=\"outline-primary\"\n      @click=\"toggleIndeterminate\"\n    >\n      Toggle Indeterminate State\n    </b-button>\n\n    <div class=\"mt-1\">\n      Checked: <strong>{{ checked }}</strong><br>\n      Indeterminate: <strong>{{ indeterminate }}</strong>\n    </div>\n  </div>\n</template>\n\n<script>\nimport { BFormCheckbox, BButton } from 'bootstrap-vue'\n\nexport default {\n  components: {\n    BFormCheckbox,\n    BCardCode,\n    BButton,\n  },\n   directives: {\n    Ripple,\n  },\n  data() {\n    return {\n      checked: true,\n      indeterminate: true,\n    }\n  },\n  methods: {\n    toggleIndeterminate() {\n      this.indeterminate = !this.indeterminate\n    },\n  },\n}\n</script>\n";
var codeVertical = "\n<template>\n   <b-tabs pills vertical>\n      <b-tab\n        title=\"Pill 1\"\n        active\n      >\n        <b-card-text>\n          Candy canes donut chupa chups candy canes lemon drops oat cake wafer. Cotton candy candy canes marzipan carrot cake. Sesame snaps lemon drops candy marzipan donut brownie tootsie roll. Icing croissant bonbon biscuit gummi bears. Bear claw donut sesame snaps bear claw liquorice jelly-o bear claw carrot cake. Icing croissant bonbon biscuit gummi bears.\n        </b-card-text>\n      </b-tab>\n      <b-tab title=\"Pill 2\">\n        <b-card-text>\n          Pudding candy canes sugar plum cookie chocolate cake powder croissant. Carrot cake tiramisu danish candy cake muffin croissant tart dessert. Tiramisu caramels candy canes chocolate cake sweet roll liquorice icing cupcake. Sesame snaps wafer marshmallow danish drag\xE9e candy muffin jelly beans tootsie roll. Jelly beans oat cake chocolate cake tiramisu sweet.\n        </b-card-text>\n      </b-tab>\n      <b-tab title=\"Pill 3\">\n        <b-card-text>\n          Carrot cake drag\xE9e chocolate. Lemon drops ice cream wafer gummies drag\xE9e. Chocolate bar liquorice cheesecake cookie chupa chups marshmallow oat cake biscuit. Dessert toffee fruitcake ice cream powder tootsie roll cake. Macaroon brownie lemon drops croissant marzipan sweet roll macaroon lollipop. Danish fruitcake bonbon bear claw gummi bears apple pie.\n        </b-card-text>\n      </b-tab>\n      <b-tab\n        title=\"Disabled\"\n        disabled\n      >\n        <b-card-text>Macaroon brownie lemon drops croissant marzipan sweet roll macaroon lollipop.</b-card-text>\n      </b-tab>\n    </b-tabs>\n</template>\n\n<script>\nimport { BTabs, BTab } from 'bootstrap-vue'\n\nexport default {\n  components: {\n    BTabs,\n    BTab,\n  },\n}\n</script>\n";

/***/ }),

/***/ "./frontend/src/views/apps/perfil/perfilStoreModule.js":
/*!*************************************************************!*\
  !*** ./frontend/src/views/apps/perfil/perfilStoreModule.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @axios */ "./frontend/src/libs/axios.js");
 // const token = localStorage.getItem('accessToken');
// const config = {
//   headers: {
//     'Contenct-type': 'multipart/form-data',
//     'Authorization': `Bearer ${token}`,
//     //'Content-Type': 'multipart/form-data'
//   },
// };

/* harmony default export */ __webpack_exports__["default"] = ({
  namespaced: true,
  state: {
    perfil: []
  },
  getters: {},
  mutations: {
    SET_PERFIL: function SET_PERFIL(state, payload) {
      state.perfil = payload;
    }
  },
  actions: {
    fetchPerfil: function fetchPerfil(ctx) {
      return new Promise(function (resolve, reject) {
        _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get('api/auth/referidos', {}).then(function (response) {
          ctx.commit('SET_PERFIL', response.data.data);
          resolve(response.data.data);
        })["catch"](function (error) {
          reject(error);
        });
      });
    },
    fetchCreatePerfil: function fetchCreatePerfil(ctx) {
      return new Promise(function (resolve, reject) {
        _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get('api/auth/referidos/create', {}).then(function (response) {
          resolve(response.data);
        })["catch"](function (error) {
          reject(error);
        });
      });
    },
    getProfile: function getProfile(ctx, data) {
      return new Promise(function (resolve, reject) {
        _axios__WEBPACK_IMPORTED_MODULE_0__["default"].post('api/auth/personas/perfil', {
          id_usuario: data.id_user
        }).then(function (response) {
          resolve(response);
        })["catch"](function (error) {
          reject(error);
        });
      });
    },
    fetchPerfilAdicional: function fetchPerfilAdicional(ctx, data) {
      return new Promise(function (resolve, reject) {
        _axios__WEBPACK_IMPORTED_MODULE_0__["default"].post('api/auth/personas/perfil/adicional', data).then(function (response) {
          resolve(response);
        })["catch"](function (error) {
          reject(error);
        });
      });
    },
    updatePerfil: function updatePerfil(ctx, referido) {
      return new Promise(function (resolve, reject) {
        _axios__WEBPACK_IMPORTED_MODULE_0__["default"].post('api/auth/users/perfil/update/informacion', referido).then(function (response) {
          resolve(response);
        })["catch"](function (error) {
          reject(error);
        });
      });
    },
    updatePerfilAgente: function updatePerfilAgente(ctx, referido) {
      return new Promise(function (resolve, reject) {
        _axios__WEBPACK_IMPORTED_MODULE_0__["default"].post('api/auth/users/perfil/update/informacion_agente', referido).then(function (response) {
          resolve(response);
        })["catch"](function (error) {
          reject(error);
        });
      });
    },
    addPerfil: function addPerfil(ctx, referido) {
      return new Promise(function (resolve, reject) {
        _axios__WEBPACK_IMPORTED_MODULE_0__["default"].post('api/auth/users/perfil/update', referido).then(function (response) {
          resolve(response);
        })["catch"](function (error) {
          reject(error);
        });
      });
    },
    validateCampos: function validateCampos(ctx, referido) {
      return new Promise(function (resolve, reject) {
        _axios__WEBPACK_IMPORTED_MODULE_0__["default"].post('api/auth/personas/perfil/validacion', referido).then(function (response) {
          resolve(response);
        })["catch"](function (error) {
          reject(error);
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! bootstrap-vue */ "./frontend/node_modules/bootstrap-vue/esm/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    BAvatar: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BAvatar"]
  },
  props: {
    variant: {
      type: String,
      "default": 'primary'
    },
    icon: {
      type: String,
      "default": null
    },
    title: {
      type: String,
      "default": null
    },
    text: {
      type: String,
      "default": null
    },
    hideClose: {
      type: Boolean,
      "default": false
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/sass-loader/dist/cjs.js??ref--11-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".toastification-close-icon[data-v-2fedfe59],\n.toastification-title[data-v-2fedfe59] {\n  line-height: 26px;\n}\n.toastification-title[data-v-2fedfe59] {\n  color: inherit;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/sass-loader/dist/cjs.js??ref--11-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/sass-loader/dist/cjs.js??ref--11-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=style&index=0&id=2fedfe59&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=template&id=2fedfe59&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/@core/components/toastification/ToastificationContent.vue?vue&type=template&id=2fedfe59&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "toastification" }, [
    _c(
      "div",
      { staticClass: "d-flex align-items-start" },
      [
        _c(
          "b-avatar",
          {
            staticClass: "mr-75 flex-shrink-0",
            attrs: { variant: _vm.variant, size: "1.8rem" }
          },
          [_c("feather-icon", { attrs: { icon: _vm.icon, size: "15" } })],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "d-flex flex-grow-1" }, [
          _c("div", [
            _vm.title
              ? _c("h5", {
                  staticClass: "mb-0 font-weight-bolder toastification-title",
                  class: "text-" + _vm.variant,
                  domProps: { textContent: _vm._s(_vm.title) }
                })
              : _vm._e(),
            _vm._v(" "),
            _vm.text
              ? _c("small", {
                  staticClass: "d-inline-block text-body",
                  domProps: { textContent: _vm._s(_vm.text) }
                })
              : _vm._e()
          ]),
          _vm._v(" "),
          _c(
            "span",
            {
              staticClass: "cursor-pointer toastification-close-icon ml-auto ",
              on: {
                click: function($event) {
                  return _vm.$emit("close-toast")
                }
              }
            },
            [
              !_vm.hideClose
                ? _c("feather-icon", {
                    staticClass: "text-body",
                    attrs: { icon: "XIcon" }
                  })
                : _vm._e()
            ],
            1
          )
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);