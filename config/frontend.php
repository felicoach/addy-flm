<?php

return [
    "reset_password_url" => env('APP_URL') . env('FRONT_FORGOT_PASSWORD')
];
