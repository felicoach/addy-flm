<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


use App\Http\Controllers\API\VerificationController as Verification;
use App\Http\Controllers\API\ResetPasswordController as Rset;
use App\Http\Controllers\API\ForgotPasswordController  as Forgt;
use App\Http\Controllers\API\LocalidadesController as Localidades;
use App\Http\Controllers\API\ApiInmuebleController as ApiInmueble;
use App\Http\Controllers\API\AgendaController as ApiAgenda;
use App\Http\Controllers\API\ClienteController as Cliente;
use App\Http\Controllers\API\DocumentosController as Documento;
use App\Http\Controllers\API\MercadeoController as Mercadeo;
use App\Http\Controllers\API\DashboardController as Dashboard;
use App\Http\Controllers\API\MessageController as MessageEmail;
use App\Http\Controllers\API\PermisosController;
use App\Http\Controllers\API\RolesController as Roles;
use App\Http\Controllers\API\AuthController as Login;
use App\Http\Controllers\API\ApiProfileController as Profile;

use App\Http\Controllers\API\ModuleController as Modules;
use App\Http\Controllers\API\UserdataController as Userdata;
use App\Http\Controllers\API\EmpresaController as Empresa;
use App\Http\Controllers\API\UserController as Users;
use App\Http\Controllers\API\SocialAuthController as Social;
use App\Http\Controllers\API\ContactController as Contact;

use App\Http\Controllers\API\CrmController as Crm;
use App\Http\Controllers\API\SoatController as Soat;
use App\Http\Controllers\API\CiencuadraController as Ciencuadra;
use App\Http\Controllers\API\MetrocuadradoController as Metrocuadrado;
use App\Http\Controllers\API\PortalMercadoLibreController as Mercadolibre;
use App\Http\Controllers\API\ClasificadosPaisController as Pais;



use App\Http\Controllers\API\PortalesController as Portales;
use App\Http\Controllers\API\PortalesCredentialsController as PortalesCredentials;

use App\Http\Controllers\API\ApiBannerController as Banners;
use App\Http\Controllers\API\ApiPaginaController as Paginas;
use App\Http\Controllers\API\ApiBlogController as Blogs;
use App\Http\Controllers\API\ApiTemplateController as Template;
use App\Http\Controllers\API\ApiPerfilesController as Perfiles;
use App\Http\Controllers\API\ApiCredencialesController as Credenciales;




use App\Http\Controllers\EmpresasController as Empresas;

use App\Http\Controllers\PersonasController as Personas;
use App\Http\Controllers\RespuestasController as Respuestas;
use App\Http\Controllers\PreguntasController as Preguntas;
use App\Models\Agenda;
use App\Models\Inmuebles;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::post('/login', [Login::class, 'login']);
Route::post('/signup', [Login::class, 'signup']);
Route::post('/recovery', [Login::class, 'recovery']);




Route::get('/inmueble/{referer}', [ApiInmueble::class, 'getInmueblesUser']);

Route::get('/inmueble_id/{id}', [ApiInmueble::class, 'getInmueblesForId']);

Route::put('/inmueble_filter/{id}', [ApiInmueble::class, 'getInmueblesForIdFilter']);

//Route::get('/api-inmueble/search-inmuebles', [ApiInmueble::class, 'searchPaginate']);
Route::get('/usuarios', [Users::class, 'getUsersCode']);


Route::post('/contact', [Contact::class, 'store']);

Route::get('xml/data/{slug}', [Soat::class, 'dataXmlPuntoPropiedad']);


Route::post('/sociallogin/{provider}', [Login::class, 'SocialSignup']);
Route::post('/socialLoginApp', [Social::class, 'socialLoginApp']);

//Route::get('/auth/{provider}/callback', [aoth::class, 'index'])->where('provider', '.*');

Route::get('/email-verification', [Verification::class, 'verify'])->name('verification.verify');
Route::get('/users/informacion/{referral_code}', [Userdata::class, 'showData']);

Route::post('/password/reset',  [Rset::class, 'reset'])->name('api.reset-password');
Route::post('/password/email', [Forgt::class, 'sendResetLinkEmail'])->name('api.forgot-password');
Route::get('/auth/api-inmueble/inmuebles/url/{slug}', [ApiInmueble::class, 'getInmueblesForSlug']);
/* Inmuebles Caracteristicas Externas */

Route::get('/blogs/listado/', [Blogs::class, 'index']);
Route::get('/banners/listado/', [Banners::class, 'index']);

Route::get('/blog_id/{id}', [Blogs::class, 'detalle']);

Route::get('/service/cienciuadra/despubicar/{id}', [Ciencuadra::class, 'despublicarCiencuadra']);
Route::post('/fincarraiz/import/departamentos', [Soat::class, 'importDepartamentos']);
Route::post('/fincarraiz/import/ciudades', [Soat::class, 'importCiudades']);
Route::post('/fincarraiz/import/zonas', [Soat::class, 'importFincarraizZonas']);
Route::post('/fincarraiz/import/barrios', [Soat::class, 'importFincarraizBarrios']);

Route::post('/fincarraiz/zonas', [Soat::class, 'importZonas']);
Route::post('/fincarraiz/barrios', [Soat::class, 'importBarrios']);

Route::post('/agente/subdomain', [Empresa::class, "reloadSubdomain"]);

Route::group(['prefix' => 'auth'], function () {

  Route::group(['middleware' => 'auth:api'], function () {

    Route::get('/logout', [Login::class, 'logout']);
    Route::get('/user', [Login::class, 'user']);
    Route::post('/service', [Soat::class, 'actionService']);

    Route::get('/portales/metrocuadrado/departamentos', [Metrocuadrado::class, 'getDepartamento']);
    Route::get('/portales/metrocuadrado/ciudades', [Metrocuadrado::class, 'getCiudades']);
    Route::get('/portales/metrocuadrado/zonas', [Metrocuadrado::class, 'getZonas']);
    Route::get('/portales/metrocuadrado/tipos_inmuebles', [Metrocuadrado::class, 'getTiposInmuebles']);
    Route::get('/portales/metrocuadrado/tipos_negocio', [Metrocuadrado::class, 'getTipoNegocio']);
    /*  mercado libre */

    Route::get('/portales/mercadolibre/countries', [Mercadolibre::class, 'getCiudadesM']);
    Route::get('/portales/mercadolibre/departamentos', [Mercadolibre::class, 'getDepartamentoM']);
    Route::get('/portales/mercadolibre/cities', [Mercadolibre::class, 'getCitiesM']);
    Route::get('/portales/mercadolibre/barrios', [Mercadolibre::class, 'getBarriosM']);

    /* fin mercado libre */
    //inicio Clacificados el pais

    Route::get('/portales/claficados_pais/countries', [Pais::class, 'getPaises']);
    Route::get('/portales/claficados_pais/departamentos', [Pais::class, 'getDepartamento']);
    Route::get('/portales/claficados_pais/cities', [Pais::class, 'getCities']);
    Route::get('/portales/claficados_pais/barrios', [Pais::class, 'getBarriosM']);

    Route::get('/service/pais/sincronizar/{id}', [Pais::class, 'publicInmuieble']);
    Route::get('/service/pais/desactivar/{id}', [Pais::class, 'cancelarAvizo']);
    Route::get('/service/pais/update/{id}', [Pais::class, 'updateInmuieble']);

    // fin Clasificados el pais

    Route::post('/service/cienciuadra', [Ciencuadra::class, 'dataApiRestCienCuadra']);
    Route::get('/service/cienciuadra/despubicar{id}', [Ciencuadra::class, 'despublicarCiencuadra']);

    Route::post('/service/cienciuadra/update', [Ciencuadra::class, 'updateApiCiencuadra']);
    Route::post('/service/cienciuadra/desactivar', [Ciencuadra::class, 'desactivarInmuebleCiencuadra']);
    Route::get('/ciencuadra/import/localidades', [Ciencuadra::class, 'getLocalidades']);


    Route::get('/service/metro_cuadrado/{id}', [Metrocuadrado::class, 'dataApiRestMetrocuadrado']);
    Route::get('/service/metro_cuadrado/update/{id}', [Metrocuadrado::class, 'updateApiCiencuadra']);
    Route::get('/service/metro_cuadrado/despublicar/{id}', [Metrocuadrado::class, 'despublicarMetrocuadrado']);


    Route::get('/fincarraiz/getStatus', [Soat::class, 'getStatusPortalFincaRaizInmuebles']);

    Route::get('/fincarraiz/despublicar/{id}', [Soat::class, 'despublicarFincarraiz']);

    Route::post('/fincarraiz/import/departamentos', [Soat::class, 'importDepartamentos']);




    Route::get('/contact', [Contact::class, 'listContact']);


    /* modulo de empresas */
    Route::get('/empresas', [Empresas::class, 'index']);
    Route::post('/empresas', [Empresas::class, 'store']);

    Route::get('/empresas/ciudad/{codigo}', [Empresas::class, 'ciudad']);

    Route::get('/api-inmueble/tipo_inmuebles', [ApiInmueble::class, 'getTipoInmueble']);
    Route::get('/api-inmueble/tipo_negocio', [ApiInmueble::class, 'getTipoNegocio']);

    Route::resource('/clientes', Cliente::class);
    Route::post('/clientes/filter', [Cliente::class, 'filterCliente']);
    Route::post('/clientes/import', [Cliente::class, 'importCliente']);

    Route::get('/modules', [Modules::class, 'getModules']);

    Route::get('/usuarios', [Users::class, 'getUsers']);
    Route::get('/usuarios', [Users::class, 'getUsersCode']);

    Route::get('/usuarios/admin-all', [Users::class, 'getUsersAllAdmin']);

    Route::post('/usuarios', [Users::class, 'addUsers']);
    Route::put('/usuarios/{id}', [Users::class, 'update']);


    //Profle
     Route::get('/prifile', [Profile::class, 'getProfile']);
 

    // end



    /*Modulo de roles */
    Route::apiResource('/users', Userdata::class);

    Route::post('/users/perfil/update/informacion', [Userdata::class, 'actualizar_informacion_perfil']);
    Route::post('/users/perfil/update/informacion_agente', [Userdata::class, 'actualizar_informacion_perfil_agente']);

    Route::post('/users/perfil/update', [Userdata::class, 'actualizacion_perfil']);
    Route::get('/users/referidos/{referer_code}', [Userdata::class, 'getReferer']);
    Route::get('/users/referido/money/{id}', [Userdata::class, 'countReferidos']);
    Route::post('/users/referido/validacion', [Userdata::class, 'validacion']);

    Route::get('/localidad/paises', [Localidades::class, 'getPaises']);

    Route::get('/localidad/ciudades/{id}', [Localidades::class, 'getCiudades']);

    Route::get('/localidad/states/{id}', [Localidades::class, 'getStates']);

    Route::get('/localidad/state/{id}', [Localidades::class, 'getState']);

    Route::get('/localidad/zona/{id}', [Localidades::class, 'getZona']);

    Route::get('/localidad/barrio/{id}', [Localidades::class, 'getBarrio']);

    Route::post('/localidad/zona', [Localidades::class, 'agregarZona']);

    Route::post('/localidad/barrio', [Localidades::class, 'agregarBarrio']);

    Route::resource('/roles', Roles::class);


    /*Agente empresas*/

    Route::get('/agente/empresas', [Empresa::class, "getEmpresas"]);
    Route::get('/agente/empresa', [Empresa::class, "getEmpresasForId"]);
    Route::post('/agente/empresas', [Empresa::class, "addEmpresas"]);


    /*dashboard*/



    Route::post('/agente/empresas/numbers', [Empresa::class, "registerNumero"]);
    Route::get('/agente/empresas-n/numbers', [Empresa::class, "getNumero"]);


    Route::post('/agente/empresas/emails', [Empresa::class, "registerEmails"]);
    Route::get('/agente/empresas-n/emails', [Empresa::class, "getEmails"]);

    Route::post('/agente/empresas/redes', [Empresa::class, "registerSocial"]);
    Route::get('/agente/empresas-n/redes', [Empresa::class, "getRedes"]);




    Route::get("/permission", [Roles::class, "getPermissions"]);
    Route::put("/permission/{code}", [Roles::class, "edictPermissions"]);

    Route::post("/permissions-module", [Roles::class, "addPermissions"]);
    Route::post("/permission/permissions-active-role", [Roles::class, "addActivePermissionsActions"]);

    Route::get("/permissions-actions", [Roles::class, "getPermissionsActions"]);
    Route::post("/permissions-active-actions", [Roles::class, "getActivePermissionsActions"]);

    Route::post("/permission", [Roles::class, "addPermissionsForId"]);
    Route::get("/permission/role/{code}", [Roles::class, "getRolePermissionForIdRole"]);
    Route::get("/permission/role-permission/{code}", [Roles::class, "getRolePermissionActionForIdRole"]);

    Route::get("/actions", [Roles::class, "getActions"]);
    Route::post("/actions-modules", [Roles::class, "addActions"]);

    Route::get("/actions/{id}", [Roles::class, "getActionsForId"]);
    Route::post("/actions", [Roles::class, "addpermissionsActions"]);


    Route::get("/permission/{id}", [Roles::class, "getPermissionRole"]);
    Route::get('/roles/permission/{slug}', [Roles::class, 'getPermissionRole']);
    Route::post('/roles/permission/{slug}', [Roles::class, 'postPermissionRole']);
    Route::get('/roles-users', [Roles::class, 'getRolesForUser']);
    Route::get('/roles-users-all', [Roles::class, 'getRolesForUserAll']);


    Route::resource('/crm', Crm::class);
    Route::get('/crm/etiquetas/{id_user}', [Crm::class, 'getEtiquetasCrm']);
    Route::get('/etiquetas', [Crm::class, 'getEtiquetas']);
    Route::post('/etiquetas', [Crm::class, 'addEtiquetas']);


    Route::post('/crm/etiquetas', [Crm::class, 'addEtiquetasCrm']);


    Route::get('/template/list', [Template::class, 'list']);
    Route::post('/template/store', [Template::class, 'store']);
    Route::post('/template/del', [Template::class, 'del']);
    Route::post('/template/upd', [Template::class, 'upd']);
    Route::post('/template/get', [Template::class, 'get']);
    Route::post('/template/settemplate/{id}', [Template::class, 'settemplate']);



    Route::get('/perfiles/list', [Perfiles::class, 'list']);
    Route::post('/perfiles/store', [Perfiles::class, 'store']);
    Route::post('/perfiles/del', [Perfiles::class, 'del']);
    Route::post('/perfiles/upd', [Perfiles::class, 'upd']);
    Route::post('/perfiles/get', [Perfiles::class, 'get']);
    Route::post('/perfiles/setperfiles/{id}', [Perfiles::class, 'setperfiles']);



    Route::get('/credenciales/list', [Credenciales::class, 'list']);
    Route::post('/credenciales/store', [Credenciales::class, 'store']);
    Route::post('/credenciales/del', [Credenciales::class, 'del']);
    Route::post('/credenciales/upd', [Credenciales::class, 'upd']);
    Route::post('/credenciales/get', [Credenciales::class, 'get']);



    Route::get('/banners/list', [Banners::class, 'list']);
    Route::post('/banners/store', [Banners::class, 'store']);
    Route::post('/banners/del', [Banners::class, 'del']);
    Route::post('/banners/upd', [Banners::class, 'upd']);
    Route::post('/banners/get', [Banners::class, 'get']);



    Route::get('/paginas/list', [Paginas::class, 'list']);
    Route::post('/paginas/store', [Paginas::class, 'store']);
    Route::post('/paginas/del', [Paginas::class, 'del']);
    Route::post('/paginas/upd', [Paginas::class, 'upd']);
    Route::post('/paginas/get', [Paginas::class, 'get']);



    Route::get('/blogs/list', [Blogs::class, 'list']);
    Route::post('/blogs/store', [Blogs::class, 'store']);
    Route::post('/blogs/del', [Blogs::class, 'del']);
    Route::post('/blogs/upd', [Blogs::class, 'upd']);
    Route::post('/blogs/get', [Blogs::class, 'get']);

    Route::get('/portales', [Portales::class, 'getPortales']);
    Route::get('/portales/credential/{portal}', [Portales::class, 'getCredentialPortal']);
    Route::get('/portales/credenciales_clasificados_pais/{portal}', [Portales::class, 'getCredencialesClasificadosPais']);
    Route::get('/portales/credenciales_metro_cuadrado/{portal}', [Portales::class, 'getCredencialesMetroCuadrado']);
    Route::get('/portales/credenciales_ciencuadra/{portal}', [Portales::class, 'getCredencialesCiencuadra']);

    Route::get('/portales_credentials_ciencuadra', [PortalesCredentials::class, 'getTokenCiencuadra']);
    Route::get('/portales_codigo_response', [PortalesCredentials::class, 'getCodigoResponsePortal']);

    Route::post('/portales/credenciales_portal', [Portales::class, 'registerCredencialesPortales']);
    Route::post('/portales/credenciales_regster_portal_clasificados_portal', [Portales::class, 'registerCredencialesClasificadosPais']);
    Route::post('/portales/credenciales_regster_portal_metro_cuadrado', [Portales::class, 'registerCredencialesMetroCuadrado']);
    Route::post('/portales/credenciales_regster_portal_ciencuadra', [Portales::class, 'registerCredencialesCienCuadra']);

    Route::get('/portales/type-services', [Portales::class, 'getTypeService']);

    Route::post('/portales', [Portales::class, 'registerPortales']);
    Route::post('/portales/edit', [Portales::class, 'editarPortales']);
    Route::post('/portales/contries_portal', [Portales::class, 'registerPortalesCountries']);
    Route::post('/portales/tipo_inmueble_portal', [Portales::class, 'registerPortalesTipoInmueble']);
    Route::post('/portales/cities_portal', [Portales::class, 'registerPortalesCities']);
    Route::post('/portales/states_portal', [Portales::class, 'registerPortalesStates']);
    Route::post('/portales/caracteristicas_internas_portal', [Portales::class, 'registerPortalesCaracteristicasInternas']);
    Route::post('/portales/caracteristicas_externas_portal', [Portales::class, 'registerPortalesCaracteristicasExternas']);

    Route::post('/portales/credenciales_get_portal', [Portales::class, 'getCredencialesPortales']);

    Route::post('/portales/tipo_negocio_portal', [Portales::class, 'registerPortalesTipoNegocio']);
    Route::post('/portales/estado_fisico_portal', [Portales::class, 'registerPortalesEstadoFisico']);
    Route::post('/portales/zonas_portales', [Portales::class, 'registerPortalesZona']);
    Route::post('/portales/barrios_portales', [Portales::class, 'registerPortalesBarrio']);







    /*Validacion de preguntas*/
    Route::post('/personas/perfil/validacion', [Personas::class, 'validacion']);

    /* Respuestas */
    Route::post('/cuestionarios/respuestas', [Respuestas::class, 'store']);

    /* Inmuebles */

    /* Inmuebles Tipo de Inmueble */
    Route::get('/api-inmueble/tipo_inmuebles', [ApiInmueble::class, 'getTipoInmueble']);
    Route::get('/dashboard/porcentage-inmueble', [ApiInmueble::class, "calculatePorcentage"]);


    /* Inmuebles Tipo de Negocio */
    Route::get('/api-inmueble/tipo_negocio', [ApiInmueble::class, 'getTipoNegocio']);

    /* Inmuebles Caracteristicas Internas */
    Route::get('/api-inmueble/caracteristicas_internas', [ApiInmueble::class, 'getCaracteristicasInternas']);

    Route::get('/api-inmueble/caracteristicas_externas', [ApiInmueble::class, 'getCaracteristicasExternas']);

    Route::get('/api-inmueble/inmuebles', [ApiInmueble::class, 'getInmuebles']);
    Route::get('/api-inmueble/rangue-price', [ApiInmueble::class, 'getInmuebleRanguePrice']);



    Route::get('/api-inmueble/stete_publication', [ApiInmueble::class, 'getStateTipePublication']);
    Route::get('/api-inmueble/segmento_mercado', [ApiInmueble::class, 'getSegmentoMercado']);
    Route::get('/api-inmueble/estado_fisico', [ApiInmueble::class, 'getStateFisico']);
    Route::get('/api-inmueble/estratos', [ApiInmueble::class, 'getEStratos']);
    Route::get('/api-inmueble/parqueaderos', [ApiInmueble::class, 'getTipoParqueadero']);
    Route::get('/api-inmueble/precios', [ApiInmueble::class, 'getTipoPrecio']);
    Route::get('/api-inmueble/tiempo_alquiler', [ApiInmueble::class, 'getTipoTiempoAlquiler']);
    Route::get('/api-inmueble/periodo_admon', [ApiInmueble::class, 'getTipoPeriodoAdmon']);
    Route::post('/api-inmueble/filter', [ApiInmueble::class, 'filterMultiple']);
    Route::post('/api-inmueble/delete-image', [ApiInmueble::class, 'deleteImage']);

    Route::post('/api-inmueble/edit-image', [ApiInmueble::class, 'editImageInmueble']);

    Route::post('/api-inmueble/add-image', [ApiInmueble::class, 'addImageInmueble']);

    Route::post('/api-inmueble/upd-image', [ApiInmueble::class, 'updImageInmueble']);

    Route::post('/api-inmueble/del-image', [ApiInmueble::class, 'delImageInmueble']);

    Route::get('/api-inmueble/arrendo', [ApiInmueble::class, 'inventarioInmuebleArrendo']);
    Route::get('/api-inmueble/antiguedad', [ApiInmueble::class, 'getAntiguedad']);


    Route::get('/api-inmueble/venta', [ApiInmueble::class, 'inventarioInmuebleVenta']);
    Route::get('/api-inmueble/export', [ApiInmueble::class, 'exportInmueble']);
    Route::get('/api-inmueble/download_pdf', [ApiInmueble::class, 'downloadPdf']);


    // apis localidades metrocuadrado
    Route::get('/portales/metrocuadrado/departamentos', [Metrocuadrado::class, 'getDepartamento']);

    //final

    /* fin de seed de inmuebles */



    Route::get('/dashboard', [Dashboard::class, 'countDashboard']);


    Route::get('/permisos/users', [PermisosController::class, 'getUsers']);


    Route::get('/mercadeo/{user_id}', [Mercadeo::class, 'index']);
    Route::get('/mercadeo-type', [Mercadeo::class, 'getMercadeoType']);

    Route::post('/mercadeo', [Mercadeo::class, 'register']);

    Route::get('/mercadeo_admin', [Mercadeo::class, 'getMercadeo']);
    Route::post('/mercadeo_admin', [Mercadeo::class, 'addMercadeoAdmin']);
    Route::post('/mercadeo_admin/edit', [Mercadeo::class, 'editMercadeoAdmin']);


    /* Inmuebles api Inmubele */
    Route::apiResource('/api-inmueble', ApiInmueble::class);
    Route::post('/api-inmueble/order-image', [ApiInmueble::class, "orderImagen"]);

    Route::post('/api-inmueble/editar_inmueble', [ApiInmueble::class, "editInmueble"]);



    Route::apiResource('/api-agenda', ApiAgenda::class);
    Route::get('/api-agenda-hoy/today', [ApiAgenda::class, 'getTodayAgenda']);
    Route::get('/api-agenda-state/agenda_cita', [ApiAgenda::class, 'getStateAgendCita']);
    Route::get('/api-agenda-state/agenda_cita/{id}', [ApiAgenda::class, 'getStateAgendCitaForCita']);
    Route::get('/api-agenda-reporte/{id}', [ApiAgenda::class, 'getCitasForInmuebles']);
    Route::post('/api-agenda-state/register', [ApiAgenda::class, 'registrarStateAgendCita']);
    Route::put('/api-agenda-state/update/{id}', [ApiAgenda::class, 'updatedStateAgendCita']);


    Route::get('/api-agenda-tipo/tipo_citas', [ApiAgenda::class, 'getTiposCitas']);

    /* Personas */

    /* --- Registro de perfil al haber nuevo registro */
    Route::post('/personas/perfil/update', [Personas::class, 'actualizacion_perfil']);

    /* -- Actualizacion de información de perfil al haber nuevo registro */
    Route::post('/personas/perfil', [Personas::class, 'edicion']);

    /* ============= Actualizacion de info adicional perfil al haber nuevo registro  ===================== */
    Route::post('/personas/perfil/adicional', [Personas::class, 'informacion_adicional']);

    /* ======================== Actualizar perfil por ID ================================= */
    Route::post('/personas/perfil/update/informacion', [Personas::class, 'actualizar_informacion_perfil']);

    /* ======================== Registro de preguntas ================================= */
    Route::post('/respuestas/registro', [Respuestas::class, 'store']);

    Route::get('/respuestas/{id}', [Respuestas::class, 'show']);
    Route::get('/respuestas/vendedor/{id}', [Respuestas::class, 'showVendedor']);

    Route::put('/respuestas/{id}', [Respuestas::class, 'update']);


    /* PREGUNTAS */
    Route::resource('/preguntas', Preguntas::class);
    Route::get('/preguntas/informacion/{id}', [Preguntas::class, 'informacion_preguntas']);

    Route::get('/documento/tipo_docuemnto', [Documento::class, 'getTipoDocumento']);
    Route::get('/documento/celulares/{id}', [Documento::class, 'getCelularCliente']);
    Route::post('/documento/celulares', [Documento::class, 'registerCelularCliente']);
    Route::delete('/documento/celulares/{id}', [Documento::class, 'eliminarNumeroCliente']);
    Route::put('/documento/celulares/{id}', [Documento::class, 'editarNumeroCliente']);

    Route::get('/documento/tipo_cliente', [Documento::class, 'getTipoCliente']);

    Route::get('/get_users', [MessageEmail::class, 'getUsers']);
    Route::get('/get_messages', [MessageEmail::class, 'getMessages']);
    Route::post('/notifications', [MessageEmail::class, 'sendMail']);
  });
});
