<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:site_name" content="Addy">
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="550019836339534" />
    <meta name="google-site-verification" content="Zqo6EboEgtg7HHXqPebzgbMpFatn2djYobQRI8s1lXE" />



    <!-- <link rel="icon" href="<%= BASE_URL %>favicon.ico"> -->
    @if (isset($data))
    @foreach ($data['tags'] as $key => $value)
    <meta property="{{ $key }}" content="{{ $value }}">
    @endforeach
    @endif

    <meta property="og:image:type" content="image/*">

    <!-- Size of image. Any size up to 300. Anything above 300px will not work in WhatsApp -->

    <meta name="author" content="">

    <title>Addy</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/logo/favicon.png') }}">
    <link href="https://cdn.jsdelivr.net/npm/morioh@1.0.9/dist/css/morioh.min.css" rel="stylesheet" />
    <link href="https://cdn.morioh.net/fa/v5.13.0/css/fontawesome.min.css" rel="stylesheet" />
    <link href="https://cdn.morioh.net/fa/v5.13.0/css/regular.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/animate.css@3.7.2/animate.min.css" rel="stylesheet" />
    
</head>

<body>
    <noscript>
        <strong></strong>
    </noscript>
    <div id="app">
    </div>

    <script async defer src="https://apis.google.com/js/api.js"></script>

    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://accounts.google.com/gsi/client" async defer></script>
    <script src="https://apis.google.com/js/client.js?onload=authorize"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.4.2/Sortable.min.js"></script>
    

    <!-- Global site tag (gtag.js) - Google Analytics -->
  

    <script src="{{ asset(mix('js/app.js')) }}"></script>
</body>

</html>